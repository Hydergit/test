using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using Newtonsoft.Json;
using Arco.Core.AppBase;
//using Arco.Model.Interface.Repository.Single;
using Arco.Model.Entity;
using Arco.Framework.Utility;
//using Arco.Model.Interface.Business;
using Arco.Core.AppInterface;
using Arco.Core.Infrastructure;
using Arco.Framework.Query;
namespace Arco.Business.Record
{



 public partial class AMWContractHeaderRepository : EFRepositoryBase
    {
	
		    public AMWContractHeaderRepository()
            : base("arco")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<AMWContractHeaderRepository>(null);
        }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
		modelBuilder.Configurations.Add(new AMWContractHeaderConfiguration());
          
			}

	}


    public partial class AMWContractHeaderBLL : BLLBase
    {
		
        public AMWContractHeaderBLL() 
			: base(new AMWContractHeaderRepository(), false) {
			_lAMWContractHeader =new Arco.Model.Entity.AMWContractHeader();
			 }


			 
        public AMWContractHeaderBLL(IRepository _IRepository)
			 : base(_IRepository, false) {
			_lAMWContractHeader =new Arco.Model.Entity.AMWContractHeader();
			 }



			//   public AMWContractHeaderBLL(IRepository context, Func<AMWContractHeader, bool> predicate, bool disposeContext = false) 
			//: base(context, disposeContext) {
			
              // _lAMWContractHeader= GetAll().Where(predicate:predicate).FirstOrDefault();
        
			 //}
        		
				public Arco.Model.Entity.AMWContractHeader _lAMWContractHeader    { get; set; }

				

		#region Property

			
		public string ContractNumber {get{return this._lAMWContractHeader.ContractNumber;} set{  this._lAMWContractHeader.ContractNumber = value; }}

		
			
		public string CustomerID {get{return this._lAMWContractHeader.CustomerID;} set{  this._lAMWContractHeader.CustomerID = value; }}

		
			
		public DateTime? ContractDate {get{return this._lAMWContractHeader.ContractDate;} set{  this._lAMWContractHeader.ContractDate = value; }}

		
			
		public string ContractSignedBy {get{return this._lAMWContractHeader.ContractSignedBy;} set{  this._lAMWContractHeader.ContractSignedBy = value; }}

		
			
		public DateTime? ValidFrom {get{return this._lAMWContractHeader.ValidFrom;} set{  this._lAMWContractHeader.ValidFrom = value; }}

		
			
		public DateTime? ValidTo {get{return this._lAMWContractHeader.ValidTo;} set{  this._lAMWContractHeader.ValidTo = value; }}

		
			
		public int? ServiceType {get{return this._lAMWContractHeader.ServiceType;} set{  this._lAMWContractHeader.ServiceType = value; }}

		
			
		public decimal? ContractValue {get{return this._lAMWContractHeader.ContractValue;} set{  this._lAMWContractHeader.ContractValue = value; }}

		
			
		public int? ContractStatus {get{return this._lAMWContractHeader.ContractStatus;} set{  this._lAMWContractHeader.ContractStatus = value; }}

		
			
		public int? ProcessStatus {get{return this._lAMWContractHeader.ProcessStatus;} set{  this._lAMWContractHeader.ProcessStatus = value; }}

		
			
		public DateTime? CreatedDatetime {get{return this._lAMWContractHeader.CreatedDatetime;} set{  this._lAMWContractHeader.CreatedDatetime = value; }}

		
			
		public string CreatedBy {get{return this._lAMWContractHeader.CreatedBy;} set{  this._lAMWContractHeader.CreatedBy = value; }}

		
			
		public DateTime? ModifiedDatetime {get{return this._lAMWContractHeader.ModifiedDatetime;} set{  this._lAMWContractHeader.ModifiedDatetime = value; }}

		
			
		public string ModifiedBy {get{return this._lAMWContractHeader.ModifiedBy;} set{  this._lAMWContractHeader.ModifiedBy = value; }}

		
			
		public string DataAreaId {get{return this._lAMWContractHeader.DataAreaId;} set{  this._lAMWContractHeader.DataAreaId = value; }}

		
			
		public long RecId {get{return this._lAMWContractHeader.RecId;} set{  this._lAMWContractHeader.RecId = value; }}

		
			
		public byte[] RecVersion {get{return this._lAMWContractHeader.RecVersion;} set{  this._lAMWContractHeader.RecVersion = value; }}

		
			
		public int? TotalManPower {get{return this._lAMWContractHeader.TotalManPower;} set{  this._lAMWContractHeader.TotalManPower = value; }}

		
			
		public decimal? AdvancePayment {get{return this._lAMWContractHeader.AdvancePayment;} set{  this._lAMWContractHeader.AdvancePayment = value; }}

		
			
		public string CountryRegion {get{return this._lAMWContractHeader.CountryRegion;} set{  this._lAMWContractHeader.CountryRegion = value; }}

		
			
		public string City {get{return this._lAMWContractHeader.City;} set{  this._lAMWContractHeader.City = value; }}

		
			
		public bool? DirectContract {get{return this._lAMWContractHeader.DirectContract;} set{  this._lAMWContractHeader.DirectContract = value; }}

		
			
		public string VirtualAccountId {get{return this._lAMWContractHeader.VirtualAccountId;} set{  this._lAMWContractHeader.VirtualAccountId = value; }}

		
			
		public string InternalContractNumber {get{return this._lAMWContractHeader.InternalContractNumber;} set{  this._lAMWContractHeader.InternalContractNumber = value; }}

		
			
		public string Remarks {get{return this._lAMWContractHeader.Remarks;} set{  this._lAMWContractHeader.Remarks = value; }}

		
			
		public string ExternalID {get{return this._lAMWContractHeader.ExternalID;} set{  this._lAMWContractHeader.ExternalID = value; }}

		
			
		public string ExternalDisplayCode {get{return this._lAMWContractHeader.ExternalDisplayCode;} set{  this._lAMWContractHeader.ExternalDisplayCode = value; }}

		
			
		public string ExternalDisplayName {get{return this._lAMWContractHeader.ExternalDisplayName;} set{  this._lAMWContractHeader.ExternalDisplayName = value; }}

		
			
		public string ExternalFamousName {get{return this._lAMWContractHeader.ExternalFamousName;} set{  this._lAMWContractHeader.ExternalFamousName = value; }}

		
			
		public string PayGroupID {get{return this._lAMWContractHeader.PayGroupID;} set{  this._lAMWContractHeader.PayGroupID = value; }}

		
			
		public string FeeSetup {get{return this._lAMWContractHeader.FeeSetup;} set{  this._lAMWContractHeader.FeeSetup = value; }}

		
			
		public int? ContractType {get{return this._lAMWContractHeader.ContractType;} set{  this._lAMWContractHeader.ContractType = value; }}

		
			
		public int? ContractSalesTotal {get{return this._lAMWContractHeader.ContractSalesTotal;} set{  this._lAMWContractHeader.ContractSalesTotal = value; }}

		
			
		public string InvoiceRuleID {get{return this._lAMWContractHeader.InvoiceRuleID;} set{  this._lAMWContractHeader.InvoiceRuleID = value; }}

		
			
		public string Approvedby {get{return this._lAMWContractHeader.Approvedby;} set{  this._lAMWContractHeader.Approvedby = value; }}

		
			
		public DateTime? ApprovedDate {get{return this._lAMWContractHeader.ApprovedDate;} set{  this._lAMWContractHeader.ApprovedDate = value; }}

		
			
		public string FinanceCustomerID {get{return this._lAMWContractHeader.FinanceCustomerID;} set{  this._lAMWContractHeader.FinanceCustomerID = value; }}

		
			
		public string RejectReason {get{return this._lAMWContractHeader.RejectReason;} set{  this._lAMWContractHeader.RejectReason = value; }}

		
			
		public string SignedbyEmployee {get{return this._lAMWContractHeader.SignedbyEmployee;} set{  this._lAMWContractHeader.SignedbyEmployee = value; }}

		
			
		public DateTime? SignedDate {get{return this._lAMWContractHeader.SignedDate;} set{  this._lAMWContractHeader.SignedDate = value; }}

		
			
		public long? ContractTemplate {get{return this._lAMWContractHeader.ContractTemplate;} set{  this._lAMWContractHeader.ContractTemplate = value; }}

		
			
		public string ManagerComments {get{return this._lAMWContractHeader.ManagerComments;} set{  this._lAMWContractHeader.ManagerComments = value; }}

		
			
		public string SignedPersonNationality {get{return this._lAMWContractHeader.SignedPersonNationality;} set{  this._lAMWContractHeader.SignedPersonNationality = value; }}

		
			
		public string SignedPersonIDNumber {get{return this._lAMWContractHeader.SignedPersonIDNumber;} set{  this._lAMWContractHeader.SignedPersonIDNumber = value; }}

		
			
		public string SignedPersonPosition {get{return this._lAMWContractHeader.SignedPersonPosition;} set{  this._lAMWContractHeader.SignedPersonPosition = value; }}

		
			
		public long? Quotation {get{return this._lAMWContractHeader.Quotation;} set{  this._lAMWContractHeader.Quotation = value; }}

		
			
		public int? ContractYears {get{return this._lAMWContractHeader.ContractYears;} set{  this._lAMWContractHeader.ContractYears = value; }}

		
			
		public long? ChargesSetupRecId {get{return this._lAMWContractHeader.ChargesSetupRecId;} set{  this._lAMWContractHeader.ChargesSetupRecId = value; }}

		
			
		public string RevisionNumber {get{return this._lAMWContractHeader.RevisionNumber;} set{  this._lAMWContractHeader.RevisionNumber = value; }}

		
			
		public int? Gender {get{return this._lAMWContractHeader.Gender;} set{  this._lAMWContractHeader.Gender = value; }}

		
			
		public DateTime? ChequeAccuralDate {get{return this._lAMWContractHeader.ChequeAccuralDate;} set{  this._lAMWContractHeader.ChequeAccuralDate = value; }}

		
			
		public string Article {get{return this._lAMWContractHeader.Article;} set{  this._lAMWContractHeader.Article = value; }}

		
			
		public long? TemplateID {get{return this._lAMWContractHeader.TemplateID;} set{  this._lAMWContractHeader.TemplateID = value; }}

		
			
		public decimal? PromissoryAmount {get{return this._lAMWContractHeader.PromissoryAmount;} set{  this._lAMWContractHeader.PromissoryAmount = value; }}

		
			
		public string ContractContent {get{return this._lAMWContractHeader.ContractContent;} set{  this._lAMWContractHeader.ContractContent = value; }}

		
			
		public int? ContractCreationType {get{return this._lAMWContractHeader.ContractCreationType;} set{  this._lAMWContractHeader.ContractCreationType = value; }}

		
			
		public string OrignalContractNumber {get{return this._lAMWContractHeader.OrignalContractNumber;} set{  this._lAMWContractHeader.OrignalContractNumber = value; }}

		
			
		public int? UsedDays {get{return this._lAMWContractHeader.UsedDays;} set{  this._lAMWContractHeader.UsedDays = value; }}

		
			
		public int? TotalDays {get{return this._lAMWContractHeader.TotalDays;} set{  this._lAMWContractHeader.TotalDays = value; }}

		
			
		public int? RemainingDays {get{return this._lAMWContractHeader.RemainingDays;} set{  this._lAMWContractHeader.RemainingDays = value; }}

		
			
		public string PreviousContractNumber {get{return this._lAMWContractHeader.PreviousContractNumber;} set{  this._lAMWContractHeader.PreviousContractNumber = value; }}

		
			
		public bool? isExtended {get{return this._lAMWContractHeader.isExtended;} set{  this._lAMWContractHeader.isExtended = value; }}

		
			
		public string ExtendedContractNumber {get{return this._lAMWContractHeader.ExtendedContractNumber;} set{  this._lAMWContractHeader.ExtendedContractNumber = value; }}

		
			
		public string CRM_GUID {get{return this._lAMWContractHeader.CRM_GUID;} set{  this._lAMWContractHeader.CRM_GUID = value; }}

		
			
		public int? Channel {get{return this._lAMWContractHeader.Channel;} set{  this._lAMWContractHeader.Channel = value; }}

		
			
		public decimal? PenalityPerDay {get{return this._lAMWContractHeader.PenalityPerDay;} set{  this._lAMWContractHeader.PenalityPerDay = value; }}

		
			
		public decimal? TotalPenalityAmount {get{return this._lAMWContractHeader.TotalPenalityAmount;} set{  this._lAMWContractHeader.TotalPenalityAmount = value; }}

		
				#endregion
				
		
	   #region GET

	    public IQueryable<Arco.Model.Entity.AMWContractHeader> GetAsQueryAble()
        {
            return _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>();
        }

	   public Arco.Model.Entity.AMWContractHeader GetByContractNumber(string contractNumber)
       {
            //Validate Input
            if (contractNumber.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("contractNumber");
            return _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>().FirstOrDefault(aMWContractHeader => aMWContractHeader.ContractNumber == contractNumber);
       }

	    
        public Arco.Model.Entity.AMWContractHeader GetByRecId(long RecId)
        {
            //Validate Input
            if (RecId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("RecId");
            return _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>().FirstOrDefault(aMWContractHeader => aMWContractHeader.RecId == RecId);
        }
		
		public List<Arco.Model.Entity.AMWContractHeader> GetAll(string orderBy = default(string))
        {
			if (string.IsNullOrEmpty(orderBy))
		            orderBy = "ContractNumber";
            return _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>().DynamicOrderBy(orderBy).ToList();
        }

        public List<Arco.Model.Entity.AMWContractHeader> GetAllPaged(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "ContractNumber";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>()
                    .DynamicOrderBy(orderBy)
                    .Skip(startRowIndex)
                    .Take(maximumRows)
                    .ToList();
        }



		 public QueryResult GetPaged(QueryCriteria _QueryCriteria)
        {

            int PageNo = 1;
            int PageSize = 20;

            if (_QueryCriteria!=null && _QueryCriteria.Pagging!=null)
            {
              PageNo=  _QueryCriteria.Pagging.PageNo;
              PageSize = _QueryCriteria.Pagging.PageSize;
            }
            return QueryExtension.CreatePagedResults(_context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>(), PageNo, PageSize);
           
        }

		public QueryResult GetList(QueryCriteria _QueryCriteria)
         {
             return QueryExtension.CreatePagedWithFilterResults(_context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>(),_QueryCriteria);

         }

		    public List<Arco.Model.Entity.AMWContractHeader> GetAllSearchDynamic(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string[] filterBy = default(string[]), string[] condition = default(string[]), string[] filterValue = default(string[]))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "ContractNumber";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>()
					.DynamicWhereQ(filterBy, condition, filterValue)
                    .DynamicOrderBy(orderBy)
                    .ToList();
        }

        public List<Arco.Model.Entity.AMWContractHeader> GetAllSearchPaged(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string filterBy = default(string), string condition = "Equals", object filterValue = default(object))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "ContractNumber";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>()
					.DynamicWhere(filterBy, condition, filterValue)
                    .DynamicOrderBy(orderBy)
                    .Skip(startRowIndex)
                    .Take(maximumRows)
                    .ToList();
        }

		public int GetTotalCountForAllSearch(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string filterBy = default(string), string condition = "Equals", object filterValue = default(object))
        {
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>().DynamicWhere(filterBy, condition, filterValue).Count();
        }

        public int GetTotalCountForAll(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int))
        {       
           return _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>().Count();                     
        }
        
		#endregion

		#region Persistence 

		

		public Arco.Model.Entity.AMWContractHeader Insert()
        {
		if (_lAMWContractHeader == null)
                throw (new ArgumentNullException("newAMWContractHeader"));

			return	Insert(_lAMWContractHeader);

		}



        public virtual Arco.Model.Entity.AMWContractHeader Insert(Arco.Model.Entity.AMWContractHeader newAMWContractHeader)
        {
			int numberOfAffectedRows = 0;

            // Validate Parameters 
            if (newAMWContractHeader == null)
                throw (new ArgumentNullException("newAMWContractHeader"));

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(newAMWContractHeader, PersistenceType.Create);
				OnInserting(newAMWContractHeader);

				newAMWContractHeader.DataAreaId = CurrentSession.DataAreaId;
				newAMWContractHeader.CreatedBy = CurrentSession.UserId;
				newAMWContractHeader.CreatedDatetime = DateTime.UtcNow;
				newAMWContractHeader.ModifiedBy = CurrentSession.UserId;
				newAMWContractHeader.ModifiedDatetime = DateTime.UtcNow;



				
			// Validate Primary key value
			try{
			string primary=newAMWContractHeader.ContractNumber.ToString();
			if (primary.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("contractNumber");

				}
				catch(Exception ex)
				{
				 BizHelper.ThrowErrorForInvalidDataKey("contractNumber");
				}



				try
				{
					_context.Add<Arco.Model.Entity.AMWContractHeader>(newAMWContractHeader);
					numberOfAffectedRows = _context.UnitOfWork.Commit();
				}
				catch (DbEntityValidationException ex)
				{
					BizHelper.ThrowDbEntityValidationException(ex);
				}

				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException("No aMWContractHeader created!");

				// Apply business workflow
				OnInserted(newAMWContractHeader);
				OnSaved(newAMWContractHeader, PersistenceType.Create);

				//scope.Complete();
			//}
            return newAMWContractHeader;
        }
		
        public virtual Arco.Model.Entity.AMWContractHeader Insert(string _contractNumber, string _dataAreaId, long _recId, string _customerID = default(string), DateTime _contractDate = default(DateTime), string _contractSignedBy = default(string), DateTime _validFrom = default(DateTime), DateTime _validTo = default(DateTime), int _serviceType = default(int), decimal _contractValue = default(decimal), int _contractStatus = default(int), int _processStatus = default(int), DateTime _createdDatetime = default(DateTime), string _createdBy = default(string), DateTime _modifiedDatetime = default(DateTime), string _modifiedBy = default(string), byte[] _recVersion = default(byte[]), int _totalManPower = default(int), decimal _advancePayment = default(decimal), string _countryRegion = default(string), string _city = default(string), bool _directContract = default(bool), string _virtualAccountId = default(string), string _internalContractNumber = default(string), string _remarks = default(string), string _externalID = default(string), string _externalDisplayCode = default(string), string _externalDisplayName = default(string), string _externalFamousName = default(string), string _payGroupID = default(string), string _feeSetup = default(string), int _contractType = default(int), int _contractSalesTotal = default(int), string _invoiceRuleID = default(string), string _approvedby = default(string), DateTime _approvedDate = default(DateTime), string _financeCustomerID = default(string), string _rejectReason = default(string), string _signedbyEmployee = default(string), DateTime _signedDate = default(DateTime), long _contractTemplate = default(long), string _managerComments = default(string), string _signedPersonNationality = default(string), string _signedPersonIDNumber = default(string), string _signedPersonPosition = default(string), long _quotation = default(long), int _contractYears = default(int), long _chargesSetupRecId = default(long), string _revisionNumber = default(string), int _gender = default(int), DateTime _chequeAccuralDate = default(DateTime), string _article = default(string), long _templateID = default(long), decimal _promissoryAmount = default(decimal), string _contractContent = default(string), int _contractCreationType = default(int), string _orignalContractNumber = default(string), int _usedDays = default(int), int _totalDays = default(int), int _remainingDays = default(int), string _previousContractNumber = default(string), bool _isExtended = default(bool), string _extendedContractNumber = default(string), string _cRM_GUID = default(string), int _channel = default(int), decimal _penalityPerDay = default(decimal), decimal _totalPenalityAmount = default(decimal))
		{
			// Validate Primary key value
			if (_contractNumber.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("contractNumber");

				
			Arco.Model.Entity.AMWContractHeader aMWContractHeaderToBeCreated = new Arco.Model.Entity.AMWContractHeader();

			aMWContractHeaderToBeCreated.ContractNumber = _contractNumber;
			if(_customerID != default(string))
				aMWContractHeaderToBeCreated.CustomerID = _customerID;
			if(_contractDate != default(DateTime))
				aMWContractHeaderToBeCreated.ContractDate = _contractDate;
			if(_contractSignedBy != default(string))
				aMWContractHeaderToBeCreated.ContractSignedBy = _contractSignedBy;
			if(_validFrom != default(DateTime))
				aMWContractHeaderToBeCreated.ValidFrom = _validFrom;
			if(_validTo != default(DateTime))
				aMWContractHeaderToBeCreated.ValidTo = _validTo;
			if(_serviceType != default(int))
				aMWContractHeaderToBeCreated.ServiceType = _serviceType;
			if(_contractValue != default(decimal))
				aMWContractHeaderToBeCreated.ContractValue = _contractValue;
			if(_contractStatus != default(int))
				aMWContractHeaderToBeCreated.ContractStatus = _contractStatus;
			if(_processStatus != default(int))
				aMWContractHeaderToBeCreated.ProcessStatus = _processStatus;
			if(_totalManPower != default(int))
				aMWContractHeaderToBeCreated.TotalManPower = _totalManPower;
			if(_advancePayment != default(decimal))
				aMWContractHeaderToBeCreated.AdvancePayment = _advancePayment;
			if(_countryRegion != default(string))
				aMWContractHeaderToBeCreated.CountryRegion = _countryRegion;
			if(_city != default(string))
				aMWContractHeaderToBeCreated.City = _city;
			if(_directContract != default(bool))
				aMWContractHeaderToBeCreated.DirectContract = _directContract;
			if(_virtualAccountId != default(string))
				aMWContractHeaderToBeCreated.VirtualAccountId = _virtualAccountId;
			if(_internalContractNumber != default(string))
				aMWContractHeaderToBeCreated.InternalContractNumber = _internalContractNumber;
			if(_remarks != default(string))
				aMWContractHeaderToBeCreated.Remarks = _remarks;
			if(_externalID != default(string))
				aMWContractHeaderToBeCreated.ExternalID = _externalID;
			if(_externalDisplayCode != default(string))
				aMWContractHeaderToBeCreated.ExternalDisplayCode = _externalDisplayCode;
			if(_externalDisplayName != default(string))
				aMWContractHeaderToBeCreated.ExternalDisplayName = _externalDisplayName;
			if(_externalFamousName != default(string))
				aMWContractHeaderToBeCreated.ExternalFamousName = _externalFamousName;
			if(_payGroupID != default(string))
				aMWContractHeaderToBeCreated.PayGroupID = _payGroupID;
			if(_feeSetup != default(string))
				aMWContractHeaderToBeCreated.FeeSetup = _feeSetup;
			if(_contractType != default(int))
				aMWContractHeaderToBeCreated.ContractType = _contractType;
			if(_contractSalesTotal != default(int))
				aMWContractHeaderToBeCreated.ContractSalesTotal = _contractSalesTotal;
			if(_invoiceRuleID != default(string))
				aMWContractHeaderToBeCreated.InvoiceRuleID = _invoiceRuleID;
			if(_approvedby != default(string))
				aMWContractHeaderToBeCreated.Approvedby = _approvedby;
			if(_approvedDate != default(DateTime))
				aMWContractHeaderToBeCreated.ApprovedDate = _approvedDate;
			if(_financeCustomerID != default(string))
				aMWContractHeaderToBeCreated.FinanceCustomerID = _financeCustomerID;
			if(_rejectReason != default(string))
				aMWContractHeaderToBeCreated.RejectReason = _rejectReason;
			if(_signedbyEmployee != default(string))
				aMWContractHeaderToBeCreated.SignedbyEmployee = _signedbyEmployee;
			if(_signedDate != default(DateTime))
				aMWContractHeaderToBeCreated.SignedDate = _signedDate;
			if(_contractTemplate != default(long))
				aMWContractHeaderToBeCreated.ContractTemplate = _contractTemplate;
			if(_managerComments != default(string))
				aMWContractHeaderToBeCreated.ManagerComments = _managerComments;
			if(_signedPersonNationality != default(string))
				aMWContractHeaderToBeCreated.SignedPersonNationality = _signedPersonNationality;
			if(_signedPersonIDNumber != default(string))
				aMWContractHeaderToBeCreated.SignedPersonIDNumber = _signedPersonIDNumber;
			if(_signedPersonPosition != default(string))
				aMWContractHeaderToBeCreated.SignedPersonPosition = _signedPersonPosition;
			if(_quotation != default(long))
				aMWContractHeaderToBeCreated.Quotation = _quotation;
			if(_contractYears != default(int))
				aMWContractHeaderToBeCreated.ContractYears = _contractYears;
			if(_chargesSetupRecId != default(long))
				aMWContractHeaderToBeCreated.ChargesSetupRecId = _chargesSetupRecId;
			if(_revisionNumber != default(string))
				aMWContractHeaderToBeCreated.RevisionNumber = _revisionNumber;
			if(_gender != default(int))
				aMWContractHeaderToBeCreated.Gender = _gender;
			if(_chequeAccuralDate != default(DateTime))
				aMWContractHeaderToBeCreated.ChequeAccuralDate = _chequeAccuralDate;
			if(_article != default(string))
				aMWContractHeaderToBeCreated.Article = _article;
			if(_templateID != default(long))
				aMWContractHeaderToBeCreated.TemplateID = _templateID;
			if(_promissoryAmount != default(decimal))
				aMWContractHeaderToBeCreated.PromissoryAmount = _promissoryAmount;
			if(_contractContent != default(string))
				aMWContractHeaderToBeCreated.ContractContent = _contractContent;
			if(_contractCreationType != default(int))
				aMWContractHeaderToBeCreated.ContractCreationType = _contractCreationType;
			if(_orignalContractNumber != default(string))
				aMWContractHeaderToBeCreated.OrignalContractNumber = _orignalContractNumber;
			if(_usedDays != default(int))
				aMWContractHeaderToBeCreated.UsedDays = _usedDays;
			if(_totalDays != default(int))
				aMWContractHeaderToBeCreated.TotalDays = _totalDays;
			if(_remainingDays != default(int))
				aMWContractHeaderToBeCreated.RemainingDays = _remainingDays;
			if(_previousContractNumber != default(string))
				aMWContractHeaderToBeCreated.PreviousContractNumber = _previousContractNumber;
			if(_isExtended != default(bool))
				aMWContractHeaderToBeCreated.isExtended = _isExtended;
			if(_extendedContractNumber != default(string))
				aMWContractHeaderToBeCreated.ExtendedContractNumber = _extendedContractNumber;
			if(_cRM_GUID != default(string))
				aMWContractHeaderToBeCreated.CRM_GUID = _cRM_GUID;
			if(_channel != default(int))
				aMWContractHeaderToBeCreated.Channel = _channel;
			if(_penalityPerDay != default(decimal))
				aMWContractHeaderToBeCreated.PenalityPerDay = _penalityPerDay;
			if(_totalPenalityAmount != default(decimal))
				aMWContractHeaderToBeCreated.TotalPenalityAmount = _totalPenalityAmount;

			return Insert(aMWContractHeaderToBeCreated);
        }




		public virtual Arco.Model.Entity.AMWContractHeader InsertDynamic(string dynamicObj)
		{

		 dynamic results = JsonConvert.DeserializeObject(dynamicObj);

           //     RecCandidate Demand1 = new RecCandidate();
                

             //   if (results.FirstName != null && results.FirstName.ToString() != "")
               //     Demand1.FirstName = results.FirstName.ToString();
				
			Arco.Model.Entity.AMWContractHeader aMWContractHeaderToBeCreated = new Arco.Model.Entity.AMWContractHeader();

			
	if(results.ContractNumber != null && results.ContractNumber.ToString() != "")
	aMWContractHeaderToBeCreated.ContractNumber =results.ContractNumber.ToString();// results.ContractNumber;
			if(results.CustomerID != null && results.CustomerID.ToString() != "")
				aMWContractHeaderToBeCreated.CustomerID =results.CustomerID.ToString();
			if(results.ContractDate != null && results.ContractDate.ToString() != "")
				aMWContractHeaderToBeCreated.ContractDate =results.ContractDate;
			if(results.ContractSignedBy != null && results.ContractSignedBy.ToString() != "")
				aMWContractHeaderToBeCreated.ContractSignedBy =results.ContractSignedBy.ToString();
			if(results.ValidFrom != null && results.ValidFrom.ToString() != "")
				aMWContractHeaderToBeCreated.ValidFrom =results.ValidFrom;
			if(results.ValidTo != null && results.ValidTo.ToString() != "")
				aMWContractHeaderToBeCreated.ValidTo =results.ValidTo;
			if(results.ServiceType != null && results.ServiceType.ToString() != "")
				aMWContractHeaderToBeCreated.ServiceType =int.Parse(results.ServiceType.ToString());
			if(results.ContractValue != null && results.ContractValue.ToString() != "")
				aMWContractHeaderToBeCreated.ContractValue =Decimal.Parse(results.ContractValue.ToString());
			if(results.ContractStatus != null && results.ContractStatus.ToString() != "")
				aMWContractHeaderToBeCreated.ContractStatus =int.Parse(results.ContractStatus.ToString());
			if(results.ProcessStatus != null && results.ProcessStatus.ToString() != "")
				aMWContractHeaderToBeCreated.ProcessStatus =int.Parse(results.ProcessStatus.ToString());
			if(results.TotalManPower != null && results.TotalManPower.ToString() != "")
				aMWContractHeaderToBeCreated.TotalManPower =int.Parse(results.TotalManPower.ToString());
			if(results.AdvancePayment != null && results.AdvancePayment.ToString() != "")
				aMWContractHeaderToBeCreated.AdvancePayment =Decimal.Parse(results.AdvancePayment.ToString());
			if(results.CountryRegion != null && results.CountryRegion.ToString() != "")
				aMWContractHeaderToBeCreated.CountryRegion =results.CountryRegion.ToString();
			if(results.City != null && results.City.ToString() != "")
				aMWContractHeaderToBeCreated.City =results.City.ToString();
			if(results.DirectContract != null && results.DirectContract.ToString() != "")
				aMWContractHeaderToBeCreated.DirectContract =bool.Parse(results.DirectContract.ToString());
			if(results.VirtualAccountId != null && results.VirtualAccountId.ToString() != "")
				aMWContractHeaderToBeCreated.VirtualAccountId =results.VirtualAccountId.ToString();
			if(results.InternalContractNumber != null && results.InternalContractNumber.ToString() != "")
				aMWContractHeaderToBeCreated.InternalContractNumber =results.InternalContractNumber.ToString();
			if(results.Remarks != null && results.Remarks.ToString() != "")
				aMWContractHeaderToBeCreated.Remarks =results.Remarks.ToString();
			if(results.ExternalID != null && results.ExternalID.ToString() != "")
				aMWContractHeaderToBeCreated.ExternalID =results.ExternalID.ToString();
			if(results.ExternalDisplayCode != null && results.ExternalDisplayCode.ToString() != "")
				aMWContractHeaderToBeCreated.ExternalDisplayCode =results.ExternalDisplayCode.ToString();
			if(results.ExternalDisplayName != null && results.ExternalDisplayName.ToString() != "")
				aMWContractHeaderToBeCreated.ExternalDisplayName =results.ExternalDisplayName.ToString();
			if(results.ExternalFamousName != null && results.ExternalFamousName.ToString() != "")
				aMWContractHeaderToBeCreated.ExternalFamousName =results.ExternalFamousName.ToString();
			if(results.PayGroupID != null && results.PayGroupID.ToString() != "")
				aMWContractHeaderToBeCreated.PayGroupID =results.PayGroupID.ToString();
			if(results.FeeSetup != null && results.FeeSetup.ToString() != "")
				aMWContractHeaderToBeCreated.FeeSetup =results.FeeSetup.ToString();
			if(results.ContractType != null && results.ContractType.ToString() != "")
				aMWContractHeaderToBeCreated.ContractType =int.Parse(results.ContractType.ToString());
			if(results.ContractSalesTotal != null && results.ContractSalesTotal.ToString() != "")
				aMWContractHeaderToBeCreated.ContractSalesTotal =int.Parse(results.ContractSalesTotal.ToString());
			if(results.InvoiceRuleID != null && results.InvoiceRuleID.ToString() != "")
				aMWContractHeaderToBeCreated.InvoiceRuleID =results.InvoiceRuleID.ToString();
			if(results.Approvedby != null && results.Approvedby.ToString() != "")
				aMWContractHeaderToBeCreated.Approvedby =results.Approvedby.ToString();
			if(results.ApprovedDate != null && results.ApprovedDate.ToString() != "")
				aMWContractHeaderToBeCreated.ApprovedDate =results.ApprovedDate;
			if(results.FinanceCustomerID != null && results.FinanceCustomerID.ToString() != "")
				aMWContractHeaderToBeCreated.FinanceCustomerID =results.FinanceCustomerID.ToString();
			if(results.RejectReason != null && results.RejectReason.ToString() != "")
				aMWContractHeaderToBeCreated.RejectReason =results.RejectReason.ToString();
			if(results.SignedbyEmployee != null && results.SignedbyEmployee.ToString() != "")
				aMWContractHeaderToBeCreated.SignedbyEmployee =results.SignedbyEmployee.ToString();
			if(results.SignedDate != null && results.SignedDate.ToString() != "")
				aMWContractHeaderToBeCreated.SignedDate =results.SignedDate;
			if(results.ContractTemplate != null && results.ContractTemplate.ToString() != "")
				aMWContractHeaderToBeCreated.ContractTemplate =long.Parse(results.ContractTemplate.ToString());
			if(results.ManagerComments != null && results.ManagerComments.ToString() != "")
				aMWContractHeaderToBeCreated.ManagerComments =results.ManagerComments.ToString();
			if(results.SignedPersonNationality != null && results.SignedPersonNationality.ToString() != "")
				aMWContractHeaderToBeCreated.SignedPersonNationality =results.SignedPersonNationality.ToString();
			if(results.SignedPersonIDNumber != null && results.SignedPersonIDNumber.ToString() != "")
				aMWContractHeaderToBeCreated.SignedPersonIDNumber =results.SignedPersonIDNumber.ToString();
			if(results.SignedPersonPosition != null && results.SignedPersonPosition.ToString() != "")
				aMWContractHeaderToBeCreated.SignedPersonPosition =results.SignedPersonPosition.ToString();
			if(results.Quotation != null && results.Quotation.ToString() != "")
				aMWContractHeaderToBeCreated.Quotation =long.Parse(results.Quotation.ToString());
			if(results.ContractYears != null && results.ContractYears.ToString() != "")
				aMWContractHeaderToBeCreated.ContractYears =int.Parse(results.ContractYears.ToString());
			if(results.ChargesSetupRecId != null && results.ChargesSetupRecId.ToString() != "")
				aMWContractHeaderToBeCreated.ChargesSetupRecId =long.Parse(results.ChargesSetupRecId.ToString());
			if(results.RevisionNumber != null && results.RevisionNumber.ToString() != "")
				aMWContractHeaderToBeCreated.RevisionNumber =results.RevisionNumber.ToString();
			if(results.Gender != null && results.Gender.ToString() != "")
				aMWContractHeaderToBeCreated.Gender =int.Parse(results.Gender.ToString());
			if(results.ChequeAccuralDate != null && results.ChequeAccuralDate.ToString() != "")
				aMWContractHeaderToBeCreated.ChequeAccuralDate =results.ChequeAccuralDate;
			if(results.Article != null && results.Article.ToString() != "")
				aMWContractHeaderToBeCreated.Article =results.Article.ToString();
			if(results.TemplateID != null && results.TemplateID.ToString() != "")
				aMWContractHeaderToBeCreated.TemplateID =long.Parse(results.TemplateID.ToString());
			if(results.PromissoryAmount != null && results.PromissoryAmount.ToString() != "")
				aMWContractHeaderToBeCreated.PromissoryAmount =Decimal.Parse(results.PromissoryAmount.ToString());
			if(results.ContractContent != null && results.ContractContent.ToString() != "")
				aMWContractHeaderToBeCreated.ContractContent =results.ContractContent.ToString();
			if(results.ContractCreationType != null && results.ContractCreationType.ToString() != "")
				aMWContractHeaderToBeCreated.ContractCreationType =int.Parse(results.ContractCreationType.ToString());
			if(results.OrignalContractNumber != null && results.OrignalContractNumber.ToString() != "")
				aMWContractHeaderToBeCreated.OrignalContractNumber =results.OrignalContractNumber.ToString();
			if(results.UsedDays != null && results.UsedDays.ToString() != "")
				aMWContractHeaderToBeCreated.UsedDays =int.Parse(results.UsedDays.ToString());
			if(results.TotalDays != null && results.TotalDays.ToString() != "")
				aMWContractHeaderToBeCreated.TotalDays =int.Parse(results.TotalDays.ToString());
			if(results.RemainingDays != null && results.RemainingDays.ToString() != "")
				aMWContractHeaderToBeCreated.RemainingDays =int.Parse(results.RemainingDays.ToString());
			if(results.PreviousContractNumber != null && results.PreviousContractNumber.ToString() != "")
				aMWContractHeaderToBeCreated.PreviousContractNumber =results.PreviousContractNumber.ToString();
			if(results.isExtended != null && results.isExtended.ToString() != "")
				aMWContractHeaderToBeCreated.isExtended =bool.Parse(results.isExtended.ToString());
			if(results.ExtendedContractNumber != null && results.ExtendedContractNumber.ToString() != "")
				aMWContractHeaderToBeCreated.ExtendedContractNumber =results.ExtendedContractNumber.ToString();
			if(results.CRM_GUID != null && results.CRM_GUID.ToString() != "")
				aMWContractHeaderToBeCreated.CRM_GUID =results.CRM_GUID.ToString();
			if(results.Channel != null && results.Channel.ToString() != "")
				aMWContractHeaderToBeCreated.Channel =int.Parse(results.Channel.ToString());
			if(results.PenalityPerDay != null && results.PenalityPerDay.ToString() != "")
				aMWContractHeaderToBeCreated.PenalityPerDay =Decimal.Parse(results.PenalityPerDay.ToString());
			if(results.TotalPenalityAmount != null && results.TotalPenalityAmount.ToString() != "")
				aMWContractHeaderToBeCreated.TotalPenalityAmount =Decimal.Parse(results.TotalPenalityAmount.ToString());

			return Insert(aMWContractHeaderToBeCreated);
        }



			public Arco.Model.Entity.AMWContractHeader Update()
        {
		

		if (_lAMWContractHeader == null || _lAMWContractHeader.RecId == default(long))
                throw (new ArgumentNullException("newAMWContractHeader"));

				return Update(_lAMWContractHeader);

		}


        public Arco.Model.Entity.AMWContractHeader Update(Arco.Model.Entity.AMWContractHeader updatedAMWContractHeader)
        {
			int numberOfAffectedRows = 0;

            // Validate Parameters
            if (updatedAMWContractHeader == null)
                throw (new ArgumentNullException("updatedAMWContractHeader"));

            // Validate Primary key value
            if (updatedAMWContractHeader.ContractNumber.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("ContractNumber");

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(updatedAMWContractHeader, PersistenceType.Update);
				OnUpdating(updatedAMWContractHeader);

				updatedAMWContractHeader.ModifiedBy = CurrentSession.UserId;
				updatedAMWContractHeader.ModifiedDatetime = DateTime.UtcNow;

				try
				{
					_context.Update<Arco.Model.Entity.AMWContractHeader>(updatedAMWContractHeader);

                   // DbEntityEntry<AMWContractHeader> entryAMWContractHeader = _context.Entry(updatedAMWContractHeader);
                  //  entryAMWContractHeader.State = EntityState.Modified;
                   // entryAMWContractHeader.Property("CreatedDatetime").IsModified = false;
                   // entryAMWContractHeader.Property("CreatedBy").IsModified = false;

					numberOfAffectedRows = _context.UnitOfWork.Commit();
				}
				catch (DbEntityValidationException ex)
				{
					BizHelper.ThrowDbEntityValidationException(ex);
				}


				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException("No aMWContractHeader updated!");

				//Apply business workflow
				OnUpdated(updatedAMWContractHeader);
				OnSaved(updatedAMWContractHeader, PersistenceType.Update);

			//	scope.Complete();
			//}
			return updatedAMWContractHeader;

        }

        public Arco.Model.Entity.AMWContractHeader Update(string _contractNumber, string _customerID = default(string), DateTime _contractDate = default(DateTime), string _contractSignedBy = default(string), DateTime _validFrom = default(DateTime), DateTime _validTo = default(DateTime), int _serviceType = default(int), decimal _contractValue = default(decimal), int _contractStatus = default(int), int _processStatus = default(int), int _totalManPower = default(int), decimal _advancePayment = default(decimal), string _countryRegion = default(string), string _city = default(string), bool _directContract = default(bool), string _virtualAccountId = default(string), string _internalContractNumber = default(string), string _remarks = default(string), string _externalID = default(string), string _externalDisplayCode = default(string), string _externalDisplayName = default(string), string _externalFamousName = default(string), string _payGroupID = default(string), string _feeSetup = default(string), int _contractType = default(int), int _contractSalesTotal = default(int), string _invoiceRuleID = default(string), string _approvedby = default(string), DateTime _approvedDate = default(DateTime), string _financeCustomerID = default(string), string _rejectReason = default(string), string _signedbyEmployee = default(string), DateTime _signedDate = default(DateTime), long _contractTemplate = default(long), string _managerComments = default(string), string _signedPersonNationality = default(string), string _signedPersonIDNumber = default(string), string _signedPersonPosition = default(string), long _quotation = default(long), int _contractYears = default(int), long _chargesSetupRecId = default(long), string _revisionNumber = default(string), int _gender = default(int), DateTime _chequeAccuralDate = default(DateTime), string _article = default(string), long _templateID = default(long), decimal _promissoryAmount = default(decimal), string _contractContent = default(string), int _contractCreationType = default(int), string _orignalContractNumber = default(string), int _usedDays = default(int), int _totalDays = default(int), int _remainingDays = default(int), string _previousContractNumber = default(string), bool _isExtended = default(bool), string _extendedContractNumber = default(string), string _cRM_GUID = default(string), int _channel = default(int), decimal _penalityPerDay = default(decimal), decimal _totalPenalityAmount = default(decimal))
		{
			if (_contractNumber.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("contractNumber");
            
			Arco.Model.Entity.AMWContractHeader aMWContractHeaderToBeUpdated = _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>().FirstOrDefault(aMWContractHeader => aMWContractHeader.ContractNumber == _contractNumber);


			if(_contractNumber != default(string))
				aMWContractHeaderToBeUpdated.ContractNumber = _contractNumber;
			if(_customerID != default(string))
				aMWContractHeaderToBeUpdated.CustomerID = _customerID;
			if(_contractDate != default(DateTime))
				aMWContractHeaderToBeUpdated.ContractDate = _contractDate;
			if(_contractSignedBy != default(string))
				aMWContractHeaderToBeUpdated.ContractSignedBy = _contractSignedBy;
			if(_validFrom != default(DateTime))
				aMWContractHeaderToBeUpdated.ValidFrom = _validFrom;
			if(_validTo != default(DateTime))
				aMWContractHeaderToBeUpdated.ValidTo = _validTo;
			if(_serviceType != default(int))
				aMWContractHeaderToBeUpdated.ServiceType = _serviceType;
			if(_contractValue != default(decimal))
				aMWContractHeaderToBeUpdated.ContractValue = _contractValue;
			if(_contractStatus != default(int))
				aMWContractHeaderToBeUpdated.ContractStatus = _contractStatus;
			if(_processStatus != default(int))
				aMWContractHeaderToBeUpdated.ProcessStatus = _processStatus;
			if(_totalManPower != default(int))
				aMWContractHeaderToBeUpdated.TotalManPower = _totalManPower;
			if(_advancePayment != default(decimal))
				aMWContractHeaderToBeUpdated.AdvancePayment = _advancePayment;
			if(_countryRegion != default(string))
				aMWContractHeaderToBeUpdated.CountryRegion = _countryRegion;
			if(_city != default(string))
				aMWContractHeaderToBeUpdated.City = _city;
			if(_directContract != default(bool))
				aMWContractHeaderToBeUpdated.DirectContract = _directContract;
			if(_virtualAccountId != default(string))
				aMWContractHeaderToBeUpdated.VirtualAccountId = _virtualAccountId;
			if(_internalContractNumber != default(string))
				aMWContractHeaderToBeUpdated.InternalContractNumber = _internalContractNumber;
			if(_remarks != default(string))
				aMWContractHeaderToBeUpdated.Remarks = _remarks;
			if(_externalID != default(string))
				aMWContractHeaderToBeUpdated.ExternalID = _externalID;
			if(_externalDisplayCode != default(string))
				aMWContractHeaderToBeUpdated.ExternalDisplayCode = _externalDisplayCode;
			if(_externalDisplayName != default(string))
				aMWContractHeaderToBeUpdated.ExternalDisplayName = _externalDisplayName;
			if(_externalFamousName != default(string))
				aMWContractHeaderToBeUpdated.ExternalFamousName = _externalFamousName;
			if(_payGroupID != default(string))
				aMWContractHeaderToBeUpdated.PayGroupID = _payGroupID;
			if(_feeSetup != default(string))
				aMWContractHeaderToBeUpdated.FeeSetup = _feeSetup;
			if(_contractType != default(int))
				aMWContractHeaderToBeUpdated.ContractType = _contractType;
			if(_contractSalesTotal != default(int))
				aMWContractHeaderToBeUpdated.ContractSalesTotal = _contractSalesTotal;
			if(_invoiceRuleID != default(string))
				aMWContractHeaderToBeUpdated.InvoiceRuleID = _invoiceRuleID;
			if(_approvedby != default(string))
				aMWContractHeaderToBeUpdated.Approvedby = _approvedby;
			if(_approvedDate != default(DateTime))
				aMWContractHeaderToBeUpdated.ApprovedDate = _approvedDate;
			if(_financeCustomerID != default(string))
				aMWContractHeaderToBeUpdated.FinanceCustomerID = _financeCustomerID;
			if(_rejectReason != default(string))
				aMWContractHeaderToBeUpdated.RejectReason = _rejectReason;
			if(_signedbyEmployee != default(string))
				aMWContractHeaderToBeUpdated.SignedbyEmployee = _signedbyEmployee;
			if(_signedDate != default(DateTime))
				aMWContractHeaderToBeUpdated.SignedDate = _signedDate;
			if(_contractTemplate != default(long))
				aMWContractHeaderToBeUpdated.ContractTemplate = _contractTemplate;
			if(_managerComments != default(string))
				aMWContractHeaderToBeUpdated.ManagerComments = _managerComments;
			if(_signedPersonNationality != default(string))
				aMWContractHeaderToBeUpdated.SignedPersonNationality = _signedPersonNationality;
			if(_signedPersonIDNumber != default(string))
				aMWContractHeaderToBeUpdated.SignedPersonIDNumber = _signedPersonIDNumber;
			if(_signedPersonPosition != default(string))
				aMWContractHeaderToBeUpdated.SignedPersonPosition = _signedPersonPosition;
			if(_quotation != default(long))
				aMWContractHeaderToBeUpdated.Quotation = _quotation;
			if(_contractYears != default(int))
				aMWContractHeaderToBeUpdated.ContractYears = _contractYears;
			if(_chargesSetupRecId != default(long))
				aMWContractHeaderToBeUpdated.ChargesSetupRecId = _chargesSetupRecId;
			if(_revisionNumber != default(string))
				aMWContractHeaderToBeUpdated.RevisionNumber = _revisionNumber;
			if(_gender != default(int))
				aMWContractHeaderToBeUpdated.Gender = _gender;
			if(_chequeAccuralDate != default(DateTime))
				aMWContractHeaderToBeUpdated.ChequeAccuralDate = _chequeAccuralDate;
			if(_article != default(string))
				aMWContractHeaderToBeUpdated.Article = _article;
			if(_templateID != default(long))
				aMWContractHeaderToBeUpdated.TemplateID = _templateID;
			if(_promissoryAmount != default(decimal))
				aMWContractHeaderToBeUpdated.PromissoryAmount = _promissoryAmount;
			if(_contractContent != default(string))
				aMWContractHeaderToBeUpdated.ContractContent = _contractContent;
			if(_contractCreationType != default(int))
				aMWContractHeaderToBeUpdated.ContractCreationType = _contractCreationType;
			if(_orignalContractNumber != default(string))
				aMWContractHeaderToBeUpdated.OrignalContractNumber = _orignalContractNumber;
			if(_usedDays != default(int))
				aMWContractHeaderToBeUpdated.UsedDays = _usedDays;
			if(_totalDays != default(int))
				aMWContractHeaderToBeUpdated.TotalDays = _totalDays;
			if(_remainingDays != default(int))
				aMWContractHeaderToBeUpdated.RemainingDays = _remainingDays;
			if(_previousContractNumber != default(string))
				aMWContractHeaderToBeUpdated.PreviousContractNumber = _previousContractNumber;
			if(_isExtended != default(bool))
				aMWContractHeaderToBeUpdated.isExtended = _isExtended;
			if(_extendedContractNumber != default(string))
				aMWContractHeaderToBeUpdated.ExtendedContractNumber = _extendedContractNumber;
			if(_cRM_GUID != default(string))
				aMWContractHeaderToBeUpdated.CRM_GUID = _cRM_GUID;
			if(_channel != default(int))
				aMWContractHeaderToBeUpdated.Channel = _channel;
			if(_penalityPerDay != default(decimal))
				aMWContractHeaderToBeUpdated.PenalityPerDay = _penalityPerDay;
			if(_totalPenalityAmount != default(decimal))
				aMWContractHeaderToBeUpdated.TotalPenalityAmount = _totalPenalityAmount;

			return Update(aMWContractHeaderToBeUpdated);
        }



		 public Arco.Model.Entity.AMWContractHeader UpdateDynamic(string dynamicObj)
		{
		 dynamic results = JsonConvert.DeserializeObject(dynamicObj);

			

			string primary=results.ContractNumber;
			if (primary.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("contractNumber");

				string _ContractNumber =results.ContractNumber;
            
			Arco.Model.Entity.AMWContractHeader aMWContractHeaderToBeUpdated = _context.GetAsQueryable<Arco.Model.Entity.AMWContractHeader>().FirstOrDefault(aMWContractHeader => aMWContractHeader.ContractNumber ==_ContractNumber);


			if(results.ContractNumber != null && results.ContractNumber.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractNumber =results.ContractNumber.ToString();// results.ContractNumber;
			if(results.CustomerID != null && results.CustomerID.ToString() != "")
				aMWContractHeaderToBeUpdated.CustomerID =results.CustomerID.ToString();// results.CustomerID;
			if(results.ContractDate != null && results.ContractDate.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractDate =results.ContractDate;// results.ContractDate;
			if(results.ContractSignedBy != null && results.ContractSignedBy.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractSignedBy =results.ContractSignedBy.ToString();// results.ContractSignedBy;
			if(results.ValidFrom != null && results.ValidFrom.ToString() != "")
				aMWContractHeaderToBeUpdated.ValidFrom =results.ValidFrom;// results.ValidFrom;
			if(results.ValidTo != null && results.ValidTo.ToString() != "")
				aMWContractHeaderToBeUpdated.ValidTo =results.ValidTo;// results.ValidTo;
			if(results.ServiceType != null && results.ServiceType.ToString() != "")
				aMWContractHeaderToBeUpdated.ServiceType =int.Parse(results.ServiceType.ToString());// results.ServiceType;
			if(results.ContractValue != null && results.ContractValue.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractValue =Decimal.Parse(results.ContractValue.ToString());// results.ContractValue;
			if(results.ContractStatus != null && results.ContractStatus.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractStatus =int.Parse(results.ContractStatus.ToString());// results.ContractStatus;
			if(results.ProcessStatus != null && results.ProcessStatus.ToString() != "")
				aMWContractHeaderToBeUpdated.ProcessStatus =int.Parse(results.ProcessStatus.ToString());// results.ProcessStatus;
			if(results.TotalManPower != null && results.TotalManPower.ToString() != "")
				aMWContractHeaderToBeUpdated.TotalManPower =int.Parse(results.TotalManPower.ToString());// results.TotalManPower;
			if(results.AdvancePayment != null && results.AdvancePayment.ToString() != "")
				aMWContractHeaderToBeUpdated.AdvancePayment =Decimal.Parse(results.AdvancePayment.ToString());// results.AdvancePayment;
			if(results.CountryRegion != null && results.CountryRegion.ToString() != "")
				aMWContractHeaderToBeUpdated.CountryRegion =results.CountryRegion.ToString();// results.CountryRegion;
			if(results.City != null && results.City.ToString() != "")
				aMWContractHeaderToBeUpdated.City =results.City.ToString();// results.City;
			if(results.DirectContract != null && results.DirectContract.ToString() != "")
				aMWContractHeaderToBeUpdated.DirectContract =bool.Parse(results.DirectContract.ToString());// results.DirectContract;
			if(results.VirtualAccountId != null && results.VirtualAccountId.ToString() != "")
				aMWContractHeaderToBeUpdated.VirtualAccountId =results.VirtualAccountId.ToString();// results.VirtualAccountId;
			if(results.InternalContractNumber != null && results.InternalContractNumber.ToString() != "")
				aMWContractHeaderToBeUpdated.InternalContractNumber =results.InternalContractNumber.ToString();// results.InternalContractNumber;
			if(results.Remarks != null && results.Remarks.ToString() != "")
				aMWContractHeaderToBeUpdated.Remarks =results.Remarks.ToString();// results.Remarks;
			if(results.ExternalID != null && results.ExternalID.ToString() != "")
				aMWContractHeaderToBeUpdated.ExternalID =results.ExternalID.ToString();// results.ExternalID;
			if(results.ExternalDisplayCode != null && results.ExternalDisplayCode.ToString() != "")
				aMWContractHeaderToBeUpdated.ExternalDisplayCode =results.ExternalDisplayCode.ToString();// results.ExternalDisplayCode;
			if(results.ExternalDisplayName != null && results.ExternalDisplayName.ToString() != "")
				aMWContractHeaderToBeUpdated.ExternalDisplayName =results.ExternalDisplayName.ToString();// results.ExternalDisplayName;
			if(results.ExternalFamousName != null && results.ExternalFamousName.ToString() != "")
				aMWContractHeaderToBeUpdated.ExternalFamousName =results.ExternalFamousName.ToString();// results.ExternalFamousName;
			if(results.PayGroupID != null && results.PayGroupID.ToString() != "")
				aMWContractHeaderToBeUpdated.PayGroupID =results.PayGroupID.ToString();// results.PayGroupID;
			if(results.FeeSetup != null && results.FeeSetup.ToString() != "")
				aMWContractHeaderToBeUpdated.FeeSetup =results.FeeSetup.ToString();// results.FeeSetup;
			if(results.ContractType != null && results.ContractType.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractType =int.Parse(results.ContractType.ToString());// results.ContractType;
			if(results.ContractSalesTotal != null && results.ContractSalesTotal.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractSalesTotal =int.Parse(results.ContractSalesTotal.ToString());// results.ContractSalesTotal;
			if(results.InvoiceRuleID != null && results.InvoiceRuleID.ToString() != "")
				aMWContractHeaderToBeUpdated.InvoiceRuleID =results.InvoiceRuleID.ToString();// results.InvoiceRuleID;
			if(results.Approvedby != null && results.Approvedby.ToString() != "")
				aMWContractHeaderToBeUpdated.Approvedby =results.Approvedby.ToString();// results.Approvedby;
			if(results.ApprovedDate != null && results.ApprovedDate.ToString() != "")
				aMWContractHeaderToBeUpdated.ApprovedDate =results.ApprovedDate;// results.ApprovedDate;
			if(results.FinanceCustomerID != null && results.FinanceCustomerID.ToString() != "")
				aMWContractHeaderToBeUpdated.FinanceCustomerID =results.FinanceCustomerID.ToString();// results.FinanceCustomerID;
			if(results.RejectReason != null && results.RejectReason.ToString() != "")
				aMWContractHeaderToBeUpdated.RejectReason =results.RejectReason.ToString();// results.RejectReason;
			if(results.SignedbyEmployee != null && results.SignedbyEmployee.ToString() != "")
				aMWContractHeaderToBeUpdated.SignedbyEmployee =results.SignedbyEmployee.ToString();// results.SignedbyEmployee;
			if(results.SignedDate != null && results.SignedDate.ToString() != "")
				aMWContractHeaderToBeUpdated.SignedDate =results.SignedDate;// results.SignedDate;
			if(results.ContractTemplate != null && results.ContractTemplate.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractTemplate =long.Parse(results.ContractTemplate.ToString());// results.ContractTemplate;
			if(results.ManagerComments != null && results.ManagerComments.ToString() != "")
				aMWContractHeaderToBeUpdated.ManagerComments =results.ManagerComments.ToString();// results.ManagerComments;
			if(results.SignedPersonNationality != null && results.SignedPersonNationality.ToString() != "")
				aMWContractHeaderToBeUpdated.SignedPersonNationality =results.SignedPersonNationality.ToString();// results.SignedPersonNationality;
			if(results.SignedPersonIDNumber != null && results.SignedPersonIDNumber.ToString() != "")
				aMWContractHeaderToBeUpdated.SignedPersonIDNumber =results.SignedPersonIDNumber.ToString();// results.SignedPersonIDNumber;
			if(results.SignedPersonPosition != null && results.SignedPersonPosition.ToString() != "")
				aMWContractHeaderToBeUpdated.SignedPersonPosition =results.SignedPersonPosition.ToString();// results.SignedPersonPosition;
			if(results.Quotation != null && results.Quotation.ToString() != "")
				aMWContractHeaderToBeUpdated.Quotation =long.Parse(results.Quotation.ToString());// results.Quotation;
			if(results.ContractYears != null && results.ContractYears.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractYears =int.Parse(results.ContractYears.ToString());// results.ContractYears;
			if(results.ChargesSetupRecId != null && results.ChargesSetupRecId.ToString() != "")
				aMWContractHeaderToBeUpdated.ChargesSetupRecId =long.Parse(results.ChargesSetupRecId.ToString());// results.ChargesSetupRecId;
			if(results.RevisionNumber != null && results.RevisionNumber.ToString() != "")
				aMWContractHeaderToBeUpdated.RevisionNumber =results.RevisionNumber.ToString();// results.RevisionNumber;
			if(results.Gender != null && results.Gender.ToString() != "")
				aMWContractHeaderToBeUpdated.Gender =int.Parse(results.Gender.ToString());// results.Gender;
			if(results.ChequeAccuralDate != null && results.ChequeAccuralDate.ToString() != "")
				aMWContractHeaderToBeUpdated.ChequeAccuralDate =results.ChequeAccuralDate;// results.ChequeAccuralDate;
			if(results.Article != null && results.Article.ToString() != "")
				aMWContractHeaderToBeUpdated.Article =results.Article.ToString();// results.Article;
			if(results.TemplateID != null && results.TemplateID.ToString() != "")
				aMWContractHeaderToBeUpdated.TemplateID =long.Parse(results.TemplateID.ToString());// results.TemplateID;
			if(results.PromissoryAmount != null && results.PromissoryAmount.ToString() != "")
				aMWContractHeaderToBeUpdated.PromissoryAmount =Decimal.Parse(results.PromissoryAmount.ToString());// results.PromissoryAmount;
			if(results.ContractContent != null && results.ContractContent.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractContent =results.ContractContent.ToString();// results.ContractContent;
			if(results.ContractCreationType != null && results.ContractCreationType.ToString() != "")
				aMWContractHeaderToBeUpdated.ContractCreationType =int.Parse(results.ContractCreationType.ToString());// results.ContractCreationType;
			if(results.OrignalContractNumber != null && results.OrignalContractNumber.ToString() != "")
				aMWContractHeaderToBeUpdated.OrignalContractNumber =results.OrignalContractNumber.ToString();// results.OrignalContractNumber;
			if(results.UsedDays != null && results.UsedDays.ToString() != "")
				aMWContractHeaderToBeUpdated.UsedDays =int.Parse(results.UsedDays.ToString());// results.UsedDays;
			if(results.TotalDays != null && results.TotalDays.ToString() != "")
				aMWContractHeaderToBeUpdated.TotalDays =int.Parse(results.TotalDays.ToString());// results.TotalDays;
			if(results.RemainingDays != null && results.RemainingDays.ToString() != "")
				aMWContractHeaderToBeUpdated.RemainingDays =int.Parse(results.RemainingDays.ToString());// results.RemainingDays;
			if(results.PreviousContractNumber != null && results.PreviousContractNumber.ToString() != "")
				aMWContractHeaderToBeUpdated.PreviousContractNumber =results.PreviousContractNumber.ToString();// results.PreviousContractNumber;
			if(results.isExtended != null && results.isExtended.ToString() != "")
				aMWContractHeaderToBeUpdated.isExtended =bool.Parse(results.isExtended.ToString());// results.isExtended;
			if(results.ExtendedContractNumber != null && results.ExtendedContractNumber.ToString() != "")
				aMWContractHeaderToBeUpdated.ExtendedContractNumber =results.ExtendedContractNumber.ToString();// results.ExtendedContractNumber;
			if(results.CRM_GUID != null && results.CRM_GUID.ToString() != "")
				aMWContractHeaderToBeUpdated.CRM_GUID =results.CRM_GUID.ToString();// results.CRM_GUID;
			if(results.Channel != null && results.Channel.ToString() != "")
				aMWContractHeaderToBeUpdated.Channel =int.Parse(results.Channel.ToString());// results.Channel;
			if(results.PenalityPerDay != null && results.PenalityPerDay.ToString() != "")
				aMWContractHeaderToBeUpdated.PenalityPerDay =Decimal.Parse(results.PenalityPerDay.ToString());// results.PenalityPerDay;
			if(results.TotalPenalityAmount != null && results.TotalPenalityAmount.ToString() != "")
				aMWContractHeaderToBeUpdated.TotalPenalityAmount =Decimal.Parse(results.TotalPenalityAmount.ToString());// results.TotalPenalityAmount;

		return	Update(aMWContractHeaderToBeUpdated);
        }



		 //ContractNumber = sample.ContractNumber,
//CustomerID = sample.CustomerID,
//ContractDate = sample.ContractDate,
//ContractSignedBy = sample.ContractSignedBy,
//ValidFrom = sample.ValidFrom,
//ValidTo = sample.ValidTo,
//ServiceType = sample.ServiceType,
//ContractValue = sample.ContractValue,
//ContractStatus = sample.ContractStatus,
//ProcessStatus = sample.ProcessStatus,
//CreatedDatetime = sample.CreatedDatetime,
//CreatedBy = sample.CreatedBy,
//ModifiedDatetime = sample.ModifiedDatetime,
//ModifiedBy = sample.ModifiedBy,
//DataAreaId = sample.DataAreaId,
//RecId = sample.RecId,
//RecVersion = sample.RecVersion,
//TotalManPower = sample.TotalManPower,
//AdvancePayment = sample.AdvancePayment,
//CountryRegion = sample.CountryRegion,
//City = sample.City,
//DirectContract = sample.DirectContract,
//VirtualAccountId = sample.VirtualAccountId,
//InternalContractNumber = sample.InternalContractNumber,
//Remarks = sample.Remarks,
//ExternalID = sample.ExternalID,
//ExternalDisplayCode = sample.ExternalDisplayCode,
//ExternalDisplayName = sample.ExternalDisplayName,
//ExternalFamousName = sample.ExternalFamousName,
//PayGroupID = sample.PayGroupID,
//FeeSetup = sample.FeeSetup,
//ContractType = sample.ContractType,
//ContractSalesTotal = sample.ContractSalesTotal,
//InvoiceRuleID = sample.InvoiceRuleID,
//Approvedby = sample.Approvedby,
//ApprovedDate = sample.ApprovedDate,
//FinanceCustomerID = sample.FinanceCustomerID,
//RejectReason = sample.RejectReason,
//SignedbyEmployee = sample.SignedbyEmployee,
//SignedDate = sample.SignedDate,
//ContractTemplate = sample.ContractTemplate,
//ManagerComments = sample.ManagerComments,
//SignedPersonNationality = sample.SignedPersonNationality,
//SignedPersonIDNumber = sample.SignedPersonIDNumber,
//SignedPersonPosition = sample.SignedPersonPosition,
//Quotation = sample.Quotation,
//ContractYears = sample.ContractYears,
//ChargesSetupRecId = sample.ChargesSetupRecId,
//RevisionNumber = sample.RevisionNumber,
//Gender = sample.Gender,
//ChequeAccuralDate = sample.ChequeAccuralDate,
//Article = sample.Article,
//TemplateID = sample.TemplateID,
//PromissoryAmount = sample.PromissoryAmount,
//ContractContent = sample.ContractContent,
//ContractCreationType = sample.ContractCreationType,
//OrignalContractNumber = sample.OrignalContractNumber,
//UsedDays = sample.UsedDays,
//TotalDays = sample.TotalDays,
//RemainingDays = sample.RemainingDays,
//PreviousContractNumber = sample.PreviousContractNumber,
//isExtended = sample.isExtended,
//ExtendedContractNumber = sample.ExtendedContractNumber,
//CRM_GUID = sample.CRM_GUID,
//Channel = sample.Channel,
//PenalityPerDay = sample.PenalityPerDay,
//TotalPenalityAmount = sample.TotalPenalityAmount,
  //<label editable-text="sample.ContractNumber"  e-name="ContractNumber" e-form="rowform">
                                           //     {{sample.ContractNumber}}
                                         //   </label>
										  //<label editable-text="sample.CustomerID"  e-name="CustomerID" e-form="rowform">
                                           //     {{sample.CustomerID}}
                                         //   </label>
										  //<label editable-text="sample.ContractDate"  e-name="ContractDate" e-form="rowform">
                                           //     {{sample.ContractDate}}
                                         //   </label>
										  //<label editable-text="sample.ContractSignedBy"  e-name="ContractSignedBy" e-form="rowform">
                                           //     {{sample.ContractSignedBy}}
                                         //   </label>
										  //<label editable-text="sample.ValidFrom"  e-name="ValidFrom" e-form="rowform">
                                           //     {{sample.ValidFrom}}
                                         //   </label>
										  //<label editable-text="sample.ValidTo"  e-name="ValidTo" e-form="rowform">
                                           //     {{sample.ValidTo}}
                                         //   </label>
										  //<label editable-text="sample.ServiceType"  e-name="ServiceType" e-form="rowform">
                                           //     {{sample.ServiceType}}
                                         //   </label>
										  //<label editable-text="sample.ContractValue"  e-name="ContractValue" e-form="rowform">
                                           //     {{sample.ContractValue}}
                                         //   </label>
										  //<label editable-text="sample.ContractStatus"  e-name="ContractStatus" e-form="rowform">
                                           //     {{sample.ContractStatus}}
                                         //   </label>
										  //<label editable-text="sample.ProcessStatus"  e-name="ProcessStatus" e-form="rowform">
                                           //     {{sample.ProcessStatus}}
                                         //   </label>
										  //<label editable-text="sample.CreatedDatetime"  e-name="CreatedDatetime" e-form="rowform">
                                           //     {{sample.CreatedDatetime}}
                                         //   </label>
										  //<label editable-text="sample.CreatedBy"  e-name="CreatedBy" e-form="rowform">
                                           //     {{sample.CreatedBy}}
                                         //   </label>
										  //<label editable-text="sample.ModifiedDatetime"  e-name="ModifiedDatetime" e-form="rowform">
                                           //     {{sample.ModifiedDatetime}}
                                         //   </label>
										  //<label editable-text="sample.ModifiedBy"  e-name="ModifiedBy" e-form="rowform">
                                           //     {{sample.ModifiedBy}}
                                         //   </label>
										  //<label editable-text="sample.DataAreaId"  e-name="DataAreaId" e-form="rowform">
                                           //     {{sample.DataAreaId}}
                                         //   </label>
										  //<label editable-text="sample.RecId"  e-name="RecId" e-form="rowform">
                                           //     {{sample.RecId}}
                                         //   </label>
										  //<label editable-text="sample.RecVersion"  e-name="RecVersion" e-form="rowform">
                                           //     {{sample.RecVersion}}
                                         //   </label>
										  //<label editable-text="sample.TotalManPower"  e-name="TotalManPower" e-form="rowform">
                                           //     {{sample.TotalManPower}}
                                         //   </label>
										  //<label editable-text="sample.AdvancePayment"  e-name="AdvancePayment" e-form="rowform">
                                           //     {{sample.AdvancePayment}}
                                         //   </label>
										  //<label editable-text="sample.CountryRegion"  e-name="CountryRegion" e-form="rowform">
                                           //     {{sample.CountryRegion}}
                                         //   </label>
										  //<label editable-text="sample.City"  e-name="City" e-form="rowform">
                                           //     {{sample.City}}
                                         //   </label>
										  //<label editable-text="sample.DirectContract"  e-name="DirectContract" e-form="rowform">
                                           //     {{sample.DirectContract}}
                                         //   </label>
										  //<label editable-text="sample.VirtualAccountId"  e-name="VirtualAccountId" e-form="rowform">
                                           //     {{sample.VirtualAccountId}}
                                         //   </label>
										  //<label editable-text="sample.InternalContractNumber"  e-name="InternalContractNumber" e-form="rowform">
                                           //     {{sample.InternalContractNumber}}
                                         //   </label>
										  //<label editable-text="sample.Remarks"  e-name="Remarks" e-form="rowform">
                                           //     {{sample.Remarks}}
                                         //   </label>
										  //<label editable-text="sample.ExternalID"  e-name="ExternalID" e-form="rowform">
                                           //     {{sample.ExternalID}}
                                         //   </label>
										  //<label editable-text="sample.ExternalDisplayCode"  e-name="ExternalDisplayCode" e-form="rowform">
                                           //     {{sample.ExternalDisplayCode}}
                                         //   </label>
										  //<label editable-text="sample.ExternalDisplayName"  e-name="ExternalDisplayName" e-form="rowform">
                                           //     {{sample.ExternalDisplayName}}
                                         //   </label>
										  //<label editable-text="sample.ExternalFamousName"  e-name="ExternalFamousName" e-form="rowform">
                                           //     {{sample.ExternalFamousName}}
                                         //   </label>
										  //<label editable-text="sample.PayGroupID"  e-name="PayGroupID" e-form="rowform">
                                           //     {{sample.PayGroupID}}
                                         //   </label>
										  //<label editable-text="sample.FeeSetup"  e-name="FeeSetup" e-form="rowform">
                                           //     {{sample.FeeSetup}}
                                         //   </label>
										  //<label editable-text="sample.ContractType"  e-name="ContractType" e-form="rowform">
                                           //     {{sample.ContractType}}
                                         //   </label>
										  //<label editable-text="sample.ContractSalesTotal"  e-name="ContractSalesTotal" e-form="rowform">
                                           //     {{sample.ContractSalesTotal}}
                                         //   </label>
										  //<label editable-text="sample.InvoiceRuleID"  e-name="InvoiceRuleID" e-form="rowform">
                                           //     {{sample.InvoiceRuleID}}
                                         //   </label>
										  //<label editable-text="sample.Approvedby"  e-name="Approvedby" e-form="rowform">
                                           //     {{sample.Approvedby}}
                                         //   </label>
										  //<label editable-text="sample.ApprovedDate"  e-name="ApprovedDate" e-form="rowform">
                                           //     {{sample.ApprovedDate}}
                                         //   </label>
										  //<label editable-text="sample.FinanceCustomerID"  e-name="FinanceCustomerID" e-form="rowform">
                                           //     {{sample.FinanceCustomerID}}
                                         //   </label>
										  //<label editable-text="sample.RejectReason"  e-name="RejectReason" e-form="rowform">
                                           //     {{sample.RejectReason}}
                                         //   </label>
										  //<label editable-text="sample.SignedbyEmployee"  e-name="SignedbyEmployee" e-form="rowform">
                                           //     {{sample.SignedbyEmployee}}
                                         //   </label>
										  //<label editable-text="sample.SignedDate"  e-name="SignedDate" e-form="rowform">
                                           //     {{sample.SignedDate}}
                                         //   </label>
										  //<label editable-text="sample.ContractTemplate"  e-name="ContractTemplate" e-form="rowform">
                                           //     {{sample.ContractTemplate}}
                                         //   </label>
										  //<label editable-text="sample.ManagerComments"  e-name="ManagerComments" e-form="rowform">
                                           //     {{sample.ManagerComments}}
                                         //   </label>
										  //<label editable-text="sample.SignedPersonNationality"  e-name="SignedPersonNationality" e-form="rowform">
                                           //     {{sample.SignedPersonNationality}}
                                         //   </label>
										  //<label editable-text="sample.SignedPersonIDNumber"  e-name="SignedPersonIDNumber" e-form="rowform">
                                           //     {{sample.SignedPersonIDNumber}}
                                         //   </label>
										  //<label editable-text="sample.SignedPersonPosition"  e-name="SignedPersonPosition" e-form="rowform">
                                           //     {{sample.SignedPersonPosition}}
                                         //   </label>
										  //<label editable-text="sample.Quotation"  e-name="Quotation" e-form="rowform">
                                           //     {{sample.Quotation}}
                                         //   </label>
										  //<label editable-text="sample.ContractYears"  e-name="ContractYears" e-form="rowform">
                                           //     {{sample.ContractYears}}
                                         //   </label>
										  //<label editable-text="sample.ChargesSetupRecId"  e-name="ChargesSetupRecId" e-form="rowform">
                                           //     {{sample.ChargesSetupRecId}}
                                         //   </label>
										  //<label editable-text="sample.RevisionNumber"  e-name="RevisionNumber" e-form="rowform">
                                           //     {{sample.RevisionNumber}}
                                         //   </label>
										  //<label editable-text="sample.Gender"  e-name="Gender" e-form="rowform">
                                           //     {{sample.Gender}}
                                         //   </label>
										  //<label editable-text="sample.ChequeAccuralDate"  e-name="ChequeAccuralDate" e-form="rowform">
                                           //     {{sample.ChequeAccuralDate}}
                                         //   </label>
										  //<label editable-text="sample.Article"  e-name="Article" e-form="rowform">
                                           //     {{sample.Article}}
                                         //   </label>
										  //<label editable-text="sample.TemplateID"  e-name="TemplateID" e-form="rowform">
                                           //     {{sample.TemplateID}}
                                         //   </label>
										  //<label editable-text="sample.PromissoryAmount"  e-name="PromissoryAmount" e-form="rowform">
                                           //     {{sample.PromissoryAmount}}
                                         //   </label>
										  //<label editable-text="sample.ContractContent"  e-name="ContractContent" e-form="rowform">
                                           //     {{sample.ContractContent}}
                                         //   </label>
										  //<label editable-text="sample.ContractCreationType"  e-name="ContractCreationType" e-form="rowform">
                                           //     {{sample.ContractCreationType}}
                                         //   </label>
										  //<label editable-text="sample.OrignalContractNumber"  e-name="OrignalContractNumber" e-form="rowform">
                                           //     {{sample.OrignalContractNumber}}
                                         //   </label>
										  //<label editable-text="sample.UsedDays"  e-name="UsedDays" e-form="rowform">
                                           //     {{sample.UsedDays}}
                                         //   </label>
										  //<label editable-text="sample.TotalDays"  e-name="TotalDays" e-form="rowform">
                                           //     {{sample.TotalDays}}
                                         //   </label>
										  //<label editable-text="sample.RemainingDays"  e-name="RemainingDays" e-form="rowform">
                                           //     {{sample.RemainingDays}}
                                         //   </label>
										  //<label editable-text="sample.PreviousContractNumber"  e-name="PreviousContractNumber" e-form="rowform">
                                           //     {{sample.PreviousContractNumber}}
                                         //   </label>
										  //<label editable-text="sample.isExtended"  e-name="isExtended" e-form="rowform">
                                           //     {{sample.isExtended}}
                                         //   </label>
										  //<label editable-text="sample.ExtendedContractNumber"  e-name="ExtendedContractNumber" e-form="rowform">
                                           //     {{sample.ExtendedContractNumber}}
                                         //   </label>
										  //<label editable-text="sample.CRM_GUID"  e-name="CRM_GUID" e-form="rowform">
                                           //     {{sample.CRM_GUID}}
                                         //   </label>
										  //<label editable-text="sample.Channel"  e-name="Channel" e-form="rowform">
                                           //     {{sample.Channel}}
                                         //   </label>
										  //<label editable-text="sample.PenalityPerDay"  e-name="PenalityPerDay" e-form="rowform">
                                           //     {{sample.PenalityPerDay}}
                                         //   </label>
										  //<label editable-text="sample.TotalPenalityAmount"  e-name="TotalPenalityAmount" e-form="rowform">
                                           //     {{sample.TotalPenalityAmount}}
                                         //   </label>
										 
        public void Delete(Arco.Model.Entity.AMWContractHeader aMWContractHeaderToBeDeleted)
        {
			int numberOfAffectedRows = 0;
            //Validate Input
            if (aMWContractHeaderToBeDeleted == null)
                throw (new ArgumentNullException("aMWContractHeaderToBeDeleted"));

            // Validate Primary key value
            if (aMWContractHeaderToBeDeleted.ContractNumber.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("ContractNumber");

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(aMWContractHeaderToBeDeleted, PersistenceType.Delete);
				OnDeleting(aMWContractHeaderToBeDeleted);

			
				_context.Remove(aMWContractHeaderToBeDeleted);
            
				numberOfAffectedRows = _context.UnitOfWork.Commit();
				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException(string.Format("AMWContractHeader (ContractNumber={0}) has not been deleted!", aMWContractHeaderToBeDeleted.ContractNumber));
            
				//Apply business workflow
				OnDeleted(aMWContractHeaderToBeDeleted);
				OnSaved(aMWContractHeaderToBeDeleted, PersistenceType.Delete);

			//	scope.Complete();
			//}

        }

        public void Delete(List<string> contractNumbersToDelete)
        {
            //Validate Input
            foreach (string contractNumber in contractNumbersToDelete)
                if (contractNumber.IsInvalidKey())
                    BizHelper.ThrowErrorForInvalidDataKey("ContractNumber");

            foreach (string contractNumber in contractNumbersToDelete)
            {
				Arco.Model.Entity.AMWContractHeader aMWContractHeaderToBeDeleted = new Arco.Model.Entity.AMWContractHeader { ContractNumber = contractNumber, DataAreaId = CurrentSession.DataAreaId };

				Delete(aMWContractHeaderToBeDeleted);
            }

        }

 
		//Delete by RecId
		public void Delete(List<long> RecIdsToDelete)
        {
            //Validate Input
            foreach (long RecId in RecIdsToDelete)
                if (RecId.IsInvalidKey())
                    BizHelper.ThrowErrorForInvalidDataKey("RecId");

            foreach (long RecId in RecIdsToDelete)
				Delete(GetByRecId(RecId));
        }

        #endregion
	

       
        public Arco.Model.Entity.AMWContractHeader Initialize()
        {
			Arco.Model.Entity.AMWContractHeader aMWContractHeader = new Arco.Model.Entity.AMWContractHeader();

			return aMWContractHeader;
		}
	}
}
