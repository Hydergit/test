using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using Newtonsoft.Json;
using Arco.Core.AppBase;
//using Arco.Model.Interface.Repository.Single;
using Arco.Model.Entity;
using Arco.Framework.Utility;
//using Arco.Model.Interface.Business;
using Arco.Core.AppInterface;
using Arco.Core.Infrastructure;
using Arco.Framework.Query;
namespace Arco.Business.Record
{



 public partial class AMWCustomerRepository : EFRepositoryBase
    {
	
		    public AMWCustomerRepository()
            : base("arco")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<AMWCustomerRepository>(null);
        }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
		modelBuilder.Configurations.Add(new AMWCustomerConfiguration());
          
			}

	}


    public partial class AMWCustomerBLL : BLLBase
    {
		
        public AMWCustomerBLL() 
			: base(new AMWCustomerRepository(), false) {
			_lAMWCustomer =new Arco.Model.Entity.AMWCustomer();
			 }


			 
        public AMWCustomerBLL(IRepository _IRepository)
			 : base(_IRepository, false) {
			_lAMWCustomer =new Arco.Model.Entity.AMWCustomer();
			 }



			//   public AMWCustomerBLL(IRepository context, Func<AMWCustomer, bool> predicate, bool disposeContext = false) 
			//: base(context, disposeContext) {
			
              // _lAMWCustomer= GetAll().Where(predicate:predicate).FirstOrDefault();
        
			 //}
        		
				public Arco.Model.Entity.AMWCustomer _lAMWCustomer    { get; set; }

				

		#region Property

			
		public string CustomerID {get{return this._lAMWCustomer.CustomerID;} set{  this._lAMWCustomer.CustomerID = value; }}

		
			
		public string FirstName {get{return this._lAMWCustomer.FirstName;} set{  this._lAMWCustomer.FirstName = value; }}

		
			
		public string MiddleName {get{return this._lAMWCustomer.MiddleName;} set{  this._lAMWCustomer.MiddleName = value; }}

		
			
		public string LastName {get{return this._lAMWCustomer.LastName;} set{  this._lAMWCustomer.LastName = value; }}

		
			
		public string FamilyName {get{return this._lAMWCustomer.FamilyName;} set{  this._lAMWCustomer.FamilyName = value; }}

		
			
		public int? Gender {get{return this._lAMWCustomer.Gender;} set{  this._lAMWCustomer.Gender = value; }}

		
			
		public string Nationality {get{return this._lAMWCustomer.Nationality;} set{  this._lAMWCustomer.Nationality = value; }}

		
			
		public DateTime? DOBHijri {get{return this._lAMWCustomer.DOBHijri;} set{  this._lAMWCustomer.DOBHijri = value; }}

		
			
		public DateTime? DOBGeorgian {get{return this._lAMWCustomer.DOBGeorgian;} set{  this._lAMWCustomer.DOBGeorgian = value; }}

		
			
		public string IDNumber {get{return this._lAMWCustomer.IDNumber;} set{  this._lAMWCustomer.IDNumber = value; }}

		
			
		public string MobileNumber {get{return this._lAMWCustomer.MobileNumber;} set{  this._lAMWCustomer.MobileNumber = value; }}

		
			
		public string PhoneNumber {get{return this._lAMWCustomer.PhoneNumber;} set{  this._lAMWCustomer.PhoneNumber = value; }}

		
			
		public string Street {get{return this._lAMWCustomer.Street;} set{  this._lAMWCustomer.Street = value; }}

		
			
		public decimal? POBOXNumber {get{return this._lAMWCustomer.POBOXNumber;} set{  this._lAMWCustomer.POBOXNumber = value; }}

		
			
		public string CountryRegion {get{return this._lAMWCustomer.CountryRegion;} set{  this._lAMWCustomer.CountryRegion = value; }}

		
			
		public string City {get{return this._lAMWCustomer.City;} set{  this._lAMWCustomer.City = value; }}

		
			
		public decimal? PostalCode {get{return this._lAMWCustomer.PostalCode;} set{  this._lAMWCustomer.PostalCode = value; }}

		
			
		public string Email {get{return this._lAMWCustomer.Email;} set{  this._lAMWCustomer.Email = value; }}

		
			
		public int? WorkingSector {get{return this._lAMWCustomer.WorkingSector;} set{  this._lAMWCustomer.WorkingSector = value; }}

		
			
		public string Occupation {get{return this._lAMWCustomer.Occupation;} set{  this._lAMWCustomer.Occupation = value; }}

		
			
		public decimal? MonthlySalary {get{return this._lAMWCustomer.MonthlySalary;} set{  this._lAMWCustomer.MonthlySalary = value; }}

		
			
		public decimal? OtherIncome {get{return this._lAMWCustomer.OtherIncome;} set{  this._lAMWCustomer.OtherIncome = value; }}

		
			
		public int? KnowVia {get{return this._lAMWCustomer.KnowVia;} set{  this._lAMWCustomer.KnowVia = value; }}

		
			
		public string Notes {get{return this._lAMWCustomer.Notes;} set{  this._lAMWCustomer.Notes = value; }}

		
			
		public string CompanyName {get{return this._lAMWCustomer.CompanyName;} set{  this._lAMWCustomer.CompanyName = value; }}

		
			
		public string LegalName {get{return this._lAMWCustomer.LegalName;} set{  this._lAMWCustomer.LegalName = value; }}

		
			
		public string RegistrationNo {get{return this._lAMWCustomer.RegistrationNo;} set{  this._lAMWCustomer.RegistrationNo = value; }}

		
			
		public string COCNo {get{return this._lAMWCustomer.COCNo;} set{  this._lAMWCustomer.COCNo = value; }}

		
			
		public string CompanyEmail {get{return this._lAMWCustomer.CompanyEmail;} set{  this._lAMWCustomer.CompanyEmail = value; }}

		
			
		public string CompanyWebsite {get{return this._lAMWCustomer.CompanyWebsite;} set{  this._lAMWCustomer.CompanyWebsite = value; }}

		
			
		public string NatureofBusiness {get{return this._lAMWCustomer.NatureofBusiness;} set{  this._lAMWCustomer.NatureofBusiness = value; }}

		
			
		public int? Sector {get{return this._lAMWCustomer.Sector;} set{  this._lAMWCustomer.Sector = value; }}

		
			
		public int? TotalEmployees {get{return this._lAMWCustomer.TotalEmployees;} set{  this._lAMWCustomer.TotalEmployees = value; }}

		
			
		public int? TotalSaudiEmployees {get{return this._lAMWCustomer.TotalSaudiEmployees;} set{  this._lAMWCustomer.TotalSaudiEmployees = value; }}

		
			
		public string CompanyNoLabourOffice {get{return this._lAMWCustomer.CompanyNoLabourOffice;} set{  this._lAMWCustomer.CompanyNoLabourOffice = value; }}

		
			
		public string CompanyActivity {get{return this._lAMWCustomer.CompanyActivity;} set{  this._lAMWCustomer.CompanyActivity = value; }}

		
			
		public string ContactPerson {get{return this._lAMWCustomer.ContactPerson;} set{  this._lAMWCustomer.ContactPerson = value; }}

		
			
		public string Extension {get{return this._lAMWCustomer.Extension;} set{  this._lAMWCustomer.Extension = value; }}

		
			
		public int? CustomerType {get{return this._lAMWCustomer.CustomerType;} set{  this._lAMWCustomer.CustomerType = value; }}

		
			
		public string ContactPersonEmail {get{return this._lAMWCustomer.ContactPersonEmail;} set{  this._lAMWCustomer.ContactPersonEmail = value; }}

		
			
		public int? CustomerStatus {get{return this._lAMWCustomer.CustomerStatus;} set{  this._lAMWCustomer.CustomerStatus = value; }}

		
			
		public DateTime? CreatedDatetime {get{return this._lAMWCustomer.CreatedDatetime;} set{  this._lAMWCustomer.CreatedDatetime = value; }}

		
			
		public string CreatedBy {get{return this._lAMWCustomer.CreatedBy;} set{  this._lAMWCustomer.CreatedBy = value; }}

		
			
		public DateTime? ModifiedDatetime {get{return this._lAMWCustomer.ModifiedDatetime;} set{  this._lAMWCustomer.ModifiedDatetime = value; }}

		
			
		public string ModifiedBy {get{return this._lAMWCustomer.ModifiedBy;} set{  this._lAMWCustomer.ModifiedBy = value; }}

		
			
		public string DataAreaId {get{return this._lAMWCustomer.DataAreaId;} set{  this._lAMWCustomer.DataAreaId = value; }}

		
			
		public long RecId {get{return this._lAMWCustomer.RecId;} set{  this._lAMWCustomer.RecId = value; }}

		
			
		public byte[] RecVersion {get{return this._lAMWCustomer.RecVersion;} set{  this._lAMWCustomer.RecVersion = value; }}

		
			
		public string AccountManager {get{return this._lAMWCustomer.AccountManager;} set{  this._lAMWCustomer.AccountManager = value; }}

		
			
		public string Supervisor {get{return this._lAMWCustomer.Supervisor;} set{  this._lAMWCustomer.Supervisor = value; }}

		
			
		public string SalesPerson {get{return this._lAMWCustomer.SalesPerson;} set{  this._lAMWCustomer.SalesPerson = value; }}

		
			
		public int? NoofFamilyMembers {get{return this._lAMWCustomer.NoofFamilyMembers;} set{  this._lAMWCustomer.NoofFamilyMembers = value; }}

		
			
		public int? NoofWifes {get{return this._lAMWCustomer.NoofWifes;} set{  this._lAMWCustomer.NoofWifes = value; }}

		
			
		public bool? OldAgePerson {get{return this._lAMWCustomer.OldAgePerson;} set{  this._lAMWCustomer.OldAgePerson = value; }}

		
			
		public int? NoofOldAgePerson {get{return this._lAMWCustomer.NoofOldAgePerson;} set{  this._lAMWCustomer.NoofOldAgePerson = value; }}

		
			
		public string OldAgePersonNotes {get{return this._lAMWCustomer.OldAgePersonNotes;} set{  this._lAMWCustomer.OldAgePersonNotes = value; }}

		
			
		public bool? SpecialDisabled {get{return this._lAMWCustomer.SpecialDisabled;} set{  this._lAMWCustomer.SpecialDisabled = value; }}

		
			
		public int? NoofSpecialDisabled {get{return this._lAMWCustomer.NoofSpecialDisabled;} set{  this._lAMWCustomer.NoofSpecialDisabled = value; }}

		
			
		public string SpecialDisabledNotes {get{return this._lAMWCustomer.SpecialDisabledNotes;} set{  this._lAMWCustomer.SpecialDisabledNotes = value; }}

		
			
		public int? SalaryRange {get{return this._lAMWCustomer.SalaryRange;} set{  this._lAMWCustomer.SalaryRange = value; }}

		
			
		public string OccupationDesc {get{return this._lAMWCustomer.OccupationDesc;} set{  this._lAMWCustomer.OccupationDesc = value; }}

		
			
		public int? CreatedSource {get{return this._lAMWCustomer.CreatedSource;} set{  this._lAMWCustomer.CreatedSource = value; }}

		
			
		public string ExternalID {get{return this._lAMWCustomer.ExternalID;} set{  this._lAMWCustomer.ExternalID = value; }}

		
			
		public long? EnquiryRefId {get{return this._lAMWCustomer.EnquiryRefId;} set{  this._lAMWCustomer.EnquiryRefId = value; }}

		
			
		public string FaxNumber {get{return this._lAMWCustomer.FaxNumber;} set{  this._lAMWCustomer.FaxNumber = value; }}

		
			
		public string CustomerNameEN {get{return this._lAMWCustomer.CustomerNameEN;} set{  this._lAMWCustomer.CustomerNameEN = value; }}

		
			
		public string WorkPlace {get{return this._lAMWCustomer.WorkPlace;} set{  this._lAMWCustomer.WorkPlace = value; }}

		
			
		public string WorkAddress {get{return this._lAMWCustomer.WorkAddress;} set{  this._lAMWCustomer.WorkAddress = value; }}

		
			
		public string WorkEmail {get{return this._lAMWCustomer.WorkEmail;} set{  this._lAMWCustomer.WorkEmail = value; }}

		
			
		public string WorkTel {get{return this._lAMWCustomer.WorkTel;} set{  this._lAMWCustomer.WorkTel = value; }}

		
			
		public string WorkFax {get{return this._lAMWCustomer.WorkFax;} set{  this._lAMWCustomer.WorkFax = value; }}

		
			
		public decimal? WorkPoBox {get{return this._lAMWCustomer.WorkPoBox;} set{  this._lAMWCustomer.WorkPoBox = value; }}

		
			
		public decimal? MailAddress {get{return this._lAMWCustomer.MailAddress;} set{  this._lAMWCustomer.MailAddress = value; }}

		
			
		public decimal? WorkMailAddress {get{return this._lAMWCustomer.WorkMailAddress;} set{  this._lAMWCustomer.WorkMailAddress = value; }}

		
			
		public decimal? WorkMailBox {get{return this._lAMWCustomer.WorkMailBox;} set{  this._lAMWCustomer.WorkMailBox = value; }}

		
			
		public decimal? MailBox {get{return this._lAMWCustomer.MailBox;} set{  this._lAMWCustomer.MailBox = value; }}

		
			
		public string WorkMobile {get{return this._lAMWCustomer.WorkMobile;} set{  this._lAMWCustomer.WorkMobile = value; }}

		
			
		public string CRM_GUID {get{return this._lAMWCustomer.CRM_GUID;} set{  this._lAMWCustomer.CRM_GUID = value; }}

		
			
		public int? Channel {get{return this._lAMWCustomer.Channel;} set{  this._lAMWCustomer.Channel = value; }}

		
			
		public string FemaleContactPersonPhoneNumber {get{return this._lAMWCustomer.FemaleContactPersonPhoneNumber;} set{  this._lAMWCustomer.FemaleContactPersonPhoneNumber = value; }}

		
			
		public int? VerificationStatus {get{return this._lAMWCustomer.VerificationStatus;} set{  this._lAMWCustomer.VerificationStatus = value; }}

		
			
		public string VerifiedBy {get{return this._lAMWCustomer.VerifiedBy;} set{  this._lAMWCustomer.VerifiedBy = value; }}

		
			
		public DateTime? VerifiedDate {get{return this._lAMWCustomer.VerifiedDate;} set{  this._lAMWCustomer.VerifiedDate = value; }}

		
			
		public string VirtualAccountId {get{return this._lAMWCustomer.VirtualAccountId;} set{  this._lAMWCustomer.VirtualAccountId = value; }}

		
			
		public string AffiliateCode {get{return this._lAMWCustomer.AffiliateCode;} set{  this._lAMWCustomer.AffiliateCode = value; }}

		
			
		public string IBANNumber {get{return this._lAMWCustomer.IBANNumber;} set{  this._lAMWCustomer.IBANNumber = value; }}

		
			
		public string PromoCode {get{return this._lAMWCustomer.PromoCode;} set{  this._lAMWCustomer.PromoCode = value; }}

		
			
		public DateTime? WorkDateOfJoining {get{return this._lAMWCustomer.WorkDateOfJoining;} set{  this._lAMWCustomer.WorkDateOfJoining = value; }}

		
			
		public DateTime? IqamaExpiryDate {get{return this._lAMWCustomer.IqamaExpiryDate;} set{  this._lAMWCustomer.IqamaExpiryDate = value; }}

		
				#endregion
				
		
	   #region GET

	    public IQueryable<Arco.Model.Entity.AMWCustomer> GetAsQueryAble()
        {
            return _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>();
        }

	   public Arco.Model.Entity.AMWCustomer GetByCustomerID(string customerID)
       {
            //Validate Input
            if (customerID.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("customerID");
            return _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>().FirstOrDefault(aMWCustomer => aMWCustomer.CustomerID == customerID);
       }

	    
        public Arco.Model.Entity.AMWCustomer GetByRecId(long RecId)
        {
            //Validate Input
            if (RecId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("RecId");
            return _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>().FirstOrDefault(aMWCustomer => aMWCustomer.RecId == RecId);
        }
		
		public List<Arco.Model.Entity.AMWCustomer> GetAll(string orderBy = default(string))
        {
			if (string.IsNullOrEmpty(orderBy))
		            orderBy = "CustomerID";
            return _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>().DynamicOrderBy(orderBy).ToList();
        }

        public List<Arco.Model.Entity.AMWCustomer> GetAllPaged(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "CustomerID";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>()
                    .DynamicOrderBy(orderBy)
                    .Skip(startRowIndex)
                    .Take(maximumRows)
                    .ToList();
        }



		 public QueryResult GetPaged(QueryCriteria _QueryCriteria)
        {

            int PageNo = 1;
            int PageSize = 20;

            if (_QueryCriteria!=null && _QueryCriteria.Pagging!=null)
            {
              PageNo=  _QueryCriteria.Pagging.PageNo;
              PageSize = _QueryCriteria.Pagging.PageSize;
            }
            return QueryExtension.CreatePagedResults(_context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>(), PageNo, PageSize);
           
        }

		public QueryResult GetList(QueryCriteria _QueryCriteria)
         {
             return QueryExtension.CreatePagedWithFilterResults(_context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>(),_QueryCriteria);

         }

		    public List<Arco.Model.Entity.AMWCustomer> GetAllSearchDynamic(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string[] filterBy = default(string[]), string[] condition = default(string[]), string[] filterValue = default(string[]))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "CustomerID";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>()
					.DynamicWhereQ(filterBy, condition, filterValue)
                    .DynamicOrderBy(orderBy)
                    .ToList();
        }

        public List<Arco.Model.Entity.AMWCustomer> GetAllSearchPaged(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string filterBy = default(string), string condition = "Equals", object filterValue = default(object))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "CustomerID";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>()
					.DynamicWhere(filterBy, condition, filterValue)
                    .DynamicOrderBy(orderBy)
                    .Skip(startRowIndex)
                    .Take(maximumRows)
                    .ToList();
        }

		public int GetTotalCountForAllSearch(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string filterBy = default(string), string condition = "Equals", object filterValue = default(object))
        {
            return  _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>().DynamicWhere(filterBy, condition, filterValue).Count();
        }

        public int GetTotalCountForAll(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int))
        {       
           return _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>().Count();                     
        }
        
		#endregion

		#region Persistence 

		

		public Arco.Model.Entity.AMWCustomer Insert()
        {
		if (_lAMWCustomer == null)
                throw (new ArgumentNullException("newAMWCustomer"));

			return	Insert(_lAMWCustomer);

		}



        public virtual Arco.Model.Entity.AMWCustomer Insert(Arco.Model.Entity.AMWCustomer newAMWCustomer)
        {
			int numberOfAffectedRows = 0;

            // Validate Parameters 
            if (newAMWCustomer == null)
                throw (new ArgumentNullException("newAMWCustomer"));

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(newAMWCustomer, PersistenceType.Create);
				OnInserting(newAMWCustomer);

				newAMWCustomer.DataAreaId = CurrentSession.DataAreaId;
				newAMWCustomer.CreatedBy = CurrentSession.UserId;
				newAMWCustomer.CreatedDatetime = DateTime.UtcNow;
				newAMWCustomer.ModifiedBy = CurrentSession.UserId;
				newAMWCustomer.ModifiedDatetime = DateTime.UtcNow;



				
			// Validate Primary key value
			try{
			string primary=newAMWCustomer.CustomerID.ToString();
			if (primary.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("customerID");

				}
				catch(Exception ex)
				{
				 BizHelper.ThrowErrorForInvalidDataKey("customerID");
				}



				try
				{
					_context.Add<Arco.Model.Entity.AMWCustomer>(newAMWCustomer);
					numberOfAffectedRows = _context.UnitOfWork.Commit();
				}
				catch (DbEntityValidationException ex)
				{
					BizHelper.ThrowDbEntityValidationException(ex);
				}

				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException("No aMWCustomer created!");

				// Apply business workflow
				OnInserted(newAMWCustomer);
				OnSaved(newAMWCustomer, PersistenceType.Create);

				//scope.Complete();
			//}
            return newAMWCustomer;
        }
		
        public virtual Arco.Model.Entity.AMWCustomer Insert(string _customerID, string _dataAreaId, long _recId, string _firstName = default(string), string _middleName = default(string), string _lastName = default(string), string _familyName = default(string), int _gender = default(int), string _nationality = default(string), DateTime _dOBHijri = default(DateTime), DateTime _dOBGeorgian = default(DateTime), string _iDNumber = default(string), string _mobileNumber = default(string), string _phoneNumber = default(string), string _street = default(string), decimal _pOBOXNumber = default(decimal), string _countryRegion = default(string), string _city = default(string), decimal _postalCode = default(decimal), string _email = default(string), int _workingSector = default(int), string _occupation = default(string), decimal _monthlySalary = default(decimal), decimal _otherIncome = default(decimal), int _knowVia = default(int), string _notes = default(string), string _companyName = default(string), string _legalName = default(string), string _registrationNo = default(string), string _cOCNo = default(string), string _companyEmail = default(string), string _companyWebsite = default(string), string _natureofBusiness = default(string), int _sector = default(int), int _totalEmployees = default(int), int _totalSaudiEmployees = default(int), string _companyNoLabourOffice = default(string), string _companyActivity = default(string), string _contactPerson = default(string), string _extension = default(string), int _customerType = default(int), string _contactPersonEmail = default(string), int _customerStatus = default(int), DateTime _createdDatetime = default(DateTime), string _createdBy = default(string), DateTime _modifiedDatetime = default(DateTime), string _modifiedBy = default(string), byte[] _recVersion = default(byte[]), string _accountManager = default(string), string _supervisor = default(string), string _salesPerson = default(string), int _noofFamilyMembers = default(int), int _noofWifes = default(int), bool _oldAgePerson = default(bool), int _noofOldAgePerson = default(int), string _oldAgePersonNotes = default(string), bool _specialDisabled = default(bool), int _noofSpecialDisabled = default(int), string _specialDisabledNotes = default(string), int _salaryRange = default(int), string _occupationDesc = default(string), int _createdSource = default(int), string _externalID = default(string), long _enquiryRefId = default(long), string _faxNumber = default(string), string _customerNameEN = default(string), string _workPlace = default(string), string _workAddress = default(string), string _workEmail = default(string), string _workTel = default(string), string _workFax = default(string), decimal _workPoBox = default(decimal), decimal _mailAddress = default(decimal), decimal _workMailAddress = default(decimal), decimal _workMailBox = default(decimal), decimal _mailBox = default(decimal), string _workMobile = default(string), string _cRM_GUID = default(string), int _channel = default(int), string _femaleContactPersonPhoneNumber = default(string), int _verificationStatus = default(int), string _verifiedBy = default(string), DateTime _verifiedDate = default(DateTime), string _virtualAccountId = default(string), string _affiliateCode = default(string), string _iBANNumber = default(string), string _promoCode = default(string), DateTime _workDateOfJoining = default(DateTime), DateTime _iqamaExpiryDate = default(DateTime))
		{
			// Validate Primary key value
			if (_customerID.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("customerID");

				
			Arco.Model.Entity.AMWCustomer aMWCustomerToBeCreated = new Arco.Model.Entity.AMWCustomer();

			aMWCustomerToBeCreated.CustomerID = _customerID;
			if(_firstName != default(string))
				aMWCustomerToBeCreated.FirstName = _firstName;
			if(_middleName != default(string))
				aMWCustomerToBeCreated.MiddleName = _middleName;
			if(_lastName != default(string))
				aMWCustomerToBeCreated.LastName = _lastName;
			if(_familyName != default(string))
				aMWCustomerToBeCreated.FamilyName = _familyName;
			if(_gender != default(int))
				aMWCustomerToBeCreated.Gender = _gender;
			if(_nationality != default(string))
				aMWCustomerToBeCreated.Nationality = _nationality;
			if(_dOBHijri != default(DateTime))
				aMWCustomerToBeCreated.DOBHijri = _dOBHijri;
			if(_dOBGeorgian != default(DateTime))
				aMWCustomerToBeCreated.DOBGeorgian = _dOBGeorgian;
			if(_iDNumber != default(string))
				aMWCustomerToBeCreated.IDNumber = _iDNumber;
			if(_mobileNumber != default(string))
				aMWCustomerToBeCreated.MobileNumber = _mobileNumber;
			if(_phoneNumber != default(string))
				aMWCustomerToBeCreated.PhoneNumber = _phoneNumber;
			if(_street != default(string))
				aMWCustomerToBeCreated.Street = _street;
			if(_pOBOXNumber != default(decimal))
				aMWCustomerToBeCreated.POBOXNumber = _pOBOXNumber;
			if(_countryRegion != default(string))
				aMWCustomerToBeCreated.CountryRegion = _countryRegion;
			if(_city != default(string))
				aMWCustomerToBeCreated.City = _city;
			if(_postalCode != default(decimal))
				aMWCustomerToBeCreated.PostalCode = _postalCode;
			if(_email != default(string))
				aMWCustomerToBeCreated.Email = _email;
			if(_workingSector != default(int))
				aMWCustomerToBeCreated.WorkingSector = _workingSector;
			if(_occupation != default(string))
				aMWCustomerToBeCreated.Occupation = _occupation;
			if(_monthlySalary != default(decimal))
				aMWCustomerToBeCreated.MonthlySalary = _monthlySalary;
			if(_otherIncome != default(decimal))
				aMWCustomerToBeCreated.OtherIncome = _otherIncome;
			if(_knowVia != default(int))
				aMWCustomerToBeCreated.KnowVia = _knowVia;
			if(_notes != default(string))
				aMWCustomerToBeCreated.Notes = _notes;
			if(_companyName != default(string))
				aMWCustomerToBeCreated.CompanyName = _companyName;
			if(_legalName != default(string))
				aMWCustomerToBeCreated.LegalName = _legalName;
			if(_registrationNo != default(string))
				aMWCustomerToBeCreated.RegistrationNo = _registrationNo;
			if(_cOCNo != default(string))
				aMWCustomerToBeCreated.COCNo = _cOCNo;
			if(_companyEmail != default(string))
				aMWCustomerToBeCreated.CompanyEmail = _companyEmail;
			if(_companyWebsite != default(string))
				aMWCustomerToBeCreated.CompanyWebsite = _companyWebsite;
			if(_natureofBusiness != default(string))
				aMWCustomerToBeCreated.NatureofBusiness = _natureofBusiness;
			if(_sector != default(int))
				aMWCustomerToBeCreated.Sector = _sector;
			if(_totalEmployees != default(int))
				aMWCustomerToBeCreated.TotalEmployees = _totalEmployees;
			if(_totalSaudiEmployees != default(int))
				aMWCustomerToBeCreated.TotalSaudiEmployees = _totalSaudiEmployees;
			if(_companyNoLabourOffice != default(string))
				aMWCustomerToBeCreated.CompanyNoLabourOffice = _companyNoLabourOffice;
			if(_companyActivity != default(string))
				aMWCustomerToBeCreated.CompanyActivity = _companyActivity;
			if(_contactPerson != default(string))
				aMWCustomerToBeCreated.ContactPerson = _contactPerson;
			if(_extension != default(string))
				aMWCustomerToBeCreated.Extension = _extension;
			if(_customerType != default(int))
				aMWCustomerToBeCreated.CustomerType = _customerType;
			if(_contactPersonEmail != default(string))
				aMWCustomerToBeCreated.ContactPersonEmail = _contactPersonEmail;
			if(_customerStatus != default(int))
				aMWCustomerToBeCreated.CustomerStatus = _customerStatus;
			if(_accountManager != default(string))
				aMWCustomerToBeCreated.AccountManager = _accountManager;
			if(_supervisor != default(string))
				aMWCustomerToBeCreated.Supervisor = _supervisor;
			if(_salesPerson != default(string))
				aMWCustomerToBeCreated.SalesPerson = _salesPerson;
			if(_noofFamilyMembers != default(int))
				aMWCustomerToBeCreated.NoofFamilyMembers = _noofFamilyMembers;
			if(_noofWifes != default(int))
				aMWCustomerToBeCreated.NoofWifes = _noofWifes;
			if(_oldAgePerson != default(bool))
				aMWCustomerToBeCreated.OldAgePerson = _oldAgePerson;
			if(_noofOldAgePerson != default(int))
				aMWCustomerToBeCreated.NoofOldAgePerson = _noofOldAgePerson;
			if(_oldAgePersonNotes != default(string))
				aMWCustomerToBeCreated.OldAgePersonNotes = _oldAgePersonNotes;
			if(_specialDisabled != default(bool))
				aMWCustomerToBeCreated.SpecialDisabled = _specialDisabled;
			if(_noofSpecialDisabled != default(int))
				aMWCustomerToBeCreated.NoofSpecialDisabled = _noofSpecialDisabled;
			if(_specialDisabledNotes != default(string))
				aMWCustomerToBeCreated.SpecialDisabledNotes = _specialDisabledNotes;
			if(_salaryRange != default(int))
				aMWCustomerToBeCreated.SalaryRange = _salaryRange;
			if(_occupationDesc != default(string))
				aMWCustomerToBeCreated.OccupationDesc = _occupationDesc;
			if(_createdSource != default(int))
				aMWCustomerToBeCreated.CreatedSource = _createdSource;
			if(_externalID != default(string))
				aMWCustomerToBeCreated.ExternalID = _externalID;
			if(_enquiryRefId != default(long))
				aMWCustomerToBeCreated.EnquiryRefId = _enquiryRefId;
			if(_faxNumber != default(string))
				aMWCustomerToBeCreated.FaxNumber = _faxNumber;
			if(_customerNameEN != default(string))
				aMWCustomerToBeCreated.CustomerNameEN = _customerNameEN;
			if(_workPlace != default(string))
				aMWCustomerToBeCreated.WorkPlace = _workPlace;
			if(_workAddress != default(string))
				aMWCustomerToBeCreated.WorkAddress = _workAddress;
			if(_workEmail != default(string))
				aMWCustomerToBeCreated.WorkEmail = _workEmail;
			if(_workTel != default(string))
				aMWCustomerToBeCreated.WorkTel = _workTel;
			if(_workFax != default(string))
				aMWCustomerToBeCreated.WorkFax = _workFax;
			if(_workPoBox != default(decimal))
				aMWCustomerToBeCreated.WorkPoBox = _workPoBox;
			if(_mailAddress != default(decimal))
				aMWCustomerToBeCreated.MailAddress = _mailAddress;
			if(_workMailAddress != default(decimal))
				aMWCustomerToBeCreated.WorkMailAddress = _workMailAddress;
			if(_workMailBox != default(decimal))
				aMWCustomerToBeCreated.WorkMailBox = _workMailBox;
			if(_mailBox != default(decimal))
				aMWCustomerToBeCreated.MailBox = _mailBox;
			if(_workMobile != default(string))
				aMWCustomerToBeCreated.WorkMobile = _workMobile;
			if(_cRM_GUID != default(string))
				aMWCustomerToBeCreated.CRM_GUID = _cRM_GUID;
			if(_channel != default(int))
				aMWCustomerToBeCreated.Channel = _channel;
			if(_femaleContactPersonPhoneNumber != default(string))
				aMWCustomerToBeCreated.FemaleContactPersonPhoneNumber = _femaleContactPersonPhoneNumber;
			if(_verificationStatus != default(int))
				aMWCustomerToBeCreated.VerificationStatus = _verificationStatus;
			if(_verifiedBy != default(string))
				aMWCustomerToBeCreated.VerifiedBy = _verifiedBy;
			if(_verifiedDate != default(DateTime))
				aMWCustomerToBeCreated.VerifiedDate = _verifiedDate;
			if(_virtualAccountId != default(string))
				aMWCustomerToBeCreated.VirtualAccountId = _virtualAccountId;
			if(_affiliateCode != default(string))
				aMWCustomerToBeCreated.AffiliateCode = _affiliateCode;
			if(_iBANNumber != default(string))
				aMWCustomerToBeCreated.IBANNumber = _iBANNumber;
			if(_promoCode != default(string))
				aMWCustomerToBeCreated.PromoCode = _promoCode;
			if(_workDateOfJoining != default(DateTime))
				aMWCustomerToBeCreated.WorkDateOfJoining = _workDateOfJoining;
			if(_iqamaExpiryDate != default(DateTime))
				aMWCustomerToBeCreated.IqamaExpiryDate = _iqamaExpiryDate;

			return Insert(aMWCustomerToBeCreated);
        }




		public virtual Arco.Model.Entity.AMWCustomer InsertDynamic(string dynamicObj)
		{

		 dynamic results = JsonConvert.DeserializeObject(dynamicObj);

           //     RecCandidate Demand1 = new RecCandidate();
                

             //   if (results.FirstName != null && results.FirstName.ToString() != "")
               //     Demand1.FirstName = results.FirstName.ToString();
				
			Arco.Model.Entity.AMWCustomer aMWCustomerToBeCreated = new Arco.Model.Entity.AMWCustomer();

			
	if(results.CustomerID != null && results.CustomerID.ToString() != "")
	aMWCustomerToBeCreated.CustomerID =results.CustomerID.ToString();// results.CustomerID;
			if(results.FirstName != null && results.FirstName.ToString() != "")
				aMWCustomerToBeCreated.FirstName =results.FirstName.ToString();
			if(results.MiddleName != null && results.MiddleName.ToString() != "")
				aMWCustomerToBeCreated.MiddleName =results.MiddleName.ToString();
			if(results.LastName != null && results.LastName.ToString() != "")
				aMWCustomerToBeCreated.LastName =results.LastName.ToString();
			if(results.FamilyName != null && results.FamilyName.ToString() != "")
				aMWCustomerToBeCreated.FamilyName =results.FamilyName.ToString();
			if(results.Gender != null && results.Gender.ToString() != "")
				aMWCustomerToBeCreated.Gender =int.Parse(results.Gender.ToString());
			if(results.Nationality != null && results.Nationality.ToString() != "")
				aMWCustomerToBeCreated.Nationality =results.Nationality.ToString();
			if(results.DOBHijri != null && results.DOBHijri.ToString() != "")
				aMWCustomerToBeCreated.DOBHijri =results.DOBHijri;
			if(results.DOBGeorgian != null && results.DOBGeorgian.ToString() != "")
				aMWCustomerToBeCreated.DOBGeorgian =results.DOBGeorgian;
			if(results.IDNumber != null && results.IDNumber.ToString() != "")
				aMWCustomerToBeCreated.IDNumber =results.IDNumber.ToString();
			if(results.MobileNumber != null && results.MobileNumber.ToString() != "")
				aMWCustomerToBeCreated.MobileNumber =results.MobileNumber.ToString();
			if(results.PhoneNumber != null && results.PhoneNumber.ToString() != "")
				aMWCustomerToBeCreated.PhoneNumber =results.PhoneNumber.ToString();
			if(results.Street != null && results.Street.ToString() != "")
				aMWCustomerToBeCreated.Street =results.Street.ToString();
			if(results.POBOXNumber != null && results.POBOXNumber.ToString() != "")
				aMWCustomerToBeCreated.POBOXNumber =Decimal.Parse(results.POBOXNumber.ToString());
			if(results.CountryRegion != null && results.CountryRegion.ToString() != "")
				aMWCustomerToBeCreated.CountryRegion =results.CountryRegion.ToString();
			if(results.City != null && results.City.ToString() != "")
				aMWCustomerToBeCreated.City =results.City.ToString();
			if(results.PostalCode != null && results.PostalCode.ToString() != "")
				aMWCustomerToBeCreated.PostalCode =Decimal.Parse(results.PostalCode.ToString());
			if(results.Email != null && results.Email.ToString() != "")
				aMWCustomerToBeCreated.Email =results.Email.ToString();
			if(results.WorkingSector != null && results.WorkingSector.ToString() != "")
				aMWCustomerToBeCreated.WorkingSector =int.Parse(results.WorkingSector.ToString());
			if(results.Occupation != null && results.Occupation.ToString() != "")
				aMWCustomerToBeCreated.Occupation =results.Occupation.ToString();
			if(results.MonthlySalary != null && results.MonthlySalary.ToString() != "")
				aMWCustomerToBeCreated.MonthlySalary =Decimal.Parse(results.MonthlySalary.ToString());
			if(results.OtherIncome != null && results.OtherIncome.ToString() != "")
				aMWCustomerToBeCreated.OtherIncome =Decimal.Parse(results.OtherIncome.ToString());
			if(results.KnowVia != null && results.KnowVia.ToString() != "")
				aMWCustomerToBeCreated.KnowVia =int.Parse(results.KnowVia.ToString());
			if(results.Notes != null && results.Notes.ToString() != "")
				aMWCustomerToBeCreated.Notes =results.Notes.ToString();
			if(results.CompanyName != null && results.CompanyName.ToString() != "")
				aMWCustomerToBeCreated.CompanyName =results.CompanyName.ToString();
			if(results.LegalName != null && results.LegalName.ToString() != "")
				aMWCustomerToBeCreated.LegalName =results.LegalName.ToString();
			if(results.RegistrationNo != null && results.RegistrationNo.ToString() != "")
				aMWCustomerToBeCreated.RegistrationNo =results.RegistrationNo.ToString();
			if(results.COCNo != null && results.COCNo.ToString() != "")
				aMWCustomerToBeCreated.COCNo =results.COCNo.ToString();
			if(results.CompanyEmail != null && results.CompanyEmail.ToString() != "")
				aMWCustomerToBeCreated.CompanyEmail =results.CompanyEmail.ToString();
			if(results.CompanyWebsite != null && results.CompanyWebsite.ToString() != "")
				aMWCustomerToBeCreated.CompanyWebsite =results.CompanyWebsite.ToString();
			if(results.NatureofBusiness != null && results.NatureofBusiness.ToString() != "")
				aMWCustomerToBeCreated.NatureofBusiness =results.NatureofBusiness.ToString();
			if(results.Sector != null && results.Sector.ToString() != "")
				aMWCustomerToBeCreated.Sector =int.Parse(results.Sector.ToString());
			if(results.TotalEmployees != null && results.TotalEmployees.ToString() != "")
				aMWCustomerToBeCreated.TotalEmployees =int.Parse(results.TotalEmployees.ToString());
			if(results.TotalSaudiEmployees != null && results.TotalSaudiEmployees.ToString() != "")
				aMWCustomerToBeCreated.TotalSaudiEmployees =int.Parse(results.TotalSaudiEmployees.ToString());
			if(results.CompanyNoLabourOffice != null && results.CompanyNoLabourOffice.ToString() != "")
				aMWCustomerToBeCreated.CompanyNoLabourOffice =results.CompanyNoLabourOffice.ToString();
			if(results.CompanyActivity != null && results.CompanyActivity.ToString() != "")
				aMWCustomerToBeCreated.CompanyActivity =results.CompanyActivity.ToString();
			if(results.ContactPerson != null && results.ContactPerson.ToString() != "")
				aMWCustomerToBeCreated.ContactPerson =results.ContactPerson.ToString();
			if(results.Extension != null && results.Extension.ToString() != "")
				aMWCustomerToBeCreated.Extension =results.Extension.ToString();
			if(results.CustomerType != null && results.CustomerType.ToString() != "")
				aMWCustomerToBeCreated.CustomerType =int.Parse(results.CustomerType.ToString());
			if(results.ContactPersonEmail != null && results.ContactPersonEmail.ToString() != "")
				aMWCustomerToBeCreated.ContactPersonEmail =results.ContactPersonEmail.ToString();
			if(results.CustomerStatus != null && results.CustomerStatus.ToString() != "")
				aMWCustomerToBeCreated.CustomerStatus =int.Parse(results.CustomerStatus.ToString());
			if(results.AccountManager != null && results.AccountManager.ToString() != "")
				aMWCustomerToBeCreated.AccountManager =results.AccountManager.ToString();
			if(results.Supervisor != null && results.Supervisor.ToString() != "")
				aMWCustomerToBeCreated.Supervisor =results.Supervisor.ToString();
			if(results.SalesPerson != null && results.SalesPerson.ToString() != "")
				aMWCustomerToBeCreated.SalesPerson =results.SalesPerson.ToString();
			if(results.NoofFamilyMembers != null && results.NoofFamilyMembers.ToString() != "")
				aMWCustomerToBeCreated.NoofFamilyMembers =int.Parse(results.NoofFamilyMembers.ToString());
			if(results.NoofWifes != null && results.NoofWifes.ToString() != "")
				aMWCustomerToBeCreated.NoofWifes =int.Parse(results.NoofWifes.ToString());
			if(results.OldAgePerson != null && results.OldAgePerson.ToString() != "")
				aMWCustomerToBeCreated.OldAgePerson =bool.Parse(results.OldAgePerson.ToString());
			if(results.NoofOldAgePerson != null && results.NoofOldAgePerson.ToString() != "")
				aMWCustomerToBeCreated.NoofOldAgePerson =int.Parse(results.NoofOldAgePerson.ToString());
			if(results.OldAgePersonNotes != null && results.OldAgePersonNotes.ToString() != "")
				aMWCustomerToBeCreated.OldAgePersonNotes =results.OldAgePersonNotes.ToString();
			if(results.SpecialDisabled != null && results.SpecialDisabled.ToString() != "")
				aMWCustomerToBeCreated.SpecialDisabled =bool.Parse(results.SpecialDisabled.ToString());
			if(results.NoofSpecialDisabled != null && results.NoofSpecialDisabled.ToString() != "")
				aMWCustomerToBeCreated.NoofSpecialDisabled =int.Parse(results.NoofSpecialDisabled.ToString());
			if(results.SpecialDisabledNotes != null && results.SpecialDisabledNotes.ToString() != "")
				aMWCustomerToBeCreated.SpecialDisabledNotes =results.SpecialDisabledNotes.ToString();
			if(results.SalaryRange != null && results.SalaryRange.ToString() != "")
				aMWCustomerToBeCreated.SalaryRange =int.Parse(results.SalaryRange.ToString());
			if(results.OccupationDesc != null && results.OccupationDesc.ToString() != "")
				aMWCustomerToBeCreated.OccupationDesc =results.OccupationDesc.ToString();
			if(results.CreatedSource != null && results.CreatedSource.ToString() != "")
				aMWCustomerToBeCreated.CreatedSource =int.Parse(results.CreatedSource.ToString());
			if(results.ExternalID != null && results.ExternalID.ToString() != "")
				aMWCustomerToBeCreated.ExternalID =results.ExternalID.ToString();
			if(results.EnquiryRefId != null && results.EnquiryRefId.ToString() != "")
				aMWCustomerToBeCreated.EnquiryRefId =long.Parse(results.EnquiryRefId.ToString());
			if(results.FaxNumber != null && results.FaxNumber.ToString() != "")
				aMWCustomerToBeCreated.FaxNumber =results.FaxNumber.ToString();
			if(results.CustomerNameEN != null && results.CustomerNameEN.ToString() != "")
				aMWCustomerToBeCreated.CustomerNameEN =results.CustomerNameEN.ToString();
			if(results.WorkPlace != null && results.WorkPlace.ToString() != "")
				aMWCustomerToBeCreated.WorkPlace =results.WorkPlace.ToString();
			if(results.WorkAddress != null && results.WorkAddress.ToString() != "")
				aMWCustomerToBeCreated.WorkAddress =results.WorkAddress.ToString();
			if(results.WorkEmail != null && results.WorkEmail.ToString() != "")
				aMWCustomerToBeCreated.WorkEmail =results.WorkEmail.ToString();
			if(results.WorkTel != null && results.WorkTel.ToString() != "")
				aMWCustomerToBeCreated.WorkTel =results.WorkTel.ToString();
			if(results.WorkFax != null && results.WorkFax.ToString() != "")
				aMWCustomerToBeCreated.WorkFax =results.WorkFax.ToString();
			if(results.WorkPoBox != null && results.WorkPoBox.ToString() != "")
				aMWCustomerToBeCreated.WorkPoBox =Decimal.Parse(results.WorkPoBox.ToString());
			if(results.MailAddress != null && results.MailAddress.ToString() != "")
				aMWCustomerToBeCreated.MailAddress =Decimal.Parse(results.MailAddress.ToString());
			if(results.WorkMailAddress != null && results.WorkMailAddress.ToString() != "")
				aMWCustomerToBeCreated.WorkMailAddress =Decimal.Parse(results.WorkMailAddress.ToString());
			if(results.WorkMailBox != null && results.WorkMailBox.ToString() != "")
				aMWCustomerToBeCreated.WorkMailBox =Decimal.Parse(results.WorkMailBox.ToString());
			if(results.MailBox != null && results.MailBox.ToString() != "")
				aMWCustomerToBeCreated.MailBox =Decimal.Parse(results.MailBox.ToString());
			if(results.WorkMobile != null && results.WorkMobile.ToString() != "")
				aMWCustomerToBeCreated.WorkMobile =results.WorkMobile.ToString();
			if(results.CRM_GUID != null && results.CRM_GUID.ToString() != "")
				aMWCustomerToBeCreated.CRM_GUID =results.CRM_GUID.ToString();
			if(results.Channel != null && results.Channel.ToString() != "")
				aMWCustomerToBeCreated.Channel =int.Parse(results.Channel.ToString());
			if(results.FemaleContactPersonPhoneNumber != null && results.FemaleContactPersonPhoneNumber.ToString() != "")
				aMWCustomerToBeCreated.FemaleContactPersonPhoneNumber =results.FemaleContactPersonPhoneNumber.ToString();
			if(results.VerificationStatus != null && results.VerificationStatus.ToString() != "")
				aMWCustomerToBeCreated.VerificationStatus =int.Parse(results.VerificationStatus.ToString());
			if(results.VerifiedBy != null && results.VerifiedBy.ToString() != "")
				aMWCustomerToBeCreated.VerifiedBy =results.VerifiedBy.ToString();
			if(results.VerifiedDate != null && results.VerifiedDate.ToString() != "")
				aMWCustomerToBeCreated.VerifiedDate =results.VerifiedDate;
			if(results.VirtualAccountId != null && results.VirtualAccountId.ToString() != "")
				aMWCustomerToBeCreated.VirtualAccountId =results.VirtualAccountId.ToString();
			if(results.AffiliateCode != null && results.AffiliateCode.ToString() != "")
				aMWCustomerToBeCreated.AffiliateCode =results.AffiliateCode.ToString();
			if(results.IBANNumber != null && results.IBANNumber.ToString() != "")
				aMWCustomerToBeCreated.IBANNumber =results.IBANNumber.ToString();
			if(results.PromoCode != null && results.PromoCode.ToString() != "")
				aMWCustomerToBeCreated.PromoCode =results.PromoCode.ToString();
			if(results.WorkDateOfJoining != null && results.WorkDateOfJoining.ToString() != "")
				aMWCustomerToBeCreated.WorkDateOfJoining =results.WorkDateOfJoining;
			if(results.IqamaExpiryDate != null && results.IqamaExpiryDate.ToString() != "")
				aMWCustomerToBeCreated.IqamaExpiryDate =results.IqamaExpiryDate;

			return Insert(aMWCustomerToBeCreated);
        }



			public Arco.Model.Entity.AMWCustomer Update()
        {
		

		if (_lAMWCustomer == null || _lAMWCustomer.RecId == default(long))
                throw (new ArgumentNullException("newAMWCustomer"));

				return Update(_lAMWCustomer);

		}


        public Arco.Model.Entity.AMWCustomer Update(Arco.Model.Entity.AMWCustomer updatedAMWCustomer)
        {
			int numberOfAffectedRows = 0;

            // Validate Parameters
            if (updatedAMWCustomer == null)
                throw (new ArgumentNullException("updatedAMWCustomer"));

            // Validate Primary key value
            if (updatedAMWCustomer.CustomerID.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("CustomerID");

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(updatedAMWCustomer, PersistenceType.Update);
				OnUpdating(updatedAMWCustomer);

				updatedAMWCustomer.ModifiedBy = CurrentSession.UserId;
				updatedAMWCustomer.ModifiedDatetime = DateTime.UtcNow;

				try
				{
					_context.Update<Arco.Model.Entity.AMWCustomer>(updatedAMWCustomer);

                   // DbEntityEntry<AMWCustomer> entryAMWCustomer = _context.Entry(updatedAMWCustomer);
                  //  entryAMWCustomer.State = EntityState.Modified;
                   // entryAMWCustomer.Property("CreatedDatetime").IsModified = false;
                   // entryAMWCustomer.Property("CreatedBy").IsModified = false;

					numberOfAffectedRows = _context.UnitOfWork.Commit();
				}
				catch (DbEntityValidationException ex)
				{
					BizHelper.ThrowDbEntityValidationException(ex);
				}


				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException("No aMWCustomer updated!");

				//Apply business workflow
				OnUpdated(updatedAMWCustomer);
				OnSaved(updatedAMWCustomer, PersistenceType.Update);

			//	scope.Complete();
			//}
			return updatedAMWCustomer;

        }

        public Arco.Model.Entity.AMWCustomer Update(string _customerID, string _firstName = default(string), string _middleName = default(string), string _lastName = default(string), string _familyName = default(string), int _gender = default(int), string _nationality = default(string), DateTime _dOBHijri = default(DateTime), DateTime _dOBGeorgian = default(DateTime), string _iDNumber = default(string), string _mobileNumber = default(string), string _phoneNumber = default(string), string _street = default(string), decimal _pOBOXNumber = default(decimal), string _countryRegion = default(string), string _city = default(string), decimal _postalCode = default(decimal), string _email = default(string), int _workingSector = default(int), string _occupation = default(string), decimal _monthlySalary = default(decimal), decimal _otherIncome = default(decimal), int _knowVia = default(int), string _notes = default(string), string _companyName = default(string), string _legalName = default(string), string _registrationNo = default(string), string _cOCNo = default(string), string _companyEmail = default(string), string _companyWebsite = default(string), string _natureofBusiness = default(string), int _sector = default(int), int _totalEmployees = default(int), int _totalSaudiEmployees = default(int), string _companyNoLabourOffice = default(string), string _companyActivity = default(string), string _contactPerson = default(string), string _extension = default(string), int _customerType = default(int), string _contactPersonEmail = default(string), int _customerStatus = default(int), string _accountManager = default(string), string _supervisor = default(string), string _salesPerson = default(string), int _noofFamilyMembers = default(int), int _noofWifes = default(int), bool _oldAgePerson = default(bool), int _noofOldAgePerson = default(int), string _oldAgePersonNotes = default(string), bool _specialDisabled = default(bool), int _noofSpecialDisabled = default(int), string _specialDisabledNotes = default(string), int _salaryRange = default(int), string _occupationDesc = default(string), int _createdSource = default(int), string _externalID = default(string), long _enquiryRefId = default(long), string _faxNumber = default(string), string _customerNameEN = default(string), string _workPlace = default(string), string _workAddress = default(string), string _workEmail = default(string), string _workTel = default(string), string _workFax = default(string), decimal _workPoBox = default(decimal), decimal _mailAddress = default(decimal), decimal _workMailAddress = default(decimal), decimal _workMailBox = default(decimal), decimal _mailBox = default(decimal), string _workMobile = default(string), string _cRM_GUID = default(string), int _channel = default(int), string _femaleContactPersonPhoneNumber = default(string), int _verificationStatus = default(int), string _verifiedBy = default(string), DateTime _verifiedDate = default(DateTime), string _virtualAccountId = default(string), string _affiliateCode = default(string), string _iBANNumber = default(string), string _promoCode = default(string), DateTime _workDateOfJoining = default(DateTime), DateTime _iqamaExpiryDate = default(DateTime))
		{
			if (_customerID.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("customerID");
            
			Arco.Model.Entity.AMWCustomer aMWCustomerToBeUpdated = _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>().FirstOrDefault(aMWCustomer => aMWCustomer.CustomerID == _customerID);


			if(_customerID != default(string))
				aMWCustomerToBeUpdated.CustomerID = _customerID;
			if(_firstName != default(string))
				aMWCustomerToBeUpdated.FirstName = _firstName;
			if(_middleName != default(string))
				aMWCustomerToBeUpdated.MiddleName = _middleName;
			if(_lastName != default(string))
				aMWCustomerToBeUpdated.LastName = _lastName;
			if(_familyName != default(string))
				aMWCustomerToBeUpdated.FamilyName = _familyName;
			if(_gender != default(int))
				aMWCustomerToBeUpdated.Gender = _gender;
			if(_nationality != default(string))
				aMWCustomerToBeUpdated.Nationality = _nationality;
			if(_dOBHijri != default(DateTime))
				aMWCustomerToBeUpdated.DOBHijri = _dOBHijri;
			if(_dOBGeorgian != default(DateTime))
				aMWCustomerToBeUpdated.DOBGeorgian = _dOBGeorgian;
			if(_iDNumber != default(string))
				aMWCustomerToBeUpdated.IDNumber = _iDNumber;
			if(_mobileNumber != default(string))
				aMWCustomerToBeUpdated.MobileNumber = _mobileNumber;
			if(_phoneNumber != default(string))
				aMWCustomerToBeUpdated.PhoneNumber = _phoneNumber;
			if(_street != default(string))
				aMWCustomerToBeUpdated.Street = _street;
			if(_pOBOXNumber != default(decimal))
				aMWCustomerToBeUpdated.POBOXNumber = _pOBOXNumber;
			if(_countryRegion != default(string))
				aMWCustomerToBeUpdated.CountryRegion = _countryRegion;
			if(_city != default(string))
				aMWCustomerToBeUpdated.City = _city;
			if(_postalCode != default(decimal))
				aMWCustomerToBeUpdated.PostalCode = _postalCode;
			if(_email != default(string))
				aMWCustomerToBeUpdated.Email = _email;
			if(_workingSector != default(int))
				aMWCustomerToBeUpdated.WorkingSector = _workingSector;
			if(_occupation != default(string))
				aMWCustomerToBeUpdated.Occupation = _occupation;
			if(_monthlySalary != default(decimal))
				aMWCustomerToBeUpdated.MonthlySalary = _monthlySalary;
			if(_otherIncome != default(decimal))
				aMWCustomerToBeUpdated.OtherIncome = _otherIncome;
			if(_knowVia != default(int))
				aMWCustomerToBeUpdated.KnowVia = _knowVia;
			if(_notes != default(string))
				aMWCustomerToBeUpdated.Notes = _notes;
			if(_companyName != default(string))
				aMWCustomerToBeUpdated.CompanyName = _companyName;
			if(_legalName != default(string))
				aMWCustomerToBeUpdated.LegalName = _legalName;
			if(_registrationNo != default(string))
				aMWCustomerToBeUpdated.RegistrationNo = _registrationNo;
			if(_cOCNo != default(string))
				aMWCustomerToBeUpdated.COCNo = _cOCNo;
			if(_companyEmail != default(string))
				aMWCustomerToBeUpdated.CompanyEmail = _companyEmail;
			if(_companyWebsite != default(string))
				aMWCustomerToBeUpdated.CompanyWebsite = _companyWebsite;
			if(_natureofBusiness != default(string))
				aMWCustomerToBeUpdated.NatureofBusiness = _natureofBusiness;
			if(_sector != default(int))
				aMWCustomerToBeUpdated.Sector = _sector;
			if(_totalEmployees != default(int))
				aMWCustomerToBeUpdated.TotalEmployees = _totalEmployees;
			if(_totalSaudiEmployees != default(int))
				aMWCustomerToBeUpdated.TotalSaudiEmployees = _totalSaudiEmployees;
			if(_companyNoLabourOffice != default(string))
				aMWCustomerToBeUpdated.CompanyNoLabourOffice = _companyNoLabourOffice;
			if(_companyActivity != default(string))
				aMWCustomerToBeUpdated.CompanyActivity = _companyActivity;
			if(_contactPerson != default(string))
				aMWCustomerToBeUpdated.ContactPerson = _contactPerson;
			if(_extension != default(string))
				aMWCustomerToBeUpdated.Extension = _extension;
			if(_customerType != default(int))
				aMWCustomerToBeUpdated.CustomerType = _customerType;
			if(_contactPersonEmail != default(string))
				aMWCustomerToBeUpdated.ContactPersonEmail = _contactPersonEmail;
			if(_customerStatus != default(int))
				aMWCustomerToBeUpdated.CustomerStatus = _customerStatus;
			if(_accountManager != default(string))
				aMWCustomerToBeUpdated.AccountManager = _accountManager;
			if(_supervisor != default(string))
				aMWCustomerToBeUpdated.Supervisor = _supervisor;
			if(_salesPerson != default(string))
				aMWCustomerToBeUpdated.SalesPerson = _salesPerson;
			if(_noofFamilyMembers != default(int))
				aMWCustomerToBeUpdated.NoofFamilyMembers = _noofFamilyMembers;
			if(_noofWifes != default(int))
				aMWCustomerToBeUpdated.NoofWifes = _noofWifes;
			if(_oldAgePerson != default(bool))
				aMWCustomerToBeUpdated.OldAgePerson = _oldAgePerson;
			if(_noofOldAgePerson != default(int))
				aMWCustomerToBeUpdated.NoofOldAgePerson = _noofOldAgePerson;
			if(_oldAgePersonNotes != default(string))
				aMWCustomerToBeUpdated.OldAgePersonNotes = _oldAgePersonNotes;
			if(_specialDisabled != default(bool))
				aMWCustomerToBeUpdated.SpecialDisabled = _specialDisabled;
			if(_noofSpecialDisabled != default(int))
				aMWCustomerToBeUpdated.NoofSpecialDisabled = _noofSpecialDisabled;
			if(_specialDisabledNotes != default(string))
				aMWCustomerToBeUpdated.SpecialDisabledNotes = _specialDisabledNotes;
			if(_salaryRange != default(int))
				aMWCustomerToBeUpdated.SalaryRange = _salaryRange;
			if(_occupationDesc != default(string))
				aMWCustomerToBeUpdated.OccupationDesc = _occupationDesc;
			if(_createdSource != default(int))
				aMWCustomerToBeUpdated.CreatedSource = _createdSource;
			if(_externalID != default(string))
				aMWCustomerToBeUpdated.ExternalID = _externalID;
			if(_enquiryRefId != default(long))
				aMWCustomerToBeUpdated.EnquiryRefId = _enquiryRefId;
			if(_faxNumber != default(string))
				aMWCustomerToBeUpdated.FaxNumber = _faxNumber;
			if(_customerNameEN != default(string))
				aMWCustomerToBeUpdated.CustomerNameEN = _customerNameEN;
			if(_workPlace != default(string))
				aMWCustomerToBeUpdated.WorkPlace = _workPlace;
			if(_workAddress != default(string))
				aMWCustomerToBeUpdated.WorkAddress = _workAddress;
			if(_workEmail != default(string))
				aMWCustomerToBeUpdated.WorkEmail = _workEmail;
			if(_workTel != default(string))
				aMWCustomerToBeUpdated.WorkTel = _workTel;
			if(_workFax != default(string))
				aMWCustomerToBeUpdated.WorkFax = _workFax;
			if(_workPoBox != default(decimal))
				aMWCustomerToBeUpdated.WorkPoBox = _workPoBox;
			if(_mailAddress != default(decimal))
				aMWCustomerToBeUpdated.MailAddress = _mailAddress;
			if(_workMailAddress != default(decimal))
				aMWCustomerToBeUpdated.WorkMailAddress = _workMailAddress;
			if(_workMailBox != default(decimal))
				aMWCustomerToBeUpdated.WorkMailBox = _workMailBox;
			if(_mailBox != default(decimal))
				aMWCustomerToBeUpdated.MailBox = _mailBox;
			if(_workMobile != default(string))
				aMWCustomerToBeUpdated.WorkMobile = _workMobile;
			if(_cRM_GUID != default(string))
				aMWCustomerToBeUpdated.CRM_GUID = _cRM_GUID;
			if(_channel != default(int))
				aMWCustomerToBeUpdated.Channel = _channel;
			if(_femaleContactPersonPhoneNumber != default(string))
				aMWCustomerToBeUpdated.FemaleContactPersonPhoneNumber = _femaleContactPersonPhoneNumber;
			if(_verificationStatus != default(int))
				aMWCustomerToBeUpdated.VerificationStatus = _verificationStatus;
			if(_verifiedBy != default(string))
				aMWCustomerToBeUpdated.VerifiedBy = _verifiedBy;
			if(_verifiedDate != default(DateTime))
				aMWCustomerToBeUpdated.VerifiedDate = _verifiedDate;
			if(_virtualAccountId != default(string))
				aMWCustomerToBeUpdated.VirtualAccountId = _virtualAccountId;
			if(_affiliateCode != default(string))
				aMWCustomerToBeUpdated.AffiliateCode = _affiliateCode;
			if(_iBANNumber != default(string))
				aMWCustomerToBeUpdated.IBANNumber = _iBANNumber;
			if(_promoCode != default(string))
				aMWCustomerToBeUpdated.PromoCode = _promoCode;
			if(_workDateOfJoining != default(DateTime))
				aMWCustomerToBeUpdated.WorkDateOfJoining = _workDateOfJoining;
			if(_iqamaExpiryDate != default(DateTime))
				aMWCustomerToBeUpdated.IqamaExpiryDate = _iqamaExpiryDate;

			return Update(aMWCustomerToBeUpdated);
        }



		 public Arco.Model.Entity.AMWCustomer UpdateDynamic(string dynamicObj)
		{
		 dynamic results = JsonConvert.DeserializeObject(dynamicObj);

			

			string primary=results.CustomerID;
			if (primary.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("customerID");

				string _CustomerID =results.CustomerID;
            
			Arco.Model.Entity.AMWCustomer aMWCustomerToBeUpdated = _context.GetAsQueryable<Arco.Model.Entity.AMWCustomer>().FirstOrDefault(aMWCustomer => aMWCustomer.CustomerID ==_CustomerID);


			if(results.CustomerID != null && results.CustomerID.ToString() != "")
				aMWCustomerToBeUpdated.CustomerID =results.CustomerID.ToString();// results.CustomerID;
			if(results.FirstName != null && results.FirstName.ToString() != "")
				aMWCustomerToBeUpdated.FirstName =results.FirstName.ToString();// results.FirstName;
			if(results.MiddleName != null && results.MiddleName.ToString() != "")
				aMWCustomerToBeUpdated.MiddleName =results.MiddleName.ToString();// results.MiddleName;
			if(results.LastName != null && results.LastName.ToString() != "")
				aMWCustomerToBeUpdated.LastName =results.LastName.ToString();// results.LastName;
			if(results.FamilyName != null && results.FamilyName.ToString() != "")
				aMWCustomerToBeUpdated.FamilyName =results.FamilyName.ToString();// results.FamilyName;
			if(results.Gender != null && results.Gender.ToString() != "")
				aMWCustomerToBeUpdated.Gender =int.Parse(results.Gender.ToString());// results.Gender;
			if(results.Nationality != null && results.Nationality.ToString() != "")
				aMWCustomerToBeUpdated.Nationality =results.Nationality.ToString();// results.Nationality;
			if(results.DOBHijri != null && results.DOBHijri.ToString() != "")
				aMWCustomerToBeUpdated.DOBHijri =results.DOBHijri;// results.DOBHijri;
			if(results.DOBGeorgian != null && results.DOBGeorgian.ToString() != "")
				aMWCustomerToBeUpdated.DOBGeorgian =results.DOBGeorgian;// results.DOBGeorgian;
			if(results.IDNumber != null && results.IDNumber.ToString() != "")
				aMWCustomerToBeUpdated.IDNumber =results.IDNumber.ToString();// results.IDNumber;
			if(results.MobileNumber != null && results.MobileNumber.ToString() != "")
				aMWCustomerToBeUpdated.MobileNumber =results.MobileNumber.ToString();// results.MobileNumber;
			if(results.PhoneNumber != null && results.PhoneNumber.ToString() != "")
				aMWCustomerToBeUpdated.PhoneNumber =results.PhoneNumber.ToString();// results.PhoneNumber;
			if(results.Street != null && results.Street.ToString() != "")
				aMWCustomerToBeUpdated.Street =results.Street.ToString();// results.Street;
			if(results.POBOXNumber != null && results.POBOXNumber.ToString() != "")
				aMWCustomerToBeUpdated.POBOXNumber =Decimal.Parse(results.POBOXNumber.ToString());// results.POBOXNumber;
			if(results.CountryRegion != null && results.CountryRegion.ToString() != "")
				aMWCustomerToBeUpdated.CountryRegion =results.CountryRegion.ToString();// results.CountryRegion;
			if(results.City != null && results.City.ToString() != "")
				aMWCustomerToBeUpdated.City =results.City.ToString();// results.City;
			if(results.PostalCode != null && results.PostalCode.ToString() != "")
				aMWCustomerToBeUpdated.PostalCode =Decimal.Parse(results.PostalCode.ToString());// results.PostalCode;
			if(results.Email != null && results.Email.ToString() != "")
				aMWCustomerToBeUpdated.Email =results.Email.ToString();// results.Email;
			if(results.WorkingSector != null && results.WorkingSector.ToString() != "")
				aMWCustomerToBeUpdated.WorkingSector =int.Parse(results.WorkingSector.ToString());// results.WorkingSector;
			if(results.Occupation != null && results.Occupation.ToString() != "")
				aMWCustomerToBeUpdated.Occupation =results.Occupation.ToString();// results.Occupation;
			if(results.MonthlySalary != null && results.MonthlySalary.ToString() != "")
				aMWCustomerToBeUpdated.MonthlySalary =Decimal.Parse(results.MonthlySalary.ToString());// results.MonthlySalary;
			if(results.OtherIncome != null && results.OtherIncome.ToString() != "")
				aMWCustomerToBeUpdated.OtherIncome =Decimal.Parse(results.OtherIncome.ToString());// results.OtherIncome;
			if(results.KnowVia != null && results.KnowVia.ToString() != "")
				aMWCustomerToBeUpdated.KnowVia =int.Parse(results.KnowVia.ToString());// results.KnowVia;
			if(results.Notes != null && results.Notes.ToString() != "")
				aMWCustomerToBeUpdated.Notes =results.Notes.ToString();// results.Notes;
			if(results.CompanyName != null && results.CompanyName.ToString() != "")
				aMWCustomerToBeUpdated.CompanyName =results.CompanyName.ToString();// results.CompanyName;
			if(results.LegalName != null && results.LegalName.ToString() != "")
				aMWCustomerToBeUpdated.LegalName =results.LegalName.ToString();// results.LegalName;
			if(results.RegistrationNo != null && results.RegistrationNo.ToString() != "")
				aMWCustomerToBeUpdated.RegistrationNo =results.RegistrationNo.ToString();// results.RegistrationNo;
			if(results.COCNo != null && results.COCNo.ToString() != "")
				aMWCustomerToBeUpdated.COCNo =results.COCNo.ToString();// results.COCNo;
			if(results.CompanyEmail != null && results.CompanyEmail.ToString() != "")
				aMWCustomerToBeUpdated.CompanyEmail =results.CompanyEmail.ToString();// results.CompanyEmail;
			if(results.CompanyWebsite != null && results.CompanyWebsite.ToString() != "")
				aMWCustomerToBeUpdated.CompanyWebsite =results.CompanyWebsite.ToString();// results.CompanyWebsite;
			if(results.NatureofBusiness != null && results.NatureofBusiness.ToString() != "")
				aMWCustomerToBeUpdated.NatureofBusiness =results.NatureofBusiness.ToString();// results.NatureofBusiness;
			if(results.Sector != null && results.Sector.ToString() != "")
				aMWCustomerToBeUpdated.Sector =int.Parse(results.Sector.ToString());// results.Sector;
			if(results.TotalEmployees != null && results.TotalEmployees.ToString() != "")
				aMWCustomerToBeUpdated.TotalEmployees =int.Parse(results.TotalEmployees.ToString());// results.TotalEmployees;
			if(results.TotalSaudiEmployees != null && results.TotalSaudiEmployees.ToString() != "")
				aMWCustomerToBeUpdated.TotalSaudiEmployees =int.Parse(results.TotalSaudiEmployees.ToString());// results.TotalSaudiEmployees;
			if(results.CompanyNoLabourOffice != null && results.CompanyNoLabourOffice.ToString() != "")
				aMWCustomerToBeUpdated.CompanyNoLabourOffice =results.CompanyNoLabourOffice.ToString();// results.CompanyNoLabourOffice;
			if(results.CompanyActivity != null && results.CompanyActivity.ToString() != "")
				aMWCustomerToBeUpdated.CompanyActivity =results.CompanyActivity.ToString();// results.CompanyActivity;
			if(results.ContactPerson != null && results.ContactPerson.ToString() != "")
				aMWCustomerToBeUpdated.ContactPerson =results.ContactPerson.ToString();// results.ContactPerson;
			if(results.Extension != null && results.Extension.ToString() != "")
				aMWCustomerToBeUpdated.Extension =results.Extension.ToString();// results.Extension;
			if(results.CustomerType != null && results.CustomerType.ToString() != "")
				aMWCustomerToBeUpdated.CustomerType =int.Parse(results.CustomerType.ToString());// results.CustomerType;
			if(results.ContactPersonEmail != null && results.ContactPersonEmail.ToString() != "")
				aMWCustomerToBeUpdated.ContactPersonEmail =results.ContactPersonEmail.ToString();// results.ContactPersonEmail;
			if(results.CustomerStatus != null && results.CustomerStatus.ToString() != "")
				aMWCustomerToBeUpdated.CustomerStatus =int.Parse(results.CustomerStatus.ToString());// results.CustomerStatus;
			if(results.AccountManager != null && results.AccountManager.ToString() != "")
				aMWCustomerToBeUpdated.AccountManager =results.AccountManager.ToString();// results.AccountManager;
			if(results.Supervisor != null && results.Supervisor.ToString() != "")
				aMWCustomerToBeUpdated.Supervisor =results.Supervisor.ToString();// results.Supervisor;
			if(results.SalesPerson != null && results.SalesPerson.ToString() != "")
				aMWCustomerToBeUpdated.SalesPerson =results.SalesPerson.ToString();// results.SalesPerson;
			if(results.NoofFamilyMembers != null && results.NoofFamilyMembers.ToString() != "")
				aMWCustomerToBeUpdated.NoofFamilyMembers =int.Parse(results.NoofFamilyMembers.ToString());// results.NoofFamilyMembers;
			if(results.NoofWifes != null && results.NoofWifes.ToString() != "")
				aMWCustomerToBeUpdated.NoofWifes =int.Parse(results.NoofWifes.ToString());// results.NoofWifes;
			if(results.OldAgePerson != null && results.OldAgePerson.ToString() != "")
				aMWCustomerToBeUpdated.OldAgePerson =bool.Parse(results.OldAgePerson.ToString());// results.OldAgePerson;
			if(results.NoofOldAgePerson != null && results.NoofOldAgePerson.ToString() != "")
				aMWCustomerToBeUpdated.NoofOldAgePerson =int.Parse(results.NoofOldAgePerson.ToString());// results.NoofOldAgePerson;
			if(results.OldAgePersonNotes != null && results.OldAgePersonNotes.ToString() != "")
				aMWCustomerToBeUpdated.OldAgePersonNotes =results.OldAgePersonNotes.ToString();// results.OldAgePersonNotes;
			if(results.SpecialDisabled != null && results.SpecialDisabled.ToString() != "")
				aMWCustomerToBeUpdated.SpecialDisabled =bool.Parse(results.SpecialDisabled.ToString());// results.SpecialDisabled;
			if(results.NoofSpecialDisabled != null && results.NoofSpecialDisabled.ToString() != "")
				aMWCustomerToBeUpdated.NoofSpecialDisabled =int.Parse(results.NoofSpecialDisabled.ToString());// results.NoofSpecialDisabled;
			if(results.SpecialDisabledNotes != null && results.SpecialDisabledNotes.ToString() != "")
				aMWCustomerToBeUpdated.SpecialDisabledNotes =results.SpecialDisabledNotes.ToString();// results.SpecialDisabledNotes;
			if(results.SalaryRange != null && results.SalaryRange.ToString() != "")
				aMWCustomerToBeUpdated.SalaryRange =int.Parse(results.SalaryRange.ToString());// results.SalaryRange;
			if(results.OccupationDesc != null && results.OccupationDesc.ToString() != "")
				aMWCustomerToBeUpdated.OccupationDesc =results.OccupationDesc.ToString();// results.OccupationDesc;
			if(results.CreatedSource != null && results.CreatedSource.ToString() != "")
				aMWCustomerToBeUpdated.CreatedSource =int.Parse(results.CreatedSource.ToString());// results.CreatedSource;
			if(results.ExternalID != null && results.ExternalID.ToString() != "")
				aMWCustomerToBeUpdated.ExternalID =results.ExternalID.ToString();// results.ExternalID;
			if(results.EnquiryRefId != null && results.EnquiryRefId.ToString() != "")
				aMWCustomerToBeUpdated.EnquiryRefId =long.Parse(results.EnquiryRefId.ToString());// results.EnquiryRefId;
			if(results.FaxNumber != null && results.FaxNumber.ToString() != "")
				aMWCustomerToBeUpdated.FaxNumber =results.FaxNumber.ToString();// results.FaxNumber;
			if(results.CustomerNameEN != null && results.CustomerNameEN.ToString() != "")
				aMWCustomerToBeUpdated.CustomerNameEN =results.CustomerNameEN.ToString();// results.CustomerNameEN;
			if(results.WorkPlace != null && results.WorkPlace.ToString() != "")
				aMWCustomerToBeUpdated.WorkPlace =results.WorkPlace.ToString();// results.WorkPlace;
			if(results.WorkAddress != null && results.WorkAddress.ToString() != "")
				aMWCustomerToBeUpdated.WorkAddress =results.WorkAddress.ToString();// results.WorkAddress;
			if(results.WorkEmail != null && results.WorkEmail.ToString() != "")
				aMWCustomerToBeUpdated.WorkEmail =results.WorkEmail.ToString();// results.WorkEmail;
			if(results.WorkTel != null && results.WorkTel.ToString() != "")
				aMWCustomerToBeUpdated.WorkTel =results.WorkTel.ToString();// results.WorkTel;
			if(results.WorkFax != null && results.WorkFax.ToString() != "")
				aMWCustomerToBeUpdated.WorkFax =results.WorkFax.ToString();// results.WorkFax;
			if(results.WorkPoBox != null && results.WorkPoBox.ToString() != "")
				aMWCustomerToBeUpdated.WorkPoBox =Decimal.Parse(results.WorkPoBox.ToString());// results.WorkPoBox;
			if(results.MailAddress != null && results.MailAddress.ToString() != "")
				aMWCustomerToBeUpdated.MailAddress =Decimal.Parse(results.MailAddress.ToString());// results.MailAddress;
			if(results.WorkMailAddress != null && results.WorkMailAddress.ToString() != "")
				aMWCustomerToBeUpdated.WorkMailAddress =Decimal.Parse(results.WorkMailAddress.ToString());// results.WorkMailAddress;
			if(results.WorkMailBox != null && results.WorkMailBox.ToString() != "")
				aMWCustomerToBeUpdated.WorkMailBox =Decimal.Parse(results.WorkMailBox.ToString());// results.WorkMailBox;
			if(results.MailBox != null && results.MailBox.ToString() != "")
				aMWCustomerToBeUpdated.MailBox =Decimal.Parse(results.MailBox.ToString());// results.MailBox;
			if(results.WorkMobile != null && results.WorkMobile.ToString() != "")
				aMWCustomerToBeUpdated.WorkMobile =results.WorkMobile.ToString();// results.WorkMobile;
			if(results.CRM_GUID != null && results.CRM_GUID.ToString() != "")
				aMWCustomerToBeUpdated.CRM_GUID =results.CRM_GUID.ToString();// results.CRM_GUID;
			if(results.Channel != null && results.Channel.ToString() != "")
				aMWCustomerToBeUpdated.Channel =int.Parse(results.Channel.ToString());// results.Channel;
			if(results.FemaleContactPersonPhoneNumber != null && results.FemaleContactPersonPhoneNumber.ToString() != "")
				aMWCustomerToBeUpdated.FemaleContactPersonPhoneNumber =results.FemaleContactPersonPhoneNumber.ToString();// results.FemaleContactPersonPhoneNumber;
			if(results.VerificationStatus != null && results.VerificationStatus.ToString() != "")
				aMWCustomerToBeUpdated.VerificationStatus =int.Parse(results.VerificationStatus.ToString());// results.VerificationStatus;
			if(results.VerifiedBy != null && results.VerifiedBy.ToString() != "")
				aMWCustomerToBeUpdated.VerifiedBy =results.VerifiedBy.ToString();// results.VerifiedBy;
			if(results.VerifiedDate != null && results.VerifiedDate.ToString() != "")
				aMWCustomerToBeUpdated.VerifiedDate =results.VerifiedDate;// results.VerifiedDate;
			if(results.VirtualAccountId != null && results.VirtualAccountId.ToString() != "")
				aMWCustomerToBeUpdated.VirtualAccountId =results.VirtualAccountId.ToString();// results.VirtualAccountId;
			if(results.AffiliateCode != null && results.AffiliateCode.ToString() != "")
				aMWCustomerToBeUpdated.AffiliateCode =results.AffiliateCode.ToString();// results.AffiliateCode;
			if(results.IBANNumber != null && results.IBANNumber.ToString() != "")
				aMWCustomerToBeUpdated.IBANNumber =results.IBANNumber.ToString();// results.IBANNumber;
			if(results.PromoCode != null && results.PromoCode.ToString() != "")
				aMWCustomerToBeUpdated.PromoCode =results.PromoCode.ToString();// results.PromoCode;
			if(results.WorkDateOfJoining != null && results.WorkDateOfJoining.ToString() != "")
				aMWCustomerToBeUpdated.WorkDateOfJoining =results.WorkDateOfJoining;// results.WorkDateOfJoining;
			if(results.IqamaExpiryDate != null && results.IqamaExpiryDate.ToString() != "")
				aMWCustomerToBeUpdated.IqamaExpiryDate =results.IqamaExpiryDate;// results.IqamaExpiryDate;

		return	Update(aMWCustomerToBeUpdated);
        }



		 //CustomerID = sample.CustomerID,
//FirstName = sample.FirstName,
//MiddleName = sample.MiddleName,
//LastName = sample.LastName,
//FamilyName = sample.FamilyName,
//Gender = sample.Gender,
//Nationality = sample.Nationality,
//DOBHijri = sample.DOBHijri,
//DOBGeorgian = sample.DOBGeorgian,
//IDNumber = sample.IDNumber,
//MobileNumber = sample.MobileNumber,
//PhoneNumber = sample.PhoneNumber,
//Street = sample.Street,
//POBOXNumber = sample.POBOXNumber,
//CountryRegion = sample.CountryRegion,
//City = sample.City,
//PostalCode = sample.PostalCode,
//Email = sample.Email,
//WorkingSector = sample.WorkingSector,
//Occupation = sample.Occupation,
//MonthlySalary = sample.MonthlySalary,
//OtherIncome = sample.OtherIncome,
//KnowVia = sample.KnowVia,
//Notes = sample.Notes,
//CompanyName = sample.CompanyName,
//LegalName = sample.LegalName,
//RegistrationNo = sample.RegistrationNo,
//COCNo = sample.COCNo,
//CompanyEmail = sample.CompanyEmail,
//CompanyWebsite = sample.CompanyWebsite,
//NatureofBusiness = sample.NatureofBusiness,
//Sector = sample.Sector,
//TotalEmployees = sample.TotalEmployees,
//TotalSaudiEmployees = sample.TotalSaudiEmployees,
//CompanyNoLabourOffice = sample.CompanyNoLabourOffice,
//CompanyActivity = sample.CompanyActivity,
//ContactPerson = sample.ContactPerson,
//Extension = sample.Extension,
//CustomerType = sample.CustomerType,
//ContactPersonEmail = sample.ContactPersonEmail,
//CustomerStatus = sample.CustomerStatus,
//CreatedDatetime = sample.CreatedDatetime,
//CreatedBy = sample.CreatedBy,
//ModifiedDatetime = sample.ModifiedDatetime,
//ModifiedBy = sample.ModifiedBy,
//DataAreaId = sample.DataAreaId,
//RecId = sample.RecId,
//RecVersion = sample.RecVersion,
//AccountManager = sample.AccountManager,
//Supervisor = sample.Supervisor,
//SalesPerson = sample.SalesPerson,
//NoofFamilyMembers = sample.NoofFamilyMembers,
//NoofWifes = sample.NoofWifes,
//OldAgePerson = sample.OldAgePerson,
//NoofOldAgePerson = sample.NoofOldAgePerson,
//OldAgePersonNotes = sample.OldAgePersonNotes,
//SpecialDisabled = sample.SpecialDisabled,
//NoofSpecialDisabled = sample.NoofSpecialDisabled,
//SpecialDisabledNotes = sample.SpecialDisabledNotes,
//SalaryRange = sample.SalaryRange,
//OccupationDesc = sample.OccupationDesc,
//CreatedSource = sample.CreatedSource,
//ExternalID = sample.ExternalID,
//EnquiryRefId = sample.EnquiryRefId,
//FaxNumber = sample.FaxNumber,
//CustomerNameEN = sample.CustomerNameEN,
//WorkPlace = sample.WorkPlace,
//WorkAddress = sample.WorkAddress,
//WorkEmail = sample.WorkEmail,
//WorkTel = sample.WorkTel,
//WorkFax = sample.WorkFax,
//WorkPoBox = sample.WorkPoBox,
//MailAddress = sample.MailAddress,
//WorkMailAddress = sample.WorkMailAddress,
//WorkMailBox = sample.WorkMailBox,
//MailBox = sample.MailBox,
//WorkMobile = sample.WorkMobile,
//CRM_GUID = sample.CRM_GUID,
//Channel = sample.Channel,
//FemaleContactPersonPhoneNumber = sample.FemaleContactPersonPhoneNumber,
//VerificationStatus = sample.VerificationStatus,
//VerifiedBy = sample.VerifiedBy,
//VerifiedDate = sample.VerifiedDate,
//VirtualAccountId = sample.VirtualAccountId,
//AffiliateCode = sample.AffiliateCode,
//IBANNumber = sample.IBANNumber,
//PromoCode = sample.PromoCode,
//WorkDateOfJoining = sample.WorkDateOfJoining,
//IqamaExpiryDate = sample.IqamaExpiryDate,
  //<label editable-text="sample.CustomerID"  e-name="CustomerID" e-form="rowform">
                                           //     {{sample.CustomerID}}
                                         //   </label>
										  //<label editable-text="sample.FirstName"  e-name="FirstName" e-form="rowform">
                                           //     {{sample.FirstName}}
                                         //   </label>
										  //<label editable-text="sample.MiddleName"  e-name="MiddleName" e-form="rowform">
                                           //     {{sample.MiddleName}}
                                         //   </label>
										  //<label editable-text="sample.LastName"  e-name="LastName" e-form="rowform">
                                           //     {{sample.LastName}}
                                         //   </label>
										  //<label editable-text="sample.FamilyName"  e-name="FamilyName" e-form="rowform">
                                           //     {{sample.FamilyName}}
                                         //   </label>
										  //<label editable-text="sample.Gender"  e-name="Gender" e-form="rowform">
                                           //     {{sample.Gender}}
                                         //   </label>
										  //<label editable-text="sample.Nationality"  e-name="Nationality" e-form="rowform">
                                           //     {{sample.Nationality}}
                                         //   </label>
										  //<label editable-text="sample.DOBHijri"  e-name="DOBHijri" e-form="rowform">
                                           //     {{sample.DOBHijri}}
                                         //   </label>
										  //<label editable-text="sample.DOBGeorgian"  e-name="DOBGeorgian" e-form="rowform">
                                           //     {{sample.DOBGeorgian}}
                                         //   </label>
										  //<label editable-text="sample.IDNumber"  e-name="IDNumber" e-form="rowform">
                                           //     {{sample.IDNumber}}
                                         //   </label>
										  //<label editable-text="sample.MobileNumber"  e-name="MobileNumber" e-form="rowform">
                                           //     {{sample.MobileNumber}}
                                         //   </label>
										  //<label editable-text="sample.PhoneNumber"  e-name="PhoneNumber" e-form="rowform">
                                           //     {{sample.PhoneNumber}}
                                         //   </label>
										  //<label editable-text="sample.Street"  e-name="Street" e-form="rowform">
                                           //     {{sample.Street}}
                                         //   </label>
										  //<label editable-text="sample.POBOXNumber"  e-name="POBOXNumber" e-form="rowform">
                                           //     {{sample.POBOXNumber}}
                                         //   </label>
										  //<label editable-text="sample.CountryRegion"  e-name="CountryRegion" e-form="rowform">
                                           //     {{sample.CountryRegion}}
                                         //   </label>
										  //<label editable-text="sample.City"  e-name="City" e-form="rowform">
                                           //     {{sample.City}}
                                         //   </label>
										  //<label editable-text="sample.PostalCode"  e-name="PostalCode" e-form="rowform">
                                           //     {{sample.PostalCode}}
                                         //   </label>
										  //<label editable-text="sample.Email"  e-name="Email" e-form="rowform">
                                           //     {{sample.Email}}
                                         //   </label>
										  //<label editable-text="sample.WorkingSector"  e-name="WorkingSector" e-form="rowform">
                                           //     {{sample.WorkingSector}}
                                         //   </label>
										  //<label editable-text="sample.Occupation"  e-name="Occupation" e-form="rowform">
                                           //     {{sample.Occupation}}
                                         //   </label>
										  //<label editable-text="sample.MonthlySalary"  e-name="MonthlySalary" e-form="rowform">
                                           //     {{sample.MonthlySalary}}
                                         //   </label>
										  //<label editable-text="sample.OtherIncome"  e-name="OtherIncome" e-form="rowform">
                                           //     {{sample.OtherIncome}}
                                         //   </label>
										  //<label editable-text="sample.KnowVia"  e-name="KnowVia" e-form="rowform">
                                           //     {{sample.KnowVia}}
                                         //   </label>
										  //<label editable-text="sample.Notes"  e-name="Notes" e-form="rowform">
                                           //     {{sample.Notes}}
                                         //   </label>
										  //<label editable-text="sample.CompanyName"  e-name="CompanyName" e-form="rowform">
                                           //     {{sample.CompanyName}}
                                         //   </label>
										  //<label editable-text="sample.LegalName"  e-name="LegalName" e-form="rowform">
                                           //     {{sample.LegalName}}
                                         //   </label>
										  //<label editable-text="sample.RegistrationNo"  e-name="RegistrationNo" e-form="rowform">
                                           //     {{sample.RegistrationNo}}
                                         //   </label>
										  //<label editable-text="sample.COCNo"  e-name="COCNo" e-form="rowform">
                                           //     {{sample.COCNo}}
                                         //   </label>
										  //<label editable-text="sample.CompanyEmail"  e-name="CompanyEmail" e-form="rowform">
                                           //     {{sample.CompanyEmail}}
                                         //   </label>
										  //<label editable-text="sample.CompanyWebsite"  e-name="CompanyWebsite" e-form="rowform">
                                           //     {{sample.CompanyWebsite}}
                                         //   </label>
										  //<label editable-text="sample.NatureofBusiness"  e-name="NatureofBusiness" e-form="rowform">
                                           //     {{sample.NatureofBusiness}}
                                         //   </label>
										  //<label editable-text="sample.Sector"  e-name="Sector" e-form="rowform">
                                           //     {{sample.Sector}}
                                         //   </label>
										  //<label editable-text="sample.TotalEmployees"  e-name="TotalEmployees" e-form="rowform">
                                           //     {{sample.TotalEmployees}}
                                         //   </label>
										  //<label editable-text="sample.TotalSaudiEmployees"  e-name="TotalSaudiEmployees" e-form="rowform">
                                           //     {{sample.TotalSaudiEmployees}}
                                         //   </label>
										  //<label editable-text="sample.CompanyNoLabourOffice"  e-name="CompanyNoLabourOffice" e-form="rowform">
                                           //     {{sample.CompanyNoLabourOffice}}
                                         //   </label>
										  //<label editable-text="sample.CompanyActivity"  e-name="CompanyActivity" e-form="rowform">
                                           //     {{sample.CompanyActivity}}
                                         //   </label>
										  //<label editable-text="sample.ContactPerson"  e-name="ContactPerson" e-form="rowform">
                                           //     {{sample.ContactPerson}}
                                         //   </label>
										  //<label editable-text="sample.Extension"  e-name="Extension" e-form="rowform">
                                           //     {{sample.Extension}}
                                         //   </label>
										  //<label editable-text="sample.CustomerType"  e-name="CustomerType" e-form="rowform">
                                           //     {{sample.CustomerType}}
                                         //   </label>
										  //<label editable-text="sample.ContactPersonEmail"  e-name="ContactPersonEmail" e-form="rowform">
                                           //     {{sample.ContactPersonEmail}}
                                         //   </label>
										  //<label editable-text="sample.CustomerStatus"  e-name="CustomerStatus" e-form="rowform">
                                           //     {{sample.CustomerStatus}}
                                         //   </label>
										  //<label editable-text="sample.CreatedDatetime"  e-name="CreatedDatetime" e-form="rowform">
                                           //     {{sample.CreatedDatetime}}
                                         //   </label>
										  //<label editable-text="sample.CreatedBy"  e-name="CreatedBy" e-form="rowform">
                                           //     {{sample.CreatedBy}}
                                         //   </label>
										  //<label editable-text="sample.ModifiedDatetime"  e-name="ModifiedDatetime" e-form="rowform">
                                           //     {{sample.ModifiedDatetime}}
                                         //   </label>
										  //<label editable-text="sample.ModifiedBy"  e-name="ModifiedBy" e-form="rowform">
                                           //     {{sample.ModifiedBy}}
                                         //   </label>
										  //<label editable-text="sample.DataAreaId"  e-name="DataAreaId" e-form="rowform">
                                           //     {{sample.DataAreaId}}
                                         //   </label>
										  //<label editable-text="sample.RecId"  e-name="RecId" e-form="rowform">
                                           //     {{sample.RecId}}
                                         //   </label>
										  //<label editable-text="sample.RecVersion"  e-name="RecVersion" e-form="rowform">
                                           //     {{sample.RecVersion}}
                                         //   </label>
										  //<label editable-text="sample.AccountManager"  e-name="AccountManager" e-form="rowform">
                                           //     {{sample.AccountManager}}
                                         //   </label>
										  //<label editable-text="sample.Supervisor"  e-name="Supervisor" e-form="rowform">
                                           //     {{sample.Supervisor}}
                                         //   </label>
										  //<label editable-text="sample.SalesPerson"  e-name="SalesPerson" e-form="rowform">
                                           //     {{sample.SalesPerson}}
                                         //   </label>
										  //<label editable-text="sample.NoofFamilyMembers"  e-name="NoofFamilyMembers" e-form="rowform">
                                           //     {{sample.NoofFamilyMembers}}
                                         //   </label>
										  //<label editable-text="sample.NoofWifes"  e-name="NoofWifes" e-form="rowform">
                                           //     {{sample.NoofWifes}}
                                         //   </label>
										  //<label editable-text="sample.OldAgePerson"  e-name="OldAgePerson" e-form="rowform">
                                           //     {{sample.OldAgePerson}}
                                         //   </label>
										  //<label editable-text="sample.NoofOldAgePerson"  e-name="NoofOldAgePerson" e-form="rowform">
                                           //     {{sample.NoofOldAgePerson}}
                                         //   </label>
										  //<label editable-text="sample.OldAgePersonNotes"  e-name="OldAgePersonNotes" e-form="rowform">
                                           //     {{sample.OldAgePersonNotes}}
                                         //   </label>
										  //<label editable-text="sample.SpecialDisabled"  e-name="SpecialDisabled" e-form="rowform">
                                           //     {{sample.SpecialDisabled}}
                                         //   </label>
										  //<label editable-text="sample.NoofSpecialDisabled"  e-name="NoofSpecialDisabled" e-form="rowform">
                                           //     {{sample.NoofSpecialDisabled}}
                                         //   </label>
										  //<label editable-text="sample.SpecialDisabledNotes"  e-name="SpecialDisabledNotes" e-form="rowform">
                                           //     {{sample.SpecialDisabledNotes}}
                                         //   </label>
										  //<label editable-text="sample.SalaryRange"  e-name="SalaryRange" e-form="rowform">
                                           //     {{sample.SalaryRange}}
                                         //   </label>
										  //<label editable-text="sample.OccupationDesc"  e-name="OccupationDesc" e-form="rowform">
                                           //     {{sample.OccupationDesc}}
                                         //   </label>
										  //<label editable-text="sample.CreatedSource"  e-name="CreatedSource" e-form="rowform">
                                           //     {{sample.CreatedSource}}
                                         //   </label>
										  //<label editable-text="sample.ExternalID"  e-name="ExternalID" e-form="rowform">
                                           //     {{sample.ExternalID}}
                                         //   </label>
										  //<label editable-text="sample.EnquiryRefId"  e-name="EnquiryRefId" e-form="rowform">
                                           //     {{sample.EnquiryRefId}}
                                         //   </label>
										  //<label editable-text="sample.FaxNumber"  e-name="FaxNumber" e-form="rowform">
                                           //     {{sample.FaxNumber}}
                                         //   </label>
										  //<label editable-text="sample.CustomerNameEN"  e-name="CustomerNameEN" e-form="rowform">
                                           //     {{sample.CustomerNameEN}}
                                         //   </label>
										  //<label editable-text="sample.WorkPlace"  e-name="WorkPlace" e-form="rowform">
                                           //     {{sample.WorkPlace}}
                                         //   </label>
										  //<label editable-text="sample.WorkAddress"  e-name="WorkAddress" e-form="rowform">
                                           //     {{sample.WorkAddress}}
                                         //   </label>
										  //<label editable-text="sample.WorkEmail"  e-name="WorkEmail" e-form="rowform">
                                           //     {{sample.WorkEmail}}
                                         //   </label>
										  //<label editable-text="sample.WorkTel"  e-name="WorkTel" e-form="rowform">
                                           //     {{sample.WorkTel}}
                                         //   </label>
										  //<label editable-text="sample.WorkFax"  e-name="WorkFax" e-form="rowform">
                                           //     {{sample.WorkFax}}
                                         //   </label>
										  //<label editable-text="sample.WorkPoBox"  e-name="WorkPoBox" e-form="rowform">
                                           //     {{sample.WorkPoBox}}
                                         //   </label>
										  //<label editable-text="sample.MailAddress"  e-name="MailAddress" e-form="rowform">
                                           //     {{sample.MailAddress}}
                                         //   </label>
										  //<label editable-text="sample.WorkMailAddress"  e-name="WorkMailAddress" e-form="rowform">
                                           //     {{sample.WorkMailAddress}}
                                         //   </label>
										  //<label editable-text="sample.WorkMailBox"  e-name="WorkMailBox" e-form="rowform">
                                           //     {{sample.WorkMailBox}}
                                         //   </label>
										  //<label editable-text="sample.MailBox"  e-name="MailBox" e-form="rowform">
                                           //     {{sample.MailBox}}
                                         //   </label>
										  //<label editable-text="sample.WorkMobile"  e-name="WorkMobile" e-form="rowform">
                                           //     {{sample.WorkMobile}}
                                         //   </label>
										  //<label editable-text="sample.CRM_GUID"  e-name="CRM_GUID" e-form="rowform">
                                           //     {{sample.CRM_GUID}}
                                         //   </label>
										  //<label editable-text="sample.Channel"  e-name="Channel" e-form="rowform">
                                           //     {{sample.Channel}}
                                         //   </label>
										  //<label editable-text="sample.FemaleContactPersonPhoneNumber"  e-name="FemaleContactPersonPhoneNumber" e-form="rowform">
                                           //     {{sample.FemaleContactPersonPhoneNumber}}
                                         //   </label>
										  //<label editable-text="sample.VerificationStatus"  e-name="VerificationStatus" e-form="rowform">
                                           //     {{sample.VerificationStatus}}
                                         //   </label>
										  //<label editable-text="sample.VerifiedBy"  e-name="VerifiedBy" e-form="rowform">
                                           //     {{sample.VerifiedBy}}
                                         //   </label>
										  //<label editable-text="sample.VerifiedDate"  e-name="VerifiedDate" e-form="rowform">
                                           //     {{sample.VerifiedDate}}
                                         //   </label>
										  //<label editable-text="sample.VirtualAccountId"  e-name="VirtualAccountId" e-form="rowform">
                                           //     {{sample.VirtualAccountId}}
                                         //   </label>
										  //<label editable-text="sample.AffiliateCode"  e-name="AffiliateCode" e-form="rowform">
                                           //     {{sample.AffiliateCode}}
                                         //   </label>
										  //<label editable-text="sample.IBANNumber"  e-name="IBANNumber" e-form="rowform">
                                           //     {{sample.IBANNumber}}
                                         //   </label>
										  //<label editable-text="sample.PromoCode"  e-name="PromoCode" e-form="rowform">
                                           //     {{sample.PromoCode}}
                                         //   </label>
										  //<label editable-text="sample.WorkDateOfJoining"  e-name="WorkDateOfJoining" e-form="rowform">
                                           //     {{sample.WorkDateOfJoining}}
                                         //   </label>
										  //<label editable-text="sample.IqamaExpiryDate"  e-name="IqamaExpiryDate" e-form="rowform">
                                           //     {{sample.IqamaExpiryDate}}
                                         //   </label>
										 
        public void Delete(Arco.Model.Entity.AMWCustomer aMWCustomerToBeDeleted)
        {
			int numberOfAffectedRows = 0;
            //Validate Input
            if (aMWCustomerToBeDeleted == null)
                throw (new ArgumentNullException("aMWCustomerToBeDeleted"));

            // Validate Primary key value
            if (aMWCustomerToBeDeleted.CustomerID.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("CustomerID");

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(aMWCustomerToBeDeleted, PersistenceType.Delete);
				OnDeleting(aMWCustomerToBeDeleted);

			
				_context.Remove(aMWCustomerToBeDeleted);
            
				numberOfAffectedRows = _context.UnitOfWork.Commit();
				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException(string.Format("AMWCustomer (CustomerID={0}) has not been deleted!", aMWCustomerToBeDeleted.CustomerID));
            
				//Apply business workflow
				OnDeleted(aMWCustomerToBeDeleted);
				OnSaved(aMWCustomerToBeDeleted, PersistenceType.Delete);

			//	scope.Complete();
			//}

        }

        public void Delete(List<string> customerIDsToDelete)
        {
            //Validate Input
            foreach (string customerID in customerIDsToDelete)
                if (customerID.IsInvalidKey())
                    BizHelper.ThrowErrorForInvalidDataKey("CustomerID");

            foreach (string customerID in customerIDsToDelete)
            {
				Arco.Model.Entity.AMWCustomer aMWCustomerToBeDeleted = new Arco.Model.Entity.AMWCustomer { CustomerID = customerID, DataAreaId = CurrentSession.DataAreaId };

				Delete(aMWCustomerToBeDeleted);
            }

        }

 
		//Delete by RecId
		public void Delete(List<long> RecIdsToDelete)
        {
            //Validate Input
            foreach (long RecId in RecIdsToDelete)
                if (RecId.IsInvalidKey())
                    BizHelper.ThrowErrorForInvalidDataKey("RecId");

            foreach (long RecId in RecIdsToDelete)
				Delete(GetByRecId(RecId));
        }

        #endregion
	

       
        public Arco.Model.Entity.AMWCustomer Initialize()
        {
			Arco.Model.Entity.AMWCustomer aMWCustomer = new Arco.Model.Entity.AMWCustomer();

			return aMWCustomer;
		}
	}
}
