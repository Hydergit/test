using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Transactions;
using Newtonsoft.Json;
using Arco.Core.AppBase;
//using Arco.Model.Interface.Repository.Single;
using Arco.Model.Entity;
using Arco.Framework.Utility;
//using Arco.Model.Interface.Business;
using Arco.Core.AppInterface;
using Arco.Core.Infrastructure;
using Arco.Framework.Query;
namespace Arco.Business.Record
{



 public partial class EmployeeMasterRepository : EFRepositoryBase
    {
	
		    public EmployeeMasterRepository()
            : base("arco")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<EmployeeMasterRepository>(null);
        }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
		modelBuilder.Configurations.Add(new EmployeeMasterConfiguration());
          
			}

	}


    public partial class EmployeeMasterBLL : BLLBase
    {
		
        public EmployeeMasterBLL() 
			: base(new EmployeeMasterRepository(), false) {
			_lEmployeeMaster =new Arco.Model.Entity.EmployeeMaster();
			 }


			 
        public EmployeeMasterBLL(IRepository _IRepository)
			 : base(_IRepository, false) {
			_lEmployeeMaster =new Arco.Model.Entity.EmployeeMaster();
			 }



			//   public EmployeeMasterBLL(IRepository context, Func<EmployeeMaster, bool> predicate, bool disposeContext = false) 
			//: base(context, disposeContext) {
			
              // _lEmployeeMaster= GetAll().Where(predicate:predicate).FirstOrDefault();
        
			 //}
        		
				public Arco.Model.Entity.EmployeeMaster _lEmployeeMaster    { get; set; }

				

		#region Property

			
		public string EmployeeId {get{return this._lEmployeeMaster.EmployeeId;} set{  this._lEmployeeMaster.EmployeeId = value; }}

		
			
		public string Name {get{return this._lEmployeeMaster.Name;} set{  this._lEmployeeMaster.Name = value; }}

		
			
		public DateTime? CreatedDatetime {get{return this._lEmployeeMaster.CreatedDatetime;} set{  this._lEmployeeMaster.CreatedDatetime = value; }}

		
			
		public string CreatedBy {get{return this._lEmployeeMaster.CreatedBy;} set{  this._lEmployeeMaster.CreatedBy = value; }}

		
			
		public DateTime? ModifiedDatetime {get{return this._lEmployeeMaster.ModifiedDatetime;} set{  this._lEmployeeMaster.ModifiedDatetime = value; }}

		
			
		public string ModifiedBy {get{return this._lEmployeeMaster.ModifiedBy;} set{  this._lEmployeeMaster.ModifiedBy = value; }}

		
			
		public string DataAreaId {get{return this._lEmployeeMaster.DataAreaId;} set{  this._lEmployeeMaster.DataAreaId = value; }}

		
			
		public long RecId {get{return this._lEmployeeMaster.RecId;} set{  this._lEmployeeMaster.RecId = value; }}

		
			
		public string FirstNameEnglish {get{return this._lEmployeeMaster.FirstNameEnglish;} set{  this._lEmployeeMaster.FirstNameEnglish = value; }}

		
			
		public string MiddleNameEnglish {get{return this._lEmployeeMaster.MiddleNameEnglish;} set{  this._lEmployeeMaster.MiddleNameEnglish = value; }}

		
			
		public string LastNameEnglish {get{return this._lEmployeeMaster.LastNameEnglish;} set{  this._lEmployeeMaster.LastNameEnglish = value; }}

		
			
		public string FirstNameArabic {get{return this._lEmployeeMaster.FirstNameArabic;} set{  this._lEmployeeMaster.FirstNameArabic = value; }}

		
			
		public string MiddleNameArabic {get{return this._lEmployeeMaster.MiddleNameArabic;} set{  this._lEmployeeMaster.MiddleNameArabic = value; }}

		
			
		public string LastNameArabic {get{return this._lEmployeeMaster.LastNameArabic;} set{  this._lEmployeeMaster.LastNameArabic = value; }}

		
			
		public string FatherName {get{return this._lEmployeeMaster.FatherName;} set{  this._lEmployeeMaster.FatherName = value; }}

		
			
		public string MotherName {get{return this._lEmployeeMaster.MotherName;} set{  this._lEmployeeMaster.MotherName = value; }}

		
			
		public int? Gender {get{return this._lEmployeeMaster.Gender;} set{  this._lEmployeeMaster.Gender = value; }}

		
			
		public DateTime? DOB {get{return this._lEmployeeMaster.DOB;} set{  this._lEmployeeMaster.DOB = value; }}

		
			
		public byte[] Photo {get{return this._lEmployeeMaster.Photo;} set{  this._lEmployeeMaster.Photo = value; }}

		
			
		public string Nationality {get{return this._lEmployeeMaster.Nationality;} set{  this._lEmployeeMaster.Nationality = value; }}

		
			
		public int? MaritalStatus {get{return this._lEmployeeMaster.MaritalStatus;} set{  this._lEmployeeMaster.MaritalStatus = value; }}

		
			
		public string NativeLanguage {get{return this._lEmployeeMaster.NativeLanguage;} set{  this._lEmployeeMaster.NativeLanguage = value; }}

		
			
		public int? BloodGroup {get{return this._lEmployeeMaster.BloodGroup;} set{  this._lEmployeeMaster.BloodGroup = value; }}

		
			
		public string VisaProfession {get{return this._lEmployeeMaster.VisaProfession;} set{  this._lEmployeeMaster.VisaProfession = value; }}

		
			
		public DateTime? DOJ {get{return this._lEmployeeMaster.DOJ;} set{  this._lEmployeeMaster.DOJ = value; }}

		
			
		public int? Recordtype {get{return this._lEmployeeMaster.Recordtype;} set{  this._lEmployeeMaster.Recordtype = value; }}

		
			
		public string DependentEmployee {get{return this._lEmployeeMaster.DependentEmployee;} set{  this._lEmployeeMaster.DependentEmployee = value; }}

		
			
		public int? EmployeeRelation {get{return this._lEmployeeMaster.EmployeeRelation;} set{  this._lEmployeeMaster.EmployeeRelation = value; }}

		
			
		public byte[] RecVersion {get{return this._lEmployeeMaster.RecVersion;} set{  this._lEmployeeMaster.RecVersion = value; }}

		
			
		public int? ResidentType {get{return this._lEmployeeMaster.ResidentType;} set{  this._lEmployeeMaster.ResidentType = value; }}

		
			
		public int? Religion {get{return this._lEmployeeMaster.Religion;} set{  this._lEmployeeMaster.Religion = value; }}

		
			
		public string CountryEntranceNo {get{return this._lEmployeeMaster.CountryEntranceNo;} set{  this._lEmployeeMaster.CountryEntranceNo = value; }}

		
			
		public DateTime? CountryEntranceDate {get{return this._lEmployeeMaster.CountryEntranceDate;} set{  this._lEmployeeMaster.CountryEntranceDate = value; }}

		
			
		public string CountryEntranceGate {get{return this._lEmployeeMaster.CountryEntranceGate;} set{  this._lEmployeeMaster.CountryEntranceGate = value; }}

		
			
		public DateTime? DateofJoingHJ {get{return this._lEmployeeMaster.DateofJoingHJ;} set{  this._lEmployeeMaster.DateofJoingHJ = value; }}

		
			
		public int? AnnualLeave {get{return this._lEmployeeMaster.AnnualLeave;} set{  this._lEmployeeMaster.AnnualLeave = value; }}

		
			
		public string LoanGroup {get{return this._lEmployeeMaster.LoanGroup;} set{  this._lEmployeeMaster.LoanGroup = value; }}

		
			
		public decimal? MaxCashLimit {get{return this._lEmployeeMaster.MaxCashLimit;} set{  this._lEmployeeMaster.MaxCashLimit = value; }}

		
			
		public int? PayrollMode {get{return this._lEmployeeMaster.PayrollMode;} set{  this._lEmployeeMaster.PayrollMode = value; }}

		
			
		public decimal? BasicPay {get{return this._lEmployeeMaster.BasicPay;} set{  this._lEmployeeMaster.BasicPay = value; }}

		
			
		public int? FamilyVisaStatus {get{return this._lEmployeeMaster.FamilyVisaStatus;} set{  this._lEmployeeMaster.FamilyVisaStatus = value; }}

		
			
		public int? EmployeeType {get{return this._lEmployeeMaster.EmployeeType;} set{  this._lEmployeeMaster.EmployeeType = value; }}

		
			
		public string WorkTimeTemplate {get{return this._lEmployeeMaster.WorkTimeTemplate;} set{  this._lEmployeeMaster.WorkTimeTemplate = value; }}

		
			
		public int? EmployeeStatus {get{return this._lEmployeeMaster.EmployeeStatus;} set{  this._lEmployeeMaster.EmployeeStatus = value; }}

		
			
		public string AddressKSA {get{return this._lEmployeeMaster.AddressKSA;} set{  this._lEmployeeMaster.AddressKSA = value; }}

		
			
		public string AddressHome {get{return this._lEmployeeMaster.AddressHome;} set{  this._lEmployeeMaster.AddressHome = value; }}

		
			
		public int? WorkStatus {get{return this._lEmployeeMaster.WorkStatus;} set{  this._lEmployeeMaster.WorkStatus = value; }}

		
			
		public int? ProblemStatus {get{return this._lEmployeeMaster.ProblemStatus;} set{  this._lEmployeeMaster.ProblemStatus = value; }}

		
			
		public string PositionID {get{return this._lEmployeeMaster.PositionID;} set{  this._lEmployeeMaster.PositionID = value; }}

		
			
		public string OrganizationUnitID {get{return this._lEmployeeMaster.OrganizationUnitID;} set{  this._lEmployeeMaster.OrganizationUnitID = value; }}

		
			
		public DateTime? StartSalaryDate {get{return this._lEmployeeMaster.StartSalaryDate;} set{  this._lEmployeeMaster.StartSalaryDate = value; }}

		
			
		public decimal? HeightinCM {get{return this._lEmployeeMaster.HeightinCM;} set{  this._lEmployeeMaster.HeightinCM = value; }}

		
			
		public decimal? HeightinFeet {get{return this._lEmployeeMaster.HeightinFeet;} set{  this._lEmployeeMaster.HeightinFeet = value; }}

		
			
		public decimal? WeightinKg {get{return this._lEmployeeMaster.WeightinKg;} set{  this._lEmployeeMaster.WeightinKg = value; }}

		
			
		public decimal? WeightinPound {get{return this._lEmployeeMaster.WeightinPound;} set{  this._lEmployeeMaster.WeightinPound = value; }}

		
			
		public int? Education {get{return this._lEmployeeMaster.Education;} set{  this._lEmployeeMaster.Education = value; }}

		
			
		public int? TotalExperience {get{return this._lEmployeeMaster.TotalExperience;} set{  this._lEmployeeMaster.TotalExperience = value; }}

		
			
		public int? GCCExperience {get{return this._lEmployeeMaster.GCCExperience;} set{  this._lEmployeeMaster.GCCExperience = value; }}

		
			
		public int? SaudiExperience {get{return this._lEmployeeMaster.SaudiExperience;} set{  this._lEmployeeMaster.SaudiExperience = value; }}

		
			
		public string Remarks {get{return this._lEmployeeMaster.Remarks;} set{  this._lEmployeeMaster.Remarks = value; }}

		
			
		public string SSICalculationID {get{return this._lEmployeeMaster.SSICalculationID;} set{  this._lEmployeeMaster.SSICalculationID = value; }}

		
			
		public int? PaymentType {get{return this._lEmployeeMaster.PaymentType;} set{  this._lEmployeeMaster.PaymentType = value; }}

		
			
		public string EmployeeStatusID {get{return this._lEmployeeMaster.EmployeeStatusID;} set{  this._lEmployeeMaster.EmployeeStatusID = value; }}

		
			
		public string MobileNumber {get{return this._lEmployeeMaster.MobileNumber;} set{  this._lEmployeeMaster.MobileNumber = value; }}

		
			
		public string Dimensions {get{return this._lEmployeeMaster.Dimensions;} set{  this._lEmployeeMaster.Dimensions = value; }}

		
			
		public string project {get{return this._lEmployeeMaster.project;} set{  this._lEmployeeMaster.project = value; }}

		
			
		public string iqamanumber {get{return this._lEmployeeMaster.iqamanumber;} set{  this._lEmployeeMaster.iqamanumber = value; }}

		
			
		public string passportnumber {get{return this._lEmployeeMaster.passportnumber;} set{  this._lEmployeeMaster.passportnumber = value; }}

		
			
		public DateTime? ResignDate {get{return this._lEmployeeMaster.ResignDate;} set{  this._lEmployeeMaster.ResignDate = value; }}

		
			
		public string ResignReason {get{return this._lEmployeeMaster.ResignReason;} set{  this._lEmployeeMaster.ResignReason = value; }}

		
			
		public string ExitReasonTypeID {get{return this._lEmployeeMaster.ExitReasonTypeID;} set{  this._lEmployeeMaster.ExitReasonTypeID = value; }}

		
			
		public string ExitTypeID {get{return this._lEmployeeMaster.ExitTypeID;} set{  this._lEmployeeMaster.ExitTypeID = value; }}

		
			
		public string ExitApprovalBy {get{return this._lEmployeeMaster.ExitApprovalBy;} set{  this._lEmployeeMaster.ExitApprovalBy = value; }}

		
			
		public DateTime? ExitApprovalDate {get{return this._lEmployeeMaster.ExitApprovalDate;} set{  this._lEmployeeMaster.ExitApprovalDate = value; }}

		
			
		public string ExitApprovalRemarks {get{return this._lEmployeeMaster.ExitApprovalRemarks;} set{  this._lEmployeeMaster.ExitApprovalRemarks = value; }}

		
			
		public long? EoSprofileID {get{return this._lEmployeeMaster.EoSprofileID;} set{  this._lEmployeeMaster.EoSprofileID = value; }}

		
			
		public string Email {get{return this._lEmployeeMaster.Email;} set{  this._lEmployeeMaster.Email = value; }}

		
			
		public string RegistrationNo {get{return this._lEmployeeMaster.RegistrationNo;} set{  this._lEmployeeMaster.RegistrationNo = value; }}

		
			
		public DateTime? ResignRequestDate {get{return this._lEmployeeMaster.ResignRequestDate;} set{  this._lEmployeeMaster.ResignRequestDate = value; }}

		
			
		public string CurrentContractNumber {get{return this._lEmployeeMaster.CurrentContractNumber;} set{  this._lEmployeeMaster.CurrentContractNumber = value; }}

		
			
		public string CurrentCustomerID {get{return this._lEmployeeMaster.CurrentCustomerID;} set{  this._lEmployeeMaster.CurrentCustomerID = value; }}

		
			
		public string IqamaProfessionId {get{return this._lEmployeeMaster.IqamaProfessionId;} set{  this._lEmployeeMaster.IqamaProfessionId = value; }}

		
			
		public string CandidateID {get{return this._lEmployeeMaster.CandidateID;} set{  this._lEmployeeMaster.CandidateID = value; }}

		
			
		public bool? CanSpeakArabic {get{return this._lEmployeeMaster.CanSpeakArabic;} set{  this._lEmployeeMaster.CanSpeakArabic = value; }}

		
			
		public bool? CanSpeakEnglish {get{return this._lEmployeeMaster.CanSpeakEnglish;} set{  this._lEmployeeMaster.CanSpeakEnglish = value; }}

		
			
		public string OtherLanguage {get{return this._lEmployeeMaster.OtherLanguage;} set{  this._lEmployeeMaster.OtherLanguage = value; }}

		
			
		public bool? CleaningHouses {get{return this._lEmployeeMaster.CleaningHouses;} set{  this._lEmployeeMaster.CleaningHouses = value; }}

		
			
		public bool? ChildCare {get{return this._lEmployeeMaster.ChildCare;} set{  this._lEmployeeMaster.ChildCare = value; }}

		
			
		public bool? OldageCare {get{return this._lEmployeeMaster.OldageCare;} set{  this._lEmployeeMaster.OldageCare = value; }}

		
			
		public bool? Cooking {get{return this._lEmployeeMaster.Cooking;} set{  this._lEmployeeMaster.Cooking = value; }}

		
			
		public bool? isActive {get{return this._lEmployeeMaster.isActive;} set{  this._lEmployeeMaster.isActive = value; }}

		
			
		public int? PhysicalStatus {get{return this._lEmployeeMaster.PhysicalStatus;} set{  this._lEmployeeMaster.PhysicalStatus = value; }}

		
			
		public bool? isIdCardCreated {get{return this._lEmployeeMaster.isIdCardCreated;} set{  this._lEmployeeMaster.isIdCardCreated = value; }}

		
			
		public byte[] BarCodeImage {get{return this._lEmployeeMaster.BarCodeImage;} set{  this._lEmployeeMaster.BarCodeImage = value; }}

		
			
		public int? IdCardProcess {get{return this._lEmployeeMaster.IdCardProcess;} set{  this._lEmployeeMaster.IdCardProcess = value; }}

		
			
		public int? OtherExperience {get{return this._lEmployeeMaster.OtherExperience;} set{  this._lEmployeeMaster.OtherExperience = value; }}

		
			
		public string StatuslikeMoqueem {get{return this._lEmployeeMaster.StatuslikeMoqueem;} set{  this._lEmployeeMaster.StatuslikeMoqueem = value; }}

		
			
		public int? MoqueemStatus {get{return this._lEmployeeMaster.MoqueemStatus;} set{  this._lEmployeeMaster.MoqueemStatus = value; }}

		
			
		public string CRM_GUID {get{return this._lEmployeeMaster.CRM_GUID;} set{  this._lEmployeeMaster.CRM_GUID = value; }}

		
			
		public int? Channel {get{return this._lEmployeeMaster.Channel;} set{  this._lEmployeeMaster.Channel = value; }}

		
			
		public bool? IsArcoDrivingLicenseSponsor {get{return this._lEmployeeMaster.IsArcoDrivingLicenseSponsor;} set{  this._lEmployeeMaster.IsArcoDrivingLicenseSponsor = value; }}

		
			
		public string JobSpecification {get{return this._lEmployeeMaster.JobSpecification;} set{  this._lEmployeeMaster.JobSpecification = value; }}

		
				#endregion
				
		
	   #region GET

	    public IQueryable<Arco.Model.Entity.EmployeeMaster> GetAsQueryAble()
        {
            return _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>();
        }

	   public Arco.Model.Entity.EmployeeMaster GetByEmployeeId(string employeeId)
       {
            //Validate Input
            if (employeeId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("employeeId");
            return _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>().FirstOrDefault(employeeMaster => employeeMaster.EmployeeId == employeeId);
       }

	    
        public Arco.Model.Entity.EmployeeMaster GetByRecId(long RecId)
        {
            //Validate Input
            if (RecId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("RecId");
            return _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>().FirstOrDefault(employeeMaster => employeeMaster.RecId == RecId);
        }
		
		public List<Arco.Model.Entity.EmployeeMaster> GetAll(string orderBy = default(string))
        {
			if (string.IsNullOrEmpty(orderBy))
		            orderBy = "EmployeeId";
            return _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>().DynamicOrderBy(orderBy).ToList();
        }

        public List<Arco.Model.Entity.EmployeeMaster> GetAllPaged(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "EmployeeId";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>()
                    .DynamicOrderBy(orderBy)
                    .Skip(startRowIndex)
                    .Take(maximumRows)
                    .ToList();
        }



		 public QueryResult GetPaged(QueryCriteria _QueryCriteria)
        {

            int PageNo = 1;
            int PageSize = 20;

            if (_QueryCriteria!=null && _QueryCriteria.Pagging!=null)
            {
              PageNo=  _QueryCriteria.Pagging.PageNo;
              PageSize = _QueryCriteria.Pagging.PageSize;
            }
            return QueryExtension.CreatePagedResults(_context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>(), PageNo, PageSize);
           
        }

		public QueryResult GetList(QueryCriteria _QueryCriteria)
         {
             return QueryExtension.CreatePagedWithFilterResults(_context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>(),_QueryCriteria);

         }

		    public List<Arco.Model.Entity.EmployeeMaster> GetAllSearchDynamic(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string[] filterBy = default(string[]), string[] condition = default(string[]), string[] filterValue = default(string[]))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "EmployeeId";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>()
					.DynamicWhereQ(filterBy, condition, filterValue)
                    .DynamicOrderBy(orderBy)
                    .ToList();
        }

        public List<Arco.Model.Entity.EmployeeMaster> GetAllSearchPaged(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string filterBy = default(string), string condition = "Equals", object filterValue = default(object))
        {
            if (string.IsNullOrEmpty(orderBy))
                orderBy = "EmployeeId";
				
			if (startRowIndex < 0) 
				throw (new ArgumentOutOfRangeException("startRowIndex"));
				
			if (maximumRows < 0) 
				throw (new ArgumentOutOfRangeException("maximumRows"));
			
            return  _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>()
					.DynamicWhere(filterBy, condition, filterValue)
                    .DynamicOrderBy(orderBy)
                    .Skip(startRowIndex)
                    .Take(maximumRows)
                    .ToList();
        }

		public int GetTotalCountForAllSearch(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int), string filterBy = default(string), string condition = "Equals", object filterValue = default(object))
        {
            return  _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>().DynamicWhere(filterBy, condition, filterValue).Count();
        }

        public int GetTotalCountForAll(string orderBy = default(string), int startRowIndex = default(int), int maximumRows = default(int))
        {       
           return _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>().Count();                     
        }
        
		#endregion

		#region Persistence 

		

		public Arco.Model.Entity.EmployeeMaster Insert()
        {
		if (_lEmployeeMaster == null)
                throw (new ArgumentNullException("newEmployeeMaster"));

			return	Insert(_lEmployeeMaster);

		}



        public virtual Arco.Model.Entity.EmployeeMaster Insert(Arco.Model.Entity.EmployeeMaster newEmployeeMaster)
        {
			int numberOfAffectedRows = 0;

            // Validate Parameters 
            if (newEmployeeMaster == null)
                throw (new ArgumentNullException("newEmployeeMaster"));

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(newEmployeeMaster, PersistenceType.Create);
				OnInserting(newEmployeeMaster);

				newEmployeeMaster.DataAreaId = CurrentSession.DataAreaId;
				newEmployeeMaster.CreatedBy = CurrentSession.UserId;
				newEmployeeMaster.CreatedDatetime = DateTime.UtcNow;
				newEmployeeMaster.ModifiedBy = CurrentSession.UserId;
				newEmployeeMaster.ModifiedDatetime = DateTime.UtcNow;



				
			// Validate Primary key value
			try{
			string primary=newEmployeeMaster.EmployeeId.ToString();
			if (primary.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("employeeId");

				}
				catch(Exception ex)
				{
				 BizHelper.ThrowErrorForInvalidDataKey("employeeId");
				}



				try
				{
					_context.Add<Arco.Model.Entity.EmployeeMaster>(newEmployeeMaster);
					numberOfAffectedRows = _context.UnitOfWork.Commit();
				}
				catch (DbEntityValidationException ex)
				{
					BizHelper.ThrowDbEntityValidationException(ex);
				}

				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException("No employeeMaster created!");

				// Apply business workflow
				OnInserted(newEmployeeMaster);
				OnSaved(newEmployeeMaster, PersistenceType.Create);

				//scope.Complete();
			//}
            return newEmployeeMaster;
        }
		
        public virtual Arco.Model.Entity.EmployeeMaster Insert(string _employeeId, string _dataAreaId, long _recId, string _name = default(string), DateTime _createdDatetime = default(DateTime), string _createdBy = default(string), DateTime _modifiedDatetime = default(DateTime), string _modifiedBy = default(string), string _firstNameEnglish = default(string), string _middleNameEnglish = default(string), string _lastNameEnglish = default(string), string _firstNameArabic = default(string), string _middleNameArabic = default(string), string _lastNameArabic = default(string), string _fatherName = default(string), string _motherName = default(string), int _gender = default(int), DateTime _dOB = default(DateTime), byte[] _photo = default(byte[]), string _nationality = default(string), int _maritalStatus = default(int), string _nativeLanguage = default(string), int _bloodGroup = default(int), string _visaProfession = default(string), DateTime _dOJ = default(DateTime), int _recordtype = default(int), string _dependentEmployee = default(string), int _employeeRelation = default(int), byte[] _recVersion = default(byte[]), int _residentType = default(int), int _religion = default(int), string _countryEntranceNo = default(string), DateTime _countryEntranceDate = default(DateTime), string _countryEntranceGate = default(string), DateTime _dateofJoingHJ = default(DateTime), int _annualLeave = default(int), string _loanGroup = default(string), decimal _maxCashLimit = default(decimal), int _payrollMode = default(int), decimal _basicPay = default(decimal), int _familyVisaStatus = default(int), int _employeeType = default(int), string _workTimeTemplate = default(string), int _employeeStatus = default(int), string _addressKSA = default(string), string _addressHome = default(string), int _workStatus = default(int), int _problemStatus = default(int), string _positionID = default(string), string _organizationUnitID = default(string), DateTime _startSalaryDate = default(DateTime), decimal _heightinCM = default(decimal), decimal _heightinFeet = default(decimal), decimal _weightinKg = default(decimal), decimal _weightinPound = default(decimal), int _education = default(int), int _totalExperience = default(int), int _gCCExperience = default(int), int _saudiExperience = default(int), string _remarks = default(string), string _sSICalculationID = default(string), int _paymentType = default(int), string _employeeStatusID = default(string), string _mobileNumber = default(string), string _dimensions = default(string), string _project = default(string), string _iqamanumber = default(string), string _passportnumber = default(string), DateTime _resignDate = default(DateTime), string _resignReason = default(string), string _exitReasonTypeID = default(string), string _exitTypeID = default(string), string _exitApprovalBy = default(string), DateTime _exitApprovalDate = default(DateTime), string _exitApprovalRemarks = default(string), long _eoSprofileID = default(long), string _email = default(string), string _registrationNo = default(string), DateTime _resignRequestDate = default(DateTime), string _currentContractNumber = default(string), string _currentCustomerID = default(string), string _iqamaProfessionId = default(string), string _candidateID = default(string), bool _canSpeakArabic = default(bool), bool _canSpeakEnglish = default(bool), string _otherLanguage = default(string), bool _cleaningHouses = default(bool), bool _childCare = default(bool), bool _oldageCare = default(bool), bool _cooking = default(bool), bool _isActive = default(bool), int _physicalStatus = default(int), bool _isIdCardCreated = default(bool), byte[] _barCodeImage = default(byte[]), int _idCardProcess = default(int), int _otherExperience = default(int), string _statuslikeMoqueem = default(string), int _moqueemStatus = default(int), string _cRM_GUID = default(string), int _channel = default(int), bool _isArcoDrivingLicenseSponsor = default(bool), string _jobSpecification = default(string))
		{
			// Validate Primary key value
			if (_employeeId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("employeeId");

				
			Arco.Model.Entity.EmployeeMaster employeeMasterToBeCreated = new Arco.Model.Entity.EmployeeMaster();

			employeeMasterToBeCreated.EmployeeId = _employeeId;
			if(_name != default(string))
				employeeMasterToBeCreated.Name = _name;
			if(_firstNameEnglish != default(string))
				employeeMasterToBeCreated.FirstNameEnglish = _firstNameEnglish;
			if(_middleNameEnglish != default(string))
				employeeMasterToBeCreated.MiddleNameEnglish = _middleNameEnglish;
			if(_lastNameEnglish != default(string))
				employeeMasterToBeCreated.LastNameEnglish = _lastNameEnglish;
			if(_firstNameArabic != default(string))
				employeeMasterToBeCreated.FirstNameArabic = _firstNameArabic;
			if(_middleNameArabic != default(string))
				employeeMasterToBeCreated.MiddleNameArabic = _middleNameArabic;
			if(_lastNameArabic != default(string))
				employeeMasterToBeCreated.LastNameArabic = _lastNameArabic;
			if(_fatherName != default(string))
				employeeMasterToBeCreated.FatherName = _fatherName;
			if(_motherName != default(string))
				employeeMasterToBeCreated.MotherName = _motherName;
			if(_gender != default(int))
				employeeMasterToBeCreated.Gender = _gender;
			if(_dOB != default(DateTime))
				employeeMasterToBeCreated.DOB = _dOB;
			if(_photo != default(byte[]))
				employeeMasterToBeCreated.Photo = _photo;
			if(_nationality != default(string))
				employeeMasterToBeCreated.Nationality = _nationality;
			if(_maritalStatus != default(int))
				employeeMasterToBeCreated.MaritalStatus = _maritalStatus;
			if(_nativeLanguage != default(string))
				employeeMasterToBeCreated.NativeLanguage = _nativeLanguage;
			if(_bloodGroup != default(int))
				employeeMasterToBeCreated.BloodGroup = _bloodGroup;
			if(_visaProfession != default(string))
				employeeMasterToBeCreated.VisaProfession = _visaProfession;
			if(_dOJ != default(DateTime))
				employeeMasterToBeCreated.DOJ = _dOJ;
			if(_recordtype != default(int))
				employeeMasterToBeCreated.Recordtype = _recordtype;
			if(_dependentEmployee != default(string))
				employeeMasterToBeCreated.DependentEmployee = _dependentEmployee;
			if(_employeeRelation != default(int))
				employeeMasterToBeCreated.EmployeeRelation = _employeeRelation;
			if(_residentType != default(int))
				employeeMasterToBeCreated.ResidentType = _residentType;
			if(_religion != default(int))
				employeeMasterToBeCreated.Religion = _religion;
			if(_countryEntranceNo != default(string))
				employeeMasterToBeCreated.CountryEntranceNo = _countryEntranceNo;
			if(_countryEntranceDate != default(DateTime))
				employeeMasterToBeCreated.CountryEntranceDate = _countryEntranceDate;
			if(_countryEntranceGate != default(string))
				employeeMasterToBeCreated.CountryEntranceGate = _countryEntranceGate;
			if(_dateofJoingHJ != default(DateTime))
				employeeMasterToBeCreated.DateofJoingHJ = _dateofJoingHJ;
			if(_annualLeave != default(int))
				employeeMasterToBeCreated.AnnualLeave = _annualLeave;
			if(_loanGroup != default(string))
				employeeMasterToBeCreated.LoanGroup = _loanGroup;
			if(_maxCashLimit != default(decimal))
				employeeMasterToBeCreated.MaxCashLimit = _maxCashLimit;
			if(_payrollMode != default(int))
				employeeMasterToBeCreated.PayrollMode = _payrollMode;
			if(_basicPay != default(decimal))
				employeeMasterToBeCreated.BasicPay = _basicPay;
			if(_familyVisaStatus != default(int))
				employeeMasterToBeCreated.FamilyVisaStatus = _familyVisaStatus;
			if(_employeeType != default(int))
				employeeMasterToBeCreated.EmployeeType = _employeeType;
			if(_workTimeTemplate != default(string))
				employeeMasterToBeCreated.WorkTimeTemplate = _workTimeTemplate;
			if(_employeeStatus != default(int))
				employeeMasterToBeCreated.EmployeeStatus = _employeeStatus;
			if(_addressKSA != default(string))
				employeeMasterToBeCreated.AddressKSA = _addressKSA;
			if(_addressHome != default(string))
				employeeMasterToBeCreated.AddressHome = _addressHome;
			if(_workStatus != default(int))
				employeeMasterToBeCreated.WorkStatus = _workStatus;
			if(_problemStatus != default(int))
				employeeMasterToBeCreated.ProblemStatus = _problemStatus;
			if(_positionID != default(string))
				employeeMasterToBeCreated.PositionID = _positionID;
			if(_organizationUnitID != default(string))
				employeeMasterToBeCreated.OrganizationUnitID = _organizationUnitID;
			if(_startSalaryDate != default(DateTime))
				employeeMasterToBeCreated.StartSalaryDate = _startSalaryDate;
			if(_heightinCM != default(decimal))
				employeeMasterToBeCreated.HeightinCM = _heightinCM;
			if(_heightinFeet != default(decimal))
				employeeMasterToBeCreated.HeightinFeet = _heightinFeet;
			if(_weightinKg != default(decimal))
				employeeMasterToBeCreated.WeightinKg = _weightinKg;
			if(_weightinPound != default(decimal))
				employeeMasterToBeCreated.WeightinPound = _weightinPound;
			if(_education != default(int))
				employeeMasterToBeCreated.Education = _education;
			if(_totalExperience != default(int))
				employeeMasterToBeCreated.TotalExperience = _totalExperience;
			if(_gCCExperience != default(int))
				employeeMasterToBeCreated.GCCExperience = _gCCExperience;
			if(_saudiExperience != default(int))
				employeeMasterToBeCreated.SaudiExperience = _saudiExperience;
			if(_remarks != default(string))
				employeeMasterToBeCreated.Remarks = _remarks;
			if(_sSICalculationID != default(string))
				employeeMasterToBeCreated.SSICalculationID = _sSICalculationID;
			if(_paymentType != default(int))
				employeeMasterToBeCreated.PaymentType = _paymentType;
			if(_employeeStatusID != default(string))
				employeeMasterToBeCreated.EmployeeStatusID = _employeeStatusID;
			if(_mobileNumber != default(string))
				employeeMasterToBeCreated.MobileNumber = _mobileNumber;
			if(_dimensions != default(string))
				employeeMasterToBeCreated.Dimensions = _dimensions;
			if(_project != default(string))
				employeeMasterToBeCreated.project = _project;
			if(_iqamanumber != default(string))
				employeeMasterToBeCreated.iqamanumber = _iqamanumber;
			if(_passportnumber != default(string))
				employeeMasterToBeCreated.passportnumber = _passportnumber;
			if(_resignDate != default(DateTime))
				employeeMasterToBeCreated.ResignDate = _resignDate;
			if(_resignReason != default(string))
				employeeMasterToBeCreated.ResignReason = _resignReason;
			if(_exitReasonTypeID != default(string))
				employeeMasterToBeCreated.ExitReasonTypeID = _exitReasonTypeID;
			if(_exitTypeID != default(string))
				employeeMasterToBeCreated.ExitTypeID = _exitTypeID;
			if(_exitApprovalBy != default(string))
				employeeMasterToBeCreated.ExitApprovalBy = _exitApprovalBy;
			if(_exitApprovalDate != default(DateTime))
				employeeMasterToBeCreated.ExitApprovalDate = _exitApprovalDate;
			if(_exitApprovalRemarks != default(string))
				employeeMasterToBeCreated.ExitApprovalRemarks = _exitApprovalRemarks;
			if(_eoSprofileID != default(long))
				employeeMasterToBeCreated.EoSprofileID = _eoSprofileID;
			if(_email != default(string))
				employeeMasterToBeCreated.Email = _email;
			if(_registrationNo != default(string))
				employeeMasterToBeCreated.RegistrationNo = _registrationNo;
			if(_resignRequestDate != default(DateTime))
				employeeMasterToBeCreated.ResignRequestDate = _resignRequestDate;
			if(_currentContractNumber != default(string))
				employeeMasterToBeCreated.CurrentContractNumber = _currentContractNumber;
			if(_currentCustomerID != default(string))
				employeeMasterToBeCreated.CurrentCustomerID = _currentCustomerID;
			if(_iqamaProfessionId != default(string))
				employeeMasterToBeCreated.IqamaProfessionId = _iqamaProfessionId;
			if(_candidateID != default(string))
				employeeMasterToBeCreated.CandidateID = _candidateID;
			if(_canSpeakArabic != default(bool))
				employeeMasterToBeCreated.CanSpeakArabic = _canSpeakArabic;
			if(_canSpeakEnglish != default(bool))
				employeeMasterToBeCreated.CanSpeakEnglish = _canSpeakEnglish;
			if(_otherLanguage != default(string))
				employeeMasterToBeCreated.OtherLanguage = _otherLanguage;
			if(_cleaningHouses != default(bool))
				employeeMasterToBeCreated.CleaningHouses = _cleaningHouses;
			if(_childCare != default(bool))
				employeeMasterToBeCreated.ChildCare = _childCare;
			if(_oldageCare != default(bool))
				employeeMasterToBeCreated.OldageCare = _oldageCare;
			if(_cooking != default(bool))
				employeeMasterToBeCreated.Cooking = _cooking;
			if(_isActive != default(bool))
				employeeMasterToBeCreated.isActive = _isActive;
			if(_physicalStatus != default(int))
				employeeMasterToBeCreated.PhysicalStatus = _physicalStatus;
			if(_isIdCardCreated != default(bool))
				employeeMasterToBeCreated.isIdCardCreated = _isIdCardCreated;
			if(_barCodeImage != default(byte[]))
				employeeMasterToBeCreated.BarCodeImage = _barCodeImage;
			if(_idCardProcess != default(int))
				employeeMasterToBeCreated.IdCardProcess = _idCardProcess;
			if(_otherExperience != default(int))
				employeeMasterToBeCreated.OtherExperience = _otherExperience;
			if(_statuslikeMoqueem != default(string))
				employeeMasterToBeCreated.StatuslikeMoqueem = _statuslikeMoqueem;
			if(_moqueemStatus != default(int))
				employeeMasterToBeCreated.MoqueemStatus = _moqueemStatus;
			if(_cRM_GUID != default(string))
				employeeMasterToBeCreated.CRM_GUID = _cRM_GUID;
			if(_channel != default(int))
				employeeMasterToBeCreated.Channel = _channel;
			if(_isArcoDrivingLicenseSponsor != default(bool))
				employeeMasterToBeCreated.IsArcoDrivingLicenseSponsor = _isArcoDrivingLicenseSponsor;
			if(_jobSpecification != default(string))
				employeeMasterToBeCreated.JobSpecification = _jobSpecification;

			return Insert(employeeMasterToBeCreated);
        }




		public virtual Arco.Model.Entity.EmployeeMaster InsertDynamic(string dynamicObj)
		{

		 dynamic results = JsonConvert.DeserializeObject(dynamicObj);

           //     RecCandidate Demand1 = new RecCandidate();
                

             //   if (results.FirstName != null && results.FirstName.ToString() != "")
               //     Demand1.FirstName = results.FirstName.ToString();
				
			Arco.Model.Entity.EmployeeMaster employeeMasterToBeCreated = new Arco.Model.Entity.EmployeeMaster();

			
	if(results.EmployeeId != null && results.EmployeeId.ToString() != "")
	employeeMasterToBeCreated.EmployeeId =results.EmployeeId.ToString();// results.EmployeeId;
			if(results.Name != null && results.Name.ToString() != "")
				employeeMasterToBeCreated.Name =results.Name.ToString();
			if(results.FirstNameEnglish != null && results.FirstNameEnglish.ToString() != "")
				employeeMasterToBeCreated.FirstNameEnglish =results.FirstNameEnglish.ToString();
			if(results.MiddleNameEnglish != null && results.MiddleNameEnglish.ToString() != "")
				employeeMasterToBeCreated.MiddleNameEnglish =results.MiddleNameEnglish.ToString();
			if(results.LastNameEnglish != null && results.LastNameEnglish.ToString() != "")
				employeeMasterToBeCreated.LastNameEnglish =results.LastNameEnglish.ToString();
			if(results.FirstNameArabic != null && results.FirstNameArabic.ToString() != "")
				employeeMasterToBeCreated.FirstNameArabic =results.FirstNameArabic.ToString();
			if(results.MiddleNameArabic != null && results.MiddleNameArabic.ToString() != "")
				employeeMasterToBeCreated.MiddleNameArabic =results.MiddleNameArabic.ToString();
			if(results.LastNameArabic != null && results.LastNameArabic.ToString() != "")
				employeeMasterToBeCreated.LastNameArabic =results.LastNameArabic.ToString();
			if(results.FatherName != null && results.FatherName.ToString() != "")
				employeeMasterToBeCreated.FatherName =results.FatherName.ToString();
			if(results.MotherName != null && results.MotherName.ToString() != "")
				employeeMasterToBeCreated.MotherName =results.MotherName.ToString();
			if(results.Gender != null && results.Gender.ToString() != "")
				employeeMasterToBeCreated.Gender =int.Parse(results.Gender.ToString());
			if(results.DOB != null && results.DOB.ToString() != "")
				employeeMasterToBeCreated.DOB =results.DOB;
			if(results.Photo != null && results.Photo.ToString() != "")
				employeeMasterToBeCreated.Photo =results.Photo;
			if(results.Nationality != null && results.Nationality.ToString() != "")
				employeeMasterToBeCreated.Nationality =results.Nationality.ToString();
			if(results.MaritalStatus != null && results.MaritalStatus.ToString() != "")
				employeeMasterToBeCreated.MaritalStatus =int.Parse(results.MaritalStatus.ToString());
			if(results.NativeLanguage != null && results.NativeLanguage.ToString() != "")
				employeeMasterToBeCreated.NativeLanguage =results.NativeLanguage.ToString();
			if(results.BloodGroup != null && results.BloodGroup.ToString() != "")
				employeeMasterToBeCreated.BloodGroup =int.Parse(results.BloodGroup.ToString());
			if(results.VisaProfession != null && results.VisaProfession.ToString() != "")
				employeeMasterToBeCreated.VisaProfession =results.VisaProfession.ToString();
			if(results.DOJ != null && results.DOJ.ToString() != "")
				employeeMasterToBeCreated.DOJ =results.DOJ;
			if(results.Recordtype != null && results.Recordtype.ToString() != "")
				employeeMasterToBeCreated.Recordtype =int.Parse(results.Recordtype.ToString());
			if(results.DependentEmployee != null && results.DependentEmployee.ToString() != "")
				employeeMasterToBeCreated.DependentEmployee =results.DependentEmployee.ToString();
			if(results.EmployeeRelation != null && results.EmployeeRelation.ToString() != "")
				employeeMasterToBeCreated.EmployeeRelation =int.Parse(results.EmployeeRelation.ToString());
			if(results.ResidentType != null && results.ResidentType.ToString() != "")
				employeeMasterToBeCreated.ResidentType =int.Parse(results.ResidentType.ToString());
			if(results.Religion != null && results.Religion.ToString() != "")
				employeeMasterToBeCreated.Religion =int.Parse(results.Religion.ToString());
			if(results.CountryEntranceNo != null && results.CountryEntranceNo.ToString() != "")
				employeeMasterToBeCreated.CountryEntranceNo =results.CountryEntranceNo.ToString();
			if(results.CountryEntranceDate != null && results.CountryEntranceDate.ToString() != "")
				employeeMasterToBeCreated.CountryEntranceDate =results.CountryEntranceDate;
			if(results.CountryEntranceGate != null && results.CountryEntranceGate.ToString() != "")
				employeeMasterToBeCreated.CountryEntranceGate =results.CountryEntranceGate.ToString();
			if(results.DateofJoingHJ != null && results.DateofJoingHJ.ToString() != "")
				employeeMasterToBeCreated.DateofJoingHJ =results.DateofJoingHJ;
			if(results.AnnualLeave != null && results.AnnualLeave.ToString() != "")
				employeeMasterToBeCreated.AnnualLeave =int.Parse(results.AnnualLeave.ToString());
			if(results.LoanGroup != null && results.LoanGroup.ToString() != "")
				employeeMasterToBeCreated.LoanGroup =results.LoanGroup.ToString();
			if(results.MaxCashLimit != null && results.MaxCashLimit.ToString() != "")
				employeeMasterToBeCreated.MaxCashLimit =Decimal.Parse(results.MaxCashLimit.ToString());
			if(results.PayrollMode != null && results.PayrollMode.ToString() != "")
				employeeMasterToBeCreated.PayrollMode =int.Parse(results.PayrollMode.ToString());
			if(results.BasicPay != null && results.BasicPay.ToString() != "")
				employeeMasterToBeCreated.BasicPay =Decimal.Parse(results.BasicPay.ToString());
			if(results.FamilyVisaStatus != null && results.FamilyVisaStatus.ToString() != "")
				employeeMasterToBeCreated.FamilyVisaStatus =int.Parse(results.FamilyVisaStatus.ToString());
			if(results.EmployeeType != null && results.EmployeeType.ToString() != "")
				employeeMasterToBeCreated.EmployeeType =int.Parse(results.EmployeeType.ToString());
			if(results.WorkTimeTemplate != null && results.WorkTimeTemplate.ToString() != "")
				employeeMasterToBeCreated.WorkTimeTemplate =results.WorkTimeTemplate.ToString();
			if(results.EmployeeStatus != null && results.EmployeeStatus.ToString() != "")
				employeeMasterToBeCreated.EmployeeStatus =int.Parse(results.EmployeeStatus.ToString());
			if(results.AddressKSA != null && results.AddressKSA.ToString() != "")
				employeeMasterToBeCreated.AddressKSA =results.AddressKSA.ToString();
			if(results.AddressHome != null && results.AddressHome.ToString() != "")
				employeeMasterToBeCreated.AddressHome =results.AddressHome.ToString();
			if(results.WorkStatus != null && results.WorkStatus.ToString() != "")
				employeeMasterToBeCreated.WorkStatus =int.Parse(results.WorkStatus.ToString());
			if(results.ProblemStatus != null && results.ProblemStatus.ToString() != "")
				employeeMasterToBeCreated.ProblemStatus =int.Parse(results.ProblemStatus.ToString());
			if(results.PositionID != null && results.PositionID.ToString() != "")
				employeeMasterToBeCreated.PositionID =results.PositionID.ToString();
			if(results.OrganizationUnitID != null && results.OrganizationUnitID.ToString() != "")
				employeeMasterToBeCreated.OrganizationUnitID =results.OrganizationUnitID.ToString();
			if(results.StartSalaryDate != null && results.StartSalaryDate.ToString() != "")
				employeeMasterToBeCreated.StartSalaryDate =results.StartSalaryDate;
			if(results.HeightinCM != null && results.HeightinCM.ToString() != "")
				employeeMasterToBeCreated.HeightinCM =Decimal.Parse(results.HeightinCM.ToString());
			if(results.HeightinFeet != null && results.HeightinFeet.ToString() != "")
				employeeMasterToBeCreated.HeightinFeet =Decimal.Parse(results.HeightinFeet.ToString());
			if(results.WeightinKg != null && results.WeightinKg.ToString() != "")
				employeeMasterToBeCreated.WeightinKg =Decimal.Parse(results.WeightinKg.ToString());
			if(results.WeightinPound != null && results.WeightinPound.ToString() != "")
				employeeMasterToBeCreated.WeightinPound =Decimal.Parse(results.WeightinPound.ToString());
			if(results.Education != null && results.Education.ToString() != "")
				employeeMasterToBeCreated.Education =int.Parse(results.Education.ToString());
			if(results.TotalExperience != null && results.TotalExperience.ToString() != "")
				employeeMasterToBeCreated.TotalExperience =int.Parse(results.TotalExperience.ToString());
			if(results.GCCExperience != null && results.GCCExperience.ToString() != "")
				employeeMasterToBeCreated.GCCExperience =int.Parse(results.GCCExperience.ToString());
			if(results.SaudiExperience != null && results.SaudiExperience.ToString() != "")
				employeeMasterToBeCreated.SaudiExperience =int.Parse(results.SaudiExperience.ToString());
			if(results.Remarks != null && results.Remarks.ToString() != "")
				employeeMasterToBeCreated.Remarks =results.Remarks.ToString();
			if(results.SSICalculationID != null && results.SSICalculationID.ToString() != "")
				employeeMasterToBeCreated.SSICalculationID =results.SSICalculationID.ToString();
			if(results.PaymentType != null && results.PaymentType.ToString() != "")
				employeeMasterToBeCreated.PaymentType =int.Parse(results.PaymentType.ToString());
			if(results.EmployeeStatusID != null && results.EmployeeStatusID.ToString() != "")
				employeeMasterToBeCreated.EmployeeStatusID =results.EmployeeStatusID.ToString();
			if(results.MobileNumber != null && results.MobileNumber.ToString() != "")
				employeeMasterToBeCreated.MobileNumber =results.MobileNumber.ToString();
			if(results.Dimensions != null && results.Dimensions.ToString() != "")
				employeeMasterToBeCreated.Dimensions =results.Dimensions.ToString();
			if(results.project != null && results.project.ToString() != "")
				employeeMasterToBeCreated.project =results.project.ToString();
			if(results.iqamanumber != null && results.iqamanumber.ToString() != "")
				employeeMasterToBeCreated.iqamanumber =results.iqamanumber.ToString();
			if(results.passportnumber != null && results.passportnumber.ToString() != "")
				employeeMasterToBeCreated.passportnumber =results.passportnumber.ToString();
			if(results.ResignDate != null && results.ResignDate.ToString() != "")
				employeeMasterToBeCreated.ResignDate =results.ResignDate;
			if(results.ResignReason != null && results.ResignReason.ToString() != "")
				employeeMasterToBeCreated.ResignReason =results.ResignReason.ToString();
			if(results.ExitReasonTypeID != null && results.ExitReasonTypeID.ToString() != "")
				employeeMasterToBeCreated.ExitReasonTypeID =results.ExitReasonTypeID.ToString();
			if(results.ExitTypeID != null && results.ExitTypeID.ToString() != "")
				employeeMasterToBeCreated.ExitTypeID =results.ExitTypeID.ToString();
			if(results.ExitApprovalBy != null && results.ExitApprovalBy.ToString() != "")
				employeeMasterToBeCreated.ExitApprovalBy =results.ExitApprovalBy.ToString();
			if(results.ExitApprovalDate != null && results.ExitApprovalDate.ToString() != "")
				employeeMasterToBeCreated.ExitApprovalDate =results.ExitApprovalDate;
			if(results.ExitApprovalRemarks != null && results.ExitApprovalRemarks.ToString() != "")
				employeeMasterToBeCreated.ExitApprovalRemarks =results.ExitApprovalRemarks.ToString();
			if(results.EoSprofileID != null && results.EoSprofileID.ToString() != "")
				employeeMasterToBeCreated.EoSprofileID =long.Parse(results.EoSprofileID.ToString());
			if(results.Email != null && results.Email.ToString() != "")
				employeeMasterToBeCreated.Email =results.Email.ToString();
			if(results.RegistrationNo != null && results.RegistrationNo.ToString() != "")
				employeeMasterToBeCreated.RegistrationNo =results.RegistrationNo.ToString();
			if(results.ResignRequestDate != null && results.ResignRequestDate.ToString() != "")
				employeeMasterToBeCreated.ResignRequestDate =results.ResignRequestDate;
			if(results.CurrentContractNumber != null && results.CurrentContractNumber.ToString() != "")
				employeeMasterToBeCreated.CurrentContractNumber =results.CurrentContractNumber.ToString();
			if(results.CurrentCustomerID != null && results.CurrentCustomerID.ToString() != "")
				employeeMasterToBeCreated.CurrentCustomerID =results.CurrentCustomerID.ToString();
			if(results.IqamaProfessionId != null && results.IqamaProfessionId.ToString() != "")
				employeeMasterToBeCreated.IqamaProfessionId =results.IqamaProfessionId.ToString();
			if(results.CandidateID != null && results.CandidateID.ToString() != "")
				employeeMasterToBeCreated.CandidateID =results.CandidateID.ToString();
			if(results.CanSpeakArabic != null && results.CanSpeakArabic.ToString() != "")
				employeeMasterToBeCreated.CanSpeakArabic =bool.Parse(results.CanSpeakArabic.ToString());
			if(results.CanSpeakEnglish != null && results.CanSpeakEnglish.ToString() != "")
				employeeMasterToBeCreated.CanSpeakEnglish =bool.Parse(results.CanSpeakEnglish.ToString());
			if(results.OtherLanguage != null && results.OtherLanguage.ToString() != "")
				employeeMasterToBeCreated.OtherLanguage =results.OtherLanguage.ToString();
			if(results.CleaningHouses != null && results.CleaningHouses.ToString() != "")
				employeeMasterToBeCreated.CleaningHouses =bool.Parse(results.CleaningHouses.ToString());
			if(results.ChildCare != null && results.ChildCare.ToString() != "")
				employeeMasterToBeCreated.ChildCare =bool.Parse(results.ChildCare.ToString());
			if(results.OldageCare != null && results.OldageCare.ToString() != "")
				employeeMasterToBeCreated.OldageCare =bool.Parse(results.OldageCare.ToString());
			if(results.Cooking != null && results.Cooking.ToString() != "")
				employeeMasterToBeCreated.Cooking =bool.Parse(results.Cooking.ToString());
			if(results.isActive != null && results.isActive.ToString() != "")
				employeeMasterToBeCreated.isActive =bool.Parse(results.isActive.ToString());
			if(results.PhysicalStatus != null && results.PhysicalStatus.ToString() != "")
				employeeMasterToBeCreated.PhysicalStatus =int.Parse(results.PhysicalStatus.ToString());
			if(results.isIdCardCreated != null && results.isIdCardCreated.ToString() != "")
				employeeMasterToBeCreated.isIdCardCreated =bool.Parse(results.isIdCardCreated.ToString());
			if(results.BarCodeImage != null && results.BarCodeImage.ToString() != "")
				employeeMasterToBeCreated.BarCodeImage =results.BarCodeImage;
			if(results.IdCardProcess != null && results.IdCardProcess.ToString() != "")
				employeeMasterToBeCreated.IdCardProcess =int.Parse(results.IdCardProcess.ToString());
			if(results.OtherExperience != null && results.OtherExperience.ToString() != "")
				employeeMasterToBeCreated.OtherExperience =int.Parse(results.OtherExperience.ToString());
			if(results.StatuslikeMoqueem != null && results.StatuslikeMoqueem.ToString() != "")
				employeeMasterToBeCreated.StatuslikeMoqueem =results.StatuslikeMoqueem.ToString();
			if(results.MoqueemStatus != null && results.MoqueemStatus.ToString() != "")
				employeeMasterToBeCreated.MoqueemStatus =int.Parse(results.MoqueemStatus.ToString());
			if(results.CRM_GUID != null && results.CRM_GUID.ToString() != "")
				employeeMasterToBeCreated.CRM_GUID =results.CRM_GUID.ToString();
			if(results.Channel != null && results.Channel.ToString() != "")
				employeeMasterToBeCreated.Channel =int.Parse(results.Channel.ToString());
			if(results.IsArcoDrivingLicenseSponsor != null && results.IsArcoDrivingLicenseSponsor.ToString() != "")
				employeeMasterToBeCreated.IsArcoDrivingLicenseSponsor =bool.Parse(results.IsArcoDrivingLicenseSponsor.ToString());
			if(results.JobSpecification != null && results.JobSpecification.ToString() != "")
				employeeMasterToBeCreated.JobSpecification =results.JobSpecification.ToString();

			return Insert(employeeMasterToBeCreated);
        }



			public Arco.Model.Entity.EmployeeMaster Update()
        {
		

		if (_lEmployeeMaster == null || _lEmployeeMaster.RecId == default(long))
                throw (new ArgumentNullException("newEmployeeMaster"));

				return Update(_lEmployeeMaster);

		}


        public Arco.Model.Entity.EmployeeMaster Update(Arco.Model.Entity.EmployeeMaster updatedEmployeeMaster)
        {
			int numberOfAffectedRows = 0;

            // Validate Parameters
            if (updatedEmployeeMaster == null)
                throw (new ArgumentNullException("updatedEmployeeMaster"));

            // Validate Primary key value
            if (updatedEmployeeMaster.EmployeeId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("EmployeeId");

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(updatedEmployeeMaster, PersistenceType.Update);
				OnUpdating(updatedEmployeeMaster);

				updatedEmployeeMaster.ModifiedBy = CurrentSession.UserId;
				updatedEmployeeMaster.ModifiedDatetime = DateTime.UtcNow;

				try
				{
					_context.Update<Arco.Model.Entity.EmployeeMaster>(updatedEmployeeMaster);

                   // DbEntityEntry<EmployeeMaster> entryEmployeeMaster = _context.Entry(updatedEmployeeMaster);
                  //  entryEmployeeMaster.State = EntityState.Modified;
                   // entryEmployeeMaster.Property("CreatedDatetime").IsModified = false;
                   // entryEmployeeMaster.Property("CreatedBy").IsModified = false;

					numberOfAffectedRows = _context.UnitOfWork.Commit();
				}
				catch (DbEntityValidationException ex)
				{
					BizHelper.ThrowDbEntityValidationException(ex);
				}


				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException("No employeeMaster updated!");

				//Apply business workflow
				OnUpdated(updatedEmployeeMaster);
				OnSaved(updatedEmployeeMaster, PersistenceType.Update);

			//	scope.Complete();
			//}
			return updatedEmployeeMaster;

        }

        public Arco.Model.Entity.EmployeeMaster Update(string _employeeId, string _name = default(string), string _firstNameEnglish = default(string), string _middleNameEnglish = default(string), string _lastNameEnglish = default(string), string _firstNameArabic = default(string), string _middleNameArabic = default(string), string _lastNameArabic = default(string), string _fatherName = default(string), string _motherName = default(string), int _gender = default(int), DateTime _dOB = default(DateTime), byte[] _photo = default(byte[]), string _nationality = default(string), int _maritalStatus = default(int), string _nativeLanguage = default(string), int _bloodGroup = default(int), string _visaProfession = default(string), DateTime _dOJ = default(DateTime), int _recordtype = default(int), string _dependentEmployee = default(string), int _employeeRelation = default(int), int _residentType = default(int), int _religion = default(int), string _countryEntranceNo = default(string), DateTime _countryEntranceDate = default(DateTime), string _countryEntranceGate = default(string), DateTime _dateofJoingHJ = default(DateTime), int _annualLeave = default(int), string _loanGroup = default(string), decimal _maxCashLimit = default(decimal), int _payrollMode = default(int), decimal _basicPay = default(decimal), int _familyVisaStatus = default(int), int _employeeType = default(int), string _workTimeTemplate = default(string), int _employeeStatus = default(int), string _addressKSA = default(string), string _addressHome = default(string), int _workStatus = default(int), int _problemStatus = default(int), string _positionID = default(string), string _organizationUnitID = default(string), DateTime _startSalaryDate = default(DateTime), decimal _heightinCM = default(decimal), decimal _heightinFeet = default(decimal), decimal _weightinKg = default(decimal), decimal _weightinPound = default(decimal), int _education = default(int), int _totalExperience = default(int), int _gCCExperience = default(int), int _saudiExperience = default(int), string _remarks = default(string), string _sSICalculationID = default(string), int _paymentType = default(int), string _employeeStatusID = default(string), string _mobileNumber = default(string), string _dimensions = default(string), string _project = default(string), string _iqamanumber = default(string), string _passportnumber = default(string), DateTime _resignDate = default(DateTime), string _resignReason = default(string), string _exitReasonTypeID = default(string), string _exitTypeID = default(string), string _exitApprovalBy = default(string), DateTime _exitApprovalDate = default(DateTime), string _exitApprovalRemarks = default(string), long _eoSprofileID = default(long), string _email = default(string), string _registrationNo = default(string), DateTime _resignRequestDate = default(DateTime), string _currentContractNumber = default(string), string _currentCustomerID = default(string), string _iqamaProfessionId = default(string), string _candidateID = default(string), bool _canSpeakArabic = default(bool), bool _canSpeakEnglish = default(bool), string _otherLanguage = default(string), bool _cleaningHouses = default(bool), bool _childCare = default(bool), bool _oldageCare = default(bool), bool _cooking = default(bool), bool _isActive = default(bool), int _physicalStatus = default(int), bool _isIdCardCreated = default(bool), byte[] _barCodeImage = default(byte[]), int _idCardProcess = default(int), int _otherExperience = default(int), string _statuslikeMoqueem = default(string), int _moqueemStatus = default(int), string _cRM_GUID = default(string), int _channel = default(int), bool _isArcoDrivingLicenseSponsor = default(bool), string _jobSpecification = default(string))
		{
			if (_employeeId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("employeeId");
            
			Arco.Model.Entity.EmployeeMaster employeeMasterToBeUpdated = _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>().FirstOrDefault(employeeMaster => employeeMaster.EmployeeId == _employeeId);


			if(_employeeId != default(string))
				employeeMasterToBeUpdated.EmployeeId = _employeeId;
			if(_name != default(string))
				employeeMasterToBeUpdated.Name = _name;
			if(_firstNameEnglish != default(string))
				employeeMasterToBeUpdated.FirstNameEnglish = _firstNameEnglish;
			if(_middleNameEnglish != default(string))
				employeeMasterToBeUpdated.MiddleNameEnglish = _middleNameEnglish;
			if(_lastNameEnglish != default(string))
				employeeMasterToBeUpdated.LastNameEnglish = _lastNameEnglish;
			if(_firstNameArabic != default(string))
				employeeMasterToBeUpdated.FirstNameArabic = _firstNameArabic;
			if(_middleNameArabic != default(string))
				employeeMasterToBeUpdated.MiddleNameArabic = _middleNameArabic;
			if(_lastNameArabic != default(string))
				employeeMasterToBeUpdated.LastNameArabic = _lastNameArabic;
			if(_fatherName != default(string))
				employeeMasterToBeUpdated.FatherName = _fatherName;
			if(_motherName != default(string))
				employeeMasterToBeUpdated.MotherName = _motherName;
			if(_gender != default(int))
				employeeMasterToBeUpdated.Gender = _gender;
			if(_dOB != default(DateTime))
				employeeMasterToBeUpdated.DOB = _dOB;
			if(_photo != default(byte[]))
				employeeMasterToBeUpdated.Photo = _photo;
			if(_nationality != default(string))
				employeeMasterToBeUpdated.Nationality = _nationality;
			if(_maritalStatus != default(int))
				employeeMasterToBeUpdated.MaritalStatus = _maritalStatus;
			if(_nativeLanguage != default(string))
				employeeMasterToBeUpdated.NativeLanguage = _nativeLanguage;
			if(_bloodGroup != default(int))
				employeeMasterToBeUpdated.BloodGroup = _bloodGroup;
			if(_visaProfession != default(string))
				employeeMasterToBeUpdated.VisaProfession = _visaProfession;
			if(_dOJ != default(DateTime))
				employeeMasterToBeUpdated.DOJ = _dOJ;
			if(_recordtype != default(int))
				employeeMasterToBeUpdated.Recordtype = _recordtype;
			if(_dependentEmployee != default(string))
				employeeMasterToBeUpdated.DependentEmployee = _dependentEmployee;
			if(_employeeRelation != default(int))
				employeeMasterToBeUpdated.EmployeeRelation = _employeeRelation;
			if(_residentType != default(int))
				employeeMasterToBeUpdated.ResidentType = _residentType;
			if(_religion != default(int))
				employeeMasterToBeUpdated.Religion = _religion;
			if(_countryEntranceNo != default(string))
				employeeMasterToBeUpdated.CountryEntranceNo = _countryEntranceNo;
			if(_countryEntranceDate != default(DateTime))
				employeeMasterToBeUpdated.CountryEntranceDate = _countryEntranceDate;
			if(_countryEntranceGate != default(string))
				employeeMasterToBeUpdated.CountryEntranceGate = _countryEntranceGate;
			if(_dateofJoingHJ != default(DateTime))
				employeeMasterToBeUpdated.DateofJoingHJ = _dateofJoingHJ;
			if(_annualLeave != default(int))
				employeeMasterToBeUpdated.AnnualLeave = _annualLeave;
			if(_loanGroup != default(string))
				employeeMasterToBeUpdated.LoanGroup = _loanGroup;
			if(_maxCashLimit != default(decimal))
				employeeMasterToBeUpdated.MaxCashLimit = _maxCashLimit;
			if(_payrollMode != default(int))
				employeeMasterToBeUpdated.PayrollMode = _payrollMode;
			if(_basicPay != default(decimal))
				employeeMasterToBeUpdated.BasicPay = _basicPay;
			if(_familyVisaStatus != default(int))
				employeeMasterToBeUpdated.FamilyVisaStatus = _familyVisaStatus;
			if(_employeeType != default(int))
				employeeMasterToBeUpdated.EmployeeType = _employeeType;
			if(_workTimeTemplate != default(string))
				employeeMasterToBeUpdated.WorkTimeTemplate = _workTimeTemplate;
			if(_employeeStatus != default(int))
				employeeMasterToBeUpdated.EmployeeStatus = _employeeStatus;
			if(_addressKSA != default(string))
				employeeMasterToBeUpdated.AddressKSA = _addressKSA;
			if(_addressHome != default(string))
				employeeMasterToBeUpdated.AddressHome = _addressHome;
			if(_workStatus != default(int))
				employeeMasterToBeUpdated.WorkStatus = _workStatus;
			if(_problemStatus != default(int))
				employeeMasterToBeUpdated.ProblemStatus = _problemStatus;
			if(_positionID != default(string))
				employeeMasterToBeUpdated.PositionID = _positionID;
			if(_organizationUnitID != default(string))
				employeeMasterToBeUpdated.OrganizationUnitID = _organizationUnitID;
			if(_startSalaryDate != default(DateTime))
				employeeMasterToBeUpdated.StartSalaryDate = _startSalaryDate;
			if(_heightinCM != default(decimal))
				employeeMasterToBeUpdated.HeightinCM = _heightinCM;
			if(_heightinFeet != default(decimal))
				employeeMasterToBeUpdated.HeightinFeet = _heightinFeet;
			if(_weightinKg != default(decimal))
				employeeMasterToBeUpdated.WeightinKg = _weightinKg;
			if(_weightinPound != default(decimal))
				employeeMasterToBeUpdated.WeightinPound = _weightinPound;
			if(_education != default(int))
				employeeMasterToBeUpdated.Education = _education;
			if(_totalExperience != default(int))
				employeeMasterToBeUpdated.TotalExperience = _totalExperience;
			if(_gCCExperience != default(int))
				employeeMasterToBeUpdated.GCCExperience = _gCCExperience;
			if(_saudiExperience != default(int))
				employeeMasterToBeUpdated.SaudiExperience = _saudiExperience;
			if(_remarks != default(string))
				employeeMasterToBeUpdated.Remarks = _remarks;
			if(_sSICalculationID != default(string))
				employeeMasterToBeUpdated.SSICalculationID = _sSICalculationID;
			if(_paymentType != default(int))
				employeeMasterToBeUpdated.PaymentType = _paymentType;
			if(_employeeStatusID != default(string))
				employeeMasterToBeUpdated.EmployeeStatusID = _employeeStatusID;
			if(_mobileNumber != default(string))
				employeeMasterToBeUpdated.MobileNumber = _mobileNumber;
			if(_dimensions != default(string))
				employeeMasterToBeUpdated.Dimensions = _dimensions;
			if(_project != default(string))
				employeeMasterToBeUpdated.project = _project;
			if(_iqamanumber != default(string))
				employeeMasterToBeUpdated.iqamanumber = _iqamanumber;
			if(_passportnumber != default(string))
				employeeMasterToBeUpdated.passportnumber = _passportnumber;
			if(_resignDate != default(DateTime))
				employeeMasterToBeUpdated.ResignDate = _resignDate;
			if(_resignReason != default(string))
				employeeMasterToBeUpdated.ResignReason = _resignReason;
			if(_exitReasonTypeID != default(string))
				employeeMasterToBeUpdated.ExitReasonTypeID = _exitReasonTypeID;
			if(_exitTypeID != default(string))
				employeeMasterToBeUpdated.ExitTypeID = _exitTypeID;
			if(_exitApprovalBy != default(string))
				employeeMasterToBeUpdated.ExitApprovalBy = _exitApprovalBy;
			if(_exitApprovalDate != default(DateTime))
				employeeMasterToBeUpdated.ExitApprovalDate = _exitApprovalDate;
			if(_exitApprovalRemarks != default(string))
				employeeMasterToBeUpdated.ExitApprovalRemarks = _exitApprovalRemarks;
			if(_eoSprofileID != default(long))
				employeeMasterToBeUpdated.EoSprofileID = _eoSprofileID;
			if(_email != default(string))
				employeeMasterToBeUpdated.Email = _email;
			if(_registrationNo != default(string))
				employeeMasterToBeUpdated.RegistrationNo = _registrationNo;
			if(_resignRequestDate != default(DateTime))
				employeeMasterToBeUpdated.ResignRequestDate = _resignRequestDate;
			if(_currentContractNumber != default(string))
				employeeMasterToBeUpdated.CurrentContractNumber = _currentContractNumber;
			if(_currentCustomerID != default(string))
				employeeMasterToBeUpdated.CurrentCustomerID = _currentCustomerID;
			if(_iqamaProfessionId != default(string))
				employeeMasterToBeUpdated.IqamaProfessionId = _iqamaProfessionId;
			if(_candidateID != default(string))
				employeeMasterToBeUpdated.CandidateID = _candidateID;
			if(_canSpeakArabic != default(bool))
				employeeMasterToBeUpdated.CanSpeakArabic = _canSpeakArabic;
			if(_canSpeakEnglish != default(bool))
				employeeMasterToBeUpdated.CanSpeakEnglish = _canSpeakEnglish;
			if(_otherLanguage != default(string))
				employeeMasterToBeUpdated.OtherLanguage = _otherLanguage;
			if(_cleaningHouses != default(bool))
				employeeMasterToBeUpdated.CleaningHouses = _cleaningHouses;
			if(_childCare != default(bool))
				employeeMasterToBeUpdated.ChildCare = _childCare;
			if(_oldageCare != default(bool))
				employeeMasterToBeUpdated.OldageCare = _oldageCare;
			if(_cooking != default(bool))
				employeeMasterToBeUpdated.Cooking = _cooking;
			if(_isActive != default(bool))
				employeeMasterToBeUpdated.isActive = _isActive;
			if(_physicalStatus != default(int))
				employeeMasterToBeUpdated.PhysicalStatus = _physicalStatus;
			if(_isIdCardCreated != default(bool))
				employeeMasterToBeUpdated.isIdCardCreated = _isIdCardCreated;
			if(_barCodeImage != default(byte[]))
				employeeMasterToBeUpdated.BarCodeImage = _barCodeImage;
			if(_idCardProcess != default(int))
				employeeMasterToBeUpdated.IdCardProcess = _idCardProcess;
			if(_otherExperience != default(int))
				employeeMasterToBeUpdated.OtherExperience = _otherExperience;
			if(_statuslikeMoqueem != default(string))
				employeeMasterToBeUpdated.StatuslikeMoqueem = _statuslikeMoqueem;
			if(_moqueemStatus != default(int))
				employeeMasterToBeUpdated.MoqueemStatus = _moqueemStatus;
			if(_cRM_GUID != default(string))
				employeeMasterToBeUpdated.CRM_GUID = _cRM_GUID;
			if(_channel != default(int))
				employeeMasterToBeUpdated.Channel = _channel;
			if(_isArcoDrivingLicenseSponsor != default(bool))
				employeeMasterToBeUpdated.IsArcoDrivingLicenseSponsor = _isArcoDrivingLicenseSponsor;
			if(_jobSpecification != default(string))
				employeeMasterToBeUpdated.JobSpecification = _jobSpecification;

			return Update(employeeMasterToBeUpdated);
        }



		 public Arco.Model.Entity.EmployeeMaster UpdateDynamic(string dynamicObj)
		{
		 dynamic results = JsonConvert.DeserializeObject(dynamicObj);

			

			string primary=results.EmployeeId;
			if (primary.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("employeeId");

				string _EmployeeId =results.EmployeeId;
            
			Arco.Model.Entity.EmployeeMaster employeeMasterToBeUpdated = _context.GetAsQueryable<Arco.Model.Entity.EmployeeMaster>().FirstOrDefault(employeeMaster => employeeMaster.EmployeeId ==_EmployeeId);


			if(results.EmployeeId != null && results.EmployeeId.ToString() != "")
				employeeMasterToBeUpdated.EmployeeId =results.EmployeeId.ToString();// results.EmployeeId;
			if(results.Name != null && results.Name.ToString() != "")
				employeeMasterToBeUpdated.Name =results.Name.ToString();// results.Name;
			if(results.FirstNameEnglish != null && results.FirstNameEnglish.ToString() != "")
				employeeMasterToBeUpdated.FirstNameEnglish =results.FirstNameEnglish.ToString();// results.FirstNameEnglish;
			if(results.MiddleNameEnglish != null && results.MiddleNameEnglish.ToString() != "")
				employeeMasterToBeUpdated.MiddleNameEnglish =results.MiddleNameEnglish.ToString();// results.MiddleNameEnglish;
			if(results.LastNameEnglish != null && results.LastNameEnglish.ToString() != "")
				employeeMasterToBeUpdated.LastNameEnglish =results.LastNameEnglish.ToString();// results.LastNameEnglish;
			if(results.FirstNameArabic != null && results.FirstNameArabic.ToString() != "")
				employeeMasterToBeUpdated.FirstNameArabic =results.FirstNameArabic.ToString();// results.FirstNameArabic;
			if(results.MiddleNameArabic != null && results.MiddleNameArabic.ToString() != "")
				employeeMasterToBeUpdated.MiddleNameArabic =results.MiddleNameArabic.ToString();// results.MiddleNameArabic;
			if(results.LastNameArabic != null && results.LastNameArabic.ToString() != "")
				employeeMasterToBeUpdated.LastNameArabic =results.LastNameArabic.ToString();// results.LastNameArabic;
			if(results.FatherName != null && results.FatherName.ToString() != "")
				employeeMasterToBeUpdated.FatherName =results.FatherName.ToString();// results.FatherName;
			if(results.MotherName != null && results.MotherName.ToString() != "")
				employeeMasterToBeUpdated.MotherName =results.MotherName.ToString();// results.MotherName;
			if(results.Gender != null && results.Gender.ToString() != "")
				employeeMasterToBeUpdated.Gender =int.Parse(results.Gender.ToString());// results.Gender;
			if(results.DOB != null && results.DOB.ToString() != "")
				employeeMasterToBeUpdated.DOB =results.DOB;// results.DOB;
			if(results.Photo != null && results.Photo.ToString() != "")
				employeeMasterToBeUpdated.Photo =results.Photo;// results.Photo;
			if(results.Nationality != null && results.Nationality.ToString() != "")
				employeeMasterToBeUpdated.Nationality =results.Nationality.ToString();// results.Nationality;
			if(results.MaritalStatus != null && results.MaritalStatus.ToString() != "")
				employeeMasterToBeUpdated.MaritalStatus =int.Parse(results.MaritalStatus.ToString());// results.MaritalStatus;
			if(results.NativeLanguage != null && results.NativeLanguage.ToString() != "")
				employeeMasterToBeUpdated.NativeLanguage =results.NativeLanguage.ToString();// results.NativeLanguage;
			if(results.BloodGroup != null && results.BloodGroup.ToString() != "")
				employeeMasterToBeUpdated.BloodGroup =int.Parse(results.BloodGroup.ToString());// results.BloodGroup;
			if(results.VisaProfession != null && results.VisaProfession.ToString() != "")
				employeeMasterToBeUpdated.VisaProfession =results.VisaProfession.ToString();// results.VisaProfession;
			if(results.DOJ != null && results.DOJ.ToString() != "")
				employeeMasterToBeUpdated.DOJ =results.DOJ;// results.DOJ;
			if(results.Recordtype != null && results.Recordtype.ToString() != "")
				employeeMasterToBeUpdated.Recordtype =int.Parse(results.Recordtype.ToString());// results.Recordtype;
			if(results.DependentEmployee != null && results.DependentEmployee.ToString() != "")
				employeeMasterToBeUpdated.DependentEmployee =results.DependentEmployee.ToString();// results.DependentEmployee;
			if(results.EmployeeRelation != null && results.EmployeeRelation.ToString() != "")
				employeeMasterToBeUpdated.EmployeeRelation =int.Parse(results.EmployeeRelation.ToString());// results.EmployeeRelation;
			if(results.ResidentType != null && results.ResidentType.ToString() != "")
				employeeMasterToBeUpdated.ResidentType =int.Parse(results.ResidentType.ToString());// results.ResidentType;
			if(results.Religion != null && results.Religion.ToString() != "")
				employeeMasterToBeUpdated.Religion =int.Parse(results.Religion.ToString());// results.Religion;
			if(results.CountryEntranceNo != null && results.CountryEntranceNo.ToString() != "")
				employeeMasterToBeUpdated.CountryEntranceNo =results.CountryEntranceNo.ToString();// results.CountryEntranceNo;
			if(results.CountryEntranceDate != null && results.CountryEntranceDate.ToString() != "")
				employeeMasterToBeUpdated.CountryEntranceDate =results.CountryEntranceDate;// results.CountryEntranceDate;
			if(results.CountryEntranceGate != null && results.CountryEntranceGate.ToString() != "")
				employeeMasterToBeUpdated.CountryEntranceGate =results.CountryEntranceGate.ToString();// results.CountryEntranceGate;
			if(results.DateofJoingHJ != null && results.DateofJoingHJ.ToString() != "")
				employeeMasterToBeUpdated.DateofJoingHJ =results.DateofJoingHJ;// results.DateofJoingHJ;
			if(results.AnnualLeave != null && results.AnnualLeave.ToString() != "")
				employeeMasterToBeUpdated.AnnualLeave =int.Parse(results.AnnualLeave.ToString());// results.AnnualLeave;
			if(results.LoanGroup != null && results.LoanGroup.ToString() != "")
				employeeMasterToBeUpdated.LoanGroup =results.LoanGroup.ToString();// results.LoanGroup;
			if(results.MaxCashLimit != null && results.MaxCashLimit.ToString() != "")
				employeeMasterToBeUpdated.MaxCashLimit =Decimal.Parse(results.MaxCashLimit.ToString());// results.MaxCashLimit;
			if(results.PayrollMode != null && results.PayrollMode.ToString() != "")
				employeeMasterToBeUpdated.PayrollMode =int.Parse(results.PayrollMode.ToString());// results.PayrollMode;
			if(results.BasicPay != null && results.BasicPay.ToString() != "")
				employeeMasterToBeUpdated.BasicPay =Decimal.Parse(results.BasicPay.ToString());// results.BasicPay;
			if(results.FamilyVisaStatus != null && results.FamilyVisaStatus.ToString() != "")
				employeeMasterToBeUpdated.FamilyVisaStatus =int.Parse(results.FamilyVisaStatus.ToString());// results.FamilyVisaStatus;
			if(results.EmployeeType != null && results.EmployeeType.ToString() != "")
				employeeMasterToBeUpdated.EmployeeType =int.Parse(results.EmployeeType.ToString());// results.EmployeeType;
			if(results.WorkTimeTemplate != null && results.WorkTimeTemplate.ToString() != "")
				employeeMasterToBeUpdated.WorkTimeTemplate =results.WorkTimeTemplate.ToString();// results.WorkTimeTemplate;
			if(results.EmployeeStatus != null && results.EmployeeStatus.ToString() != "")
				employeeMasterToBeUpdated.EmployeeStatus =int.Parse(results.EmployeeStatus.ToString());// results.EmployeeStatus;
			if(results.AddressKSA != null && results.AddressKSA.ToString() != "")
				employeeMasterToBeUpdated.AddressKSA =results.AddressKSA.ToString();// results.AddressKSA;
			if(results.AddressHome != null && results.AddressHome.ToString() != "")
				employeeMasterToBeUpdated.AddressHome =results.AddressHome.ToString();// results.AddressHome;
			if(results.WorkStatus != null && results.WorkStatus.ToString() != "")
				employeeMasterToBeUpdated.WorkStatus =int.Parse(results.WorkStatus.ToString());// results.WorkStatus;
			if(results.ProblemStatus != null && results.ProblemStatus.ToString() != "")
				employeeMasterToBeUpdated.ProblemStatus =int.Parse(results.ProblemStatus.ToString());// results.ProblemStatus;
			if(results.PositionID != null && results.PositionID.ToString() != "")
				employeeMasterToBeUpdated.PositionID =results.PositionID.ToString();// results.PositionID;
			if(results.OrganizationUnitID != null && results.OrganizationUnitID.ToString() != "")
				employeeMasterToBeUpdated.OrganizationUnitID =results.OrganizationUnitID.ToString();// results.OrganizationUnitID;
			if(results.StartSalaryDate != null && results.StartSalaryDate.ToString() != "")
				employeeMasterToBeUpdated.StartSalaryDate =results.StartSalaryDate;// results.StartSalaryDate;
			if(results.HeightinCM != null && results.HeightinCM.ToString() != "")
				employeeMasterToBeUpdated.HeightinCM =Decimal.Parse(results.HeightinCM.ToString());// results.HeightinCM;
			if(results.HeightinFeet != null && results.HeightinFeet.ToString() != "")
				employeeMasterToBeUpdated.HeightinFeet =Decimal.Parse(results.HeightinFeet.ToString());// results.HeightinFeet;
			if(results.WeightinKg != null && results.WeightinKg.ToString() != "")
				employeeMasterToBeUpdated.WeightinKg =Decimal.Parse(results.WeightinKg.ToString());// results.WeightinKg;
			if(results.WeightinPound != null && results.WeightinPound.ToString() != "")
				employeeMasterToBeUpdated.WeightinPound =Decimal.Parse(results.WeightinPound.ToString());// results.WeightinPound;
			if(results.Education != null && results.Education.ToString() != "")
				employeeMasterToBeUpdated.Education =int.Parse(results.Education.ToString());// results.Education;
			if(results.TotalExperience != null && results.TotalExperience.ToString() != "")
				employeeMasterToBeUpdated.TotalExperience =int.Parse(results.TotalExperience.ToString());// results.TotalExperience;
			if(results.GCCExperience != null && results.GCCExperience.ToString() != "")
				employeeMasterToBeUpdated.GCCExperience =int.Parse(results.GCCExperience.ToString());// results.GCCExperience;
			if(results.SaudiExperience != null && results.SaudiExperience.ToString() != "")
				employeeMasterToBeUpdated.SaudiExperience =int.Parse(results.SaudiExperience.ToString());// results.SaudiExperience;
			if(results.Remarks != null && results.Remarks.ToString() != "")
				employeeMasterToBeUpdated.Remarks =results.Remarks.ToString();// results.Remarks;
			if(results.SSICalculationID != null && results.SSICalculationID.ToString() != "")
				employeeMasterToBeUpdated.SSICalculationID =results.SSICalculationID.ToString();// results.SSICalculationID;
			if(results.PaymentType != null && results.PaymentType.ToString() != "")
				employeeMasterToBeUpdated.PaymentType =int.Parse(results.PaymentType.ToString());// results.PaymentType;
			if(results.EmployeeStatusID != null && results.EmployeeStatusID.ToString() != "")
				employeeMasterToBeUpdated.EmployeeStatusID =results.EmployeeStatusID.ToString();// results.EmployeeStatusID;
			if(results.MobileNumber != null && results.MobileNumber.ToString() != "")
				employeeMasterToBeUpdated.MobileNumber =results.MobileNumber.ToString();// results.MobileNumber;
			if(results.Dimensions != null && results.Dimensions.ToString() != "")
				employeeMasterToBeUpdated.Dimensions =results.Dimensions.ToString();// results.Dimensions;
			if(results.project != null && results.project.ToString() != "")
				employeeMasterToBeUpdated.project =results.project.ToString();// results.project;
			if(results.iqamanumber != null && results.iqamanumber.ToString() != "")
				employeeMasterToBeUpdated.iqamanumber =results.iqamanumber.ToString();// results.iqamanumber;
			if(results.passportnumber != null && results.passportnumber.ToString() != "")
				employeeMasterToBeUpdated.passportnumber =results.passportnumber.ToString();// results.passportnumber;
			if(results.ResignDate != null && results.ResignDate.ToString() != "")
				employeeMasterToBeUpdated.ResignDate =results.ResignDate;// results.ResignDate;
			if(results.ResignReason != null && results.ResignReason.ToString() != "")
				employeeMasterToBeUpdated.ResignReason =results.ResignReason.ToString();// results.ResignReason;
			if(results.ExitReasonTypeID != null && results.ExitReasonTypeID.ToString() != "")
				employeeMasterToBeUpdated.ExitReasonTypeID =results.ExitReasonTypeID.ToString();// results.ExitReasonTypeID;
			if(results.ExitTypeID != null && results.ExitTypeID.ToString() != "")
				employeeMasterToBeUpdated.ExitTypeID =results.ExitTypeID.ToString();// results.ExitTypeID;
			if(results.ExitApprovalBy != null && results.ExitApprovalBy.ToString() != "")
				employeeMasterToBeUpdated.ExitApprovalBy =results.ExitApprovalBy.ToString();// results.ExitApprovalBy;
			if(results.ExitApprovalDate != null && results.ExitApprovalDate.ToString() != "")
				employeeMasterToBeUpdated.ExitApprovalDate =results.ExitApprovalDate;// results.ExitApprovalDate;
			if(results.ExitApprovalRemarks != null && results.ExitApprovalRemarks.ToString() != "")
				employeeMasterToBeUpdated.ExitApprovalRemarks =results.ExitApprovalRemarks.ToString();// results.ExitApprovalRemarks;
			if(results.EoSprofileID != null && results.EoSprofileID.ToString() != "")
				employeeMasterToBeUpdated.EoSprofileID =long.Parse(results.EoSprofileID.ToString());// results.EoSprofileID;
			if(results.Email != null && results.Email.ToString() != "")
				employeeMasterToBeUpdated.Email =results.Email.ToString();// results.Email;
			if(results.RegistrationNo != null && results.RegistrationNo.ToString() != "")
				employeeMasterToBeUpdated.RegistrationNo =results.RegistrationNo.ToString();// results.RegistrationNo;
			if(results.ResignRequestDate != null && results.ResignRequestDate.ToString() != "")
				employeeMasterToBeUpdated.ResignRequestDate =results.ResignRequestDate;// results.ResignRequestDate;
			if(results.CurrentContractNumber != null && results.CurrentContractNumber.ToString() != "")
				employeeMasterToBeUpdated.CurrentContractNumber =results.CurrentContractNumber.ToString();// results.CurrentContractNumber;
			if(results.CurrentCustomerID != null && results.CurrentCustomerID.ToString() != "")
				employeeMasterToBeUpdated.CurrentCustomerID =results.CurrentCustomerID.ToString();// results.CurrentCustomerID;
			if(results.IqamaProfessionId != null && results.IqamaProfessionId.ToString() != "")
				employeeMasterToBeUpdated.IqamaProfessionId =results.IqamaProfessionId.ToString();// results.IqamaProfessionId;
			if(results.CandidateID != null && results.CandidateID.ToString() != "")
				employeeMasterToBeUpdated.CandidateID =results.CandidateID.ToString();// results.CandidateID;
			if(results.CanSpeakArabic != null && results.CanSpeakArabic.ToString() != "")
				employeeMasterToBeUpdated.CanSpeakArabic =bool.Parse(results.CanSpeakArabic.ToString());// results.CanSpeakArabic;
			if(results.CanSpeakEnglish != null && results.CanSpeakEnglish.ToString() != "")
				employeeMasterToBeUpdated.CanSpeakEnglish =bool.Parse(results.CanSpeakEnglish.ToString());// results.CanSpeakEnglish;
			if(results.OtherLanguage != null && results.OtherLanguage.ToString() != "")
				employeeMasterToBeUpdated.OtherLanguage =results.OtherLanguage.ToString();// results.OtherLanguage;
			if(results.CleaningHouses != null && results.CleaningHouses.ToString() != "")
				employeeMasterToBeUpdated.CleaningHouses =bool.Parse(results.CleaningHouses.ToString());// results.CleaningHouses;
			if(results.ChildCare != null && results.ChildCare.ToString() != "")
				employeeMasterToBeUpdated.ChildCare =bool.Parse(results.ChildCare.ToString());// results.ChildCare;
			if(results.OldageCare != null && results.OldageCare.ToString() != "")
				employeeMasterToBeUpdated.OldageCare =bool.Parse(results.OldageCare.ToString());// results.OldageCare;
			if(results.Cooking != null && results.Cooking.ToString() != "")
				employeeMasterToBeUpdated.Cooking =bool.Parse(results.Cooking.ToString());// results.Cooking;
			if(results.isActive != null && results.isActive.ToString() != "")
				employeeMasterToBeUpdated.isActive =bool.Parse(results.isActive.ToString());// results.isActive;
			if(results.PhysicalStatus != null && results.PhysicalStatus.ToString() != "")
				employeeMasterToBeUpdated.PhysicalStatus =int.Parse(results.PhysicalStatus.ToString());// results.PhysicalStatus;
			if(results.isIdCardCreated != null && results.isIdCardCreated.ToString() != "")
				employeeMasterToBeUpdated.isIdCardCreated =bool.Parse(results.isIdCardCreated.ToString());// results.isIdCardCreated;
			if(results.BarCodeImage != null && results.BarCodeImage.ToString() != "")
				employeeMasterToBeUpdated.BarCodeImage =results.BarCodeImage;// results.BarCodeImage;
			if(results.IdCardProcess != null && results.IdCardProcess.ToString() != "")
				employeeMasterToBeUpdated.IdCardProcess =int.Parse(results.IdCardProcess.ToString());// results.IdCardProcess;
			if(results.OtherExperience != null && results.OtherExperience.ToString() != "")
				employeeMasterToBeUpdated.OtherExperience =int.Parse(results.OtherExperience.ToString());// results.OtherExperience;
			if(results.StatuslikeMoqueem != null && results.StatuslikeMoqueem.ToString() != "")
				employeeMasterToBeUpdated.StatuslikeMoqueem =results.StatuslikeMoqueem.ToString();// results.StatuslikeMoqueem;
			if(results.MoqueemStatus != null && results.MoqueemStatus.ToString() != "")
				employeeMasterToBeUpdated.MoqueemStatus =int.Parse(results.MoqueemStatus.ToString());// results.MoqueemStatus;
			if(results.CRM_GUID != null && results.CRM_GUID.ToString() != "")
				employeeMasterToBeUpdated.CRM_GUID =results.CRM_GUID.ToString();// results.CRM_GUID;
			if(results.Channel != null && results.Channel.ToString() != "")
				employeeMasterToBeUpdated.Channel =int.Parse(results.Channel.ToString());// results.Channel;
			if(results.IsArcoDrivingLicenseSponsor != null && results.IsArcoDrivingLicenseSponsor.ToString() != "")
				employeeMasterToBeUpdated.IsArcoDrivingLicenseSponsor =bool.Parse(results.IsArcoDrivingLicenseSponsor.ToString());// results.IsArcoDrivingLicenseSponsor;
			if(results.JobSpecification != null && results.JobSpecification.ToString() != "")
				employeeMasterToBeUpdated.JobSpecification =results.JobSpecification.ToString();// results.JobSpecification;

		return	Update(employeeMasterToBeUpdated);
        }



		 //EmployeeId = sample.EmployeeId,
//Name = sample.Name,
//CreatedDatetime = sample.CreatedDatetime,
//CreatedBy = sample.CreatedBy,
//ModifiedDatetime = sample.ModifiedDatetime,
//ModifiedBy = sample.ModifiedBy,
//DataAreaId = sample.DataAreaId,
//RecId = sample.RecId,
//FirstNameEnglish = sample.FirstNameEnglish,
//MiddleNameEnglish = sample.MiddleNameEnglish,
//LastNameEnglish = sample.LastNameEnglish,
//FirstNameArabic = sample.FirstNameArabic,
//MiddleNameArabic = sample.MiddleNameArabic,
//LastNameArabic = sample.LastNameArabic,
//FatherName = sample.FatherName,
//MotherName = sample.MotherName,
//Gender = sample.Gender,
//DOB = sample.DOB,
//Photo = sample.Photo,
//Nationality = sample.Nationality,
//MaritalStatus = sample.MaritalStatus,
//NativeLanguage = sample.NativeLanguage,
//BloodGroup = sample.BloodGroup,
//VisaProfession = sample.VisaProfession,
//DOJ = sample.DOJ,
//Recordtype = sample.Recordtype,
//DependentEmployee = sample.DependentEmployee,
//EmployeeRelation = sample.EmployeeRelation,
//RecVersion = sample.RecVersion,
//ResidentType = sample.ResidentType,
//Religion = sample.Religion,
//CountryEntranceNo = sample.CountryEntranceNo,
//CountryEntranceDate = sample.CountryEntranceDate,
//CountryEntranceGate = sample.CountryEntranceGate,
//DateofJoingHJ = sample.DateofJoingHJ,
//AnnualLeave = sample.AnnualLeave,
//LoanGroup = sample.LoanGroup,
//MaxCashLimit = sample.MaxCashLimit,
//PayrollMode = sample.PayrollMode,
//BasicPay = sample.BasicPay,
//FamilyVisaStatus = sample.FamilyVisaStatus,
//EmployeeType = sample.EmployeeType,
//WorkTimeTemplate = sample.WorkTimeTemplate,
//EmployeeStatus = sample.EmployeeStatus,
//AddressKSA = sample.AddressKSA,
//AddressHome = sample.AddressHome,
//WorkStatus = sample.WorkStatus,
//ProblemStatus = sample.ProblemStatus,
//PositionID = sample.PositionID,
//OrganizationUnitID = sample.OrganizationUnitID,
//StartSalaryDate = sample.StartSalaryDate,
//HeightinCM = sample.HeightinCM,
//HeightinFeet = sample.HeightinFeet,
//WeightinKg = sample.WeightinKg,
//WeightinPound = sample.WeightinPound,
//Education = sample.Education,
//TotalExperience = sample.TotalExperience,
//GCCExperience = sample.GCCExperience,
//SaudiExperience = sample.SaudiExperience,
//Remarks = sample.Remarks,
//SSICalculationID = sample.SSICalculationID,
//PaymentType = sample.PaymentType,
//EmployeeStatusID = sample.EmployeeStatusID,
//MobileNumber = sample.MobileNumber,
//Dimensions = sample.Dimensions,
//project = sample.project,
//iqamanumber = sample.iqamanumber,
//passportnumber = sample.passportnumber,
//ResignDate = sample.ResignDate,
//ResignReason = sample.ResignReason,
//ExitReasonTypeID = sample.ExitReasonTypeID,
//ExitTypeID = sample.ExitTypeID,
//ExitApprovalBy = sample.ExitApprovalBy,
//ExitApprovalDate = sample.ExitApprovalDate,
//ExitApprovalRemarks = sample.ExitApprovalRemarks,
//EoSprofileID = sample.EoSprofileID,
//Email = sample.Email,
//RegistrationNo = sample.RegistrationNo,
//ResignRequestDate = sample.ResignRequestDate,
//CurrentContractNumber = sample.CurrentContractNumber,
//CurrentCustomerID = sample.CurrentCustomerID,
//IqamaProfessionId = sample.IqamaProfessionId,
//CandidateID = sample.CandidateID,
//CanSpeakArabic = sample.CanSpeakArabic,
//CanSpeakEnglish = sample.CanSpeakEnglish,
//OtherLanguage = sample.OtherLanguage,
//CleaningHouses = sample.CleaningHouses,
//ChildCare = sample.ChildCare,
//OldageCare = sample.OldageCare,
//Cooking = sample.Cooking,
//isActive = sample.isActive,
//PhysicalStatus = sample.PhysicalStatus,
//isIdCardCreated = sample.isIdCardCreated,
//BarCodeImage = sample.BarCodeImage,
//IdCardProcess = sample.IdCardProcess,
//OtherExperience = sample.OtherExperience,
//StatuslikeMoqueem = sample.StatuslikeMoqueem,
//MoqueemStatus = sample.MoqueemStatus,
//CRM_GUID = sample.CRM_GUID,
//Channel = sample.Channel,
//IsArcoDrivingLicenseSponsor = sample.IsArcoDrivingLicenseSponsor,
//JobSpecification = sample.JobSpecification,
  //<label editable-text="sample.EmployeeId"  e-name="EmployeeId" e-form="rowform">
                                           //     {{sample.EmployeeId}}
                                         //   </label>
										  //<label editable-text="sample.Name"  e-name="Name" e-form="rowform">
                                           //     {{sample.Name}}
                                         //   </label>
										  //<label editable-text="sample.CreatedDatetime"  e-name="CreatedDatetime" e-form="rowform">
                                           //     {{sample.CreatedDatetime}}
                                         //   </label>
										  //<label editable-text="sample.CreatedBy"  e-name="CreatedBy" e-form="rowform">
                                           //     {{sample.CreatedBy}}
                                         //   </label>
										  //<label editable-text="sample.ModifiedDatetime"  e-name="ModifiedDatetime" e-form="rowform">
                                           //     {{sample.ModifiedDatetime}}
                                         //   </label>
										  //<label editable-text="sample.ModifiedBy"  e-name="ModifiedBy" e-form="rowform">
                                           //     {{sample.ModifiedBy}}
                                         //   </label>
										  //<label editable-text="sample.DataAreaId"  e-name="DataAreaId" e-form="rowform">
                                           //     {{sample.DataAreaId}}
                                         //   </label>
										  //<label editable-text="sample.RecId"  e-name="RecId" e-form="rowform">
                                           //     {{sample.RecId}}
                                         //   </label>
										  //<label editable-text="sample.FirstNameEnglish"  e-name="FirstNameEnglish" e-form="rowform">
                                           //     {{sample.FirstNameEnglish}}
                                         //   </label>
										  //<label editable-text="sample.MiddleNameEnglish"  e-name="MiddleNameEnglish" e-form="rowform">
                                           //     {{sample.MiddleNameEnglish}}
                                         //   </label>
										  //<label editable-text="sample.LastNameEnglish"  e-name="LastNameEnglish" e-form="rowform">
                                           //     {{sample.LastNameEnglish}}
                                         //   </label>
										  //<label editable-text="sample.FirstNameArabic"  e-name="FirstNameArabic" e-form="rowform">
                                           //     {{sample.FirstNameArabic}}
                                         //   </label>
										  //<label editable-text="sample.MiddleNameArabic"  e-name="MiddleNameArabic" e-form="rowform">
                                           //     {{sample.MiddleNameArabic}}
                                         //   </label>
										  //<label editable-text="sample.LastNameArabic"  e-name="LastNameArabic" e-form="rowform">
                                           //     {{sample.LastNameArabic}}
                                         //   </label>
										  //<label editable-text="sample.FatherName"  e-name="FatherName" e-form="rowform">
                                           //     {{sample.FatherName}}
                                         //   </label>
										  //<label editable-text="sample.MotherName"  e-name="MotherName" e-form="rowform">
                                           //     {{sample.MotherName}}
                                         //   </label>
										  //<label editable-text="sample.Gender"  e-name="Gender" e-form="rowform">
                                           //     {{sample.Gender}}
                                         //   </label>
										  //<label editable-text="sample.DOB"  e-name="DOB" e-form="rowform">
                                           //     {{sample.DOB}}
                                         //   </label>
										  //<label editable-text="sample.Photo"  e-name="Photo" e-form="rowform">
                                           //     {{sample.Photo}}
                                         //   </label>
										  //<label editable-text="sample.Nationality"  e-name="Nationality" e-form="rowform">
                                           //     {{sample.Nationality}}
                                         //   </label>
										  //<label editable-text="sample.MaritalStatus"  e-name="MaritalStatus" e-form="rowform">
                                           //     {{sample.MaritalStatus}}
                                         //   </label>
										  //<label editable-text="sample.NativeLanguage"  e-name="NativeLanguage" e-form="rowform">
                                           //     {{sample.NativeLanguage}}
                                         //   </label>
										  //<label editable-text="sample.BloodGroup"  e-name="BloodGroup" e-form="rowform">
                                           //     {{sample.BloodGroup}}
                                         //   </label>
										  //<label editable-text="sample.VisaProfession"  e-name="VisaProfession" e-form="rowform">
                                           //     {{sample.VisaProfession}}
                                         //   </label>
										  //<label editable-text="sample.DOJ"  e-name="DOJ" e-form="rowform">
                                           //     {{sample.DOJ}}
                                         //   </label>
										  //<label editable-text="sample.Recordtype"  e-name="Recordtype" e-form="rowform">
                                           //     {{sample.Recordtype}}
                                         //   </label>
										  //<label editable-text="sample.DependentEmployee"  e-name="DependentEmployee" e-form="rowform">
                                           //     {{sample.DependentEmployee}}
                                         //   </label>
										  //<label editable-text="sample.EmployeeRelation"  e-name="EmployeeRelation" e-form="rowform">
                                           //     {{sample.EmployeeRelation}}
                                         //   </label>
										  //<label editable-text="sample.RecVersion"  e-name="RecVersion" e-form="rowform">
                                           //     {{sample.RecVersion}}
                                         //   </label>
										  //<label editable-text="sample.ResidentType"  e-name="ResidentType" e-form="rowform">
                                           //     {{sample.ResidentType}}
                                         //   </label>
										  //<label editable-text="sample.Religion"  e-name="Religion" e-form="rowform">
                                           //     {{sample.Religion}}
                                         //   </label>
										  //<label editable-text="sample.CountryEntranceNo"  e-name="CountryEntranceNo" e-form="rowform">
                                           //     {{sample.CountryEntranceNo}}
                                         //   </label>
										  //<label editable-text="sample.CountryEntranceDate"  e-name="CountryEntranceDate" e-form="rowform">
                                           //     {{sample.CountryEntranceDate}}
                                         //   </label>
										  //<label editable-text="sample.CountryEntranceGate"  e-name="CountryEntranceGate" e-form="rowform">
                                           //     {{sample.CountryEntranceGate}}
                                         //   </label>
										  //<label editable-text="sample.DateofJoingHJ"  e-name="DateofJoingHJ" e-form="rowform">
                                           //     {{sample.DateofJoingHJ}}
                                         //   </label>
										  //<label editable-text="sample.AnnualLeave"  e-name="AnnualLeave" e-form="rowform">
                                           //     {{sample.AnnualLeave}}
                                         //   </label>
										  //<label editable-text="sample.LoanGroup"  e-name="LoanGroup" e-form="rowform">
                                           //     {{sample.LoanGroup}}
                                         //   </label>
										  //<label editable-text="sample.MaxCashLimit"  e-name="MaxCashLimit" e-form="rowform">
                                           //     {{sample.MaxCashLimit}}
                                         //   </label>
										  //<label editable-text="sample.PayrollMode"  e-name="PayrollMode" e-form="rowform">
                                           //     {{sample.PayrollMode}}
                                         //   </label>
										  //<label editable-text="sample.BasicPay"  e-name="BasicPay" e-form="rowform">
                                           //     {{sample.BasicPay}}
                                         //   </label>
										  //<label editable-text="sample.FamilyVisaStatus"  e-name="FamilyVisaStatus" e-form="rowform">
                                           //     {{sample.FamilyVisaStatus}}
                                         //   </label>
										  //<label editable-text="sample.EmployeeType"  e-name="EmployeeType" e-form="rowform">
                                           //     {{sample.EmployeeType}}
                                         //   </label>
										  //<label editable-text="sample.WorkTimeTemplate"  e-name="WorkTimeTemplate" e-form="rowform">
                                           //     {{sample.WorkTimeTemplate}}
                                         //   </label>
										  //<label editable-text="sample.EmployeeStatus"  e-name="EmployeeStatus" e-form="rowform">
                                           //     {{sample.EmployeeStatus}}
                                         //   </label>
										  //<label editable-text="sample.AddressKSA"  e-name="AddressKSA" e-form="rowform">
                                           //     {{sample.AddressKSA}}
                                         //   </label>
										  //<label editable-text="sample.AddressHome"  e-name="AddressHome" e-form="rowform">
                                           //     {{sample.AddressHome}}
                                         //   </label>
										  //<label editable-text="sample.WorkStatus"  e-name="WorkStatus" e-form="rowform">
                                           //     {{sample.WorkStatus}}
                                         //   </label>
										  //<label editable-text="sample.ProblemStatus"  e-name="ProblemStatus" e-form="rowform">
                                           //     {{sample.ProblemStatus}}
                                         //   </label>
										  //<label editable-text="sample.PositionID"  e-name="PositionID" e-form="rowform">
                                           //     {{sample.PositionID}}
                                         //   </label>
										  //<label editable-text="sample.OrganizationUnitID"  e-name="OrganizationUnitID" e-form="rowform">
                                           //     {{sample.OrganizationUnitID}}
                                         //   </label>
										  //<label editable-text="sample.StartSalaryDate"  e-name="StartSalaryDate" e-form="rowform">
                                           //     {{sample.StartSalaryDate}}
                                         //   </label>
										  //<label editable-text="sample.HeightinCM"  e-name="HeightinCM" e-form="rowform">
                                           //     {{sample.HeightinCM}}
                                         //   </label>
										  //<label editable-text="sample.HeightinFeet"  e-name="HeightinFeet" e-form="rowform">
                                           //     {{sample.HeightinFeet}}
                                         //   </label>
										  //<label editable-text="sample.WeightinKg"  e-name="WeightinKg" e-form="rowform">
                                           //     {{sample.WeightinKg}}
                                         //   </label>
										  //<label editable-text="sample.WeightinPound"  e-name="WeightinPound" e-form="rowform">
                                           //     {{sample.WeightinPound}}
                                         //   </label>
										  //<label editable-text="sample.Education"  e-name="Education" e-form="rowform">
                                           //     {{sample.Education}}
                                         //   </label>
										  //<label editable-text="sample.TotalExperience"  e-name="TotalExperience" e-form="rowform">
                                           //     {{sample.TotalExperience}}
                                         //   </label>
										  //<label editable-text="sample.GCCExperience"  e-name="GCCExperience" e-form="rowform">
                                           //     {{sample.GCCExperience}}
                                         //   </label>
										  //<label editable-text="sample.SaudiExperience"  e-name="SaudiExperience" e-form="rowform">
                                           //     {{sample.SaudiExperience}}
                                         //   </label>
										  //<label editable-text="sample.Remarks"  e-name="Remarks" e-form="rowform">
                                           //     {{sample.Remarks}}
                                         //   </label>
										  //<label editable-text="sample.SSICalculationID"  e-name="SSICalculationID" e-form="rowform">
                                           //     {{sample.SSICalculationID}}
                                         //   </label>
										  //<label editable-text="sample.PaymentType"  e-name="PaymentType" e-form="rowform">
                                           //     {{sample.PaymentType}}
                                         //   </label>
										  //<label editable-text="sample.EmployeeStatusID"  e-name="EmployeeStatusID" e-form="rowform">
                                           //     {{sample.EmployeeStatusID}}
                                         //   </label>
										  //<label editable-text="sample.MobileNumber"  e-name="MobileNumber" e-form="rowform">
                                           //     {{sample.MobileNumber}}
                                         //   </label>
										  //<label editable-text="sample.Dimensions"  e-name="Dimensions" e-form="rowform">
                                           //     {{sample.Dimensions}}
                                         //   </label>
										  //<label editable-text="sample.project"  e-name="project" e-form="rowform">
                                           //     {{sample.project}}
                                         //   </label>
										  //<label editable-text="sample.iqamanumber"  e-name="iqamanumber" e-form="rowform">
                                           //     {{sample.iqamanumber}}
                                         //   </label>
										  //<label editable-text="sample.passportnumber"  e-name="passportnumber" e-form="rowform">
                                           //     {{sample.passportnumber}}
                                         //   </label>
										  //<label editable-text="sample.ResignDate"  e-name="ResignDate" e-form="rowform">
                                           //     {{sample.ResignDate}}
                                         //   </label>
										  //<label editable-text="sample.ResignReason"  e-name="ResignReason" e-form="rowform">
                                           //     {{sample.ResignReason}}
                                         //   </label>
										  //<label editable-text="sample.ExitReasonTypeID"  e-name="ExitReasonTypeID" e-form="rowform">
                                           //     {{sample.ExitReasonTypeID}}
                                         //   </label>
										  //<label editable-text="sample.ExitTypeID"  e-name="ExitTypeID" e-form="rowform">
                                           //     {{sample.ExitTypeID}}
                                         //   </label>
										  //<label editable-text="sample.ExitApprovalBy"  e-name="ExitApprovalBy" e-form="rowform">
                                           //     {{sample.ExitApprovalBy}}
                                         //   </label>
										  //<label editable-text="sample.ExitApprovalDate"  e-name="ExitApprovalDate" e-form="rowform">
                                           //     {{sample.ExitApprovalDate}}
                                         //   </label>
										  //<label editable-text="sample.ExitApprovalRemarks"  e-name="ExitApprovalRemarks" e-form="rowform">
                                           //     {{sample.ExitApprovalRemarks}}
                                         //   </label>
										  //<label editable-text="sample.EoSprofileID"  e-name="EoSprofileID" e-form="rowform">
                                           //     {{sample.EoSprofileID}}
                                         //   </label>
										  //<label editable-text="sample.Email"  e-name="Email" e-form="rowform">
                                           //     {{sample.Email}}
                                         //   </label>
										  //<label editable-text="sample.RegistrationNo"  e-name="RegistrationNo" e-form="rowform">
                                           //     {{sample.RegistrationNo}}
                                         //   </label>
										  //<label editable-text="sample.ResignRequestDate"  e-name="ResignRequestDate" e-form="rowform">
                                           //     {{sample.ResignRequestDate}}
                                         //   </label>
										  //<label editable-text="sample.CurrentContractNumber"  e-name="CurrentContractNumber" e-form="rowform">
                                           //     {{sample.CurrentContractNumber}}
                                         //   </label>
										  //<label editable-text="sample.CurrentCustomerID"  e-name="CurrentCustomerID" e-form="rowform">
                                           //     {{sample.CurrentCustomerID}}
                                         //   </label>
										  //<label editable-text="sample.IqamaProfessionId"  e-name="IqamaProfessionId" e-form="rowform">
                                           //     {{sample.IqamaProfessionId}}
                                         //   </label>
										  //<label editable-text="sample.CandidateID"  e-name="CandidateID" e-form="rowform">
                                           //     {{sample.CandidateID}}
                                         //   </label>
										  //<label editable-text="sample.CanSpeakArabic"  e-name="CanSpeakArabic" e-form="rowform">
                                           //     {{sample.CanSpeakArabic}}
                                         //   </label>
										  //<label editable-text="sample.CanSpeakEnglish"  e-name="CanSpeakEnglish" e-form="rowform">
                                           //     {{sample.CanSpeakEnglish}}
                                         //   </label>
										  //<label editable-text="sample.OtherLanguage"  e-name="OtherLanguage" e-form="rowform">
                                           //     {{sample.OtherLanguage}}
                                         //   </label>
										  //<label editable-text="sample.CleaningHouses"  e-name="CleaningHouses" e-form="rowform">
                                           //     {{sample.CleaningHouses}}
                                         //   </label>
										  //<label editable-text="sample.ChildCare"  e-name="ChildCare" e-form="rowform">
                                           //     {{sample.ChildCare}}
                                         //   </label>
										  //<label editable-text="sample.OldageCare"  e-name="OldageCare" e-form="rowform">
                                           //     {{sample.OldageCare}}
                                         //   </label>
										  //<label editable-text="sample.Cooking"  e-name="Cooking" e-form="rowform">
                                           //     {{sample.Cooking}}
                                         //   </label>
										  //<label editable-text="sample.isActive"  e-name="isActive" e-form="rowform">
                                           //     {{sample.isActive}}
                                         //   </label>
										  //<label editable-text="sample.PhysicalStatus"  e-name="PhysicalStatus" e-form="rowform">
                                           //     {{sample.PhysicalStatus}}
                                         //   </label>
										  //<label editable-text="sample.isIdCardCreated"  e-name="isIdCardCreated" e-form="rowform">
                                           //     {{sample.isIdCardCreated}}
                                         //   </label>
										  //<label editable-text="sample.BarCodeImage"  e-name="BarCodeImage" e-form="rowform">
                                           //     {{sample.BarCodeImage}}
                                         //   </label>
										  //<label editable-text="sample.IdCardProcess"  e-name="IdCardProcess" e-form="rowform">
                                           //     {{sample.IdCardProcess}}
                                         //   </label>
										  //<label editable-text="sample.OtherExperience"  e-name="OtherExperience" e-form="rowform">
                                           //     {{sample.OtherExperience}}
                                         //   </label>
										  //<label editable-text="sample.StatuslikeMoqueem"  e-name="StatuslikeMoqueem" e-form="rowform">
                                           //     {{sample.StatuslikeMoqueem}}
                                         //   </label>
										  //<label editable-text="sample.MoqueemStatus"  e-name="MoqueemStatus" e-form="rowform">
                                           //     {{sample.MoqueemStatus}}
                                         //   </label>
										  //<label editable-text="sample.CRM_GUID"  e-name="CRM_GUID" e-form="rowform">
                                           //     {{sample.CRM_GUID}}
                                         //   </label>
										  //<label editable-text="sample.Channel"  e-name="Channel" e-form="rowform">
                                           //     {{sample.Channel}}
                                         //   </label>
										  //<label editable-text="sample.IsArcoDrivingLicenseSponsor"  e-name="IsArcoDrivingLicenseSponsor" e-form="rowform">
                                           //     {{sample.IsArcoDrivingLicenseSponsor}}
                                         //   </label>
										  //<label editable-text="sample.JobSpecification"  e-name="JobSpecification" e-form="rowform">
                                           //     {{sample.JobSpecification}}
                                         //   </label>
										 
        public void Delete(Arco.Model.Entity.EmployeeMaster employeeMasterToBeDeleted)
        {
			int numberOfAffectedRows = 0;
            //Validate Input
            if (employeeMasterToBeDeleted == null)
                throw (new ArgumentNullException("employeeMasterToBeDeleted"));

            // Validate Primary key value
            if (employeeMasterToBeDeleted.EmployeeId.IsInvalidKey())
                BizHelper.ThrowErrorForInvalidDataKey("EmployeeId");

			//Wrapping all the data manipulations with the single transaction scope
			//using (var scope = new TransactionScope(TransactionScopeOption.Required, new TransactionOptions { IsolationLevel = System.Transactions.IsolationLevel.ReadCommitted }))
           // {

				// Apply business rules
				OnSaving(employeeMasterToBeDeleted, PersistenceType.Delete);
				OnDeleting(employeeMasterToBeDeleted);

			
				_context.Remove(employeeMasterToBeDeleted);
            
				numberOfAffectedRows = _context.UnitOfWork.Commit();
				if (numberOfAffectedRows == 0) 
					throw new DataNotUpdatedException(string.Format("EmployeeMaster (EmployeeId={0}) has not been deleted!", employeeMasterToBeDeleted.EmployeeId));
            
				//Apply business workflow
				OnDeleted(employeeMasterToBeDeleted);
				OnSaved(employeeMasterToBeDeleted, PersistenceType.Delete);

			//	scope.Complete();
			//}

        }

        public void Delete(List<string> employeeIdsToDelete)
        {
            //Validate Input
            foreach (string employeeId in employeeIdsToDelete)
                if (employeeId.IsInvalidKey())
                    BizHelper.ThrowErrorForInvalidDataKey("EmployeeId");

            foreach (string employeeId in employeeIdsToDelete)
            {
				Arco.Model.Entity.EmployeeMaster employeeMasterToBeDeleted = new Arco.Model.Entity.EmployeeMaster { EmployeeId = employeeId, DataAreaId = CurrentSession.DataAreaId };

				Delete(employeeMasterToBeDeleted);
            }

        }

 
		//Delete by RecId
		public void Delete(List<long> RecIdsToDelete)
        {
            //Validate Input
            foreach (long RecId in RecIdsToDelete)
                if (RecId.IsInvalidKey())
                    BizHelper.ThrowErrorForInvalidDataKey("RecId");

            foreach (long RecId in RecIdsToDelete)
				Delete(GetByRecId(RecId));
        }

        #endregion
	

       
        public Arco.Model.Entity.EmployeeMaster Initialize()
        {
			Arco.Model.Entity.EmployeeMaster employeeMaster = new Arco.Model.Entity.EmployeeMaster();

			return employeeMaster;
		}
	}
}
