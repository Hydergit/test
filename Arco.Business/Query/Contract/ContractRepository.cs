﻿using Arco.Core.Infrastructure;
using Arco.Model.Entity;
using Arco.Model.Interface.Repository.Contract;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Business.Query.Contract
{
    public class ContractRepository : EFRepositoryBase, IContractDataRepository
    {

        public ContractRepository()
            : base("arco")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<ContractRepository>(null);
        }



        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {

              modelBuilder.Configurations.Add(new AMWCustomerConfiguration());

              modelBuilder.Configurations.Add(new AMWContractHeaderConfiguration());
        }




    }
}
