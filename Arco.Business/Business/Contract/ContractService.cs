﻿using Arco.Business.Record;
using Arco.Framework.Query;
using Arco.Model.Entity;
using Arco.Model.Interface.Repository.Contract;
using Arco.Model.Interface.Service.Contract;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Arco.Business.Business.Contract
{
    public partial class ContractService : IContractService
    {


    
        private IContractDataRepository _IContractRepository;
      

        public ContractService(IContractDataRepository IContractRepository)
        {
            _IContractRepository = IContractRepository;
          
        }


        public dynamic GetCustomers()
        {
         var customers=_IContractRepository.GetAsQueryable<AMWCustomer>().ToList();
         return customers;
    }
        public dynamic GetCustomersById(string CustomerId)
        {
            var customers = _IContractRepository.GetAsQueryable<AMWCustomer>();


            
           // return Arco.Framework.Utility.DynamicQueryable.CreatePagedResults<AMWCustomer>(customers, 1, 20, "CustomerID", true);

            return customers;
        }

        public dynamic CreateCustomer(string CustomerId,string Name,string IDNumber)
        {
            AMWCustomerBLL _AMWCustomerBLL = new AMWCustomerBLL();

            _AMWCustomerBLL.CustomerID = CustomerId;
            _AMWCustomerBLL.CompanyName = Name;
            _AMWCustomerBLL.IDNumber = IDNumber;

            //AMWCustomer cust= new AMWCustomer();
            //cust.CustomerID=CustomerId;
            //cust.CompanyName=Name;
            //cust.IDNumber=IDNumber;

            return _AMWCustomerBLL.Insert();
        }


        public dynamic GetCustomersPaged(string Criteria)
        {

            QueryCriteria pagingCriteria = JsonConvert.DeserializeObject<QueryCriteria>(string.IsNullOrEmpty(Criteria) ? "{}" : Criteria);
                
            AMWCustomerBLL _AMWCustomerBLL = new AMWCustomerBLL();
            var customers = _AMWCustomerBLL.GetList(pagingCriteria);
           
            return customers;
        }


        public dynamic GetContractsPaged(string Criteria)
        {

            QueryCriteria pagingCriteria = JsonConvert.DeserializeObject<QueryCriteria>(string.IsNullOrEmpty(Criteria) ? "{}" : Criteria);



            var data = (from contract in _IContractRepository.GetAsQueryable<AMWContractHeader>(pagingCriteria,"contract")
                        join customer in _IContractRepository.GetAsQueryable<AMWCustomer>(pagingCriteria, "customer") on contract.CustomerID equals customer.CustomerID
                        select new
                        {
                            ContractNumber = contract.ContractNumber,
                            ContractStatus = contract.ContractStatus,
                            CustomerId = customer.CustomerID,
                            Nationality = customer.Nationality,
                            Name = customer.CustomerType == 1 ? customer.CompanyName : (customer.FirstName + customer.MiddleName + customer.LastName),
                            RecId=contract.RecId.ToString(),
                        });

            return QueryExtension.CreatePagedWithFilterResults(data, pagingCriteria, "All");
        }


    }
}
