﻿using Arco.Core.AppBase;
using Arco.Core.AppInterface;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Arco.Core.Ioc;

namespace Arco.Business
{
   public class Module : IModule
    {
       public void SetupIoc(ref IUnityContainer container)
       {
           container.BindInRequestScope<Arco.Model.Interface.Repository.Contract.IContractDataRepository, Arco.Business.Query.Contract.ContractRepository>();
           container.BindInRequestScope<Arco.Model.Interface.Service.Contract.IContractService, Arco.Business.Business.Contract.ContractService>();
      //     container.Register<Arco.Business.Business.Contract.ContractService>(
      //new InjectionFactory(c => new Arco.Business.Business.Contract.ContractService()));
           
       }
        //public void SetupIoc()
        //{
        //    throw new NotImplementedException();
        //}

        public void SetupDatabase()
        {
            throw new NotImplementedException();
        }

        public void Initialize()
        {
            throw new NotImplementedException();
        }

        public void PostInitialize()
        {
            throw new NotImplementedException();
        }

        public void Uninstall()
        {
            throw new NotImplementedException();
        }
    }
}
