﻿using Arco.WorkFlow.Domain.Interface.IWorkFlowService;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace Arco.WorkFlow.Business.Controller
{
    public class WorkFlowApiController: ApiController
    {
        IWorkFlowService _IWorkFlowService;

        public WorkFlowApiController(IWorkFlowService workFlowService)
        {
            _IWorkFlowService = workFlowService;
        }

        #region WorkFlow
        [HttpGet]
        [Route("api/GetWorkFlowList")]
        public dynamic GetWorkFlowList()
        {
            try
            {
                return Json(_IWorkFlowService.GetWorkFlowList());
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }


        [HttpPost]
        [Route("api/CreateWorkFlow")]
        public dynamic CreateWorkFlow(string WorkFlow)
        {
            try
            {
                var data = _IWorkFlowService.CreateWorkFlow(WorkFlow);
                return Json(new { status = true, msg = data });

            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }

        }

        [HttpPut]
        [Route("api/UpdateWorkFlow")]
        public dynamic UpdateWorkFlow(string WorkFlow)
        {
            try
            {
                var data = _IWorkFlowService.UpdateWorkFlow(WorkFlow);
                return Json(new { status = true, msg = data });


            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }


        }


        [HttpGet]
        [Route("api/GetEditByWorkFlow")]
        public dynamic GetEditByWorkFlow(string RecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetEditByWorkFlow(RecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }


        }
        [HttpPost]
        [Route("api/DeleteWorkFlow")]
        public dynamic DeleteWorkFlow(string[] RecId)
        {
            try
            {
                return Json(_IWorkFlowService.DeleteWorkFlow(RecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

        #endregion

        #region WorkFlowProcess

        [HttpGet]
        [Route("api/GetWorkFlowProcessList")]
        public dynamic GetWorkFlowProcessList(string WorkFlowRecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetWorkFlowProcessList(WorkFlowRecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [HttpPost]
        [Route("api/CreateWorkFlowProcess")]
        public dynamic CreateWorkFlowProcess(string WorkFlowProcess)
        {
            try
            {
                var data = _IWorkFlowService.CreateWorkFlowProcess(WorkFlowProcess);
                return Json(new { status = true, msg = data });

            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }

        }


        [HttpPut]
        [Route("api/UpdateWorkFlowProcess")]
        public dynamic UpdateWorkFlowProcess(string WorkFlowProcess)
        {
            try
            {
                var data = _IWorkFlowService.UpdateWorkFlowProcess(WorkFlowProcess);
                return Json(new { status = true, msg = data });


            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }


        }


        [HttpGet]
        [Route("api/GetEditByWorkFlowProcess")]
        public dynamic GetEditByWorkFlowProcess(string RecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetEditByWorkFlowProcess(RecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }


        }
        [HttpPost]
        [Route("api/DeleteWorkFlowProcess")]
        public dynamic DeleteWorkFlowProcess(string[] RecId)
        {
            try
            {
                return Json(_IWorkFlowService.DeleteWorkFlowProcess(RecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

        #endregion


        #region WorkFlowProcessStage

        [HttpGet]
        [Route("api/GetWorkFlowProcessStageList")]
        public dynamic GetWorkFlowProcessStageList(string WorkFlowProcessRecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetWorkFlowProcessStageList(WorkFlowProcessRecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }

        [HttpPost]
        [Route("api/CreateWorkFlowProcessStage")]
        public dynamic CreateWorkFlowProcessStage(string WorkFlowProcessStage)
        {
            try
            {
                var data = _IWorkFlowService.CreateWorkFlowProcessStage(WorkFlowProcessStage);
                return Json(new { status = true, msg = data });

            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }

        }


        [HttpPut]
        [Route("api/UpdateWorkFlowProcessStage")]
        public dynamic UpdateWorkFlowProcessStage(string WorkFlowProcessStage)
        {
            try
            {
                var data = _IWorkFlowService.UpdateWorkFlowProcessStage(WorkFlowProcessStage);
                return Json(new { status = true, msg = data });   
            }
            catch (Exception ex)
            {      
                return Json(new { status = false, msg = ex.Message });
            }


        }


        [HttpGet]
        [Route("api/GetEditByWorkFlowProcessStage")]
        public dynamic GetEditByWorkFlowProcessStage(string RecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetEditByWorkFlowProcessStage(RecId));  
            }
            catch (Exception ex)
            {         
                return ex.Message;
            }


        }
        [HttpPost]
        [Route("api/DeleteWorkFlowProcessStage")]
        public dynamic DeleteWorkFlowProcessStage(string[] RecId)
        {
            try
            {
                return Json(_IWorkFlowService.DeleteWorkFlowProcessStage(RecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

        #endregion


        #region AssignRoleAndUser      

        [HttpGet]
        [Route("api/GetAssignUnAssignRoleAndUser")]
        public dynamic GetAssignUnAssignRoleAndUser(string WorkFlowProcessStageRecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetAssignUnAssignRoleAndUser(WorkFlowProcessStageRecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }       
        }

        [HttpPost]
        [Route("api/AssignRoleAndUserForStges")]
        public dynamic AssignRoleAndUserForStges(string[] AssigneeId, string WorkFlowProcessStageRecId, string AssignType)
        {
            try
            {
                var data = _IWorkFlowService.AssignRoleAndUserForStges(AssigneeId, WorkFlowProcessStageRecId, AssignType);
                return Json(new { status = true, msg = data });     
            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }  
        }  

        [HttpPost]
        [Route("api/DeleteRoleAndUserForStges")]
        public dynamic DeleteRoleAndUserForStges(string[] AssigneeId, string WorkFlowProcessStageRecId, string AssignType)
        {
            try
            {
                return Json(_IWorkFlowService.DeleteRoleAndUserForStges(AssigneeId, WorkFlowProcessStageRecId, AssignType));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }       

        [HttpGet]
        [Route("api/GetStageAssingDetails")]
        public dynamic GetStageAssingDetails(string WorkFlowProcessStageRecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetStageAssingDetails(WorkFlowProcessStageRecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }       
        }

        #endregion

        #region WorkFlowProcessStageStatusList

        [HttpGet]
        [Route("api/GetWorkFlowProcessStageStatusList")]
        public dynamic GetWorkFlowProcessStageStatusList()
        {
            try
            {
                return Json(_IWorkFlowService.GetWorkFlowProcessStageStatusList());
            }
            catch (Exception ex)
            {

                return ex.Message;
            }
        }


        [HttpPost]
        [Route("api/CreateWorkFlowProcessStageStatusList")]
        public dynamic CreateWorkFlowProcessStageStatusList(string WorkFlowProcessStageStatusList)
        {
            try
            {
                var data = _IWorkFlowService.CreateWorkFlowProcessStageStatusList(WorkFlowProcessStageStatusList);
                return Json(new { status = true, msg = data });

            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }

        }

        [HttpPut]
        [Route("api/UpdateWorkFlowProcessStageStatusList")]
        public dynamic UpdateWorkFlowProcessStageStatusList(string WorkFlowProcessStageStatusList)
        {
            try
            {
                var data = _IWorkFlowService.UpdateWorkFlowProcessStageStatusList(WorkFlowProcessStageStatusList);
                return Json(new { status = true, msg = data });


            }
            catch (Exception ex)
            {

                return Json(new { status = false, msg = ex.Message });
            }


        }


        [HttpGet]
        [Route("api/GetEditByWorkFlowProcessStageStatusList")]
        public dynamic GetEditByWorkFlowProcessStageStatusList(string RecId)
        {
            try
            {
                return Json(_IWorkFlowService.GetEditByWorkFlowProcessStageStatusList(RecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }


        }
        [HttpPost]
        [Route("api/DeleteWorkFlowProcessStageStatusList")]
        public dynamic DeleteWorkFlowProcessStageStatusList(string[] RecId)
        {
            try
            {
                return Json(_IWorkFlowService.DeleteWorkFlowProcessStageStatusList(RecId));

            }
            catch (Exception ex)
            {

                return ex.Message;
            }

        }

        #endregion

    }
}
