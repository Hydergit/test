﻿using Arco.Core.Infrastructure;
using Arco.WorkFlow.Domain.Entity;
using Arco.WorkFlow.Domain.Interface.IWorkFlowRepository;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Business.Repository.WorkFlowRepository
{
    public class WorkFlowRepository : EFRepositoryBase, IWorkFlowRepository
    {
        public WorkFlowRepository()
         : base("arco")
        {
            this.Configuration.AutoDetectChangesEnabled = false;
            this.Configuration.ProxyCreationEnabled = false;
            Database.SetInitializer<WorkFlowRepository>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new WorkflowConfiguration());
            modelBuilder.Configurations.Add(new WorkflowProcessConfiguration());
            modelBuilder.Configurations.Add(new WorkflowProcessStageConfiguration());
            modelBuilder.Configurations.Add(new WorkFlowProcessStageStatuConfiguration());
            modelBuilder.Configurations.Add(new WorkFlowNextProcessConfiguration());
            modelBuilder.Configurations.Add(new ProcessStageAssignmentConfiguration());
            modelBuilder.Configurations.Add(new UserLoginConfiguration());
            modelBuilder.Configurations.Add(new SysRoleMasterConfiguration());

        }

        public IQueryable<Workflow> Workflows
        {
            get { return GetAsQueryable<Workflow>(); }
        }

        public IQueryable<WorkflowProcess> WorkflowProcess
        {
            get { return GetAsQueryable<WorkflowProcess>(); }
        }

        public IQueryable<WorkflowProcessStage> WorkflowProcessStage
        {
            get { return GetAsQueryable<WorkflowProcessStage>(); }
        }

        public IQueryable<WorkFlowProcessStageStatu> WorkFlowProcessStageStatus
        {
            get { return GetAsQueryable<WorkFlowProcessStageStatu>(); }
        }

        public IQueryable<WorkFlowNextProcess> WorkFlowNextProcess
        {
            get { return GetAsQueryable<WorkFlowNextProcess>(); }
        }

        public IQueryable<ProcessStageAssignment> ProcessStageAssignment
        {
            get { return GetAsQueryable<ProcessStageAssignment>(); }
        }

        public IQueryable<SysRoleMaster> SysRoleMaster
        {
            get { return GetAsQueryable<SysRoleMaster>(); }
        }

        public IQueryable<UserLogin> UserLogin
        {
            get { return GetAsQueryable<UserLogin>(); }
        }

        public IQueryable<WorkFlowProcessStageStatusList> WorkFlowProcessStageStatusList
        {
            get { return GetAsQueryable<WorkFlowProcessStageStatusList>(); }
        }
    }
}
