﻿using Arco.WorkFlow.Domain.Entity;
using Arco.WorkFlow.Domain.Enum;
using Arco.WorkFlow.Domain.Interface.IWorkFlowRepository;
using Arco.WorkFlow.Domain.Interface.IWorkFlowService;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Transactions;

namespace Arco.WorkFlow.Business.Business.WorkFlow
{
    public partial class WorkFlowServices : IWorkFlowService
    {
        private IWorkFlowRepository _IWorkFlowRepository;


        public WorkFlowServices(IWorkFlowRepository workFlowRepository)
        {
            _IWorkFlowRepository = workFlowRepository;

        }





        #region WorkFlow            

        public dynamic GetWorkFlowList()
        {
            var data = _IWorkFlowRepository.Workflows;
            return data;
        }

        public dynamic CreateWorkFlow(string WorkFlow)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlow);

                Workflow _Workflow = new Workflow();

                if (results.WorkflowVersion != null && !string.IsNullOrEmpty(results.WorkflowVersion.ToString()))
                    _Workflow.WorkflowVersion = decimal.Parse(results.WorkflowVersion.ToString());

                if (results.WorkflowDescription != null && !string.IsNullOrEmpty(results.WorkflowDescription.ToString()))
                    _Workflow.WorkflowDescription = results.WorkflowDescription.ToString();

                if (results.IsActive != null && !string.IsNullOrEmpty(results.IsActive.ToString()))
                    _Workflow.IsActive = bool.Parse(results.IsActive.ToString());
                else
                    _Workflow.IsActive = false;

                _Workflow.WorkflowType = (_IWorkFlowRepository.Workflows.Max(x => x.WorkflowType ?? "1")) + 1;

                _IWorkFlowRepository.AddSave(_Workflow);
                scope.Complete();
                return "Created SuccessFully";
            }
        }

        public dynamic UpdateWorkFlow(string WorkFlow)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlow);

                long? recid = null;
                if (results.RecId != null && !string.IsNullOrEmpty(results.RecId.ToString()))
                    recid = long.Parse(results.RecId.ToString());
                else
                    throw new Exception("No Record Found");

                var _Workflow = _IWorkFlowRepository.Workflows.Where(x => x.RecId == recid).FirstOrDefault();

                if (results.WorkflowDescription != null && !string.IsNullOrEmpty(results.WorkflowDescription.ToString()))
                    _Workflow.WorkflowDescription = results.WorkflowDescription.ToString();

                if (results.IsActive != null && !string.IsNullOrEmpty(results.IsActive.ToString()))
                    _Workflow.IsActive = bool.Parse(results.IsActive.ToString());
                else
                    _Workflow.IsActive = false;

                _IWorkFlowRepository.UpdateSave(_Workflow);
                scope.Complete();
                return "Update SuccessFully";
            }
        }

        public dynamic GetEditByWorkFlow(string RecId)
        {
            long? recid = null;
            if (!string.IsNullOrEmpty(RecId))
                recid = long.Parse(RecId);
            else
                throw new Exception("No Record Found");

            var data = _IWorkFlowRepository.Workflows.Where(x => x.RecId == recid).FirstOrDefault();
            return data;
        }

        public dynamic DeleteWorkFlow(string[] RecId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var item in RecId)
                {
                    long rec = long.Parse(item.ToString());
                    var data = _IWorkFlowRepository.Workflows.Where(x => x.RecId == rec).FirstOrDefault();
                    _IWorkFlowRepository.RemoveSave(data);
                }
                scope.Complete();
                return "Deleted SuccessFully";
            }
        }
        #endregion


        #region WorkFlowProcess
                                  
        
        public dynamic GetWorkFlowProcessList(string WorkFlowRecId)
        {
            long WKFRecId = long.Parse(WorkFlowRecId);
            var data = (from workflowprocess in _IWorkFlowRepository.WorkflowProcess
                        join workflow in _IWorkFlowRepository.Workflows on workflowprocess.WorkFlowRecid equals workflow.RecId into grpworkflow
                        from subworkflow in grpworkflow.DefaultIfEmpty()
                        where workflowprocess.WorkFlowRecid == WKFRecId

                        select new
                        {
                            //workflowprocess
                            WorkflowVersion = workflowprocess.WorkflowVersion,
                            ProcessId = workflowprocess.ProcessId,
                            ProcessIdEn = workflowprocess.ProcessId == null ? "" : ((MobilizationProcessStages)workflowprocess.ProcessId).ToString(),
                            ProcessDescription = workflowprocess.ProcessDescription,
                            NextProcess = workflowprocess.NextProcess,
                            SequenceNumber = workflowprocess.SequenceNumber,
                            WorkFlowRecid = workflowprocess.WorkFlowRecid,
                            url = workflowprocess.url,
                            Parameter = workflowprocess.Parameter,
                            RefTableName = workflowprocess.RefTableName,
                            intervaldays = workflowprocess.intervaldays,
                            IsActive = workflowprocess.IsActive,
                            ActiveStatus = workflowprocess.ActiveStatus,
                            MappedActiveVersion = workflowprocess.MappedActiveVersion,
                            Paper = workflowprocess.Paper,
                            RecId = workflowprocess.RecId,

                            //workflow
                            WorkflowType = subworkflow.WorkflowType == null ? "" : subworkflow.WorkflowType,
                            WKWorkflowVersion = subworkflow.WorkflowVersion == null ? null : subworkflow.WorkflowVersion,
                            WorkflowDescription = subworkflow.WorkflowDescription == null ? "" : subworkflow.WorkflowDescription,
                            WKIsActive = subworkflow.IsActive == true ? "Yes" : "No",
                        });
            return data;
        }

        public dynamic CreateWorkFlowProcess(string WorkFlowProcess)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlowProcess);

                WorkflowProcess _WorkflowProcess = new WorkflowProcess();


                if (results.ProcessId != null && !string.IsNullOrEmpty(results.ProcessId.ToString()))
                    _WorkflowProcess.ProcessId = (MobilizationProcessStages)int.Parse(results.ProcessId.ToString());

                if (results.ProcessDescription != null && !string.IsNullOrEmpty(results.ProcessDescription.ToString()))
                    _WorkflowProcess.ProcessDescription = results.ProcessDescription.ToString();

                if (results.WorkFlowRecid != null && !string.IsNullOrEmpty(results.WorkFlowRecid.ToString()))
                    _WorkflowProcess.WorkFlowRecid = long.Parse(results.WorkFlowRecid.ToString());

                if (_IWorkFlowRepository.WorkflowProcess.Where(x => x.ProcessId == _WorkflowProcess.ProcessId).Count() == 0)
                {
                    _WorkflowProcess.WorkflowVersion = 1;
                    _WorkflowProcess.ActiveStatus = ActionStatus.Activate;
                    _WorkflowProcess.IsActive = true;
                    _IWorkFlowRepository.AddSave(_WorkflowProcess);
                  //  InsertWorkFlowProcess(_WorkflowProcess);
                }
                else
                {
                    var preversion = _IWorkFlowRepository.WorkflowProcess.Where(x => x.ProcessId == _WorkflowProcess.ProcessId).OrderByDescending(x => x.WorkflowVersion).FirstOrDefault();

                    VersionType? _VersionType = null;

                    if (results.VersionChanges != null && !string.IsNullOrEmpty(results.VersionChanges.ToString()))
                        _VersionType = (VersionType)int.Parse(results.VersionChanges.ToString());


                    decimal? currentversion = preversion.WorkflowVersion;
                    if (_VersionType == VersionType.Minor)
                    {
                        currentversion = preversion.WorkflowVersion + decimal.Parse((0.1).ToString());
                    }
                    else
                    {
                        currentversion = preversion.WorkflowVersion + 1;
                    }


                    bool _isCopy = false;
                    if (results.CopyVersion != null && !string.IsNullOrEmpty(results.CopyVersion.ToString()))
                        _isCopy = true;
                    if (_isCopy)
                    {
                        decimal? mycopyversion = null;
                        if (results.CopyVersion != null && !string.IsNullOrEmpty(results.CopyVersion.ToString()))
                            mycopyversion = long.Parse(results.CopyVersion.ToString());

                        if (currentversion != null)
                            _WorkflowProcess.WorkflowVersion = currentversion;

                        _WorkflowProcess.ActiveStatus = ActionStatus.Draft;

                        _IWorkFlowRepository.AddSave(_WorkflowProcess);
                       // InsertWorkFlowProcess(_WorkflowProcess);


                        var stages = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.WorkflowProcessRecId == mycopyversion).ToList();

                        foreach (var item in stages)
                        {
                            WorkflowProcessStage _WorkflowProcessStage = new WorkflowProcessStage();
                            _WorkflowProcessStage.WorkflowProcessRecId = _WorkflowProcess.RecId;
                            _WorkflowProcessStage.StageId = item.StageId;
                            _WorkflowProcessStage.StageDescription = item.StageDescription;
                            _WorkflowProcessStage.Subject = item.Subject;
                            _WorkflowProcessStage.Instruction = item.Instruction;
                            _WorkflowProcessStage.IsEnding = item.IsEnding;
                            _WorkflowProcessStage.IsPayment = item.IsPayment;
                            _WorkflowProcessStage.IsStarting = item.IsStarting;
                            _WorkflowProcessStage.IsDocument = item.IsDocument;
                            _WorkflowProcessStage.IsEscaped = item.IsEscaped;
                            _WorkflowProcessStage.IsSentToCustomer = item.IsSentToCustomer;
                            _WorkflowProcessStage.IsReceiving = item.IsReceiving;
                            _WorkflowProcessStage.intervaldays = item.intervaldays;
                            _IWorkFlowRepository.AddSave(_WorkflowProcessStage);
                           // InsertWorkflowProcessStage(_WorkflowProcessStage);
                            //  CreateWorkFlowProcess(string _WorkflowProcessStage);

                            var processAssigmnet = _IWorkFlowRepository.ProcessStageAssignment.Where(x => x.ProcessRefRecid == item.RecId).ToList();

                            foreach (var pitem in processAssigmnet)
                            {
                                ProcessStageAssignment ProcessAssignment = new ProcessStageAssignment();
                                ProcessAssignment.ProcessId = pitem.ProcessId;
                                ProcessAssignment.ProcessRefRecid = _WorkflowProcessStage.RecId;
                                ProcessAssignment.AssignedtoType = pitem.AssignedtoType;
                                ProcessAssignment.Assignedto = pitem.Assignedto;
                                _IWorkFlowRepository.AddSave(ProcessAssignment);
                               // InsertProcessStageAssignment(ProcessAssignment);

                            }
                        }

                        var NewStages = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.WorkflowProcessRecId == _WorkflowProcess.RecId).ToList();
                        foreach (var newstage in NewStages)
                        {
                            var oldCurrentStage = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.WorkflowProcessRecId == mycopyversion && x.StageId == newstage.StageId).FirstOrDefault();
                            var StatusAssigmnet = _IWorkFlowRepository.WorkFlowProcessStageStatus.Where(x => x.StageRecid == oldCurrentStage.RecId).ToList();
                            foreach (var statusitem in StatusAssigmnet)
                            {

                                if (statusitem.CompleteStage != null)
                                {
                                    WorkflowProcessStage comstage = null;
                                    var comoldstage = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.RecId == statusitem.CompleteStage).FirstOrDefault();
                                    if (comoldstage != null)
                                    {
                                        comstage = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.WorkflowProcessRecId == _WorkflowProcess.RecId && x.StageId == comoldstage.StageId).FirstOrDefault();
                                    }

                                    WorkFlowProcessStageStatu ProcessAssignment = new WorkFlowProcessStageStatu();
                                    ProcessAssignment.CompleteStage = comstage == null ? null : (int?)int.Parse(comstage.RecId.ToString());
                                    ProcessAssignment.ActionType = statusitem.ActionType;
                                    ProcessAssignment.StageRecid = newstage.RecId;
                                    ProcessAssignment.StatusId = statusitem.StatusId;
                                    _IWorkFlowRepository.AddSave(ProcessAssignment);
                                  //  InsertWorkFlowProcessStageStatus(ProcessAssignment);
                                }
                                else
                                {
                                    WorkFlowProcessStageStatu ProcessAssignment = new WorkFlowProcessStageStatu();
                                    ProcessAssignment.CompleteStage = statusitem.CompleteStage;
                                    ProcessAssignment.ActionType = statusitem.ActionType;
                                    ProcessAssignment.StageRecid = newstage.RecId;
                                    ProcessAssignment.StatusId = statusitem.StatusId;
                                    _IWorkFlowRepository.AddSave(ProcessAssignment);
                                    //InsertWorkFlowProcessStageStatus(ProcessAssignment);
                                }

                            }
                        }

                        if (stages.Count() > 0)
                        {

                            var nextprocess = _IWorkFlowRepository.WorkFlowNextProcess.Where(x => x.WorkflowProcessRecId == mycopyversion).ToList();
                            foreach (var nxtproces in nextprocess)
                            {
                                WorkFlowNextProcess _WorkFlowNextProcess = new WorkFlowNextProcess();

                                _WorkFlowNextProcess.ProcessId = nxtproces.ProcessId;
                                _WorkFlowNextProcess.ProcessStage = nxtproces.ProcessStage;
                                _WorkFlowNextProcess.NextProcess = nxtproces.NextProcess;
                                _WorkFlowNextProcess.WorkflowProcessRecId = _WorkflowProcess.RecId;
                                _WorkFlowNextProcess.NextProcessStage = nxtproces.NextProcessStage;
                                _WorkFlowNextProcess.DependType = nxtproces.DependType;
                                _IWorkFlowRepository.AddSave(_WorkFlowNextProcess);
                                //InsertWorkFlowNextProcess(_WorkFlowNextProcess);

                            }
                        }

                    }
                    else
                    {
                        _WorkflowProcess.WorkflowVersion = currentversion;
                        _WorkflowProcess.ActiveStatus = ActionStatus.Draft;
                        _IWorkFlowRepository.AddSave(_WorkflowProcess);
                       //InsertWorkFlowProcess(_WorkflowProcess);
                    }



                }
                scope.Complete();
                return "Created SuccessFully";

            }


        }


        public dynamic UpdateWorkFlowProcess(string WorkFlowProcess)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlowProcess);

                long? recid = null;
                if (results.RecId != null && !string.IsNullOrEmpty(results.RecId.ToString()))
                    recid = long.Parse(results.RecId.ToString());
                else
                    throw new Exception("No Record Found");

                var _WorkflowProcess = _IWorkFlowRepository.WorkflowProcess.Where(x => x.RecId == recid).FirstOrDefault();

                if (results.ProcessDescription != null && !string.IsNullOrEmpty(results.ProcessDescription.ToString()))
                    _WorkflowProcess.ProcessDescription = results.ProcessDescription.ToString();

                if (results.ProcessId != null && !string.IsNullOrEmpty(results.ProcessId.ToString()))
                    _WorkflowProcess.ProcessId = (MobilizationProcessStages)int.Parse(results.ProcessId.ToString());
                _IWorkFlowRepository.UpdateSave(_WorkflowProcess);

                scope.Complete();
                return "Updated SuccessFully";
            }
        }

        public dynamic GetEditByWorkFlowProcess(string RecId)
        {
            long? recid = null;
            if (!string.IsNullOrEmpty(RecId))
                recid = long.Parse(RecId);
            else
                throw new Exception("No Record Found");

            var data = _IWorkFlowRepository.WorkflowProcess.Where(x => x.RecId == recid).FirstOrDefault();
            return data;
        }

        public dynamic DeleteWorkFlowProcess(string[] RecId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var item in RecId)
                {
                    long rec = long.Parse(item.ToString());
                    var data = _IWorkFlowRepository.WorkflowProcess.Where(x => x.RecId == rec).FirstOrDefault();
                    _IWorkFlowRepository.RemoveSave(data); 
                    //RemoveWorkFlowProcess(data);
                }
                scope.Complete();
                return "Deleted SuccessFully";
            }
        }

        #endregion

        #region WorkFlowProcessStage

       

        public dynamic GetWorkFlowProcessStageList(string WorkFlowProcessRecId)
        {
            long wkfprecid = long.Parse(WorkFlowProcessRecId);
            var data = (from workflowprocesstage in _IWorkFlowRepository.WorkflowProcessStage
                        where workflowprocesstage.WorkflowProcessRecId == wkfprecid
                        select new
                        {
                            WorkflowProcessRecId = workflowprocesstage.WorkflowProcessRecId,
                            StageId = workflowprocesstage.StageId,
                            StageDescription = workflowprocesstage.StageDescription,
                            IsPayment = workflowprocesstage.IsPayment,
                            IsStarting = workflowprocesstage.IsStarting,
                            StageSequenceNumber = workflowprocesstage.StageSequenceNumber,
                            IsEnding = workflowprocesstage.IsEnding,
                            IsDocument = workflowprocesstage.IsDocument,
                            IsSentToCustomer = workflowprocesstage.IsSentToCustomer,
                            IsReceiving = workflowprocesstage.IsReceiving,
                            Subject = workflowprocesstage.Subject,
                            Instruction = workflowprocesstage.Instruction,
                            url = workflowprocesstage.url,
                            IsEscaped = workflowprocesstage.IsEscaped,
                            intervaldays = workflowprocesstage.intervaldays,
                            IsEscaping = workflowprocesstage.IsEscaping,
                            WorkMapStageRecId = workflowprocesstage.WorkMapStageRecId,
                            IsScan = workflowprocesstage.IsScan,
                            Design = workflowprocesstage.Design,
                            Label = workflowprocesstage.Label,
                            Description = workflowprocesstage.Description,
                            ElementId = workflowprocesstage.ElementId,
                            StageNumber = workflowprocesstage.StageNumber,
                            StageOrderId = workflowprocesstage.StageOrderId,
                            RecId = workflowprocesstage.RecId
                        }).OrderBy(x => x.StageOrderId);
            return data;
        }

        public dynamic CreateWorkFlowProcessStage(string WorkFlowProcessStage)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlowProcessStage);

                WorkflowProcessStage _WorkflowProcessStage = new WorkflowProcessStage();


                if (results.WorkflowProcessRecId != null && !string.IsNullOrEmpty(results.WorkflowProcessRecId.ToString()))
                    _WorkflowProcessStage.WorkflowProcessRecId = long.Parse(results.WorkflowProcessRecId.ToString());

                if (results.StageId != null && !string.IsNullOrEmpty(results.StageId.ToString()))
                    _WorkflowProcessStage.StageId = int.Parse(results.StageId.ToString());

                if (results.StageDescription != null && !string.IsNullOrEmpty(results.StageDescription.ToString()))
                    _WorkflowProcessStage.StageDescription = results.StageDescription.ToString();

                if (results.Subject != null && !string.IsNullOrEmpty(results.Subject.ToString()))
                    _WorkflowProcessStage.Subject = results.Subject.ToString();

                if (results.Instruction != null && !string.IsNullOrEmpty(results.Instruction.ToString()))
                    _WorkflowProcessStage.Instruction = results.Instruction.ToString();

                if (results.intervaldays != null && !string.IsNullOrEmpty(results.intervaldays.ToString()))
                    _WorkflowProcessStage.intervaldays = int.Parse(results.intervaldays.ToString());

                if (results.IsPayment != null && !string.IsNullOrEmpty(results.IsPayment.ToString()))
                    _WorkflowProcessStage.IsPayment = bool.Parse(results.IsPayment.ToString());
                else
                    _WorkflowProcessStage.IsPayment = false;

                if (results.IsDocument != null && !string.IsNullOrEmpty(results.IsDocument.ToString()))
                    _WorkflowProcessStage.IsDocument = bool.Parse(results.IsDocument.ToString());
                else
                    _WorkflowProcessStage.IsDocument = false;

                if (results.IsSentToCustomer != null && !string.IsNullOrEmpty(results.IsSentToCustomer.ToString()))
                    _WorkflowProcessStage.IsSentToCustomer = bool.Parse(results.IsSentToCustomer.ToString());
                else
                    _WorkflowProcessStage.IsSentToCustomer = false;

                if (results.IsReceiving != null && !string.IsNullOrEmpty(results.IsReceiving.ToString()))
                    _WorkflowProcessStage.IsReceiving = bool.Parse(results.IsReceiving.ToString());
                else
                    _WorkflowProcessStage.IsReceiving = false;

                if (results.IsEscaped != null && !string.IsNullOrEmpty(results.IsEscaped.ToString()))
                    _WorkflowProcessStage.IsEscaped = bool.Parse(results.IsEscaped.ToString());
                else
                    _WorkflowProcessStage.IsEscaped = false;

                if (results.IsStarting != null && !string.IsNullOrEmpty(results.IsStarting.ToString()))
                    _WorkflowProcessStage.IsStarting = bool.Parse(results.IsStarting.ToString());
                else
                    _WorkflowProcessStage.IsStarting = false;

                if (results.IsEnding != null && !string.IsNullOrEmpty(results.IsEnding.ToString()))
                    _WorkflowProcessStage.IsEnding = bool.Parse(results.IsEnding.ToString());
                else
                    _WorkflowProcessStage.IsEnding = false;

                if (results.IsScan != null && !string.IsNullOrEmpty(results.IsScan.ToString()))
                    _WorkflowProcessStage.IsScan = bool.Parse(results.IsScan.ToString());
                else
                    _WorkflowProcessStage.IsScan = false;

                if (results.IsScan != null && !string.IsNullOrEmpty(results.IsScan.ToString()))
                    _WorkflowProcessStage.IsEscaping = bool.Parse(results.IsScan.ToString());
                else
                    _WorkflowProcessStage.IsScan = false;

                if (results.StageOrderId != null && !string.IsNullOrEmpty(results.StageOrderId.ToString()))
                    _WorkflowProcessStage.StageOrderId = int.Parse(results.StageOrderId.ToString());

                if (results.StageNumber != null && !string.IsNullOrEmpty(results.StageNumber.ToString()))
                    _WorkflowProcessStage.StageNumber = int.Parse(results.StageNumber.ToString());

                if (results.WorkMapStageRecId != null && !string.IsNullOrEmpty(results.WorkMapStageRecId.ToString()))
                    _WorkflowProcessStage.WorkMapStageRecId = long.Parse(results.WorkMapStageRecId.ToString());

                if (results.StageSequenceNumber != null && !string.IsNullOrEmpty(results.StageSequenceNumber.ToString()))
                    _WorkflowProcessStage.StageSequenceNumber = int.Parse(results.StageSequenceNumber.ToString());

                if (results.Description != null && !string.IsNullOrEmpty(results.Description.ToString()))
                    _WorkflowProcessStage.Description = results.Description.ToString();

                if (results.Design != null && !string.IsNullOrEmpty(results.Design.ToString()))
                    _WorkflowProcessStage.Design = results.Design.ToString();

                if (results.Label != null && !string.IsNullOrEmpty(results.Label.ToString()))
                    _WorkflowProcessStage.Label = results.Label.ToString();

                if (results.url != null && !string.IsNullOrEmpty(results.url.ToString()))
                    _WorkflowProcessStage.url = results.url.ToString();

                if (results.ElementId != null && !string.IsNullOrEmpty(results.ElementId.ToString()))
                    _WorkflowProcessStage.ElementId = results.ElementId.ToString();

                _IWorkFlowRepository.AddSave(_WorkflowProcessStage);
                //InsertWorkflowProcessStage(_WorkflowProcessStage);

                scope.Complete();
                return "Created SuccessFully";
            }


        }

        public dynamic UpdateWorkFlowProcessStage(string WorkFlowProcessStage)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlowProcessStage);

                long? recid = null;
                if (results.RecId != null && !string.IsNullOrEmpty(results.RecId.ToString()))
                    recid = long.Parse(results.RecId.ToString());
                else
                    throw new Exception("No Record Found");

                var _WorkflowProcessStage = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.RecId == recid).FirstOrDefault();

                if (results.WorkflowProcessRecId != null && !string.IsNullOrEmpty(results.WorkflowProcessRecId.ToString()))
                    _WorkflowProcessStage.WorkflowProcessRecId = long.Parse(results.WorkflowProcessRecId.ToString());

                if (results.StageId != null && !string.IsNullOrEmpty(results.StageId.ToString()))
                    _WorkflowProcessStage.StageId = int.Parse(results.StageId.ToString());

                if (results.StageDescription != null && !string.IsNullOrEmpty(results.StageDescription.ToString()))
                    _WorkflowProcessStage.StageDescription = results.StageDescription.ToString();

                if (results.Subject != null && !string.IsNullOrEmpty(results.Subject.ToString()))
                    _WorkflowProcessStage.Subject = results.Subject.ToString();

                if (results.Instruction != null && !string.IsNullOrEmpty(results.Instruction.ToString()))
                    _WorkflowProcessStage.Instruction = results.Instruction.ToString();

                if (results.intervaldays != null && !string.IsNullOrEmpty(results.intervaldays.ToString()))
                    _WorkflowProcessStage.intervaldays = int.Parse(results.intervaldays.ToString());

                if (results.IsPayment != null && !string.IsNullOrEmpty(results.IsPayment.ToString()))
                    _WorkflowProcessStage.IsPayment = bool.Parse(results.IsPayment.ToString());
                else
                    _WorkflowProcessStage.IsPayment = false;

                if (results.IsDocument != null && !string.IsNullOrEmpty(results.IsDocument.ToString()))
                    _WorkflowProcessStage.IsDocument = bool.Parse(results.IsDocument.ToString());
                else
                    _WorkflowProcessStage.IsDocument = false;

                if (results.IsSentToCustomer != null && !string.IsNullOrEmpty(results.IsSentToCustomer.ToString()))
                    _WorkflowProcessStage.IsSentToCustomer = bool.Parse(results.IsSentToCustomer.ToString());
                else
                    _WorkflowProcessStage.IsSentToCustomer = false;

                if (results.IsReceiving != null && !string.IsNullOrEmpty(results.IsReceiving.ToString()))
                    _WorkflowProcessStage.IsReceiving = bool.Parse(results.IsReceiving.ToString());
                else
                    _WorkflowProcessStage.IsReceiving = false;

                if (results.IsEscaped != null && !string.IsNullOrEmpty(results.IsEscaped.ToString()))
                    _WorkflowProcessStage.IsEscaped = bool.Parse(results.IsEscaped.ToString());
                else
                    _WorkflowProcessStage.IsEscaped = false;

                if (results.IsStarting != null && !string.IsNullOrEmpty(results.IsStarting.ToString()))
                    _WorkflowProcessStage.IsStarting = bool.Parse(results.IsStarting.ToString());
                else
                    _WorkflowProcessStage.IsStarting = false;

                if (results.IsEnding != null && !string.IsNullOrEmpty(results.IsEnding.ToString()))
                    _WorkflowProcessStage.IsEnding = bool.Parse(results.IsEnding.ToString());
                else
                    _WorkflowProcessStage.IsEnding = false;

                if (results.IsScan != null && !string.IsNullOrEmpty(results.IsScan.ToString()))
                    _WorkflowProcessStage.IsScan = bool.Parse(results.IsScan.ToString());
                else
                    _WorkflowProcessStage.IsScan = false;

                if (results.IsEscaping != null && !string.IsNullOrEmpty(results.IsEscaping.ToString()))
                    _WorkflowProcessStage.IsEscaping = bool.Parse(results.IsEscaping.ToString());
                else
                    _WorkflowProcessStage.IsEscaping = false;

                if (results.StageOrderId != null && !string.IsNullOrEmpty(results.StageOrderId.ToString()))
                    _WorkflowProcessStage.StageOrderId = int.Parse(results.StageOrderId.ToString());

                if (results.StageNumber != null && !string.IsNullOrEmpty(results.StageNumber.ToString()))
                    _WorkflowProcessStage.StageNumber = int.Parse(results.StageNumber.ToString());

                if (results.WorkMapStageRecId != null && !string.IsNullOrEmpty(results.WorkMapStageRecId.ToString()))
                    _WorkflowProcessStage.WorkMapStageRecId = long.Parse(results.WorkMapStageRecId.ToString());

                if (results.StageSequenceNumber != null && !string.IsNullOrEmpty(results.StageSequenceNumber.ToString()))
                    _WorkflowProcessStage.StageSequenceNumber = int.Parse(results.StageSequenceNumber.ToString());

                if (results.Description != null && !string.IsNullOrEmpty(results.Description.ToString()))
                    _WorkflowProcessStage.Description = results.Description.ToString();

                if (results.Design != null && !string.IsNullOrEmpty(results.Design.ToString()))
                    _WorkflowProcessStage.Design = results.Design.ToString();

                if (results.Label != null && !string.IsNullOrEmpty(results.Label.ToString()))
                    _WorkflowProcessStage.Label = results.Label.ToString();

                if (results.url != null && !string.IsNullOrEmpty(results.url.ToString()))
                    _WorkflowProcessStage.url = results.url.ToString();

                if (results.ElementId != null && !string.IsNullOrEmpty(results.ElementId.ToString()))
                    _WorkflowProcessStage.ElementId = results.ElementId.ToString();

                _IWorkFlowRepository.UpdateSave(_WorkflowProcessStage);
                //ModifyWorkflowProcessStage(_WorkflowProcessStage);
                scope.Complete();
                return "Updated SuccessFully";
            }
        }

        public dynamic GetEditByWorkFlowProcessStage(string RecId)
        {
            long? recid = null;
            if (!string.IsNullOrEmpty(RecId))
                recid = long.Parse(RecId);
            else
                throw new Exception("No Record Found");
            var data = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.RecId == recid).FirstOrDefault();
            return data;
        }

        public dynamic DeleteWorkFlowProcessStage(string[] RecId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var item in RecId)
                {
                    long rec = long.Parse(item.ToString());
                    var data = _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.RecId == rec).FirstOrDefault();
                    _IWorkFlowRepository.RemoveSave(data);
                    //RemoveWorkflowProcessStage(data);
                }
                scope.Complete();
                return "Deleted SuccessFully";
            }
        }

        #endregion


        #region AssignRoleAndUser

        public dynamic GetAssignUnAssignRoleAndUser(string WorkFlowProcessStageRecId)
        {
            long wkprocessstageid = long.Parse(WorkFlowProcessStageRecId);
            var Roles = (from sysrolemaster in _IWorkFlowRepository.SysRoleMaster
                         join processstageassignment in _IWorkFlowRepository.ProcessStageAssignment on sysrolemaster.RoleID.ToString() equals processstageassignment.Assignedto into grpprocessstageassignment
                         from subprocessstageassignment in grpprocessstageassignment.DefaultIfEmpty()
                         where (subprocessstageassignment.ProcessRefRecid == wkprocessstageid && subprocessstageassignment.AssignedtoType == (int)AssignedType.Role)
                         select new
                         {
                             RoleId = sysrolemaster.RoleID,
                             IsUser = (subprocessstageassignment == null ? false : true),
                             RoleName = sysrolemaster.RoleDescription_EN,
                             ProcessStageAssignRecId = (subprocessstageassignment == null ? 0 : subprocessstageassignment.RecId),
                         }).ToList();

            var Users = (from userlogin in _IWorkFlowRepository.UserLogin.Where(x => x.EmployeeId != null)
                         join processstageassignment in _IWorkFlowRepository.ProcessStageAssignment on userlogin.UserName equals processstageassignment.Assignedto into grpprocessstageassignment
                         from subprocessstageassignment in grpprocessstageassignment.DefaultIfEmpty()
                         where (subprocessstageassignment.ProcessRefRecid == wkprocessstageid && subprocessstageassignment.AssignedtoType == (int)AssignedType.User)
                         select new
                         {
                             UserName = userlogin.UserName,
                             IsUser = (subprocessstageassignment == null ? false : true),
                             Name = userlogin.Name,
                             ProcessStageAssignRecId = (subprocessstageassignment == null ? 0 : subprocessstageassignment.RecId),
                         }).ToList();

            return new { Roles, Users };
        }

        public dynamic AssignRoleAndUserForStges(string[] AssigneeId, string WorkFlowProcessStageRecId, string AssignType)
        {
            using (TransactionScope scope = new TransactionScope())
            {

                AssignedType? assignedType = null;
                if (!string.IsNullOrEmpty(AssignType))
                    assignedType = (AssignedType)int.Parse(AssignType.ToString());
                else
                    throw new Exception("Assigned Type Not Found");

                long wkprocessstageid = long.Parse(WorkFlowProcessStageRecId);
                foreach (var item in AssigneeId)
                {
                    if (_IWorkFlowRepository.ProcessStageAssignment.Where(x => x.ProcessRefRecid == wkprocessstageid && x.AssignedtoType == (int)assignedType && x.Assignedto == item.ToString()).Count() == 0)
                    {
                        ProcessStageAssignment _ProcessStageAssignment = new ProcessStageAssignment();
                        _ProcessStageAssignment.ProcessRefRecid = wkprocessstageid;
                        _ProcessStageAssignment.Assignedto = item.ToString();
                        _ProcessStageAssignment.AssignedtoType = (int)assignedType;
                        _IWorkFlowRepository.AddSave(_ProcessStageAssignment);   
                       // InsertProcessStageAssignment(_ProcessStageAssignment);
                    }
                }
                scope.Complete();
                return "Assinged SuccessFully";
            }
        }

        public dynamic DeleteRoleAndUserForStges(string[] AssigneeId, string WorkFlowProcessStageRecId, string AssignType)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                AssignedType? assignedType = null;
                if (!string.IsNullOrEmpty(AssignType))
                    assignedType = (AssignedType)int.Parse(AssignType.ToString());
                else
                    throw new Exception("Assigned Type Not Found");

                long wkprocessstageid = long.Parse(WorkFlowProcessStageRecId);
                foreach (var item in AssigneeId)
                {
                    string userroleforid = item.ToString();
                    var _ProcessStageAssignment = _IWorkFlowRepository.ProcessStageAssignment.Where(x => x.ProcessRefRecid == wkprocessstageid && x.Assignedto == userroleforid && x.AssignedtoType == (int)assignedType).FirstOrDefault();
                    _IWorkFlowRepository.RemoveSave(_ProcessStageAssignment);
                    //RemoveProcessStageAssignment(_ProcessStageAssignment);
                }
                scope.Complete();
                return "UnAssign SuccessFully";
            }
        }

        public dynamic GetStageAssingDetails(string WorkFlowProcessStageRecId)
        {
            long wkstagerecid = long.Parse(WorkFlowProcessStageRecId);
            var data = (from processstageassing in _IWorkFlowRepository.ProcessStageAssignment
                        join sysrolemaster in _IWorkFlowRepository.SysRoleMaster on processstageassing.Assignedto equals sysrolemaster.RoleID.ToString() into grpsysrolemaster
                        from subsysrolemaster in grpsysrolemaster.DefaultIfEmpty()
                        join wkprocessstage in _IWorkFlowRepository.WorkflowProcessStage on processstageassing.ProcessRefRecid equals wkprocessstage.RecId into grpwkprocessstage
                        from subwkprocessstage in grpwkprocessstage.DefaultIfEmpty()
                        where processstageassing.ProcessRefRecid == wkstagerecid
                        select new
                        {
                            Stage = processstageassing.ProcessRefRecid,
                            StageEn = subwkprocessstage == null ? "" : subwkprocessstage.Description,
                            AssignToType = processstageassing.AssignedtoType,
                            AssignToTypeEn = processstageassing.AssignedtoType == null ? "" : ((AssignedType)processstageassing.AssignedtoType).ToString(),
                            AssignedTo = processstageassing.Assignedto,
                            AssignedToEn = subsysrolemaster.RoleDescription_EN == null ? "" : subsysrolemaster.RoleDescription_EN,
                            AssignedToAr = subsysrolemaster.RoleDescription_AR == null ? "" : subsysrolemaster.RoleDescription_AR,
                            RecId = processstageassing.RecId
                        }).OrderBy(x => x.AssignToType);
            return data;
        }


        public dynamic AssignRoleAndUserForSelectedStages(string[] WorkFlowProcessStageRecId, string AssigneeId, string AssignType)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                AssignedType? assignedType = null;
                if (!string.IsNullOrEmpty(AssignType))
                    assignedType = (AssignedType)int.Parse(AssignType.ToString());
                else
                    throw new Exception("Assigned Type Not Found");

                if (assignedType == AssignedType.User)
                    if (string.IsNullOrEmpty(AssigneeId))
                        throw new Exception("UserId Not Found");
                    else
                 if (string.IsNullOrEmpty(AssigneeId))
                        throw new Exception("RoleId Not Found");

                foreach (var item in WorkFlowProcessStageRecId)
                {
                    if (_IWorkFlowRepository.ProcessStageAssignment.Where(x => x.ProcessRefRecid == long.Parse(item.ToString()) && x.AssignedtoType == (int)assignedType && x.Assignedto == AssigneeId).Count() == 0)
                    {
                        ProcessStageAssignment _ProcessStageAssignment = new ProcessStageAssignment();
                        _ProcessStageAssignment.ProcessRefRecid = long.Parse(item.ToString());
                        _ProcessStageAssignment.Assignedto = AssigneeId.ToString();
                        _ProcessStageAssignment.AssignedtoType = (int)assignedType;
                        _IWorkFlowRepository.AddSave(_ProcessStageAssignment);
                        //InsertProcessStageAssignment(_ProcessStageAssignment);
                    }
                }
                scope.Complete();
                return "Assinged SuccessFully";
            }
        }

        public dynamic DeleteRoleAndUserForSelectedStages(string[] WorkFlowProcessStageRecId, string AssigneeId, string AssignType)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                AssignedType? assignedType = null;
                if (!string.IsNullOrEmpty(AssignType))
                    assignedType = (AssignedType)int.Parse(AssignType.ToString());
                else
                    throw new Exception("Assigned Type Not Found");

                if (assignedType == AssignedType.User)
                    if (string.IsNullOrEmpty(AssigneeId))
                        throw new Exception("UserId Not Found");
                    else
                 if (string.IsNullOrEmpty(AssigneeId))
                        throw new Exception("RoleId Not Found");


                foreach (var item in WorkFlowProcessStageRecId)
                {
                    long wkprocessstagerecid = long.Parse(item.ToString());
                    var _ProcessStageAssignment = _IWorkFlowRepository.ProcessStageAssignment.Where(x => x.ProcessRefRecid == wkprocessstagerecid && x.Assignedto == AssigneeId && x.AssignedtoType == (int)assignedType).FirstOrDefault();
                    _IWorkFlowRepository.RemoveSave(_ProcessStageAssignment);
                    //RemoveProcessStageAssignment(_ProcessStageAssignment);
                }
                scope.Complete();
                return "UnAssign SuccessFully";
            }
        }

        public dynamic GetUserAndRoleBaseForSelectedStages(string AssigneeId, string WorkflowProcessRecId, string AssignType)
        {
            long wkprocessrecid = long.Parse(WorkflowProcessRecId);
            var AssignUnAssingStage = (from workflowprocessstage in _IWorkFlowRepository.WorkflowProcessStage.Where(x => x.WorkflowProcessRecId == wkprocessrecid)
                                       join processassignment in _IWorkFlowRepository.ProcessStageAssignment on workflowprocessstage.RecId.ToString() equals processassignment.Assignedto into grpprocessassignment
                                       from subprocessassignment in grpprocessassignment.DefaultIfEmpty()
                                       where subprocessassignment.Assignedto == AssigneeId && subprocessassignment.AssignedtoType == int.Parse(AssignType.ToString())
                                       select new
                                       {
                                           StageId = workflowprocessstage.RecId,
                                           IsStatus = (subprocessassignment == null ? false : true),
                                           StageName = workflowprocessstage.StageDescription,
                                       });
            return AssignUnAssingStage;
        }

        public dynamic GetUserAndRoleSelectedStageStages(string AssigneeId, string WorkflowProcessRecId)
        {

            long wkprocessrecid = long.Parse(WorkflowProcessRecId);
            var SelectedUserAndRoleStageDetails = (from workflowprocessstage in _IWorkFlowRepository.WorkflowProcessStage
                                                   join processstageassignment in _IWorkFlowRepository.ProcessStageAssignment on workflowprocessstage.RecId equals processstageassignment.ProcessRefRecid
                                                   where workflowprocessstage.WorkflowProcessRecId == wkprocessrecid && processstageassignment.Assignedto == AssigneeId
                                                   select workflowprocessstage).OrderBy(x => x.StageOrderId);
            return SelectedUserAndRoleStageDetails;

        }


        #endregion


        #region WorkFlowProcessStageStatusList
       

        public dynamic GetWorkFlowProcessStageStatusList()
        {
            var data = _IWorkFlowRepository.WorkFlowProcessStageStatusList;
            return data;
        }

        public dynamic CreateWorkFlowProcessStageStatusList(string WorkFlowProcessStageStatusList)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlowProcessStageStatusList);

                WorkFlowProcessStageStatusList _WorkFlowProcessStageStatusList = new WorkFlowProcessStageStatusList();

                if (results.StatusName != null && !string.IsNullOrEmpty(results.StatusName.ToString()))
                    _WorkFlowProcessStageStatusList.StatusName = results.StatusName.ToString();

                if (results.ActionDescription != null && !string.IsNullOrEmpty(results.ActionDescription.ToString()))
                    _WorkFlowProcessStageStatusList.ActionDescription = results.ActionDescription.ToString();

                _WorkFlowProcessStageStatusList.StatusId = (_IWorkFlowRepository.WorkFlowProcessStageStatusList.Max(x => (int?)x.StatusId ?? 1)) + 1;

                _IWorkFlowRepository.AddSave(_WorkFlowProcessStageStatusList);
                //InsertWorkFlowProcessStageStatusList(_WorkFlowProcessStageStatusList);
                scope.Complete();
                return "Update SuccessFully";
            }
        }


        public dynamic UpdateWorkFlowProcessStageStatusList(string WorkFlowProcessStageStatusList)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                dynamic results = JsonConvert.DeserializeObject(WorkFlowProcessStageStatusList);

                long? recid = null;
                if (results.RecId != null && !string.IsNullOrEmpty(results.RecId.ToString()))
                    recid = long.Parse(results.RecId.ToString());
                else
                    throw new Exception("No Record Found");

                var _WorkFlowProcessStageStatusList = _IWorkFlowRepository.WorkFlowProcessStageStatusList.Where(x => x.RecId == recid).FirstOrDefault();

                if (results.StatusName != null && !string.IsNullOrEmpty(results.StatusName.ToString()))
                    _WorkFlowProcessStageStatusList.StatusName = results.StatusName.ToString();

                if (results.ActionDescription != null && !string.IsNullOrEmpty(results.ActionDescription.ToString()))
                    _WorkFlowProcessStageStatusList.ActionDescription = results.ActionDescription.ToString();

                _IWorkFlowRepository.UpdateSave(_WorkFlowProcessStageStatusList);
                //ModifyWorkFlowProcessStageStatusList(_WorkFlowProcessStageStatusList);
                scope.Complete();
                return "Created SuccessFully";
            }
        }

        public dynamic GetEditByWorkFlowProcessStageStatusList(string RecId)
        {
            var data = _IWorkFlowRepository.WorkFlowProcessStageStatusList.FirstOrDefault();
            return data;
        }

        public dynamic DeleteWorkFlowProcessStageStatusList(string[] RecId)
        {
            using (TransactionScope scope = new TransactionScope())
            {
                foreach (var item in RecId)
                {
                    long rec = long.Parse(item.ToString());
                    var data = _IWorkFlowRepository.WorkFlowProcessStageStatusList.Where(x => x.RecId == rec).FirstOrDefault();
                    _IWorkFlowRepository.RemoveSave(data);
                    //RemoveWorkFlowProcessStageStatusList(data);
                }
                scope.Complete();
                return "Deleted SuccessFully";
            }
        }




        #endregion





    }
}
