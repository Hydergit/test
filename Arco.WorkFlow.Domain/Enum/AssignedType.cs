﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Enum
{
    public enum AssignedType
    {
        User = 1,

        Role = 2
    }
}
