﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Enum
{
    public enum ActionStatus
    {
        Draft = 1,
        Submitted = 2,
        Approved = 3,
        Mapping = 4,
        Activate = 5,
        DeActivate = 6,
        Cancel = 7
    }
}
