﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Enum
{
    public enum MobilizationProcessStages
    {
        [Display(Name = "Interview")]
        NotApplicable = 0,

        [Display(Name = "Interview Selected")]
        InterviewSelected = 10,

        [Display(Name = "Visa Medical Finished")]
        VisaMedicalFinished = 20,

        [Display(Name = "Visa Stamped")]
        VisaStamped = 30,

        [Display(Name = "Ticket Booked")]
        TicketBooked = 40,


        Arrived = 50,

        [Display(Name = "Medical Test")]
        IqamaMedicalFinished = 60,

        [Display(Name = "Labour Office")]
        LabourOfficeFinished = 70,

        [Display(Name = "Medical Insurance")]
        MedicalInsuranceIssued = 80,

        [Display(Name = "Iqama Issued")]
        IqamaIssued = 90,

        [Display(Name = "ATM")]
        ATMIssued = 100,


        Operational = 110,

        [Display(Name = "Payroll")]
        Payroll = 120,

        [Display(Name = "On Demand")]
        OnDemand = 130,

        [Display(Name = "Driving License")]
        DrivingLicense = 140,

        [Display(Name = "Health Certificate")]
        HealthCertificate = 150,

        [Display(Name = "Gosi Salary")]
        GosiSalary = 160,

        [Display(Name = "Medical Insurance Card")]
        MedicalInsuranceCard = 170,

        IqamaRenewal = 180,

        LifeInsurance = 185,

        [Display(Name = "Time Sheet")]
        TimeSheet = 190,

        [Display(Name = "Invoice")]
        Invoice = 200,

        [Display(Name = "Medical Renewal")]
        MedicalRenewal = 201,

        ATMRenewal = 210,

        ATMReIssue = 220,

        IqamaReIssue = 230,

        MedicalInsuranceReIssue = 240,

        LeaveRequest = 250,

        VisaRequest = 260,

        TravelTicket = 270,

        VacationSettlement = 280,
        EmployeeRetirement = 290,

        FinalSettlement = 300,
        ReactivateATM = 600,
        DeactivateATM = 610,
        Escaped = 500,
        Package = 510,
        Envelope = 520,


        DemandLetter = 620,

        CadidateSelection = 630,

        Contract = 640,


        CustPayment = 680,
        CustInvoice = 690,
        DeliveryRequest = 700,
        CheckOutFlow = 710,

        CustomerNewRequest = 720,

        LodgingCheckInFlow = 730,



        LodgingCheckOutFlow = 740,

        ContractTemplate = 750,
        LabourTransfer = 760,
        TransferOrder = 770,
        AllCheckOutFlow = 780,


        ContractExchange = 650,

        ContractTerminate = 660,

        [Display(Name = "Contract Cancel")]
        ContractCancel = 645,

        ContractPackageChange = 655,
        ContractEndRequest = 665,
        ContractExtendRequest = 670,
        ContractNewRequest = 675,

        EmployeeReturnRequest = 820,

        FinanceApplication = 830,

        EnvelopeIndividualWorkFlow = 840,

        RefundRequest = 850,

        CustSponserFlow = 860,

        VacationReturn = 870,

        MedicalInsuranceCancellation = 880,
        LoanReSchedule = 890,

        EmployeeHospitalTransaction = 900,
        EmployeeDeadTransaction = 910,
        EmployeeJailTransaction = 920,
        EmployeeRefuseToWorkTransaction = 930,
        IqamaNumberChangeRequest = 940,
        TransportRequestFlow = 950,
        DeliveryToHomeFlow = 951,
        DeliveryToBranchFlow = 952,
    }
}
