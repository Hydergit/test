﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Interface.IWorkFlowService
{
    public interface IWorkFlowService
    {
        #region WorkFlow
        dynamic GetWorkFlowList();
        dynamic CreateWorkFlow(string  WorkFlow);
        dynamic UpdateWorkFlow(string WorkFlow);
        dynamic GetEditByWorkFlow(string RecId);
        dynamic DeleteWorkFlow(string[] RecId);


        #endregion
        #region WorkFlowProcess
        dynamic GetWorkFlowProcessList(string WorkFlowRecId);
        dynamic CreateWorkFlowProcess(string WorkFlowProcess);
        dynamic UpdateWorkFlowProcess(string WorkFlowProcess);
        dynamic GetEditByWorkFlowProcess(string RecId);
        dynamic DeleteWorkFlowProcess(string[] RecId);
        #endregion

        #region WorkFlowProcessStage
        dynamic GetWorkFlowProcessStageList(string WorkFlowProcessRecId);
        dynamic CreateWorkFlowProcessStage(string WorkFlowProcessStage);
        dynamic UpdateWorkFlowProcessStage(string WorkFlowProcessStage);
        dynamic GetEditByWorkFlowProcessStage(string RecId);
        dynamic DeleteWorkFlowProcessStage(string[] RecId);
        #endregion


        #region AssignRoleAndUser
        dynamic GetAssignUnAssignRoleAndUser(string WorkFlowProcessStageRecId);
        dynamic AssignRoleAndUserForStges(string[] AssigneeId, string WorkFlowProcessStageRecId, string AssignType);
        dynamic DeleteRoleAndUserForStges(string[] AssigneeId, string WorkFlowProcessStageRecId, string AssignType);
        dynamic GetStageAssingDetails(string WorkFlowProcessStageRecId);

        dynamic AssignRoleAndUserForSelectedStages(string[] WorkFlowProcessStageRecId, string AssigneeId, string AssignType);
        dynamic DeleteRoleAndUserForSelectedStages(string[] WorkFlowProcessStageRecId, string AssigneeId, string AssignType);
        dynamic GetUserAndRoleBaseForSelectedStages(string AssigneeId, string WorkflowProcessRecId, string AssignType);
        dynamic GetUserAndRoleSelectedStageStages(string AssigneeId, string WorkflowProcessRecId);
        
        #endregion

        #region WorkFlowProcessStageStatusList
        dynamic GetWorkFlowProcessStageStatusList();
        dynamic CreateWorkFlowProcessStageStatusList(string WorkFlowProcessStageStatusList);
        dynamic UpdateWorkFlowProcessStageStatusList(string WorkFlowProcessStageStatusList);
        dynamic GetEditByWorkFlowProcessStageStatusList(string RecId);
        dynamic DeleteWorkFlowProcessStageStatusList(string[] RecId);


        #endregion
    }
}
