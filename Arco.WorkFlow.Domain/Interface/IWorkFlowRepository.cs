﻿using Arco.Core.AppInterface;
using Arco.WorkFlow.Domain.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Interface.IWorkFlowRepository
{
    public interface IWorkFlowRepository: IRepository
    {
        IQueryable<Workflow> Workflows { get; }

        IQueryable<WorkflowProcess> WorkflowProcess { get; }

        IQueryable<WorkflowProcessStage> WorkflowProcessStage { get; }

        IQueryable<WorkFlowProcessStageStatu> WorkFlowProcessStageStatus { get; }

        IQueryable<WorkFlowNextProcess> WorkFlowNextProcess { get; }

        IQueryable<ProcessStageAssignment> ProcessStageAssignment { get; }

        IQueryable<SysRoleMaster> SysRoleMaster { get; }

        IQueryable<UserLogin> UserLogin { get; }

        IQueryable<WorkFlowProcessStageStatusList> WorkFlowProcessStageStatusList { get; }

    }
}
