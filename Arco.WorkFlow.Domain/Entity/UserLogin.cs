using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // UserLogin

    public partial class UserLogin : ModelBase
    {

 public string Name { get; set; } // Name

 public string EmployeeId { get; set; } // EmployeeId

 public int? Status { get; set; } // Status

 public string UserName { get; set; } // UserName (Primary key)

 public string Password { get; set; } // Password

 public string Email { get; set; } // Email

 public string TimeZone { get; set; } // TimeZone

 public bool? Finance { get; set; } // Finance

 public bool? HR { get; set; } // HR

 public bool? CRM { get; set; } // CRM

 public bool? OPS { get; set; } // OPS

 public bool? Inventory { get; set; } // Inventory








 public bool? Lodging { get; set; } // Lodging

 public string SalesManager { get; set; } // SalesManager

 public string MobileNumber { get; set; } // MobileNumber

 public bool? AdminModule { get; set; } // AdminModule

 public bool? WebAdmin { get; set; } // WebAdmin

 public bool? Payroll { get; set; } // Payroll

 public decimal? CashLimit { get; set; } // CashLimit

 public int? PasswordStatus { get; set; } // PasswordStatus

 public string UserNameEmployee { get; set; } // UserNameEmployee

 public string DomainUsername { get; set; } // DomainUsername

 public int? UserType { get; set; } // UserType

 public string CustomerId { get; set; } // CustomerId



        
        public UserLogin()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
