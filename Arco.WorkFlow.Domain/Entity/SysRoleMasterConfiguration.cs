using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // SysRoleMaster
    public partial class SysRoleMasterConfiguration : EntityTypeConfiguration<SysRoleMaster>
    {
        public SysRoleMasterConfiguration()
            : this("dbo")
        {
        }
 
        public SysRoleMasterConfiguration(string schema)
        {
            ToTable(schema + ".SysRoleMaster");
            HasKey(x => new { x.DataAreaId, x.RoleID });

            Property(x => x.RoleID).HasColumnName("RoleID").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RoleDescription_EN).HasColumnName("RoleDescription_EN").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.RoleDescription_AR).HasColumnName("RoleDescription_AR").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
