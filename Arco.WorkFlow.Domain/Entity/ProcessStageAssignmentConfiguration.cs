using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // ProcessStageAssignment
    public partial class ProcessStageAssignmentConfiguration : EntityTypeConfiguration<ProcessStageAssignment>
    {
        public ProcessStageAssignmentConfiguration()
            : this("dbo")
        {
        }
 
        public ProcessStageAssignmentConfiguration(string schema)
        {
            ToTable(schema + ".ProcessStageAssignment");
            HasKey(x => new { x.DataAreaId, x.RecId });

            Property(x => x.ProcessId).HasColumnName("ProcessId").IsOptional().HasColumnType("int");
            Property(x => x.ProcessRefRecid).HasColumnName("ProcessRefRecid").IsOptional().HasColumnType("bigint");
            Property(x => x.AssignedtoType).HasColumnName("AssignedtoType").IsOptional().HasColumnType("int");
            Property(x => x.Assignedto).HasColumnName("Assignedto").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.Message).HasColumnName("Message").IsOptional().HasColumnType("nvarchar").HasMaxLength(1000);
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
