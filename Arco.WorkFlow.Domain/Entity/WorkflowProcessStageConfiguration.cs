using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkflowProcessStages
    public partial class WorkflowProcessStageConfiguration : EntityTypeConfiguration<WorkflowProcessStage>
    {
        public WorkflowProcessStageConfiguration()
            : this("dbo")
        {
        }
 
        public WorkflowProcessStageConfiguration(string schema)
        {
            ToTable(schema + ".WorkflowProcessStages");
            HasKey(x => new { x.RecId, x.DataAreaId });

            Property(x => x.WorkflowProcessRecId).HasColumnName("WorkflowProcessRecId").IsOptional().HasColumnType("bigint");
            Property(x => x.StageId).HasColumnName("StageId").IsOptional().HasColumnType("int");
            Property(x => x.StageDescription).HasColumnName("StageDescription").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.IsPayment).HasColumnName("IsPayment").IsOptional().HasColumnType("bit");
            Property(x => x.IsStarting).HasColumnName("IsStarting").IsOptional().HasColumnType("bit");
            Property(x => x.StageSequenceNumber).HasColumnName("StageSequenceNumber").IsOptional().HasColumnType("int");
            Property(x => x.IsEnding).HasColumnName("IsEnding").IsOptional().HasColumnType("bit");
            Property(x => x.IsDocument).HasColumnName("IsDocument").IsOptional().HasColumnType("bit");
            Property(x => x.IsSentToCustomer).HasColumnName("IsSentToCustomer").IsOptional().HasColumnType("bit");
            Property(x => x.IsReceiving).HasColumnName("IsReceiving").IsOptional().HasColumnType("bit");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.Subject).HasColumnName("Subject").IsOptional().HasColumnType("nvarchar").HasMaxLength(500);
            Property(x => x.Instruction).HasColumnName("Instruction").IsOptional().HasColumnType("nvarchar");
            Property(x => x.url).HasColumnName("url").IsOptional().HasColumnType("nvarchar");
            Property(x => x.IsEscaped).HasColumnName("IsEscaped").IsOptional().HasColumnType("bit");
            Property(x => x.intervaldays).HasColumnName("intervaldays").IsOptional().HasColumnType("int");
            Property(x => x.IsEscaping).HasColumnName("IsEscaping").IsOptional().HasColumnType("bit");
            Property(x => x.WorkMapStageRecId).HasColumnName("WorkMapStageRecId").IsOptional().HasColumnType("bigint");
            Property(x => x.IsScan).HasColumnName("IsScan").IsOptional().HasColumnType("bit");
            Property(x => x.Design).HasColumnName("Design").IsOptional().HasColumnType("nvarchar");
            Property(x => x.Label).HasColumnName("Label").IsOptional().HasColumnType("nvarchar");
            Property(x => x.Description).HasColumnName("Description").IsOptional().HasColumnType("nvarchar");
            Property(x => x.ElementId).HasColumnName("ElementId").IsOptional().HasColumnType("nvarchar");
            Property(x => x.StageNumber).HasColumnName("StageNumber").IsOptional().HasColumnType("int");
            Property(x => x.StageOrderId).HasColumnName("StageOrderId").IsOptional().HasColumnType("int");

            // Foreign keys
            //HasRequired(a => a.WorkflowProcess).WithMany(b => b.WorkflowProcessStages).HasForeignKey(c => new { c.WorkflowProcessRecId, c.DataAreaId }); // FK_WorkflowProcessStages_WorkflowProcess
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
