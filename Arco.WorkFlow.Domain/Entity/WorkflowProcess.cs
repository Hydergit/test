using Arco.Core.AppBase;
using Arco.WorkFlow.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkflowProcess

    public partial class WorkflowProcess : ModelBase
    {

 public decimal? WorkflowVersion { get; set; } // WorkflowVersion

 public MobilizationProcessStages? ProcessId { get; set; } // ProcessId

 public string ProcessDescription { get; set; } // ProcessDescription

 public int? NextProcess { get; set; } // NextProcess

 public int? SequenceNumber { get; set; } // SequenceNumber








 public long? WorkFlowRecid { get; set; } // WorkFlowRecid

 public string url { get; set; } // url

 public string Parameter { get; set; } // Parameter

 public string RefTableName { get; set; } // RefTableName

 public int? intervaldays { get; set; } // intervaldays

 public bool? IsActive { get; set; } // IsActive

 public ActionStatus? ActiveStatus { get; set; } // ActiveStatus

 public long? MappedActiveVersion { get; set; } // MappedActiveVersion

 public string Paper { get; set; } // Paper



        
        public WorkflowProcess()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
