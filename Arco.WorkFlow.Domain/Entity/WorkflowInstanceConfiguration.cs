using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkflowInstance
    public partial class WorkflowInstanceConfiguration : EntityTypeConfiguration<WorkflowInstance>
    {
        public WorkflowInstanceConfiguration()
            : this("dbo")
        {
        }
 
        public WorkflowInstanceConfiguration(string schema)
        {
            ToTable(schema + ".WorkflowInstance");
            HasKey(x => new { x.RecId, x.DataAreaId });

            Property(x => x.RefKeyId).HasColumnName("RefKeyId").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.RefTableName).HasColumnName("RefTableName").IsOptional().HasColumnType("nvarchar").HasMaxLength(1000);
            Property(x => x.RefRecId).HasColumnName("RefRecId").IsRequired().HasColumnType("bigint");
            Property(x => x.WorkflowVersion).HasColumnName("WorkflowVersion").IsOptional().HasColumnType("decimal").HasPrecision(4,1);
            Property(x => x.StartDate).HasColumnName("StartDate").IsOptional().HasColumnType("date");
            Property(x => x.EndDate).HasColumnName("EndDate").IsOptional().HasColumnType("date");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
