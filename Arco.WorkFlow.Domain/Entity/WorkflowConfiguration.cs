using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{

    // Workflow
    public partial class WorkflowConfiguration : EntityTypeConfiguration<Workflow>
    {
        public WorkflowConfiguration()
            : this("dbo")
        {
        }

        public WorkflowConfiguration(string schema)
        {
            ToTable(schema + ".Workflow");
            HasKey(x => new { x.RecId, x.DataAreaId });

            Property(x => x.WorkflowType).HasColumnName("WorkflowType").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.WorkflowVersion).HasColumnName("WorkflowVersion").IsOptional().HasColumnType("decimal").HasPrecision(4, 1);
            Property(x => x.WorkflowDescription).HasColumnName("WorkflowDescription").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional().HasColumnType("bit");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            InitializePartial();
        }
        partial void InitializePartial();
    }
}

