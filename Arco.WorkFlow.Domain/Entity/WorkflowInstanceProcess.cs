using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{ // WorkflowInstanceProcess

    public partial class WorkflowInstanceProcess : ModelBase
    {

 public string RefTableName { get; set; } // RefTableName

 public string RefKeyId { get; set; } // RefKeyId

 public long RefRecId { get; set; } // RefRecId

 public long? WorkflowInstanceRecId { get; set; } // WorkflowInstanceRecId

 public int? ProcessId { get; set; } // ProcessId

 public int? ProcessStage { get; set; } // ProcessStage

 public string ActionBy { get; set; } // ActionBy

 public DateTime? ActionDate { get; set; } // ActionDate

 public string Comments { get; set; } // Comments

 public DateTime? StartDate { get; set; } // StartDate

 public DateTime? EndDate { get; set; } // EndDate








 public int? WorkFlowType { get; set; } // WorkFlowType

 public long? WorkFlowProcessRecId { get; set; } // WorkFlowProcessRecId



        
        public WorkflowInstanceProcess()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
