using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkFlowProcessStageStatus
    public partial class WorkFlowProcessStageStatuConfiguration : EntityTypeConfiguration<WorkFlowProcessStageStatu>
    {
        public WorkFlowProcessStageStatuConfiguration()
            : this("dbo")
        {
        }
 
        public WorkFlowProcessStageStatuConfiguration(string schema)
        {
            ToTable(schema + ".WorkFlowProcessStageStatus");
            HasKey(x => new { x.DataAreaId, x.RecId });

            Property(x => x.StatusName).HasColumnName("StatusName").IsOptional().HasColumnType("nvarchar").HasMaxLength(1000);
            Property(x => x.IsComplete).HasColumnName("IsComplete").IsOptional().HasColumnType("bit");
            Property(x => x.StatusId).HasColumnName("StatusId").IsOptional().HasColumnType("int");
            Property(x => x.StageRecid).HasColumnName("StageRecid").IsOptional().HasColumnType("bigint");
            Property(x => x.ActionDescription).HasColumnName("ActionDescription").IsOptional().HasColumnType("nvarchar");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.ActionType).HasColumnName("ActionType").IsOptional().HasColumnType("int");
            Property(x => x.CompleteStage).HasColumnName("CompleteStage").IsOptional().HasColumnType("int");
            Property(x => x.Design).HasColumnName("Design").IsOptional().HasColumnType("nvarchar");
            Property(x => x.Label).HasColumnName("Label").IsOptional().HasColumnType("nvarchar");
            Property(x => x.LinkId).HasColumnName("LinkId").IsOptional().HasColumnType("nvarchar");
            Property(x => x.TargetId).HasColumnName("TargetId").IsOptional().HasColumnType("nvarchar");
            Property(x => x.SourceId).HasColumnName("SourceId").IsOptional().HasColumnType("nvarchar");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
