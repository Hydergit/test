using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkflowInstanceProcessStageStatus
    public partial class WorkflowInstanceProcessStageStatu : ModelBase
    {

 public string RefTableName { get; set; } // RefTableName

 public long? WorkflowInstanceProcessRecId { get; set; } // WorkflowInstanceProcessRecId

 public int? ProcessId { get; set; } // ProcessId

 public int? ProcessStage { get; set; } // ProcessStage

 public int? ProcessStatus { get; set; } // ProcessStatus

 public string ActionBy { get; set; } // ActionBy

 public DateTime? ActionDate { get; set; } // ActionDate

 public string Comments { get; set; } // Comments

 public DateTime? StartDate { get; set; } // StartDate

 public DateTime? EndDate { get; set; } // EndDate










        
        public WorkflowInstanceProcessStageStatu()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
