using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{

    // Workflow
    public partial class Workflow : ModelBase
    {

        public string WorkflowType { get; set; } // WorkflowType

        public decimal? WorkflowVersion { get; set; } // WorkflowVersion

        public string WorkflowDescription { get; set; } // WorkflowDescription

        public bool? IsActive { get; set; } // IsActive











        public Workflow()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
