using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkFlowProcessStageStatus
    public partial class WorkFlowProcessStageStatu : ModelBase
    {

 public string StatusName { get; set; } // StatusName

 public bool? IsComplete { get; set; } // IsComplete

 public int? StatusId { get; set; } // StatusId

 public long? StageRecid { get; set; } // StageRecid

 public string ActionDescription { get; set; } // ActionDescription








 public int? ActionType { get; set; } // ActionType

 public int? CompleteStage { get; set; } // CompleteStage

 public string Design { get; set; } // Design

 public string Label { get; set; } // Label

 public string LinkId { get; set; } // LinkId

 public string TargetId { get; set; } // TargetId

 public string SourceId { get; set; } // SourceId



        
        public WorkFlowProcessStageStatu()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
