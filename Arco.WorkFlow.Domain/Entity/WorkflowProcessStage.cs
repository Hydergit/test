using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkflowProcessStages
    public partial class WorkflowProcessStage : ModelBase
    {

 public long? WorkflowProcessRecId { get; set; } // WorkflowProcessRecId

 public int? StageId { get; set; } // StageId

 public string StageDescription { get; set; } // StageDescription

 public bool? IsPayment { get; set; } // IsPayment

 public bool? IsStarting { get; set; } // IsStarting

 public int? StageSequenceNumber { get; set; } // StageSequenceNumber

 public bool? IsEnding { get; set; } // IsEnding

 public bool? IsDocument { get; set; } // IsDocument

 public bool? IsSentToCustomer { get; set; } // IsSentToCustomer

 public bool? IsReceiving { get; set; } // IsReceiving








 public string Subject { get; set; } // Subject

 public string Instruction { get; set; } // Instruction

 public string url { get; set; } // url

 public bool? IsEscaped { get; set; } // IsEscaped

 public int? intervaldays { get; set; } // intervaldays

 public bool? IsEscaping { get; set; } // IsEscaping

 public long? WorkMapStageRecId { get; set; } // WorkMapStageRecId

 public bool? IsScan { get; set; } // IsScan

 public string Design { get; set; } // Design

 public string Label { get; set; } // Label

 public string Description { get; set; } // Description

 public string ElementId { get; set; } // ElementId

 public int? StageNumber { get; set; } // StageNumber

 public int? StageOrderId { get; set; } // StageOrderId



        
        public WorkflowProcessStage()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
