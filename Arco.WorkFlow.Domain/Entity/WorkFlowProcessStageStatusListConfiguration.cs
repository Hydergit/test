using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkFlowProcessStageStatusList
    public partial class WorkFlowProcessStageStatusListConfiguration : EntityTypeConfiguration<WorkFlowProcessStageStatusList>
    {
        public WorkFlowProcessStageStatusListConfiguration()
            : this("dbo")
        {
        }
 
        public WorkFlowProcessStageStatusListConfiguration(string schema)
        {
            ToTable(schema + ".WorkFlowProcessStageStatusList");
            HasKey(x => new { x.DataAreaId, x.StatusId });

            Property(x => x.StatusId).HasColumnName("StatusId").IsRequired().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.StatusName).HasColumnName("StatusName").IsOptional().HasColumnType("nvarchar").HasMaxLength(1000);
            Property(x => x.ActionDescription).HasColumnName("ActionDescription").IsOptional().HasColumnType("nvarchar");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
