using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkFlowNextProcess
    public partial class WorkFlowNextProcess : ModelBase
    {

 public int? ProcessId { get; set; } // ProcessId

 public int? ProcessStage { get; set; } // ProcessStage

 public int? NextProcess { get; set; } // NextProcess

 public int? NextProcessStage { get; set; } // NextProcessStage

 public int? DependType { get; set; } // DependType

 public string Comments { get; set; } // Comments








 public long? StageRecId { get; set; } // StageRecId

 public long? WorkflowProcessRecId { get; set; } // WorkflowProcessRecId



        
        public WorkFlowNextProcess()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
