using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkFlowNextProcess
    public partial class WorkFlowNextProcessConfiguration : EntityTypeConfiguration<WorkFlowNextProcess>
    {
        public WorkFlowNextProcessConfiguration()
            : this("dbo")
        {
        }
 
        public WorkFlowNextProcessConfiguration(string schema)
        {
            ToTable(schema + ".WorkFlowNextProcess");
            HasKey(x => new { x.DataAreaId, x.RecId });

            Property(x => x.ProcessId).HasColumnName("ProcessId").IsOptional().HasColumnType("int");
            Property(x => x.ProcessStage).HasColumnName("ProcessStage").IsOptional().HasColumnType("int");
            Property(x => x.NextProcess).HasColumnName("NextProcess").IsOptional().HasColumnType("int");
            Property(x => x.NextProcessStage).HasColumnName("NextProcessStage").IsOptional().HasColumnType("int");
            Property(x => x.DependType).HasColumnName("DependType").IsOptional().HasColumnType("int");
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasColumnType("nvarchar").HasMaxLength(200);
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.StageRecId).HasColumnName("StageRecId").IsOptional().HasColumnType("bigint");
            Property(x => x.WorkflowProcessRecId).HasColumnName("WorkflowProcessRecId").IsOptional().HasColumnType("bigint");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
