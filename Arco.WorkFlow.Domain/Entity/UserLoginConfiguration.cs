using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // UserLogin
    public partial class UserLoginConfiguration : EntityTypeConfiguration<UserLogin>
    {
        public UserLoginConfiguration()
            : this("dbo")
        {
        }
 
        public UserLoginConfiguration(string schema)
        {
            ToTable(schema + ".UserLogin");
            HasKey(x => new { x.DataAreaId, x.UserName });

            Property(x => x.Name).HasColumnName("Name").IsOptional().HasColumnType("nvarchar");
            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.Status).HasColumnName("Status").IsOptional().HasColumnType("int");
            Property(x => x.UserName).HasColumnName("UserName").IsRequired().HasColumnType("nvarchar").HasMaxLength(50).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Password).HasColumnName("Password").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.Email).HasColumnName("Email").IsOptional().HasColumnType("nvarchar").HasMaxLength(200);
            Property(x => x.TimeZone).HasColumnName("TimeZone").IsOptional().IsUnicode(false).HasColumnType("varchar").HasMaxLength(100);
            Property(x => x.Finance).HasColumnName("Finance").IsOptional().HasColumnType("bit");
            Property(x => x.HR).HasColumnName("HR").IsOptional().HasColumnType("bit");
            Property(x => x.CRM).HasColumnName("CRM").IsOptional().HasColumnType("bit");
            Property(x => x.OPS).HasColumnName("OPS").IsOptional().HasColumnType("bit");
            Property(x => x.Inventory).HasColumnName("Inventory").IsOptional().HasColumnType("bit");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.Lodging).HasColumnName("Lodging").IsOptional().HasColumnType("bit");
            Property(x => x.SalesManager).HasColumnName("SalesManager").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.MobileNumber).HasColumnName("MobileNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.AdminModule).HasColumnName("AdminModule").IsOptional().HasColumnType("bit");
            Property(x => x.WebAdmin).HasColumnName("WebAdmin").IsOptional().HasColumnType("bit");
            Property(x => x.Payroll).HasColumnName("Payroll").IsOptional().HasColumnType("bit");
            Property(x => x.CashLimit).HasColumnName("CashLimit").IsOptional().HasColumnType("decimal").HasPrecision(10,3);
            Property(x => x.PasswordStatus).HasColumnName("PasswordStatus").IsOptional().HasColumnType("int");
            Property(x => x.UserNameEmployee).HasColumnName("UserNameEmployee").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DomainUsername).HasColumnName("DomainUsername").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.UserType).HasColumnName("UserType").IsOptional().HasColumnType("int");
            Property(x => x.CustomerId).HasColumnName("CustomerId").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
