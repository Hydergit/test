using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkflowInstanceProcess
    public partial class WorkflowInstanceProcessConfiguration : EntityTypeConfiguration<WorkflowInstanceProcess>
    {
        public WorkflowInstanceProcessConfiguration()
            : this("dbo")
        {
        }
 
        public WorkflowInstanceProcessConfiguration(string schema)
        {
            ToTable(schema + ".WorkflowInstanceProcess");
            HasKey(x => new { x.RecId, x.DataAreaId });

            Property(x => x.RefTableName).HasColumnName("RefTableName").IsOptional().HasColumnType("nvarchar").HasMaxLength(1000);
            Property(x => x.RefKeyId).HasColumnName("RefKeyId").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.RefRecId).HasColumnName("RefRecId").IsRequired().HasColumnType("bigint");
            Property(x => x.WorkflowInstanceRecId).HasColumnName("WorkflowInstanceRecId").IsOptional().HasColumnType("bigint");
            Property(x => x.ProcessId).HasColumnName("ProcessId").IsOptional().HasColumnType("int");
            Property(x => x.ProcessStage).HasColumnName("ProcessStage").IsOptional().HasColumnType("int");
            Property(x => x.ActionBy).HasColumnName("ActionBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.ActionDate).HasColumnName("ActionDate").IsOptional().HasColumnType("date");
            Property(x => x.Comments).HasColumnName("Comments").IsOptional().HasColumnType("nvarchar").HasMaxLength(500);
            Property(x => x.StartDate).HasColumnName("StartDate").IsOptional().HasColumnType("date");
            Property(x => x.EndDate).HasColumnName("EndDate").IsOptional().HasColumnType("date");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.WorkFlowType).HasColumnName("WorkFlowType").IsOptional().HasColumnType("int");
            Property(x => x.WorkFlowProcessRecId).HasColumnName("WorkFlowProcessRecId").IsOptional().HasColumnType("bigint");

            // Foreign keys
            //HasRequired(a => a.WorkflowInstance).WithMany(b => b.WorkflowInstanceProcesses).HasForeignKey(c => new { c.WorkflowInstanceRecId, c.DataAreaId }); // FK_WorkflowInstanceProcess_WorkflowInstance
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
