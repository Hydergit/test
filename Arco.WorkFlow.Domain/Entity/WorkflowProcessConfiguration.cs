using Arco.Core.AppBase;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.WorkFlow.Domain.Entity
{
    // WorkflowProcess
    public partial class WorkflowProcessConfiguration : EntityTypeConfiguration<WorkflowProcess>
    {
        public WorkflowProcessConfiguration()
            : this("dbo")
        {
        }
 
        public WorkflowProcessConfiguration(string schema)
        {
            ToTable(schema + ".WorkflowProcess");
            HasKey(x => new { x.RecId, x.DataAreaId });

            Property(x => x.WorkflowVersion).HasColumnName("WorkflowVersion").IsOptional().HasColumnType("decimal").HasPrecision(4,1);
            Property(x => x.ProcessId).HasColumnName("ProcessId").IsOptional().HasColumnType("int");
            Property(x => x.ProcessDescription).HasColumnName("ProcessDescription").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.NextProcess).HasColumnName("NextProcess").IsOptional().HasColumnType("int");
            Property(x => x.SequenceNumber).HasColumnName("SequenceNumber").IsOptional().HasColumnType("int");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.WorkFlowRecid).HasColumnName("WorkFlowRecid").IsOptional().HasColumnType("bigint");
            Property(x => x.url).HasColumnName("url").IsOptional().HasColumnType("nvarchar");
            Property(x => x.Parameter).HasColumnName("Parameter").IsOptional().HasColumnType("nvarchar");
            Property(x => x.RefTableName).HasColumnName("RefTableName").IsOptional().HasColumnType("nvarchar");
            Property(x => x.intervaldays).HasColumnName("intervaldays").IsOptional().HasColumnType("int");
            Property(x => x.IsActive).HasColumnName("IsActive").IsOptional().HasColumnType("bit");
            Property(x => x.ActiveStatus).HasColumnName("ActiveStatus").IsOptional().HasColumnType("int");
            Property(x => x.MappedActiveVersion).HasColumnName("MappedActiveVersion").IsOptional().HasColumnType("bigint");
            Property(x => x.Paper).HasColumnName("Paper").IsOptional().HasColumnType("nvarchar");
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
