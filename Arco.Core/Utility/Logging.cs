﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
namespace Arco.Framework.Utility
{
    public class Logging
    {
        public static void LogError()
        {
            System.Exception ex = System.Web.HttpContext.Current.Server.GetLastError();
            LogError(ex);

        }
        public static void LogError(Exception ex, string fileName = "", string JobName = "")
        {
            var currentContext = HttpContext.Current;

            if (ex.Message.Equals("File does not exist."))
            {
                currentContext.ClearError();
                return;
            }

            string logSummery, logDetails, filePath = "No file path found.", url = "No url found to be reported.", logInnerDetails;

            if (currentContext != null)
            {
                filePath = currentContext.Request.FilePath;
                url = currentContext.Request.Url.AbsoluteUri;
            }

            logSummery = ex.Message;
            logDetails = ex.ToString() + "\n--------------------\n";
            logInnerDetails = (ex.InnerException == null ? "" : ex.InnerException.Message);

            //-----------------------------------------------------

            //  string path = System.Web.HttpContext.Current.Server.MapPath("~/SystemLog/" + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt");

            var pathstring = "~/SystemLog/" + fileName + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt";

            string path = "";
            if (currentContext == null)
            {
                // System.Web.Hosting.HostingEnvironment.MapPath().
                path = System.Web.Hosting.HostingEnvironment.MapPath(pathstring);
            }
            else
            {
                path = System.Web.HttpContext.Current.Server.MapPath(pathstring);
            }
            //   FileStream _FileStream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            using (FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
            {

                BufferedStream bfs = new BufferedStream(fStream);
                StreamWriter sWriter = new StreamWriter(bfs);

                //insert a separator line
                sWriter.WriteLine("=================================================================================================");

                //create log for header
                if (!string.IsNullOrEmpty(JobName))
                {
                    sWriter.WriteLine(JobName);
                }
                sWriter.WriteLine(logSummery);
                sWriter.WriteLine("Log time:" + DateTime.Now);
                sWriter.WriteLine("Log Ip Address:" + Utility.CurrentSession.CurrentIpAddress);
                sWriter.WriteLine("Log User:" + (string.IsNullOrEmpty(Utility.CurrentSession.UserId) ? "UnAuthorize" : Utility.CurrentSession.UserId));
                sWriter.WriteLine("URL: " + url);
                sWriter.WriteLine("File Path: " + filePath);

                //create log for body
                sWriter.WriteLine(logDetails);
                sWriter.WriteLine("Inner Details: " + logInnerDetails);
                //insert a separator line
                sWriter.WriteLine("=================================================================================================");

                sWriter.Close();
                fStream.Close();
            }

        }




        public static void LogStatus(string Message, string fileName = "", string JobName = "")
        {
            var currentContext = HttpContext.Current;



            string logSummery, logDetails, filePath = "No file path found.", url = "No url found to be reported.", logInnerDetails;

            if (currentContext != null)
            {
                filePath = currentContext.Request.FilePath;
                url = currentContext.Request.Url.AbsoluteUri;
            }

            logSummery = JobName;
            logDetails = Message + "\n--------------------\n";


            //-----------------------------------------------------

            //  string path = System.Web.HttpContext.Current.Server.MapPath("~/SystemLog/" + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt");
            fileName = string.IsNullOrEmpty(fileName) ? "LogMessage" : fileName;
            var pathstring = "~/SystemLog/" + fileName + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt";

            string path = "";
            if (currentContext == null)
            {
                // System.Web.Hosting.HostingEnvironment.MapPath().
                path = System.Web.Hosting.HostingEnvironment.MapPath(pathstring);
            }
            else
            {
                path = System.Web.HttpContext.Current.Server.MapPath(pathstring);
            }
            //   FileStream _FileStream = new FileStream(path, FileMode.Create, FileAccess.ReadWrite, FileShare.ReadWrite);
            using (FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write, FileShare.ReadWrite))
            {

                BufferedStream bfs = new BufferedStream(fStream);
                StreamWriter sWriter = new StreamWriter(bfs);

                //insert a separator line
                sWriter.WriteLine("=================================================================================================");

                //create log for header

                sWriter.WriteLine(logSummery);
                sWriter.WriteLine("Log time:" + DateTime.Now);
                sWriter.WriteLine("Log Ip Address:" + Utility.CurrentSession.CurrentIpAddress);
                sWriter.WriteLine("Log User:" + (string.IsNullOrEmpty(Utility.CurrentSession.UserId) ? "UnAuthorize" : Utility.CurrentSession.UserId));
                sWriter.WriteLine("URL: " + url);
                sWriter.WriteLine("File Path: " + filePath);

                //create log for body
                sWriter.WriteLine(logDetails);

                //insert a separator line
                sWriter.WriteLine("=================================================================================================");

                sWriter.Close();
                fStream.Close();
            }

        }


        public static void LogSms(eSms esms)
        {
            string iStatus = string.Empty;
            var currentContext = HttpContext.Current;
            string filePath = "No file path found.", url = "No url found to be reported.";
            string txtHighlight = "************************************";
            if (currentContext != null)
            {
                filePath = currentContext.Request.FilePath;
                url = currentContext.Request.Url.AbsoluteUri;
            }
            //switch (esms.resultCode)
            //{
            //    case "0": { iStatus = "Invalid Error Code"; break; }
            //    case "1": { iStatus = "Message Sent Successfully"; break; }
            //    case "10": { iStatus = "Login Failed"; break; }
            //    case "20": { iStatus = "Invalid TagName Format"; break; }
            //    case "30": { iStatus = "Invalid TagName (Spelling,Status,.. etc)"; break; }
            //    case "40": { iStatus = "Insufficient Fund"; break; }
            //    case "50": { iStatus = "Recepient Number Length does not equal"; break; }
            //    case "51": { iStatus = "varsList Length does not equal to one of the of the token in the replacment list"; break; }
            //    case "60": { iStatus = "Invalid Recipient mobile number"; break; }
            //}

            var pathstring = "~/SystemLog/Sms" + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt";

            string path = "";
            if (currentContext == null)
            {
                // System.Web.Hosting.HostingEnvironment.MapPath().
                path = System.Web.Hosting.HostingEnvironment.MapPath(pathstring);
            }
            else
            {
                path = System.Web.HttpContext.Current.Server.MapPath(pathstring);
            }

            FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write);
            BufferedStream bfs = new BufferedStream(fStream);
            StreamWriter sWriter = new StreamWriter(bfs, Encoding.Unicode);
            sWriter.WriteLine(string.Format("{4}{1}Time     : {0}{1}To       : {2}{1}Message  : {3}{1}Status    : {5}{1}{4}", DateTime.Now, Environment.NewLine, esms.mobileNumber, esms.finalMessage, txtHighlight, esms.resultCode));
            sWriter.Close();
        }

        public static void LogSalary(Exception ex)
        {
            string iStatus = string.Empty;
            var currentContext = HttpContext.Current;
            string filePath = "No file path found.", url = "No url found to be reported.";
            string txtHighlight = "************************************";
            if (currentContext != null)
            {
                filePath = currentContext.Request.FilePath;
                url = currentContext.Request.Url.AbsoluteUri;
            }
            //switch (esms.resultCode)
            //{
            //    case "0": { iStatus = "Invalid Error Code"; break; }
            //    case "1": { iStatus = "Message Sent Successfully"; break; }
            //    case "10": { iStatus = "Login Failed"; break; }
            //    case "20": { iStatus = "Invalid TagName Format"; break; }
            //    case "30": { iStatus = "Invalid TagName (Spelling,Status,.. etc)"; break; }
            //    case "40": { iStatus = "Insufficient Fund"; break; }
            //    case "50": { iStatus = "Recepient Number Length does not equal"; break; }
            //    case "51": { iStatus = "varsList Length does not equal to one of the of the token in the replacment list"; break; }
            //    case "60": { iStatus = "Invalid Recipient mobile number"; break; }
            //}

            var pathstring = "~/SystemLog/" + "Salary" + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt";

            string path = "";
            if (currentContext == null)
            {
                // System.Web.Hosting.HostingEnvironment.MapPath().
                path = System.Web.Hosting.HostingEnvironment.MapPath(pathstring);
            }
            else
            {
                path = System.Web.HttpContext.Current.Server.MapPath(pathstring);
            }

            FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write);
            BufferedStream bfs = new BufferedStream(fStream);
            StreamWriter sWriter = new StreamWriter(bfs, Encoding.Unicode);
            sWriter.WriteLine(ex.Message);
            sWriter.Close();
        }

        public static void CheckJobTiming()
        {




            //-----------------------------------------------------

            string path = System.Web.HttpContext.Current.Server.MapPath("~/SystemLog/" + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt");
            FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write);
            BufferedStream bfs = new BufferedStream(fStream);
            StreamWriter sWriter = new StreamWriter(bfs);

            //insert a separator line
            sWriter.WriteLine("=================================================================================================");

            //create log for header

            sWriter.WriteLine("JobExcute time:" + DateTime.Now);
            sWriter.WriteLine("Log Ip Address:" + Utility.CurrentSession.CurrentIpAddress);
            sWriter.WriteLine("Log User:" + (string.IsNullOrEmpty(Utility.CurrentSession.UserId) ? "UnAuthorize" : Utility.CurrentSession.UserId));


            //create log for body


            //insert a separator line
            sWriter.WriteLine("=================================================================================================");

            sWriter.Close();

        }


        public static void LogDataConflictInfo()
        {




            //-----------------------------------------------------

            string path = System.Web.HttpContext.Current.Server.MapPath("~/SystemLog/Conflict" + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt");
            FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write);
            BufferedStream bfs = new BufferedStream(fStream);
            StreamWriter sWriter = new StreamWriter(bfs);

            //insert a separator line
            sWriter.WriteLine("=================================================================================================");

            //create log for header

            sWriter.WriteLine("JobExcute time:" + DateTime.Now);
            sWriter.WriteLine("Log Ip Address:" + Utility.CurrentSession.CurrentIpAddress);
            sWriter.WriteLine("Log User:" + (string.IsNullOrEmpty(Utility.CurrentSession.UserId) ? "UnAuthorize" : Utility.CurrentSession.UserId));


            //create log for body


            //insert a separator line
            sWriter.WriteLine("=================================================================================================");

            sWriter.Close();

        }


        //Test Logging
        public static void AddError(string message,Exception ex=null)
        {

            log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            if (ex == null)
            {
                log.Error(message);
            }
            else
            {
                log.Error(message,ex);
            }
        }

        public static void AddInfo(string message, Exception ex = null)
        {

            log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
            if (ex == null)
            {
                log.Info(message);
            }
            else
            {
                log.Info(message, ex);
            }
        }



        #region 09 sep 2013  website
        public static void webLogError()
        {
            System.Exception ex = System.Web.HttpContext.Current.Server.GetLastError();
            webLogError(ex);
        }

        public static void webLogError(Exception ex)
        {
            var currentContext = HttpContext.Current;
            if (ex.Message.Equals("File does not exist."))
            {
                currentContext.ClearError();
                return;
            }
            string logSummery, logDetails, filePath = "No file path found.", url = "No url found to be reported.", txtHighlight = "=================================================================================================";
            if (currentContext != null)
            {
                filePath = currentContext.Request.FilePath;
                url = currentContext.Request.Url.AbsoluteUri;
            }
            logSummery = ex.Message;
            logDetails = ex.ToString();

            string path = System.Web.HttpContext.Current.Server.MapPath("~/SystemLog/" + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt");
            FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write);
            BufferedStream bfs = new BufferedStream(fStream);
            StreamWriter sWriter = new StreamWriter(bfs, Encoding.Unicode);
            sWriter.WriteLine(string.Format("{0}{1}{2}{1}Log time:{3}{1}URL:{4}{1}File Path:{5}{1}{6}{1}{0}", txtHighlight, Environment.NewLine, logSummery, DateTime.Now, url, filePath, logDetails));
            sWriter.Close();
        }

        //public static void LogSms(HTERP.Utility.Communication.Model.eSms esms)
        //{
        //    string iStatus = string.Empty;
        //    var currentContext = HttpContext.Current;
        //    string filePath = "No file path found.", url = "No url found to be reported.";
        //    string txtHighlight = "************************************";
        //    if (currentContext != null)
        //    {
        //        filePath = currentContext.Request.FilePath;
        //        url = currentContext.Request.Url.AbsoluteUri;
        //    }
        //    switch (esms.resultCode)
        //    {
        //        case "0": { iStatus = "Invalid Error Code"; break; }
        //        case "1": { iStatus = "Message Sent Successfully"; break; }
        //        case "10": { iStatus = "Login Failed"; break; }
        //        case "20": { iStatus = "Invalid TagName Format"; break; }
        //        case "30": { iStatus = "Invalid TagName (Spelling,Status,.. etc)"; break; }
        //        case "40": { iStatus = "Insufficient Fund"; break; }
        //        case "50": { iStatus = "Recepient Number Length does not equal"; break; }
        //        case "51": { iStatus = "varsList Length does not equal to one of the of the token in the replacment list"; break; }
        //        case "60": { iStatus = "Invalid Recipient mobile number"; break; }
        //    }
        //    string path = System.Web.HttpContext.Current.Server.MapPath(HTERP.Configuration.Resources.LoggingPath.Sms + DateTime.Now.ToDateString(DateFormat.YYYYMMDD, string.Empty) + ".txt");
        //    FileStream fStream = new FileStream(path, FileMode.Append, FileAccess.Write);
        //    BufferedStream bfs = new BufferedStream(fStream);
        //    StreamWriter sWriter = new StreamWriter(bfs, Encoding.Unicode);
        //    sWriter.WriteLine(string.Format("{4}{1}Time     : {0}{1}To       : {2}{1}Message  : {3}{1}Status    : {5}{1}{4}", DateTime.Now, Environment.NewLine, esms.mobileNumber, esms.finalMessage, txtHighlight, iStatus));
        //    sWriter.Close();
        //}

        #endregion





    }

    public partial class eSms
    {
        public string mobileNumber { get; set; }





        public string finalMessage { get; set; }
        public string resultCode { get; set; }

    }
}
