﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Framework.Utility
{
    public partial class BizHelper
    {
        public static void ThrowDbEntityValidationException(DbEntityValidationException ex)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var failure in ex.EntityValidationErrors)
            {
                sb.AppendFormat("{0} failed validation\n", failure.Entry.Entity.GetType());
                foreach (var error in failure.ValidationErrors)
                {
                    sb.AppendFormat("- {0} : {1}", error.PropertyName, error.ErrorMessage);
                    sb.AppendLine();
                }
            }
            throw new EntityValidationFailedException(sb.ToString());
        }

        public static void ThrowErrorForInvalidDataKey(string dataFieldName)
        {
            throw new InvalidDataKeyException("Data field is not a valid data key. Data field name: " + dataFieldName);
        }

        public static void ThrowErrorForEmptyValue(string dataFieldName)
        {
            throw new EmptyValueNotAllowedException("Data field is not allowed to be empty. Data field name: " + dataFieldName);
        }

        public static void ThrowErrorForNullValue(string dataFieldName)
        {
            throw new NullValueNotAllowedException("Data field is not allowed to be null. Data field name: " + dataFieldName);
        }

        public static string SubmitException(Exception _Exception, string strScreenId)
        {
            return ExceptionManager.DoLogAndGetFriendlyMessageForException(_Exception);
        }


    }
}
