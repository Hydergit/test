﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Framework.Utility
{
    public class InvalidDataKeyException : Exception
    {
        public InvalidDataKeyException(string message) : base(message) { }
    }

    public class NullValueNotAllowedException : Exception
    {
        public NullValueNotAllowedException(string message) : base(message) { }
    }

    public class EmptyValueNotAllowedException : Exception
    {
        public EmptyValueNotAllowedException(string message) : base(message) { }
    }

    public class DataNotUpdatedException : Exception
    {
        public DataNotUpdatedException(string message) : base(message) { }
    }

    public class BusinessRuleViolationOnInMemoryException : Exception
    {
        public BusinessRuleViolationOnInMemoryException(string message) : base(message) { }
    }

    public class BusinessRuleViolationOnDbAccessException : Exception
    {
        public BusinessRuleViolationOnDbAccessException(string message) : base(message) { }
    }

    public class LoginException : Exception
    {
        public LoginException(string message) : base(message) { }
    }

    public class EntityValidationFailedException : Exception
    {
        public EntityValidationFailedException(string message) : base(message) { }
    }
}
