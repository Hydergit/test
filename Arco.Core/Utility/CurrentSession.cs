﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Arco.Framework.Utility
{
    public static class CurrentSession
    {
        public static string UserId
        {
            get
            {
                //if (!HttpContext.Current.Request.IsAuthenticated)
                //    new LoginException("User doesn't Logged In");
                string username = string.Empty;
                //if (HttpContext.Current != null)
                //{
                //    if (HttpContext.Current.Session[SessionNames.websiteType] != null)
                //    {
                //        if (HttpContext.Current.Session[SessionNames.websiteType].ToString() == CommonConfigValues.publicsite)
                //            username = CookieHelper.GetCookieValue(CookieNames.webloginId);
                //        else if (HttpContext.Current.Session[SessionNames.websiteType].ToString() == CommonConfigValues.EEPsite)
                //            username = CookieHelper.GetCookieValue("username");
                //    }
                //    else
                //    {
                username = CookieHelper.GetCookieValue("username");
                //  }
                //}
                //else
                //{
                //    username = "winuser";
                //}

                return username;
                //return "Admin";
            }
        }


        public static string CustomerId
        {
            get
            {
                //if (!HttpContext.Current.Request.IsAuthenticated)
                //    new LoginException("User doesn't Logged In");
                string username = string.Empty;
                //if (HttpContext.Current != null)
                //{
                //    if (HttpContext.Current.Session[SessionNames.websiteType] != null)
                //    {
                //        if (HttpContext.Current.Session[SessionNames.websiteType].ToString() == CommonConfigValues.publicsite)
                //            username = CookieHelper.GetCookieValue(CookieNames.webloginId);
                //        else if (HttpContext.Current.Session[SessionNames.websiteType].ToString() == CommonConfigValues.EEPsite)
                //            username = CookieHelper.GetCookieValue("username");
                //    }
                //    else
                //    {
                username = CookieHelper.GetCookieValue("CustomerID");
                //  }
                //}
                //else
                //{
                //    username = "winuser";
                //}

                return username;
                //return "Admin";
            }
        }

        public static string IpAddress
        {
            get
            {


                string username = string.Empty;

                //username = CookieHelper.GetCurrentIpValue();

                username = "";
                //  }
                //}
                //else
                //{
                //    username = "winuser";
                //}

                return username;
                //return "Admin";
            }
        }

        public static string CurrentIpAddress
        {
            get
            {


                string username = string.Empty;

                try
                {
                    username = CookieHelper.GetCurrentIpValue();
                }
                catch (Exception)
                {
                    username = string.Empty;
                }

                //  }
                //}
                //else
                //{
                //    username = "winuser";
                //}

                return username;
                //return "Admin";
            }
        }

        public static string DataAreaId
        {
            get
            {
                // if (!HttpContext.Current.Request.IsAuthenticated)
                //     new LoginException("User doesn't Logged In");

                //CookieHelper.GetCookieValue("DataAreaId");
                return "AYN";
            }
        }


        public static string IpAddressNew
        {
            get
            {
                string ipaddress;
                ipaddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                if (ipaddress == "" || ipaddress == null)
                    ipaddress = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
                return ipaddress + "  " + HttpContext.Current.Request.UserHostName;

            }
        }

        public static string SuccessMessage { get { return "SuccessMessage"; } }
        public static string ErrorMessage { get { return "ErrorMessage"; } }

        //public static string websiteType
        //{

        //}
    }
}
