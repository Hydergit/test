﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Framework.Utility
{
    public enum MessageType
    {
        Success,
        Error,
        Notice,
        WebSucess, //07 sep 2013
        WebError,
        WebInfo,
        WebNotice,
        WebSysError,
        Warning
    }

    public class MessageFormatter
    {
        public static string GetFormattedSuccessMessage(string message)
        {
            return GetFormattedMessage(message, MessageType.Success);
        }

        public static string GetFormattedRecordFoundMessage(string message, long recordCount)
        {
            return GetFormattedMessage(message, recordCount, MessageType.Success);
        }

        public static string GetFormattedErrorMessage(string message)
        {
            return GetFormattedMessage(message, MessageType.Error);
        }

        public static string GetFormattedNoticeMessage(string message)
        {
            return GetFormattedMessage(message, MessageType.Notice);
        }

        public static string GetFormattedWarningMessage(string message)
        {
            return GetFormattedMessage(message, MessageType.Warning);
        }

        public static string GetFormattedMessage(string message, long recordCount, MessageType messageType = MessageType.Notice)
        {
            return "<div class='success'>" + recordCount.ToString() + " : " + message + "</div>";
        }

        //07 sep 2013 . using result variable
        public static string GetFormattedMessage(string message, MessageType messageType = MessageType.Notice)
        {
            //copied from default block...
            string result = "<div class='notice'>" + message + "</div>";
            switch (messageType)
            {

                case MessageType.Success:
                    {
                        result = "<div class='success'>" + message + "</div>";
                        break;
                    }
                case MessageType.Warning:
                    {
                        result = "<div class='warning'>" + message + "</div>";
                        break;
                    }
                case MessageType.Error:
                    {
                        string s = "<script type=";
                        s = s + " text/javascript";
                        s = s + ">jQuery(document).ready(function() {$(";
                        s = s + ".alert .toggle-alert";
                        s = s + ").click(function(){ $(this).closest(";
                        s = s + ".alert";
                        s = s + ").slideUp(); return false;});});</script>";
                        result = s + "<div class='error'>" + message + "</div>";
                        break;
                    }
                case MessageType.WebError:
                    {

                        result = "<div class='errorWeb'>" + message + "</div>";
                        break;
                    }
                case MessageType.WebSucess:
                    {
                        // message = string.Format(Configuration.Resources.Exceptions.SuccessPrefix, message); 
                        result = "<div class='successWeb'>" + message + "</div>";
                        break;
                    }
                case MessageType.WebInfo:
                    {
                        //message = string.Format(Configuration.Resources.Exceptions.InfoPrefix, message); 
                        result = "<div class='infoWeb'>" + message + "</div>";
                        break;
                    }

                case MessageType.WebNotice:
                    {
                        //message = string.Format(Configuration.Resources.Exceptions.NoticePrefix, message); 
                        result = "<div class='noticeWeb'>" + message + "</div>";
                        break;
                    }
                case MessageType.WebSysError:
                    {
                        //message = string.Format(Configuration.Resources.Exceptions.NoticePrefix, message); 
                        result = "<div class='errorWeb'>" + message + "</div>";
                        break;
                    }
            }
            return result;
        }

        #region 07 sep 2013 for website .. [different style]

        public static string show_UserErrorMsg_web(string message)
        {
            return GetFormattedMessage(message, MessageType.WebError);
        }
        public static string show_systemErrorMsg_web(string message)
        {
            return GetFormattedMessage(message, MessageType.WebSysError);
        }
        public static string show_SucessMsg_web(string message)
        {
            return GetFormattedMessage(message, MessageType.WebSucess);
        }

        public static string show_InfoMsg_web(string message)
        {
            return GetFormattedMessage(message, MessageType.WebInfo);
        }

        public static string show_NoticeMsg_web(string message)
        {
            return GetFormattedMessage(message, MessageType.WebNotice);
        }

        #endregion
    }
}
