﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;

namespace Arco.Framework.Utility
{
    public static class CookieHelper
    {
        public static bool CookieExists(string cookieName)
        {
            if (HttpContext.Current == null) return false;
            if (String.IsNullOrEmpty(cookieName)) return false;
            return (HttpContext.Current.Request.Cookies[cookieName] != null);
        }
        public static string GetCookieValue(string cookieName)
        {
            if (HttpContext.Current == null) return String.Empty;
            if (String.IsNullOrEmpty(cookieName)) return String.Empty;
            if (HttpContext.Current.Request.Cookies[cookieName] == null)
            {
                return string.Empty;
            }
            return HttpContext.Current.Request.Cookies.Get(cookieName).Value;
        }


        public static string GetCurrentIpValue()
        {
            //string hostName = Dns.GetHostName(); // Retrive the Name of HOST

            //// Get the IP

            //string myIP = Dns.GetHostByName(hostName).AddressList[0].ToString();

            string myIP;

            //return Dns.GetHostByName(Dns.GetHostName()).AddressList[0].ToString();

            myIP = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (myIP == "" || myIP == null)
                myIP = HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];




            if (string.IsNullOrEmpty(myIP))
            {
                return string.Empty;
            }
            else
            {
                return myIP;
            }

        }


        public static void SetPersistentCookie(String cookieName, String cookieValue)
        {
            if (String.IsNullOrEmpty(cookieName) || String.IsNullOrEmpty(cookieValue)) return;
            if (HttpContext.Current != null)
            {
                HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.Now.AddYears(1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
        public static void SetCookie(String cookieName, String cookieValue, bool persistent = false)
        {
            if (String.IsNullOrEmpty(cookieName) || String.IsNullOrEmpty(cookieValue)) return;
            if (HttpContext.Current != null)
            {
                if (persistent)
                {
                    SetPersistentCookie(cookieName, cookieValue);
                }
                else
                {
                    HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
                    cookie.HttpOnly = true;
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
        }
        public static void SetCookie_uid(String cookieName, String cookieValue, bool persistent = false)
        {
            if (String.IsNullOrEmpty(cookieName) || String.IsNullOrEmpty(cookieValue)) return;
            if (HttpContext.Current != null)
            {
                if (persistent)
                {
                    SetPersistentCookie(cookieName, cookieValue);
                }
                else
                {
                    //HttpContext.Current.Response.Cookies[Configuration.Resources.CookieNames.gridRecCntPgeSze].Value = null;
                    //HttpContext.Current.Response.Cookies[Configuration.Resources.CookieNames.gridRecCntPgeSze].HttpOnly = true;
                    //HttpContext.Current.Response.Cookies[Configuration.Resources.CookieNames.gridRecCntPgeSze].Value = cookieValue;

                    HttpContext.Current.Response.Cookies[cookieName].Expires = DateTime.Now.AddSeconds(2);
                    HttpContext.Current.Response.Cookies[cookieName].Value = null;
                    HttpContext.Current.Response.Cookies[cookieName].HttpOnly = true;
                    HttpContext.Current.Response.Cookies[cookieName].Value = cookieValue;
                }
            }
        }

        public static void SetCookie_uid_page(String cookieName, String cookieValue, bool persistent = false)
        {
            if (String.IsNullOrEmpty(cookieName) || String.IsNullOrEmpty(cookieValue)) return;
            if (HttpContext.Current != null)
            {
                if (persistent)
                {
                    SetPersistentCookie(cookieName, cookieValue);
                }
                else
                {
                    HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
                    cookie.HttpOnly = true;
                    HttpContext.Current.Response.Cookies.Add(cookie);

                }
            }
        }

        public static string GetCookieValue_uid(string cookieName)
        {
            if (HttpContext.Current == null) return "0";
            if (String.IsNullOrEmpty(cookieName)) return "0";
            if (HttpContext.Current.Request.Cookies[cookieName] == null)
            {
                return "0";
            }
            return HttpContext.Current.Request.Cookies[cookieName].Value;
        }
        public static void ExpireCookie(String cookieName)
        {
            if (String.IsNullOrEmpty(cookieName)) return;
            if (HttpContext.Current != null)
            {
                HttpCookie cookie = new HttpCookie(cookieName, string.Empty);
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.Now.AddYears(-5);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
        public static void SetSecureCookie(String cookieName, String cookieValue)
        {
            if (String.IsNullOrEmpty(cookieName) || String.IsNullOrEmpty(cookieValue)) return;
            if (HttpContext.Current == null) return;
            HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
            cookie.HttpOnly = true;
            SignAndSecureCookie(cookie, HttpContext.Current.Request.ServerVariables);
            HttpContext.Current.Response.Cookies.Add(cookie);
        }
        public static string GetSecureCookieValue(string cookieName)
        {
            if (HttpContext.Current == null) return String.Empty;
            if (String.IsNullOrEmpty(cookieName)) return String.Empty;
            HttpCookie cookie = HttpContext.Current.Request.Cookies.Get(cookieName);
            if (cookie == null) return string.Empty;
            //CryptoHelper cryptoHelper = new CryptoHelper();
            string value = DecryptAndVerifyCookie(cookie, HttpContext.Current.Request.ServerVariables);
            return value.ToString();
        }
        public static void SignAndSecureCookie(HttpCookie cookie, NameValueCollection serverVariables)
        {
            //need to implement encript functionality
        }
        public static string DecryptAndVerifyCookie(HttpCookie cookie, NameValueCollection serverVariables)
        {
            //need to implement decript functionality
            return cookie.Value;
        }
        #region web-guest user cookie -- 1 mins    18 july 2013 ..used for web-guest user....
        public static void WebGuest_SetCookie(String cookieName, String cookieValue)
        {
            if (HttpContext.Current != null)
            {
                //due to cookies override........23 sep 2013
                if (HttpContext.Current.Request.Cookies[cookieName] != null)
                {
                    CookieHelper.ExpireCookie(cookieName, "dummyWeb");
                }
                HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.Now.AddMinutes(1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
        //due to cookies override.............23 sep 2013
        public static void SetCookie(String cookieName, String cookieValue, string dummyWeb, bool persistent = false)
        {
            if (String.IsNullOrEmpty(cookieName) || String.IsNullOrEmpty(cookieValue)) return;
            if (HttpContext.Current != null)
            {
                if (persistent)
                {
                    SetPersistentCookie(cookieName, cookieValue, dummyWeb);
                }
                else
                {
                    //due to cookies override........23 sep 2013
                    if (HttpContext.Current.Request.Cookies[cookieName] != null)
                    {
                        CookieHelper.ExpireCookie(cookieName, dummyWeb);
                    }
                    HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
                    cookie.HttpOnly = true;
                    HttpContext.Current.Response.Cookies.Add(cookie);
                }
            }
        }
        //due to cookies override ..............23 sep 2013
        public static void SetPersistentCookie(String cookieName, String cookieValue, string dummyWeb)
        {
            if (String.IsNullOrEmpty(cookieName) || String.IsNullOrEmpty(cookieValue)) return;
            if (HttpContext.Current != null)
            {
                //due to cookies override........23 sep 2013
                if (HttpContext.Current.Request.Cookies[cookieName] != null)
                {
                    CookieHelper.ExpireCookie(cookieName, dummyWeb);
                }
                HttpCookie cookie = new HttpCookie(cookieName, cookieValue);
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.Now.AddYears(1);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
        //23 sep 2013
        public static void ExpireCookie(String cookieName, string dummyWeb)
        {
            if (String.IsNullOrEmpty(cookieName)) return;
            if (HttpContext.Current != null)
            {
                HttpContext.Current.Request.Cookies.Remove(cookieName);
                HttpCookie cookie = new HttpCookie(cookieName, string.Empty);
                cookie.HttpOnly = true;
                cookie.Expires = DateTime.Now.AddYears(-5);
                HttpContext.Current.Response.Cookies.Add(cookie);
            }
        }
        #endregion
    }
}
