﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.Utility
{
    public enum DateFormat
    {
        DDMMYY,
        DDMMyyyy,
        DDmmmYY,
        DDmmmyyyy,
        MMDDYY,
        MMDDyyyy,
        mmmDDYY,
        mmmDDyyy,
        YYYYMMDD,
        MMMYYYY,
        MMYYYY
    }

    public static class PrimitiveExtension
    {
        public static DateTime Convertdate(this string date)
        {
            DateTime dt = DateTime.ParseExact(date, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            return dt;
        }

        public static DateTime FirstDateMonth(this DateTime date)
        {
            return date.AddDays(-(date.Day - 1)).Date;
        }

        public static DateTime? FirstDateMonth(this DateTime? Date)
        {
            if (!Date.IsEmpty())
                return ((DateTime)Date).FirstDateMonth();
            else
                return Date;
        }

        public static DateTime LastDateMonth(this DateTime date)
        {
            DateTime _date = date.AddMonths(1);
            _date = _date.AddDays(-(_date.Day));
            return _date.Date;
        }

        public static DateTime? LastDateMonth(this DateTime? Date)
        {
            if (!Date.IsEmpty())
                return ((DateTime)Date).LastDateMonth();
            else
                return Date;
        }

        public static int WeekofYear(this DateTime date)
        {
            var currentCulture = CultureInfo.CurrentCulture;
            var weekNo = currentCulture.Calendar.GetWeekOfYear(
                            date,
                            currentCulture.DateTimeFormat.CalendarWeekRule,
                            currentCulture.DateTimeFormat.FirstDayOfWeek);
            return weekNo;
        }

        public static int WeekofYear(this DateTime? Date)
        {
            if (!Date.IsEmpty())
                return ((DateTime)Date).WeekofYear();
            else
                return 0;
        }


        public static string ToDateString(this DateTime? value, DateFormat dateFormat = DateFormat.DDmmmYY, string delimiter = "/")
        {
            if (value.HasValue)
                return ((DateTime)value).ToDateString(dateFormat, delimiter);
            else
                return string.Empty;
        }

        public static string ToDateString(this DateTime value, DateFormat dateFormat = DateFormat.DDmmmYY, string delimiter = "/")
        {
            if (!value.IsEmpty())
            {
                switch (dateFormat)
                {
                    case DateFormat.DDMMYY:
                        return String.Format("{0:dd}{1}{0:MM}{1}{0:yy}", value, delimiter);
                    case DateFormat.DDMMyyyy:
                        return String.Format("{0:dd}{1}{0:MM}{1}{0:yyyy}", value, delimiter);
                    case DateFormat.DDmmmYY:
                        return String.Format("{0:dd}{1}{0:MMM}{1}{0:yyyy}", value, delimiter);
                    case DateFormat.DDmmmyyyy:
                        return String.Format("{0:dd}{1}{0:MMM}{1}{0:yyyy}", value, delimiter);
                    case DateFormat.MMDDYY:
                        return String.Format("{0:MM}{1}dd{1}{0:yy}", value, delimiter);
                    case DateFormat.MMDDyyyy:
                        return String.Format("{0:MM}{1}dd{1}{0:yyyy}", value, delimiter);
                    case DateFormat.mmmDDYY:
                        return String.Format("{0:MMM}{1}dd{1}{0:yy}", value, delimiter);
                    case DateFormat.mmmDDyyy:
                        return String.Format("{0:MMM}{1}DD{1}{0:yyyy}", value, delimiter);
                    case DateFormat.YYYYMMDD:
                        return String.Format("{0:yyyy}{1}{0:MM}{1}{0:dd}", value, delimiter);
                    case DateFormat.MMMYYYY:
                        return String.Format("{0:MMM}{1}{0:yyyy}", value, delimiter);
                    case DateFormat.MMYYYY:
                        return String.Format("{0:MM}{1}{0:yyyy}", value, delimiter);
                }
                return String.Format("{0:dd}{1}{0:MM}{1}{0:yy}", value, delimiter);
            }
            else
                return string.Empty;
        }

        public static string ToRelativeDateString(this DateTime date)
        {
            return GetRelativeDateValue(date, DateTime.Now);
        }

        public static string ToRelativeDateStringUtc(this DateTime date)
        {
            return GetRelativeDateValue(date, DateTime.UtcNow);
        }

        private static string GetRelativeDateValue(DateTime date, DateTime comparedTo)
        {
            TimeSpan diff = comparedTo.Subtract(date);

            if (diff.Days >= 7)
                return string.Concat("on ", date.ToString("MMMM dd, yyyy"));
            else if (diff.Days > 1)
                return string.Concat(diff.Days, " days ago");
            else if (diff.Days == 1)
                return "yesterday";
            else if (diff.Hours >= 2)
                return string.Concat(diff.Hours, " hours ago");
            else if (diff.Minutes >= 60)
                return "more than an hour ago";
            else if (diff.Minutes >= 5)
                return string.Concat(diff.Minutes, " minutes ago");
            if (diff.Minutes >= 1)
                return "a few minutes ago";
            else
                return "less than a minute ago";
        }

        public static DateTime ToTimeZoneDate(this DateTime utcDate, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(utcDate, timeZone);
        }

        public static DateTime? ToTimeZoneDate(this DateTime? Date, TimeZoneInfo timeZone)
        {
            if (!Date.IsEmpty())
                return ((DateTime)Date).ToTimeZoneDate(timeZone); //Util.ToUserLocalTime((DateTime)Date, timeZone);
            else
                return Date;
        }

        public static DateTime ToUTCDate(this DateTime Date, TimeZoneInfo timeZone)
        {
            return TimeZoneInfo.ConvertTimeToUtc(Date, timeZone);
        }

        public static DateTime? ToUTCDate(this DateTime? Date, TimeZoneInfo timeZone)
        {
            if (!Date.IsEmpty())
                return ((DateTime)Date).ToUTCDate(timeZone);
            else
                return Date;
        }

        public static DateTime ToDateTime(this string Date)
        {
            DateTime dt = new DateTime();

            if (!Date.IsEmpty())
            {
                DateTime.TryParse(Date, out dt);
                return dt;
            }
            else
                return DateTime.Now;
        }


        public static string ToDateString(this string Date)
        {
            DateTime dt = new DateTime();

            if (!Date.IsEmpty())
            {
                DateTime.TryParse(Date, out dt);
                return dt.ToShortDateString();
            }
            else
                return DateTime.Now.ToShortDateString();
        }

        public static bool IsEmpty(this DateTime value)
        {
            return value == DateTime.MinValue;
        }

        public static bool IsEmpty(this DateTime? value)
        {
            if (value != null)
                return ((DateTime)value).IsEmpty();

            return false;
        }

        public static bool IsInvalidKey(this decimal value)
        {
            return (value == 0 || value < 0);
        }

        public static string RoundedString(this decimal value)
        {
            return string.Format("{0:0.00}", value.Round());
        }

        public static decimal Round(this decimal value)
        {
            return Math.Round(value, 2, MidpointRounding.AwayFromZero);
        }

        public static decimal Round(this decimal? value)
        {
            if (value != null)
                return ((decimal)value).Round();

            return 0;
        }
        //string 
        #region string Extension Methods

        public static bool IsEmpty(this string value)
        {
            return value == string.Empty;
        }

        public static bool IsNull(this string value)
        {
            return value == null;
        }

        public static bool IsInvalidKey(this string value)
        {
            return (value.IsEmpty() || string.IsNullOrWhiteSpace(value));
        }

        public static string ToSerialDate(this string s)
        {
            if (s.Length == 8)
            {
                return s.Substring(0, 4) + "-" + s.Substring(4, 2) + "-" + s.Substring(6, 2);
            }
            return s;
        }

        public static bool IsCaseInsensitiveMatch(this string source, string value)
        {
            return string.Equals(source, value, StringComparison.InvariantCultureIgnoreCase);

        }

        public static bool ContainsCaseInsensitive(this string source, string value)
        {
            int results = source.IndexOf(value, StringComparison.CurrentCultureIgnoreCase);
            return results == -1 ? false : true;
        }

        public static string DecodeBase64String(string base64String, Encoding encoding)
        {
            if (string.IsNullOrEmpty(base64String)) { return base64String; }
            byte[] encodedBytes = Convert.FromBase64String(base64String);
            return encoding.GetString(encodedBytes, 0, encodedBytes.Length);

        }

        public static string HtmlEscapeQuotes(this string s)
        {
            if (string.IsNullOrEmpty(s)) { return s; }

            return s.Replace("'", "&#39;").Replace("\"", "&#34;");

        }

        public static string CsvEscapeQuotes(this string s)
        {
            if (string.IsNullOrEmpty(s)) { return s; }

            return s.Replace("\"", "\"\"");

        }

        public static string RemoveAngleBrackets(this string s)
        {
            if (string.IsNullOrEmpty(s)) { return s; }

            return s.Replace("<", string.Empty).Replace(">", string.Empty);

        }

        public static string Coalesce(this string s, string alt)
        {
            if (string.IsNullOrEmpty(s)) { return alt; }
            return s;
        }

        public static string RemoveNonNumeric(this string s)
        {
            if (string.IsNullOrEmpty(s)) { return s; }

            char[] result = new char[s.Length];
            int resultIndex = 0;
            foreach (char c in s)
            {
                if (char.IsNumber(c))
                    result[resultIndex++] = c;
            }
            if (0 == resultIndex)
                s = string.Empty;
            else if (result.Length != resultIndex)
                s = new string(result, 0, resultIndex);

            return s;
        }

        public static string RemoveLineBreaks(this string s)
        {
            if (string.IsNullOrEmpty(s)) { return s; }

            return s.Replace("\r\n", string.Empty).Replace("\n", string.Empty).Replace("\r", string.Empty);
        }


        public static string EscapeXml(this string s)
        {
            string xml = s;
            if (!string.IsNullOrEmpty(xml))
            {
                // replace literal values with entities
                xml = xml.Replace("&", "&amp;");
                xml = xml.Replace("&lt;", "&lt;");
                xml = xml.Replace("&gt;", "&gt;");
                xml = xml.Replace("\"", "&quot;");
                xml = xml.Replace("'", "&apos;");
            }
            return xml;
        }

        public static string UnescapeXml(this string s)
        {
            string unxml = s;
            if (!string.IsNullOrEmpty(unxml))
            {
                // replace entities with literal values
                unxml = unxml.Replace("&apos;", "'");
                unxml = unxml.Replace("&quot;", "\"");
                unxml = unxml.Replace("&gt;", "&gt;");
                unxml = unxml.Replace("&lt;", "&lt;");
                unxml = unxml.Replace("&amp;", "&");
            }
            return unxml;
        }


        public static List<string> SplitOnChar(this string s, char c)
        {
            List<string> list = new List<string>();
            if (string.IsNullOrEmpty(s)) { return list; }

            string[] a = s.Split(c);
            foreach (string item in a)
            {
                if (!string.IsNullOrEmpty(item)) { list.Add(item); }
            }


            return list;
        }

        public static List<string> SplitOnCharAndTrim(this string s, char c)
        {
            List<string> list = new List<string>();
            if (string.IsNullOrEmpty(s)) { return list; }

            string[] a = s.Split(c);
            foreach (string item in a)
            {
                if (!string.IsNullOrEmpty(item)) { list.Add(item.Trim()); }
            }


            return list;
        }

        public static List<string> SplitOnPipes(string s)
        {
            List<string> list = new List<string>();
            if (string.IsNullOrEmpty(s)) { return list; }

            string[] a = s.Split('|');
            foreach (string item in a)
            {
                if (!string.IsNullOrEmpty(item)) { list.Add(item); }
            }


            return list;
        }

        #endregion


        //byte array

        public static bool IsEmpty(this byte[] value)
        {
            if (value != null)
                return value.Length == 0;

            return false;
        }

        //int

        public static bool IsEmpty(this int value)
        {
            return value == 0;
        }

        public static bool IsEmpty(this int? value)
        {
            if (value != null)
                return ((int)value).IsEmpty();

            return false;
        }

        public static bool IsInvalidKey(this int value)
        {
            return value <= 0;
        }

        //big int

        public static bool IsEmpty(this Int64 value)
        {
            return value == 0;
        }

        public static bool IsEmpty(this Int64? value)
        {
            if (value != null)
                return ((Int64)value).IsEmpty();

            return false;
        }

        public static bool IsInvalidKey(this Int64 value)
        {
            return value <= 0;
        }

        //short

        public static bool IsEmpty(this short value)
        {
            return value == 0;
        }

        public static bool IsEmpty(this short? value)
        {
            if (value != null)
                return ((short)value).IsEmpty();

            return false;
        }

        public static bool IsInvalidKey(this short value)
        {
            return value <= 0;
        }

        //short

        public static bool IsEmpty(this byte value)
        {
            return value == 0;
        }

        public static bool IsEmpty(this byte? value)
        {
            if (value != null)
                return ((byte)value).IsEmpty();

            return false;
        }

        public static bool IsInvalidKey(this byte value)
        {
            return value <= 0;
        }

        //float

        public static bool IsEmpty(this float value)
        {
            return value == 0;
        }

        public static bool IsEmpty(this float? value)
        {
            if (value != null)
                return ((float)value).IsEmpty();

            return false;
        }

        //double

        public static bool IsEmpty(this double value)
        {
            return value == 0;
        }

        public static bool IsEmpty(this double? value)
        {
            if (value != null)
                return ((double)value).IsEmpty();

            return false;
        }

        //guid

        public static bool IsEmpty(this Guid value)
        {
            return value == Guid.Empty;
        }

        public static bool IsEmpty(this Guid? value)
        {
            if (value != null)
                return ((Guid)value).IsEmpty();

            return false;
        }

        public static bool IsInvalidKey(this Guid value)
        {
            return value.IsEmpty();
        }

        public static bool IsInt(this string s)
        {
            int x = 0;
            return int.TryParse(s, out x);
        }

        public static bool IsBool(this string s)
        {
            bool x = true;
            return bool.TryParse(s, out x);
        }


        public static bool IsDecimal(this string s)
        {
            decimal x = 0;
            return decimal.TryParse(s, out x);
        }

        public static bool IsLong(this string s)
        {
            long x = 0;
            return long.TryParse(s, out x);
        }
        public static bool IsValid(this object s)
        {
            if (s != null && s != "")
                return true;
            else
                return false;

        }

        public static bool IsNotNull(this object s)
        {
            if (s != null)
                return true;
            else
                return false;

        }
      

        public static string GetDynamicString(dynamic value = null)
        {
            if (value != null && !string.IsNullOrEmpty(value.ToString()))
                return value.ToString();

            return null;
        }

        public static int? GetDynamicInt(dynamic value = null, int? defaultValue = null)
        {
            int i;
           
            if (int.TryParse(GetDynamicString(value), out i)) {
                return i;
            }

            return defaultValue;
        }

        public static long? GetDynamicLong(dynamic value = null, long? defaultValue = null)
        {
            long i;
            if (long.TryParse(GetDynamicString(value), out i)) { return i; }

            return defaultValue;
        }

        public static decimal? GetDynamicDecimal(dynamic value = null, decimal? defaultValue = null)
        {
            decimal i;
            if (decimal.TryParse(GetDynamicString(value), out i)) { return i; }

            return defaultValue;
        }

        public static bool GetDynamicBool(dynamic value = null, bool defaultValue = false)
        {
            bool ret = defaultValue;
            if (GetDynamicString(value) == "true")
                ret = true;
            else if (GetDynamicString(value) == "false")
                ret = false;

            return ret;
        }

    }
}
