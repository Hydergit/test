﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Framework.Utility
{
    public class CustomFilter
    {
        public string TableName { get; set; }
        public string Alias { get; set; }
        public string Field { get; set; }
        public string Condition { get; set; }
        public string Value { get; set; }
    }
    public class FilterHelper
    {



        public static List<CustomFilter> ConvertDynamicToList(dynamic filters)
        {
            List<CustomFilter> filter = ((JArray)filters).Select(x => new CustomFilter
            {
                TableName = (string)x["TableName"],
                Alias = (string)x["Alias"],
                Field = (string)x["Field"],
                Condition = (string)x["Condition"],
                Value = (string)x["Value"],

            }).ToList();
            return filter;
        }
        public static List<CustomFilter> ConvertDynamicAdvFilterToList(dynamic filters)
        {
            List<CustomFilter> filter = ((JArray)filters).Select(x => new CustomFilter
            {
                TableName = (string)x["TableName"],
                Alias = (string)x["Alias"],
                Field = (string)x["field"],
                Condition = (string)x["Condition"],
                Value = (string)x["term"],

            }).ToList();
            return filter;
        }

    }
}
