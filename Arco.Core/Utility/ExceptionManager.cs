﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Framework.Utility
{
    public class ExceptionManager
    {
        public static string DoLogAndGetFriendlyMessageForException(Exception ex)
        {
            try
            {
                if (ex.InnerException == null)
                    Logging.LogError(ex);
            }
            catch (Exception)
            {
            }

            return GetFriendlyMessageForException(ex);
        }

        public static string GetFriendlyMessageForException(Exception ex)
        {
            string message = "Error: There was a problem while processing your request: " + ex.Message;
            if (ex.InnerException != null)
            {
                Exception inner = ex.InnerException;
                if (inner is System.Data.Common.DbException)
                    message = "Our database is currently experiencing problems. " + inner.Message;
                else if (inner is NullReferenceException)
                    message = "There are one or more required fields that are missing.";
                else if (inner is ArgumentException)
                {
                    string paramName = ((ArgumentException)inner).ParamName;
                    message = string.Concat("The ", paramName, " value is illegal.");
                }
                else if (inner is ApplicationException)
                    message = "Exception in application" + inner.Message;
                else
                    message = inner.Message;
            }
            return MessageFormatter.GetFormattedErrorMessage(message);
        }

        #region website  09 sep 2013
        public static string webDoLogAndGetFriendlyMessageForException(Exception ex)
        {
            try
            {
                Logging.webLogError(ex);
            }
            catch (Exception)
            {
            }
            return webGetFriendlyMessageForException(ex);
        }
        public static string webGetFriendlyMessageForException(Exception ex)
        {
            string message = "Error: There was a problem while processing your request: " + ex.Message;
            if (ex.InnerException != null)
            {
                Exception inner = ex.InnerException;
                if (inner is System.Data.Common.DbException)
                    message = "Our database is currently experiencing problems. " + inner.Message;
                else if (inner is NullReferenceException)
                    message = "There are one or more required fields that are missing.";
                else if (inner is ArgumentException)
                {
                    string paramName = ((ArgumentException)inner).ParamName;
                    message = string.Concat("The ", paramName, " value is illegal.");
                }
                else if (inner is ApplicationException)
                    message = "Exception in application" + inner.Message;
                else
                    message = inner.Message;
            }

            return MessageFormatter.show_systemErrorMsg_web(message);
        }
        #endregion
    }
}
