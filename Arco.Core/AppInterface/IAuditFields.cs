﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Arco.Core.AppInterface
{
    public interface IAuditFields
    {
        Nullable<System.DateTime> CreatedDatetime { get; set; }
        string CreatedBy { get; set; }
        Nullable<System.DateTime> ModifiedDatetime { get; set; }
        string ModifiedBy { get; set; }
        string DataAreaId { get; set; }
    }
}
