﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.AppInterface
{


    public interface IQueryRepo : IDisposable
    {

        IQueryable<T> GetAsQueryable<T>() where T : class;

    }
}