﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.AppInterface
{
    public interface IModule
    {
        void SetupIoc(ref IUnityContainer container);
       
        void SetupDatabase();

        void Initialize();

       
        void PostInitialize();

        
        void Uninstall();
    }
}
