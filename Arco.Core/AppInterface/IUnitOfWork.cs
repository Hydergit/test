﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.AppInterface
{
    public interface IUnitOfWork
    {
        int Commit();
        void CommitAndRefreshChanges();
        void RollbackChanges();
    }
}
