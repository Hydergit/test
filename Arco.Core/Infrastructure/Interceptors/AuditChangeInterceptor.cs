﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Entity.Infrastructure;
using Arco.Core.AppInterface;

namespace Arco.Framework.Infrastructure.Interceptors
{
    public class AuditChangeInterceptor : ChangeInterceptor<IAuditFields>
    {
        public override void OnBeforeInsert(DbEntityEntry entry, IAuditFields item)
        {
            base.OnBeforeInsert(entry, item);

            var currentTime = DateTime.UtcNow;

            item.CreatedDatetime = currentTime;
            item.ModifiedDatetime = currentTime;
        }

        public override void OnBeforeUpdate(DbEntityEntry entry, IAuditFields item)
        {
            base.OnBeforeUpdate(entry, item);
            var currentTime = DateTime.UtcNow;
            item.ModifiedDatetime = currentTime;
        }

        public override void OnAfterInsert(DbEntityEntry entry, IAuditFields item)
        {
            base.OnAfterInsert(entry, item);
        }
    }  
}
