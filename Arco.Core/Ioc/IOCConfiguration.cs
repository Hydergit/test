﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.Ioc
{
    public partial class IOCConfiguration
    {
        partial void RegisterSingleTable(ref IUnityContainer   container);

        public static IUnityContainer Register(IUnityContainer container)
        {
            var config = new IOCConfiguration();

            config.RegisterSingleTable(ref container);

            return container;
        }
    }
}

