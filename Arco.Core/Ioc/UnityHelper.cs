﻿
using Arco.Core.AppInterface;
using Arco.Framework.Utility;
using log4net;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.InterceptionExtension;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using UnityLog4NetExtension.Log4Net;

namespace Arco.Core.Ioc
{
    // http://weblogs.asp.net/shijuvarghese/archive/2010/05/07/dependency-injection-in-asp-net-mvc-nerddinner-app-using-unity-2-0.aspx


    /// <summary>
    /// Bind the given interface in request scope
    /// </summary>
    public static class IocExtensions
    {
        public static void BindInRequestScope<T1, T2>(this IUnityContainer container) where T2 : T1
        {
            //commented by minhaj to avoide intial loading issue
            //container.RegisterType<T1, T2>(new HierarchicalLifetimeManager());
            container.RegisterType<T1, T2>(
                new Interceptor<InterfaceInterceptor>(),
                new InterceptionBehavior<MyInterceptionBehavior>());
        }

        public static void BindInSingletonScope<T1, T2>(this IUnityContainer container) where T2 : T1
        {
            //commented by minhaj to avoide intial loading issue
            //container.RegisterType<T1, T2>(new ContainerControlledLifetimeManager());
            container.RegisterType<T1, T2>();
        }
    }

    /// <summary>
    /// The injection for Unity
    /// </summary>
    public static class UnityHelper
    {

        public static IUnityContainer Start()
        {
            var container = BuildUnityContainer();

            var type = typeof(IModule);

            var tttypesq = AppDomain.CurrentDomain.GetAssemblies();

            //foreach (var item in tttypesq)
            //{
            //    var ss = item.GetTypes();
            //}

            var typesq = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x=> x.FullName.Contains("Arco."))
              .SelectMany(s => s.GetTypes())
              .Where(p => type.IsAssignableFrom(p));

            var types = AppDomain.CurrentDomain.GetAssemblies()
                .Where(x => x.FullName.Contains("Arco."))
                .SelectMany(s => s.GetTypes())
                .Where(p => type.IsAssignableFrom(p) && p.Name=="Module").Select( t=>  Activator.CreateInstance(t) as IModule);

            foreach (var item in types)
            {
                item.SetupIoc(ref container);
            }


            IOCConfiguration.Register(container);
            DependencyResolver.SetResolver(new Microsoft.Practices.Unity.Mvc.UnityDependencyResolver(container));

            return container;
        }

        /// <summary>
        /// Inject
        /// </summary>
        /// <returns></returns>
        private static IUnityContainer BuildUnityContainer()
        {
            var container = new UnityContainer().AddNewExtension<LogCreation>() //Custom extension that executes logger setup
    .AddNewExtension<Log4NetExtension>().AddNewExtension<Interception>();


            //Bind the various domain model services and repositories that e.g. our controllers require         
            //container.BindInRequestScope<Arco.Model.Interface.Repository.Contract.IContractDataRepository, Arco.Business.Query.Contract.ContractRepository>();
            //container.BindInRequestScope<Arco.Model.Interface.Service.Contract.IContractService, Arco.Business.Business.Contract.ContractService>();
           
            return container;
        }
    }



    public class MyInterceptionBehavior : IInterceptionBehavior
    {
        

        [Dependency]
        public ILog Log { get; set; }

        public IMethodReturn Invoke(IMethodInvocation input,
            GetNextInterceptionBehaviorDelegate getNext)
        {
            if(input !=null && input.MethodBase!=null && input.MethodBase.Name=="GetAsQueryable")
            {
               return getNext()(input, getNext);
            }

            //This behavior will record the start and stop time
            //of a method and will log the method name and elapsed time.
            //Get the current time.
            var startTime = DateTime.Now;

            //Log the start time of the method.
            //This could be ommitted if you just want to see the response times of a method.
            WriteLog(String.Format(
              "Invoking method {0} at {1}",
              input.MethodBase, startTime.ToLongTimeString()));

            // Invoke the next behavior in the chain.
            var result = getNext()(input, getNext);

            //Calculate the elapsed time.
            var endTime = DateTime.Now;
            var timeSpan = endTime - startTime;


            //The following will log the method name and elapsed time.
            if (result.Exception != null)
            {
                //Method threw an exception.
                WriteLog(String.Format(
                  "Method {0} threw exception {1} at {2}.  Elapsed Time: {3} ms",
                  input.MethodBase, result.Exception.Message,
                  endTime.ToLongTimeString(),
                  timeSpan.TotalMilliseconds));
            }
            else
            {
                //Method completed normally.
                WriteLog(String.Format(
                  "Method {0} returned {1} at {2}.  Elapsed Time: {3} ms",
                  input.MethodBase, result.ReturnValue,
                  endTime.ToLongTimeString(),
                  timeSpan.TotalMilliseconds));
            }

            return result;
        }

        public IEnumerable<Type> GetRequiredInterfaces()
        {
            return Type.EmptyTypes;
        }

        public bool WillExecute
        {
            get { return true; }
        }

        private void WriteLog(string message)
        {
            if (Log != null)
            {
                Log.DebugFormat("Profiler: {0}", message);
            }
        }


    }


    //Initialize Logging SetUp
    public class LogCreation : UnityContainerExtension
    {
        protected override void Initialize()
        {
            Logger.Setup();
        }
    }
}

