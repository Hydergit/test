﻿

using Arco.Core.AppInterface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.AppBase
{
    public enum PersistenceType
    {

        Create,

        Update,

        Read,

        Delete
    }

    public abstract class BLLBase : IDisposable
    {
        protected IRepository _context;
        private bool _disposeContext;


        public BLLBase(IRepository context, bool disposeContext)
        {
            this._context = context;
            this._disposeContext = disposeContext;
        }



        public virtual void Init() { }
        public virtual void OnInserting(ModelBase entity) { }
        public virtual void OnInserted(ModelBase entity) { }

        public virtual void OnUpdating(ModelBase entity) { }
        public virtual void OnUpdated(ModelBase entity) { }

        public virtual void OnDeleting(ModelBase entity) { }
        public virtual void OnDeleted(ModelBase entity) { }

        public virtual void OnSaving(ModelBase entity, PersistenceType persistenceType) { }
        public virtual void OnSaved(ModelBase entity, PersistenceType persistenceType) { }

        public void Dispose()
        {
            if (_context != null)
            {
                if (_disposeContext)
                    _context.Dispose();

                _context = null;
            }
            GC.SuppressFinalize(this);
        }
    }
}
