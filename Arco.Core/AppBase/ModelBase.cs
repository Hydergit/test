﻿using Arco.Core.AppInterface;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.AppBase
{
    public class ModelBase : IAuditFields
    {
        public virtual string TableName
        {
            get
            {
                return this.GetType().Name.Split('_')[0];
            }
        }

        //public virtual string RecTitle
        //{
        //    get;
        //    set;
        //}

        //public virtual string DisplayColumn
        //{
        //    get;
        //    set;
        //}

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public virtual Nullable<System.DateTime> CreatedDatetime
        {
            get;
            set;
        }

        [StringLength(10, ErrorMessage = "MaxLength")]
        public virtual string CreatedBy
        {
            get;
            set;
        }

        [DataType(DataType.DateTime)]
        [DisplayFormat(DataFormatString = "{0:dd/MM/yyyy hh:mm:ss tt}", ApplyFormatInEditMode = true)]
        public virtual Nullable<System.DateTime> ModifiedDatetime
        {
            get;
            set;
        }

        [StringLength(10, ErrorMessage = "MaxLength")]
        public virtual string ModifiedBy
        {
            get;
            set;
        }

        [StringLength(10, ErrorMessage = "MaxLength")]
        [Required(ErrorMessage = "Required")]
        [Key]
        public virtual string DataAreaId
        {
            //get { return "AYN"; }
            //set { this.DataAreaId = "AYN"; }
            get;
            set;

        }

        [Required(ErrorMessage = "Required")]
        public virtual long RecId
        {
            get;
            set;
        }

        public virtual byte[] RecVersion
        {
            get;
            set;
        }
    }
}
