﻿using Arco.Core.AppInterface;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Core.AppBase
{
    public abstract class ModuleBase : IModule
    {
        public virtual void SetupIoc(ref IUnityContainer container)
        {
        }

        public virtual void SetupDatabase()
        {
        }

        public virtual void Initialize()
        {
        }

        public virtual void PostInitialize()
        {
        }

        public void Uninstall()
        {
        }
    }
}
