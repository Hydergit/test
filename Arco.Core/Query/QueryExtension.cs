﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Arco.Framework.Utility;

namespace Arco.Framework.Query
{

    public class Pagging
    {
        public int PageNo { get; set; }
        public int PageSize { get; set; }
    }

  

    public class SelectField
    {
        public string TableAlias { get; set; }
        public string FieldAlias { get; set; }        
    }

    public class QueryData
    {
        public dynamic List { get; set; }
        public int TotalCount { get; set; }

    }

    public class QueryCriteria
    {
        //       Query-Input
        //-ViewID
        //-Select Fields
        //-Filter
        //-Order Fields
        //-Paging 
        //--PageNo
        //--PageSize
        public string ViewID { get; set; }
        public SelectField[] Select { get; set; }
        public Condition[] Where { get; set; }
        public Pagging Pagging { get; set; }
    }

    public class QueryResult
    {
        //        -StatusId
        //-StatusMessage
        //-Data
        //--List
        //--TotalCount
        public int StatusId { get; set; }
        public string StatusMessage { get; set; }
        public QueryData Data
        {
            get;
            set;
        }

    }



    public class PagedResults<T>
    {
        /// <summary>
        /// The page number this page represents.
        /// </summary>
        public int PageNumber { get; set; }

        /// <summary>
        /// The size of this page.
        /// </summary>
        public int PageSize { get; set; }

        /// <summary>
        /// The total number of pages available.
        /// </summary>
        public int TotalNumberOfPages { get; set; }

        /// <summary>
        /// The total number of records available.
        /// </summary>
        public int TotalNumberOfRecords { get; set; }

        /// <summary>
        /// The URL to the next page - if null, there are no more pages.
        /// </summary>
        public string NextPageUrl { get; set; }

        /// <summary>
        /// The records this page represents.
        /// </summary>
        public IEnumerable<T> Results { get; set; }
    }


    public static class QueryExtension
    {

      
   
        //public static PagedResults<TReturn> CreatePagedResults<T, TReturn>(
        // IQueryable<T> queryable,
        // int page,
        // int pageSize,
        // string orderBy,
        // bool ascending)
        //{
        //    var skipAmount = pageSize * (page - 1);

        //    var projection = queryable
        //        .OrderByPropertyOrField(orderBy, ascending)
        //        .Skip(skipAmount)
        //        .Take(pageSize).ProjectTo<TReturn>();

        //    var totalNumberOfRecords = queryable.Count();
        //    var results = projection.ToList();

        //    var mod = totalNumberOfRecords % pageSize;
        //    var totalPageCount = (totalNumberOfRecords / pageSize) + (mod == 0 ? 0 : 1);

        //    var nextPageUrl = "";

        //    return new PagedResults<TReturn>
        //    {
        //        Results = results,
        //        PageNumber = page,
        //        PageSize = results.Count,
        //        TotalNumberOfPages = totalPageCount,
        //        TotalNumberOfRecords = totalNumberOfRecords,
        //        NextPageUrl = nextPageUrl
        //    };
        //}


        public static QueryResult CreatePagedResults<T>(
      IQueryable<T> queryable,
      int page,
      int pageSize,List<Condition> condition=null,
      string orderBy="RecId",
      bool ascending=true)
        {
            var skipAmount = pageSize * (page - 1);

            if(condition==null)
            {
                condition = new List<Condition>();
            }

            var projection = queryable.DynamicWhere(condition)//.DynamicOrderBy(orderBy)
               .OrderByPropertyOrField(orderBy,ascending)
                .Skip(skipAmount)
                .Take(pageSize);//.ProjectTo<T>();

            var totalNumberOfRecords = queryable.Count();
            var results = projection.ToList();

            var mod = totalNumberOfRecords % pageSize;
            var totalPageCount = (totalNumberOfRecords / pageSize) + (mod == 0 ? 0 : 1);

          

            return new QueryResult
            {
                StatusId=1,
                StatusMessage="Record Completed",
                Data=new QueryData{
                List=results,
                TotalCount=totalNumberOfRecords
                },
              
            };
        }



        public static QueryResult CreatePagedWithFilterResults<T>(
   IQueryable<T> queryable,QueryCriteria _queryCriteria,string TableAlias=null)
        {

            int PageNo = 1;
            int PageSize = 20;

            if (_queryCriteria != null && _queryCriteria.Pagging != null)
            {
                PageNo = _queryCriteria.Pagging.PageNo;
                PageSize = _queryCriteria.Pagging.PageSize;
            }

            List<Condition> filters = new List<Condition>();
            
            if(_queryCriteria.Where!=null && _queryCriteria.Where.Count()>0)
            {

                filters = _queryCriteria.Where.ToList();
                if (!string.IsNullOrEmpty(TableAlias))
                {
                    filters = _queryCriteria.Where.Where(x=> x.TableAlias==TableAlias).ToList();
                }
            }

            return CreatePagedResults(queryable, PageNo, PageSize, filters);
        }


      
    }


}
