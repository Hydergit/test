﻿
using Arco.Core.AppInterface;
using Arco.Core.Ioc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Unity.WebApi;

namespace Arco.WebAPI
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
           //GlobalConfiguration.Configure(Arco.HR.Business.WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            var unityContainer = UnityHelper.Start();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(unityContainer);

            //System.Reflection.Assembly.LoadFrom(@"C:\Users\FtsAdmin1\Documents\NewArchPublish\bin\Arco.HR.Business.dll");
            //var instances = from t in Assembly.GetExecutingAssembly().GetTypes()
            //                where t.GetInterfaces().Contains(typeof(IModule))
            //                         && t.GetConstructor(Type.EmptyTypes) != null
            //                select Activator.CreateInstance(t) as IModule;

            //foreach (var instance in instances)
            //{
            //    instance.SetupIoc(ref unityContainer); // where Foo is a method of ISomething
            //}


            #region GetAssembly temp comment
            //var type = typeof(IModule);
            //var types = AppDomain.CurrentDomain.GetAssemblies()
            //    .SelectMany(s => s.GetTypes())
            //    .Where(p => type.IsAssignableFrom(p));

            //AppDomain currentDomain = AppDomain.CurrentDomain;
            //currentDomain.AssemblyResolve += new ResolveEventHandler(currentDomain_AssemblyResolve);
            #endregion


            //currentDomain.ExecuteAssembly(@"E:\ArcoERPNewArchitecture\ArcoNewArchiteccture\Arco.HR.Business\bin\Debug\Arco.HR.Business.dll");



            //            AppDomain.CurrentDomain.FirstChanceException += (sender, eventArgs) =>
            //{

            //};
        }

        System.Reflection.Assembly currentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            //if (args.Name == "Arco.HR.Domain, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")
            //{
            //    String assemblyPath = @"E:\Working\MetadbWithNewArchtecture\Solution\ArcoNewArchiteccture_11_01_2018\ArcoNewArchiteccture\Arco.HR.Business\bin\Debug\Arco.HR.Domain.dll";
            //    Assembly assembly = Assembly.LoadFrom(assemblyPath);
            //    return assembly;
            //}
            //else if (args.Name == "Arco.HR.Business, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null")
            //{
            //    String assemblyPath = @"E:\Working\MetadbWithNewArchtecture\Solution\ArcoNewArchiteccture_11_01_2018\ArcoNewArchiteccture\Arco.HR.Business\bin\Debug\Arco.HR.Business.dll";
            //    Assembly assembly = Assembly.LoadFrom(assemblyPath);
            //    return assembly;
            //}
            return null;

        }




//        private void LoadAddinClasses(string assemblyFile)
//{
//    Assembly asm = null;
//    Type[] types = null;

//    try
//    {
//        asm = Assembly.LoadFrom(assemblyFile);
//        types = asm.GetTypes();
//    }
//    catch(Exception ex)
//    {
//        //var msg = $"Unable to load add-in assembly: {Path.GetFileNameWithoutExtension(assemblyFile)}";                
//        //mmApp.Log(msg, ex);
//        return;
//    }

//    foreach (var type in types)
//    {
//        var typeList = type.FindInterfaces(AddinInterfaceFilter, typeof(IMarkdownMonsterAddin));
//        if (typeList.Length > 0)
//        {
//            var ai = Activator.CreateInstance(type) as MarkdownMonsterAddin;
//            AddIns.Add(ai);
//        }
//    }
//}


    }
}
