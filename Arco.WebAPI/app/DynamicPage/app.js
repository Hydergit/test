﻿
var indsectorapp = angular.module('IndividualSectorApp', ['ui.router', 'xeditable', 'ui.bootstrap', 'ui.grid', 'ui.grid.pagination', 'ui.grid.cellNav', 'ui.grid.edit', 'ui.grid.resizeColumns', 'ui.grid.pinning', 'ui.grid.selection', 'ui.grid.moveColumns', 'ui.grid.exporter', 'ui.grid.importer', 'ui.grid.grouping', 'ngCookies', 'datePicker', 'pascalprecht.translate', 'angular.css.injector', 'ngDraggable', 'ngMaterial', 'ngSanitize', 'textAngular', 'timer']);

function readCookie(name) {

    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}


function initialtoolbar() {
    $("#BtnCreate").addClass("inactiveLink");
    $("#BtnCreate").removeClass("bg-themeprimary");

    $("#btnDelete").addClass("inactiveLink");
    $("#btnDelete").removeClass("bg-themeprimary");
}


function removebacktoolbar() {

    $("#backtopage").addClass("inactiveLink");
    $("#backtopage").removeClass("bg-themeprimary");
}


function Addbacktoolbar() {
    $("#backtopage").removeClass("inactiveLink");
    $("#backtopage").addClass("bg-themeprimary");
}
indsectorapp.factory('httpRequestInterceptor', function ($cookies, $cookieStore) {
    return {
        request: function (config) {

            //var s = readCookie('MyUser');
            //alert(readCookie('MyUser'));
            // use this to destroying other existing headers
            config.headers = { 'Token': readCookie('MyUser') }

            // use this to prevent destroying other existing headers
            // config.headers['Authorization'] = 'authentication';

            return config;
        }
    };
});


indsectorapp.directive('checkfieldevent', function () {
    return {
        restrict: "A",
        //scope: true,
        scope: {
            //HeaderId: '=popid',
            currenteventdata: '=currenteventdata',
            //source: '=source',
            //gridname: '=gridname',
            //columdef: '=columdef',
            //currentmodel: '=currentmodel',
            //popdata: '=popdata'
        },
       
        controller: ['$scope', '$compile','$element', '$attrs', function ($scope, $compile, $element, $attrs) {
            //$scope.CheckClick = function () {
            //    debugger;
            //    alert("click works");
            //};
            //alert($scope.currenteventdata);
            //alert($scope.$eval($attrs.checkfieldevent));
            debugger;
          

            if ($scope.currenteventdata) {
                debugger;
                var currentEvent = $scope.currenteventdata;
               
                if (currentEvent && currentEvent.EventKey && !$attrs[currentEvent.EventKey]) {
                    alert(currentEvent);
                    //$attrs.$set('ngClick', 'CheckClick' + '()');
                    $attrs.$set(currentEvent.EventKey, currentEvent.EventName + '()');
                    $compile($element)($scope);
                }
            }
            

            }],
            compile: function (element, attributes) {

                //if (attributes.eventdata) {
                //    debugger;
                //    alert(attributes.eventdata);
                //    var currentEvent = attributes.eventdata;
                //    attributes.$set('ngClick', 'CheckClick' + '()');
                //    //if (currentEvent.EventKey && currentEvent.EventName) {
                //    //    //($compile('<script>' + mdata + '</script><form action="' + sucessUrl + '" class="paymentWidgets">VISA MASTER</form>')($scope)
                //    //    $compile(attributes.$set(currentEvent.EventKey, currentEvent.EventName + '()'))($scope);
                //    //    //attributes.$set(currentEvent.EventKey, currentEvent.EventName+'()');
                //    //}
                //}
                   
                }
}
});

indsectorapp.directive('checkappendevent', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs, ngModelCtrl) {

            debugger;
            if (scope && scope.$parent && scope.$parent.$parent && scope.$parent.$parent.datafield && scope.$parent.$parent.datafield.EventData) {
                var currentEvent = scope.$parent.$parent.datafield.EventData;

                if (currentEvent && currentEvent.EventKey && !attrs[currentEvent.EventKey]) {
                  //  alert(currentEvent);
                   
                    attrs.$set(currentEvent.EventKey, currentEvent.EventName + '()');
                    $compile(elem)(scope);
                }
            }
            //if (!attrs['ngClick']) {
            //    attrs.$set('ngClick', 'CheckClick' + '()');
            //    //attrs.$set('total', 20);
            //    $compile(elem)(scope);
            //}
        }
    };
});

indsectorapp.directive('appenddynamicscript', function ($compile) {
    return {
        restrict: 'A',
        link: function (scope, elem, attrs, ngModelCtrl) {

            debugger;
            if (scope && scope.$parent && scope.$parent.$parent && scope.$parent.$parent.datafield && scope.$parent.$parent.datafield.EventData) {
                var currentEvent = scope.$parent.$parent.datafield.EventData;

                if (currentEvent && currentEvent.EventKey && !attrs[currentEvent.EventKey]) {
                    //  alert(currentEvent);

                    attrs.$set(currentEvent.EventKey, currentEvent.EventName + '()');
                    $compile(elem)(scope);
                }
            }
            //if (!attrs['ngClick']) {
            //    attrs.$set('ngClick', 'CheckClick' + '()');
            //    //attrs.$set('total', 20);
            //    $compile(elem)(scope);
            //}
        }
    };
});

indsectorapp.config(function ($translateProvider) {


    var cookie = readCookie('MyLanguage');
    if (cookie) {

        $translateProvider.preferredLanguage(cookie == '%22Eng%22' ? 'Eng' : 'Ar');
    } else {
        $translateProvider.preferredLanguage('Eng');
    }

    //$translateProvider.preferredLanguage('Eng');
    $translateProvider.useLoader('asyncLoader');
});


indsectorapp.factory('asyncLoader', function ($q, $timeout, $http, $cookies, $cookieStore) {

    return function (options) {
        var deferred = $q.defer(),
            translations;
        $cookieStore.remove('MyLanguage');
        $cookieStore.put('MyLanguage', options.key);
        //  $cookies['MyLanguage'] = options.key;


        if (options.key === 'Eng') {


            $http.get("/api/GetLanguageValues")
.success(function (data) {
    
    deferred.resolve(data[0].source);

});

        } else {
            $http.get("/api/GetLanguageValues")
.success(function (data) {
    
    deferred.resolve(data[1].source);

});
        }

        //$timeout(function () {
        //    deferred.resolve(translations);
        //}, 2000);

        return deferred.promise;
    };
});


function initialtoolbar() {
    $("#BtnCreate").addClass("inactiveLink");
    $("#BtnCreate").removeClass("bg-themeprimary");

    $("#btnDelete").addClass("inactiveLink");
    $("#btnDelete").removeClass("bg-themeprimary");
}


function removebacktoolbar() {

    $("#backtopage").addClass("inactiveLink");
    $("#backtopage").removeClass("bg-themeprimary");
}


function Addbacktoolbar() {
    $("#backtopage").removeClass("inactiveLink");
    $("#backtopage").addClass("bg-themeprimary");
}
//indsectorapp.run(function ( $timeout) {
//    $timeout(function () {
//        alert("timefinish");
//        $("#logutscreen").show();
//         $timeout.cancel(timer);
//    }, 30000);
//});

//indsectorapp.config(function ($translateProvider) {


//    //$translateProvider.translations('en', {
//    //    'T ITLE': 'Hello',
//    //    FOO: 'This is a paragraph.',
//    //    BUTTON_LANG_EN: 'english',
//    //    BUTTON_LANG_DE: 'german',

//    //});
//    //$translateProvider.translations('de', {
//    //    'T ITLE': 'Hallo',
//    //    FOO: 'Dies ist ein Paragraph.',
//    //    BUTTON_LANG_EN: 'englisch',
//    //    BUTTON_LANG_DE: 'deutsch'
//    //});

//    var cookie = readCookie('MyLanguage');
//    if (cookie)
//    {

//        $translateProvider.preferredLanguage(cookie == '%22Eng%22' ? 'Eng' : 'Ar');
//    } else {
//        $translateProvider.preferredLanguage('Eng');
//    }

//    //$translateProvider.preferredLanguage('Eng');
//    $translateProvider.useLoader('asyncLoader');
//});


//indsectorapp.factory('asyncLoader', function ($q, $timeout, $http, $cookies, $cookieStore) {

//    return function (options) {
//        var deferred = $q.defer(),
//            translations;
//        $cookieStore.remove('MyLanguage');
//        $cookieStore.put('MyLanguage', options.key);
//      //  $cookies['MyLanguage'] = options.key;


//        if (options.key === 'Eng') {


//            $http.get("/api/GetLanguageValues")
//.success(function (data) {
//    
//    deferred.resolve(data[0].source);

//});

//        } else {
//            $http.get("/api/GetLanguageValues")
//.success(function (data) {
//    
//    deferred.resolve(data[1].source);

//});
//        }

//        //$timeout(function () {
//        //    deferred.resolve(translations);
//        //}, 2000);

//        return deferred.promise;
//    };
//});

//indsectorapp.config(function ($httpProvider) {
//    $httpProvider.interceptors.push('httpRequestInterceptor');
//});


indsectorapp.filter("jsDate", function () {
    return function (x) {
        if (x) {
            return new Date(parseInt(x.substr(6)));

        }
        else {
            return x;
        }
    };
});
indsectorapp.filter("substringDate", function () {
    return function (x) {
        if (x) {
            var val = x.substring(6, x.length - 2);
            return val;

        }
        else {
            return x;
        }
    };
});


indsectorapp.filter("jsToFormateDate", function ($filter) {
    return function (x) {
        if (x) {
            var c = new Date(parseInt(x.substr(6)));

            return $filter('date')(c, 'dd/MM/yyyy');

        }
        else {
            return x;
        }
    };
});

indsectorapp.filter("jsToFormateDateMM", function ($filter) {
    return function (x) {
        if (x) {
            var c = new Date(parseInt(x.substr(6)));

            return $filter('date')(c, 'MM/dd/yyyy');

        }
        else {
            return x;
        }
    };
});

indsectorapp.filter("BoolToString", function () {

    return function (x) {


        var s = x == true ? "Yes" : "No";
        return s;

    };
});

indsectorapp.filter("BoolToString1", function () {

    return function (x) {    
        var s = x == true ? "Yes" : x == false ? "No" : "";
        return s;

    };
});

indsectorapp.directive('disabledir', function () {

    return {

        restrict: 'A',

        scope: {
            'applydisable': '=',

        },

        controller: function ($scope) {

            //$scope.disabFun = function () {
            //    


            //    var myEl = angular.element( document.querySelector("[applydisable='isneed']"));
            //    myEl.attr({ 'disabled': 'disabled'});

            //};
            //$scope.EnabFun = function () {
            //    



            //    var myEl = angular.element( document.querySelector("[applydisable='isneed']"));
            //    myEl.removeAttr('disabled')

            //};


        },

        replace: true,
        link: function (scope, elem, attr) {

            scope.$watch('applydisable', function (newValue, oldValue) {
                

                if (newValue == true) {
                    //scope.disabFun();
                    elem.attr({ 'disabled': 'disabled' });
                }
                else if (newValue == false) {
                    elem.removeAttr('disabled')
                    //scope.EnabFun();
                }

            }, true);

            elem[0].addEventListener("click", function () {
                

                scope.applydisable = true;
                //scope.$digest();
            }, true);


        }
    };
});

indsectorapp.directive('datedir', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        //$(function(){
        //    element.datepicker({
        //        dateFormat:'dd/mm/yy',
        //        onSelect:function (date) {
        //            scope.$apply(function () {
        //                ngModelCtrl.$setViewValue(date);
        //            });
        //        }
        //    });
        //});
        link: function (scope, element, attrs, ngModelCtrl) {
            element.attr("readonly", true);
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                set: function (date) {

                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            }).on("changeDate", function (e) {
                ngModelCtrl.$setViewValue($(this).val());
            });

            if (attrs.format) {
                element.datepicker({
                    format: 'dd/MM/yyyy',

                });
            }

            //element[0].addEventListener("focusout", function (e) {
            //    alert("out");
            //    ngModelCtrl.$setViewValue($(this).val());
            //    $(".datepicker dropdown-menu").css("display","none")
            //}, true);


            //element.datepicker("option", 'onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue($(this).val());
            //});

            //element.bind('onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue( $(this).val());
            //});

        }


    };



});


indsectorapp.directive('datedirtodaymax', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        //$(function(){
        //    element.datepicker({
        //        dateFormat:'dd/mm/yy',
        //        onSelect:function (date) {
        //            scope.$apply(function () {
        //                ngModelCtrl.$setViewValue(date);
        //            });
        //        }
        //    });
        //});
        link: function (scope, element, attrs, ngModelCtrl) {
            element.attr("readonly", true);
            element.datepicker({
                dateFormat: 'yy-mm-dd',
                onRender: function (date) {
                    return date.valueOf() > new Date().valueOf() ? 'disabled' : '';
                },
                set: function (date) {

                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            }).on("changeDate", function (e) {
                ngModelCtrl.$setViewValue($(this).val());
            });

            if (attrs.format) {
                element.datepicker({
                    format: 'dd/MM/yyyy',

                });
            }

            //element[0].addEventListener("focusout", function (e) {
            //    alert("out");
            //    ngModelCtrl.$setViewValue($(this).val());
            //    $(".datepicker dropdown-menu").css("display","none")
            //}, true);


            //element.datepicker("option", 'onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue($(this).val());
            //});

            //element.bind('onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue( $(this).val());
            //});

        }


    };



});



indsectorapp.directive('datedirtodayfuture', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        //$(function(){
        //    element.datepicker({
        //        dateFormat:'dd/mm/yy',
        //        onSelect:function (date) {
        //            scope.$apply(function () {
        //                ngModelCtrl.$setViewValue(date);
        //            });
        //        }
        //    });
        //});
        link: function (scope, element, attrs, ngModelCtrl) {
            element.attr("readonly", true);
            element.datepicker({
                format: 'dd/mm/yyyy',
                onRender: function (date) {
                    return date.valueOf() < new Date().valueOf() ? 'disabled' : '';
                },
                set: function (date) {

                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            }).on("changeDate", function (e) {
                ngModelCtrl.$setViewValue($(this).val());
            });

            if (attrs.format) {
                element.datepicker({
                    format: 'dd/mm/yyyy',

                });
            }

            //element[0].addEventListener("focusout", function (e) {
            //    alert("out");
            //    ngModelCtrl.$setViewValue($(this).val());
            //    $(".datepicker dropdown-menu").css("display","none")
            //}, true);


            //element.datepicker("option", 'onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue($(this).val());
            //});

            //element.bind('onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue( $(this).val());
            //});

        }


    };



});




indsectorapp.directive('datedirminmax', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',


        scope: {
            'mindatevales': '=mindatevales',
            'maxdatevales': '=maxdatevales'
        },

        link: function (scope, element, attrs, ngModelCtrl) {
            element.attr("readonly", true);

            function datefunction(dat) {

                if (scope.mindatevales && dat.valueOf() < new Date(scope.mindatevales).valueOf()) {
                    return 'disabled';
                }


                if (!scope.maxdatevales || dat.valueOf() <= new Date(scope.maxdatevales).valueOf()) {
                    return '';
                }
                else {
                    return 'disabled';

                }
            }

            element.datepicker({
                dateFormat: 'yy-mm-dd',
                //onRender: datefunction,
                //function (date) {
                //    return date.valueOf() > new Date().valueOf() ? 'disabled' : '';
                //},
                set: function (date) {

                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            }).on("changeDate", function (e) {
                ngModelCtrl.$setViewValue($(this).val());
            });

            //if (attrs.format) {
            //    element.datepicker({
            //        format: 'dd/MM/yyyy',

            //    });
            //}


            scope.$watch("mindatevales", function () {


                element.datepicker({
                    dateFormat: 'yy-mm-dd',
                    onRender: datefunction,

                    set: function (date) {

                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    }
                }).on("changeDate", function (e) {
                    ngModelCtrl.$setViewValue($(this).val());
                });
            });
            scope.$watch("maxdatevales", function () {

                element.datepicker({
                    dateFormat: 'yy-mm-dd',
                    onRender: datefunction,
                    //function (date) {
                    //    return date.valueOf() > new Date().valueOf() ? 'disabled' : '';
                    //},
                    set: function (date) {

                        scope.$apply(function () {
                            ngModelCtrl.$setViewValue(date);
                        });
                    }
                }).on("changeDate", function (e) {
                    ngModelCtrl.$setViewValue($(this).val());
                });
            });

        }


    };



});




indsectorapp.directive('datedirdd', function ($timeout) {
    return {
        restrict: 'A',
        require: 'ngModel',
        //$(function(){
        //    element.datepicker({
        //        dateFormat:'dd/mm/yy',
        //        onSelect:function (date) {
        //            scope.$apply(function () {
        //                ngModelCtrl.$setViewValue(date);
        //            });
        //        }
        //    });
        //});
        link: function (scope, element, attrs, ngModelCtrl) {
            element.attr("readonly", true);
            element.datepicker({
                format: 'dd/mm/yyyy',
                set: function (date) {

                    scope.$apply(function () {
                        ngModelCtrl.$setViewValue(date);
                    });
                }
            }).on("changeDate", function (e) {
                ngModelCtrl.$setViewValue($(this).val());
            });

            if (attrs.format) {
                element.datepicker({
                    format: 'dd/mm/yyyy',

                });
            }

            //element[0].addEventListener("focusout", function (e) {
            //    alert("out");
            //    ngModelCtrl.$setViewValue($(this).val());
            //    $(".datepicker dropdown-menu").css("display","none")
            //}, true);


            //element.datepicker("option", 'onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue($(this).val());
            //});

            //element.bind('onSelect', function () {
            //    alert($(this).val());
            //    ngModelCtrl.$setViewValue( $(this).val());
            //});

        }


    };



});






//<button ng-click="Undisabledd()" >ChangeDisable</button>
//           <button disabledir applydisable="isneed" >ClickMe</button>

//indsectorapp.run(function ($rootScope) {

//   

//    function readCookie(name) {
//        var nameEQ = name + "=";
//        var ca = document.cookie.split(';');
//        for(var i=0;i < ca.length;i++) {
//            var c = ca[i];
//            while (c.charAt(0)==' ') c = c.substring(1,c.length);
//            if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
//        }
//        return null;
//    }

//    $rootScope.Token = readCookie('username');
//});


indsectorapp.config(['$httpProvider', function ($httpProvider) {
    //initialize get if not there
    if (!$httpProvider.defaults.headers.get) {
        $httpProvider.defaults.headers.get = {};
    }

    // Answer edited to include suggestions from comments
    // because previous version of code introduced browser-related errors

    //disable IE ajax request caching
    $httpProvider.defaults.headers.get['If-Modified-Since'] = 'Mon, 26 Jul 1997 05:00:00 GMT';
    // extra
    $httpProvider.defaults.headers.get['Cache-Control'] = 'no-cache';
    $httpProvider.defaults.headers.get['Pragma'] = 'no-cache';
}]);

indsectorapp.config(['$urlRouterProvider', '$stateProvider', function ($urlRouterProvider, $stateProvider) {


    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('Home', {
            url: '/',

            templateUrl: '/app/IndividualSector/View/Index.html'
        })

    $stateProvider
    .state('TransactionReason', {
        url: '/TransactionReason',
        controller: 'TransactionReasonCtrl',
        templateUrl: '/app/IndividualSector/View/TransactionReasonPage.html'
    })

    $stateProvider
   .state('PackageScreen', {
       url: '/PackageScreen?CustomerId',
       controller: 'PackageScreenController',
       templateUrl: '/app/IndividualSector/View/PackageScreen.html'
   })

    $stateProvider
 .state('SelectPackage', {
     url: '/SelectPackage?CustomerId',
     controller: 'PackageSelectCtrl',
     templateUrl: '/app/IndividualSector/View/PackageSelectScreen.html'
 })



    $stateProvider
   .state('LodgingCheckIn', {
       url: '/LodgingCheckIn?Gender',
       controller: 'LodgingCheckInCtrl',
       templateUrl: '/app/IndividualSector/View/LodgingCheckIn.html'
   })
    $stateProvider
      .state('LodgingCheckInDetail', {
          url: '/LodgingCheckInDetail?RecId',

          controller: 'LodgingCheckInDetailCtrl',
          templateUrl: '/app/IndividualSector/View/LodgingCheckInDetails.html'
      })

    // $stateProvider
    //.state('TransactionReasonDetails', {
    //    url: '/TransactionReasonDetails?RecId',
    //    controller: 'TransactionReasonDetailCtrl',
    //    templateUrl: '/app/IndividualSector/TransactionReasonDetailCtrlPage.html'
    //})




    //$stateProvider
    //   .state('CustomerRequest', {
    //       url: '/CustomerRequest',

    //       controller: 'CustomerRequestCtrl',
    //       templateUrl: '/app/IndividualSector/CustomerRequest.html'
    //   })


    $stateProvider
      .state('CustomerView', {
          url: '/CustomerView',

          controller: 'CustomerViewCtrl',
          templateUrl: '/app/IndividualSector/View/CustomerPage1.html'
      })

    $stateProvider
     .state('CustomerDetailsView', {
         url: '/CustomerDetailsView?CustomerID&Source',

         controller: 'CustomerDetailsViewCtrl',
         templateUrl: '/app/IndividualSector/View/CustromerDetails1.html'
     })

    $stateProvider
    .state('Customer', {
        url: '/Customer',

        controller: 'CustomerCtrl',
        templateUrl: '/app/IndividualSector/View/CustomerPage.html'
    })

    $stateProvider
     .state('CustomerDetails', {
         url: '/CustomerDetails?CustomerID&Source',

         controller: 'CustomerDetailsCtrl',
         templateUrl: '/app/IndividualSector/View/CustomerDetailsPage.html'
     })



    $stateProvider
       .state('Contract', {
           url: '/Contract',

           controller: 'ContractCtrl',
           templateUrl: '/app/IndividualSector/View/Contract.html'
       })

    $stateProvider
      .state('ContactprintSetup', {
          url: '/ContactprintSetup',

          controller: 'ContactprintSetupCtrl',
          templateUrl: '/app/IndividualSector/View/ContactprintSetup.html'
      })
    $stateProvider
     .state('ContractprintSetupDetail', {
         url: '/ContractprintSetupDetail?RecId',

         controller: 'ContactprintSetupDetailCtrl',
         templateUrl: '/app/IndividualSector/View/ContactprintSetupDetail.html'
     })

    $stateProvider
      .state('ContractPrint', {
          url: '/ContractPrint',

          controller: 'ContractCtrl',
          templateUrl: '/app/IndividualSector/View/ContractPrint.html'
      })

    $stateProvider
      .state('IndContract', {
          url: '/IndContract',

          controller: 'IndContractCtrl',
          templateUrl: '/app/IndividualSector/View/IndContract.html'
      })

    $stateProvider
    .state('IndContractQuick', {
        url: '/IndContractQuick',
        controller: 'IndContractQuickCtrl',
        templateUrl: '/app/IndividualSector/View/IndContractQuick.html'
    })

    $stateProvider
    .state('EmployeeList', {
        url: '/EmployeeList',

        controller: 'EmployeeListCtrl',
        templateUrl: '/app/IndividualSector/View/EmployeeList.html'
    })

    $stateProvider
 .state('EmployeeDetails', {
     url: '/EmployeeDetails?EmployeeId',

     controller: 'EmployeeDetailsCtrl',
     templateUrl: '/app/IndividualSector/View/EmployeeDetail.html'
 })
    $stateProvider
 .state('EmployeeInLodging', {
     url: '/EmployeeInLodging',

     controller: 'EmployeeInLodgingCtrl1',
     templateUrl: '/app/IndividualSector/View/EmployeeInLodging.html'
 })



    $stateProvider
     .state('CustPaymentlist', {
         url: '/CustPaymentlist',

         controller: 'PaymentCtrl',
         templateUrl: '/app/IndividualSector/View/Payment.html'
     })

    $stateProvider
   .state('PaymentDetail', {
       url: '/PaymentDetail?PaymentNumber',
       controller: 'PaymentDeatilsCtrl',
       templateUrl: '/app/IndividualSector/View/PaymentDetail.html'
   })

    $stateProvider
       .state('LookUp', {
           url: '/LookUp',
           controller: 'LookUpNewCtrl',
           templateUrl: '/app/IndividualSector/View/LookUpNew.html'
       })



      .state('LookUpValues', {
          url: '/LookUpValues?LookupTypeId',
          controller: 'LookUpValuesNewCtrl',
          templateUrl: '/app/IndividualSector/View/LookUpValuesNew.html'

      })
    //ContractDetail.js actually here
    $stateProvider
    .state('ContractDetails', {
        url: '/ContractDetails?RecId',
        controller: 'ContractLinesCtrl',
        templateUrl: '/app/IndividualSector/View/ContractDetail.html'
    })

    $stateProvider
     .state('CustomerRequest', {
         url: '/CustomerRequest?ProcessType',

         controller: 'CustomerRequestFlowCtrl',
         templateUrl: '/app/IndividualSector/View/CustomerRequestFlow.html'
     })


    $stateProvider
    .state('FinanceWorkflow', {
        url: '/FinanceWorkflow',

        controller: 'FinanceWorkflowCtrl',
        templateUrl: '/app/IndividualSector/View/FinanceWorkflow.html'
    })

    $stateProvider
   .state('FinanceWorkflowDetails', {
       url: '/FinanceWorkflowDetails?Value&Stage',

       controller: 'FinanceWorkflowDetailCtrl',
       templateUrl: '/app/IndividualSector/View/FinanceWorkflowDetail.html'
   })

    $stateProvider
   .state('FinanceApplication', {
       url: '/FinanceApplication?ProcessType',

       controller: 'FinanceApplication',
       templateUrl: '/app/IndividualSector/View/FinanceApplication.html'
   })

    $stateProvider
   .state('FinanceApplicationDetails', {
       url: '/FinanceApplicationDetails?RecId&ApplicationId&ProcessType',
       controller: 'FinanceApplicationDetails',
       templateUrl: '/app/IndividualSector/View/FinanceApplicationDetails.html',
   })


    $stateProvider
    .state('NewContract', {
        url: '/NewContract?ProcessType',

        controller: 'NewContractRequestCtrl',
        templateUrl: '/app/IndividualSector/View/NewContractRequest.html'
    })

    $stateProvider
   .state('NewContractDetails', {
       url: '/NewContractDetails?RecId&ProcessType&ContractId',
       controller: 'NewContractRequestDetailsCtrl',
       templateUrl: '/app/IndividualSector/View/NewContractRequestDetails.html',
   })

    $stateProvider
   .state('CancelContract', {
       url: '/CancelContract',

       controller: 'ContractCancelFlowCtrl',
       templateUrl: '/app/IndividualSector/View/ContractCancel.html'
   })

    $stateProvider
  .state('ContractCancelDetails', {
      url: '/ContractCancelDetails?RecId',

      controller: 'ContractCancelLinesCtrl',
      templateUrl: '/app/IndividualSector/View/ContractCancelDetails.html'
  })

    

    $stateProvider
     .state('CustomerSponserChangeFlow', {
         url: '/CustomerSponserChangeFlow',

         controller: 'CustSponserChangeCtrl',
         templateUrl: '/app/IndividualSector/View/CustSponserChangeFlow.html'
     })
    $stateProvider
    .state('CustomerSponserChangeFlowDetails', {
        url: '/CustomerSponserChangeFlowDetails?RecId',

        controller: 'CustSponsorDetailsCtrl',
        templateUrl: '/app/IndividualSector/View/CustSponserChangeFlowDetails.html'
    })
    
    $stateProvider
      .state('DeliveryRequest', {
          url: '/DeliveryRequest',

          controller: 'DeliveryRequCtrl',
          templateUrl: '/app/IndividualSector/View/DeliveryRequest.html'
      })

    $stateProvider
     .state('DeliveryRequestFlow', {
         url: '/DeliveryRequestFlow',

         controller: 'DeliveryRequestFlowCtrl',
         templateUrl: '/app/IndividualSector/View/DeliveryRequestFlow.html'
     })

    $stateProvider
   .state('IndividualEnvelopeFlow', {
       url: '/IndividualEnvelopeFlow',

       controller: 'IndividualEnvelopeFlowCtrl',
       templateUrl: '/app/IndividualSector/View/IndividualEnvelopeFlow.html'
   })

    $stateProvider
   .state('LabourTransferFlow', {
       url: '/LabourTransferFlow',

       controller: 'LabourTransferFlowCtrl',
       templateUrl: '/app/IndividualSector/View/LabourTransferFlow.html'
   })


    $stateProvider
 .state('DeliveryRequestDetails', {
     url: '/DeliveryRequestDetails?RecId',

     controller: 'DeliveyRequestDetailsCtrl',
     templateUrl: '/app/IndividualSector/View/DeliveryRequestFlowDetails.html'
 })

    $stateProvider
    .state('TransferOrderRequestFlow', {
        url: '/TransferOrderRequestFlow',

        controller: 'TransferOrderRequestFlowCtrl',
        templateUrl: '/app/IndividualSector/View/TransferOrderRequestFlow.html'
    })
    
    $stateProvider
.state('TransferOrderRequestFlowDetails', {
    url: '/TransferOrderRequestFlowDetails?OrderId',

    controller: 'TransferOrderRequestFlowDetailsCtrl',
    templateUrl: '/app/IndividualSector/View/TransferOrderRequestFlowDetails.html'
})
    $stateProvider
     .state('CheckOutFlow', {
         url: '/CheckOutFlow?Type',

         controller: 'CheckOutFlowCtrl',
         templateUrl: '/app/IndividualSector/View/CheckOutFlow.html'
     })



    $stateProvider
  .state('CheckoutDetails', {
      url: '/CheckoutDetails?RecId',

      controller: 'CheckOutFlowDetailsCtrl',
      templateUrl: '/app/IndividualSector/View/CheckOutFlowDetails.html'
  })

    $stateProvider
  .state('CustomerRequestDetails', {
      url: '/CustomerRequestDetails?RecId&ProcessType&ContractId',
      controller: 'CustomerRequestDetailsCtrl',
      templateUrl: '/app/IndividualSector/View/CustomerRequestDetailsFlow.html',
      //params: {
      //    RecId: null,
      //    ProcessType: null
      //}
      //  resolve: {
      //data: ['$stateParams', function($stateParams) {
      //    console.log($stateParams.table);
      //}]
      //}
  })


    $stateProvider
    .state('Invoice', {
        url: '/Invoice',

        controller: 'InvoiceController',
        templateUrl: '/app/IndividualSector/View/Invoice.html'
    })
    $stateProvider
    .state('InvoiceDetails', {
        url: '/InvoiceDetails?InvoiceNumber',
        controller: 'InvoiceDetailCtrl',
        templateUrl: '/app/IndividualSector/View/InvoiceDetail.html'
    })


    $stateProvider
      .state('mypage', {
          url: '/mypage?pageId',
          controller: 'DynamicPageCtrl',
          templateUrl: '/app/DynamicPage/Views/DynamicPage - Copy.html'
      })

    $stateProvider
     .state('mypageDetails', {
         url: '/mypageDetails?pageId&inputparameter',
         controller: 'DynamicPageCtrl',
         templateUrl: '/app/DynamicPage/Views/DynamicPage - Copy.html'
     })

    //$stateProvider
    // .state('mytaskpage', {
    //     url: '/mytaskpage?pageId=PGS0000002',
    //     controller: 'DynamicPageCtrl',
    //     templateUrl: '/app/DynamicPage/Views/DynamicPage.html'
    // })

    $stateProvider
      .state('PendingVerification', {
          url: '/PendingVerification',
          controller: 'PendingVerificationCtrl',
          templateUrl: '/app/IndividualSector/View/PendingVerification.html'
      })

    $stateProvider
   .state('PaymentSettlement', {
       url: '/PaymentSettlement',

       controller: 'PaymentSettementCtrl',
       templateUrl: '/app/IndividualSector/View/PaymentSettlement.html'
   })

    $stateProvider
     .state('Opportunity', {
         url: '/Opportunity',

         controller: 'OpportunityCtrl',
         templateUrl: '/app/IndividualSector/View/Opportunity.html'
     })
    $stateProvider
.state('RequestEmployeeLatest', {
    url: '/RequestEmployeeLatest?id&Checkout&CustomerId',
    controller: 'RequestEmployeeLatestCtrl',
    templateUrl: '/app/IndividualSector/View/RequestEmployeeLatest.html'
})

    $stateProvider
.state('CommissionTransaction', {
    url: '/CommissionTransaction',
    controller: 'CommissionTransactionCtrl',
    templateUrl: '/app/IndividualSector/View/CommissionTransaction.html'
})

    $stateProvider
.state('CommissionPaymentDetails', {
    url: '/CommissionPaymentDetails?PaymentId',
    controller: 'CommissionPaymentDetailsCtrl',
    templateUrl: '/app/IndividualSector/View/CommissionPaymentDetails.html'
})


    $stateProvider
.state('CommissionPayment', {
    url: '/CommissionPayment',
    controller: 'CommissionPaymentCtrl',
    templateUrl: '/app/IndividualSector/View/CommissionPayment.html'
})


    $stateProvider
.state('PromotionCode', {
    url: '/PromotionCode',
    controller: 'PromotionCodeCtrl',
    templateUrl: '/app/IndividualSector/View/PromotionCode.html'
})
    $stateProvider
.state('PromotionSetup', {
    url: '/PromotionSetup',
    controller: 'PromotionSetupCtrl',
    templateUrl: '/app/IndividualSector/View/PromotionSetup.html'
})

    $stateProvider
.state('PromotionSetupDetails', {
    url: '/PromotionSetupDetails?RecId',
    controller: 'PromotionSetupDetailsCtrl',
    templateUrl: '/app/IndividualSector/View/PromotionSetupDetails.html'
})

    $stateProvider
.state('OfferSetup', {
    url: '/OfferSetup',
    controller: 'OfferSetupCtrl',
    templateUrl: '/app/IndividualSector/View/OfferSetup.html'
})

    $stateProvider
.state('OfferSetupDetails', {
    url: '/OfferSetupDetails?RecId',
    controller: 'OfferSetupDetails',
    templateUrl: '/app/IndividualSector/View/OfferSetupDetails.html'
})

    $stateProvider
.state('BankAction', {
    url: '/BankAction',
    controller: 'BankActionCtrl',
    templateUrl: '/app/IndividualSector/View/BankAction.html'
})

    $stateProvider
 .state('AccountingPeriod', {
     url: '/AccountingPeriod',
     controller: 'AccountingPeriodCtrl',
     templateUrl: '/app/IndividualSector/View/AccountingPeriod.html'
 })
 $stateProvider
    .state('InvoicingPeriod', {
        url: '/InvoicingPeriod',
        controller: 'InvoicingPeriod.js',
        templateUrl: '/app/IndividualSector/View/InvoicingPeriod.html'
})


    $stateProvider
.state('Callcenter', {
    url: '/Callcenter?ContractNumber',
    controller: 'CallcenterPageCtrl',
    templateUrl: '/app/IndividualSector/View/CallcenterPage.html'
})

    $stateProvider
   .state('CustomerEnquiry', {
       url: '/CustomerEnquiry?CustomerId',
       controller: 'CustomerEnquiryCtrl',
       templateUrl: '/app/IndividualSector/View/CustomerEnquiry.html'
   })

    $stateProvider
.state('EmployeeEnquiry', {
    url: '/EmployeeEnquiry?EmployeeId',
    controller: 'EmployeeEnquiryPageCtrl',
    templateUrl: '/app/IndividualSector/View/EmployeeEnquiryPage.html'
})
 

$stateProvider
.state('CustomerPayment', {
    url: '/CustomerPayment',
    controller: 'CustomerPaymentCtrl',
    templateUrl: '/app/IndividualSector/View/CustomerPayment.html'
})

//    $stateProvider
//.state('TabTest', {
//    url: '/TabTest',
//    controller: 'TabTestCtrl',
//    templateUrl: '/app/IndividualSector/View/TabTestPage.html'
//})

    $stateProvider
.state('IndividualEnvelopeFlowDetails', {
    url: '/IndividualEnvelopeFlowDetails?EnvelopeId&ProcessType',

    controller: 'IndividualEnvelopeFlowDetailsCtrl',
    templateUrl: '/app/IndividualSector/View/IndividualEnvelopeFlowDetails.html'
})
    
    $stateProvider
    .state('PosPaymnets', {
        url: '/PosPaymnets',
        controller: 'PosPaymnetsCtrl',
        templateUrl: '/app/IndividualSector/View/PosPaymnets.html'
    })

    $stateProvider
    .state('Notes', {
        url: '/Notes',
        controller: 'NotesCtrl',
        templateUrl: '/app/IndividualSector/View/Notes.html'
    })
    $stateProvider
 .state('VacationSetUp', {
     url: '/VacationSetUp',
     controller: 'VacationSetUpCtrl',
     templateUrl: '/app/IndividualSector/View/VocationSetUp.html'
 })

    $stateProvider
.state('InvoicePaymentView', {
    url: '/InvoicePaymentView',
    controller: 'InvoicePaymentDetailsCtrl',
    templateUrl: '/app/IndividualSector/View/InvoicePaymentDetailsView.html'
})

    $stateProvider
.state('ContractPaymentView', {
    url: '/ContractPaymentView',
    controller: 'ContractPaymentDetailsCtrl',
    templateUrl: '/app/IndividualSector/View/ContractPaymentDetailsView.html'
})

    $stateProvider
.state('PaymentAdvanceSettlementView', {
    url: '/PaymentAdvanceSettlementView',
    controller: 'PaymentAdvanceSettlementCtrl',
    templateUrl: '/app/IndividualSector/View/PaymentAdvanceSetllementView.html'
})

    $stateProvider
.state('PaymentComparisionForCustomerView', {
    url: '/PaymentComparisionForCustomerView',
    controller: 'PaymentComparisionForCustomerCtrl',
    templateUrl: '/app/IndividualSector/View/PaymentComparissionForCustomer.html'
})

    $stateProvider
.state('PaymentComparisionForContractView', {
    url: '/PaymentComparisionForContractView',
    controller: 'PaymentComparisionForContractCtrl',
    templateUrl: '/app/IndividualSector/View/PaymentComparissionForContract.html'
})
    $stateProvider
  .state('VacationSetUpDetails', {
      url: '/VacationSetUpDetails?RecId',
      controller: 'VacationSetUpDetailsCtrl',
      templateUrl: '/app/IndividualSector/View/VacationSetUpDetails.html'
  })
    $stateProvider
 .state('TaxSetup', {
     url: '/TaxSetup',
     controller: 'TaxSetupCtrl',
     templateUrl: '/app/IndividualSector/View/TaxSetup.html'
 })

    $stateProvider
 .state('PackageLineDetails', {
     url: '/PackageLineDetails?RecId',
     controller: 'PackageDetailsCtrl',
     templateUrl: '/app/IndividualSector/View/PackageDetails.html'
 })


    $stateProvider
.state('ExpenseList', {
    url: '/ExpenseList',
    controller: 'QuotationCostTypeSetUpCtrl',
    templateUrl: '/app/IndividualSector/View/QuotationCostTypeSetUp.html'
})
    $stateProvider
.state('ExpenseSetup', {
    url: '/ExpenseSetup',

    controller: 'ExpenceTypeCtrl',
    templateUrl: '/app/IndividualSector/View/ExpenceTypeSetUp.html'
})


    $stateProvider
.state('SadadBill', {
    url: '/SadadBill',
    controller: 'SadadBillCtrl',
    templateUrl: '/app/IndividualSector/View/SadadBill.html'
})

    $stateProvider
 .state('SadadBillDetail', {
     url: '/SadadBillDetail?RecId',
     controller: 'SadadBillDetailsCtrl',
     templateUrl: '/app/IndividualSector/View/SadadBillDetail.html'
 })


    $stateProvider
.state('SadadAccounts', {
    url: '/SadadAccounts',
    controller: 'SadadAccountsCtrl',
    templateUrl: '/app/IndividualSector/View/SadadAccounts.html'
})

    $stateProvider
.state('SadadCustPayment', {
    url: '/SadadCustPayment',
    controller: 'SadadCustPaymentTransactionCtrl',
    templateUrl: '/app/IndividualSector/View/SadadCustPaymentTransaction.html'
})

    $stateProvider
.state('CustRefund', {
    url: '/CustRefund',
    controller: 'CustRefundCtrl',
    templateUrl: '/app/IndividualSector/View/CustRefund.html'


})

}]);



//indsectorapp.directive('loading', ['$http', function ($http) {
//    return {
//        restrict: 'A',
//        link: function (scope, elm, attrs) {
//            scope.isLoading = function () {
//                return $http.pendingRequests.length > 0;
//            };
//            scope.$watch(scope.isLoading, function (v) {
//                if (v) {
//                    elm.show();
//                } else {
//                    elm.hide();
//                }
//            });
//        }
//    };
//}]);



indsectorapp.directive('commentpopup', function () {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            //HeaderId: '=popid',
            reftable: '=reftable',
            onSelect: '=onSelect'
        },
        controller: ['$scope', '$http', function ($scope, $http) {
            $scope.AddComments = {
                RefTableName: $scope.reftable,
            };
            $scope.onSelect = function (h, r) {
                // alert("Header" + h + "red" + r);
                $("#showcommentpopup").click();

                $scope.AddComments = {
                    RefRecId: h,
                    RefTableName: $scope.reftable,
                };
                GetTaskComments();

            };
            function GetTaskComments() {
                

                var response = $http({
                    method: "post",
                    url: "/api/GetComments",

                    params: {

                        id: $scope.AddComments.RefRecId, RefTable: $scope.reftable

                    }
                });

                response.then(function (emp) {
                    

                    $scope.commentslist = emp.data;
                });

            }
            $scope.saveupdate = function () {
                

                if ($scope.AddComments && $scope.AddComments.FollowUp) {
                    var data = new FormData();

                    var fileInput = document.getElementById('fileUpload');
                    for (i = 0; i < fileInput.files.length; i++) {
                        data.append(fileInput.files[i].name, fileInput.files[i]);
                    }

                    var strtask = JSON.stringify($scope.AddComments);

                    data.append("Task", strtask);

                    var response = $http({
                        method: "post",
                        url: "/FileUse/AddTaskComments",
                        data: data,

                    });

                    response.then(function (emp) {
                        
                        if (emp.data != "Comment Update Successfully") {
                            swal("", emp.data, "success");
                        }
                        $scope.AddComments.FollowUp = null;
                        $scope.AddComments.Title = null;

                        GetTaskComments();
                        $("#fileUpload").val("");

                    });
                }
                else {
                    swal("", "Please Fill Comments", "warning");
                }
            }

        }],
        link: function (scope, element, attrs) {



        },
        templateUrl: '/app/Agent/CommentPopUp.html'
    };
});





indsectorapp.directive('mygrid', function () {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            //HeaderId: '=popid',
            currentgrid: '=currentgrid',
            source: '=source',
            gridname: '=gridname',
            columdef: '=columdef',
            currentmodel: '=currentmodel',
            popdata: '=popdata'
        },
        controller: ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
        function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {


            $scope.currentgrid = {
                toggleFiltering: "",
                expandAll: "",
                toggleRow: "",
                changeGrouping: "",
                gridrefresh: "",
                CreatePop: "",
                getcheckIds: "",
            };

            var gridname = $scope.gridname;
            gridview();
            function gridview() {

                $templateCache.put('ui-grid/selectionRowHeaderButtons',
       "<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
       );

                $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                    if (col.filters[0].term) {
                        return 'header-filtered';
                    } else {
                        return '';
                    }
                };

                $scope.gridOptions = {
                    enableFiltering: false,
                    enableColumnResizing: true,
                    enableGridMenu: true,
                    enableRowSelection: true,
                    enableSelectAll: true,
                    selectionRowHeaderWidth: 35,
                    exporterMenuPdf: false,
                    enableGroupHeaderSelection: true,
                    exporterCsvFilename: gridname + '.csv',
                    paginationPageSizes: [15, 25, 50, 100, 500],
                    paginationPageSize: 15,
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;

                        $scope.gridApi.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                            if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                                // this is a group header
                                children = $scope.gridApi.treeBase.getRowChildren(rowChanged);
                                children.forEach(function (child) {
                                    if (rowChanged.isSelected) {
                                        $scope.gridApi.selection.selectRow(child.entity);
                                    } else {
                                        $scope.gridApi.selection.unSelectRow(child.entity);
                                    }
                                });
                            }
                        });
                    },


                    //data: $scope.source
                };
            }



            //var DetailAction = {
            //    field: 'Action', pinnedLeft: true, name: '', width: '60', enableFiltering: false, enableSorting: false,
            //    headerCellTemplate: '<div></div>',
            //    cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="glyphicon glyphicon-pencil themeprimary" ng-click="grid.appScope.GetRequestById(row.entity)" ></span>| <span  class="glyphicon glyphicon-list-alt themeprimary " ui-sref="mypageDetails({pageId: \'PGS0000005\',inputParameter:row.entity.RecId})" ></span> </div></div>'
            //};

            $scope.$watch("source", function () {

                

                $scope.gridOptions.data = $scope.source;
            });

            //if ($scope.columdef) {



            //    $scope.gridOptions.columnDefs = $scope.columdef;

            //}

            $scope.$watch("columdef", function () {

                

                $scope.gridOptions.columnDefs = $scope.columdef;
            });




            $scope.currentgrid.toggleFiltering = function () {

                $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
            };


            $scope.currentgrid.CreatePop = function () {
                alert("CreateClick");
                $("#GetCreatePopUp").show();

                $scope.currentmodel.CurrentMode = 1;

                var dname = $scope.currentmodel.DataSourceName;
                $scope.popdata[dname] = [];

                $scope.currentmodel.CurrentMode = 1;

            };

            $scope.GetRequestById = function (grid) {

                $("#GetCreatePopUp").show();
                
                $scope.currentmodel.CurrentMode = 2;
                var dname = $scope.currentmodel.DataSourceName;
                $scope.popdata[dname] = [];
                $scope.popdata[dname][0] = grid;

            };

            $scope.setprimarylocation2 = function (grid) {

                //$("#GetCreatePopUp").show();
                //$scope.currentmodel.CurrentMode = 2;
                //var dname = $scope.currentmodel.DataSourceName;
                //$scope.popdata[dname] = [];
                //$scope.popdata[dname][0] = grid;
                $scope.gridfn.GetRequestById(grid);

            };




            $scope.currentgrid.getcheckIds = function (r) {
                $scope.SelectValue = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                    return gridRow.RecId;
                });

                return $scope.SelectValue;
            }

            $scope.currentgrid.expandAll = function () {
                $scope.gridApi.treeBase.expandAllRows();
            };

            $scope.currentgrid.toggleRow = function (rowNum) {
                $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
            };

            $scope.currentgrid.changeGrouping = function () {
                $scope.gridApi.grouping.clearGrouping();

            };

            $scope.currentgrid.gridrefresh = function () {

                $scope.gridOptions.data = $scope.source;
                // $scope.gridOptions.columnDefs = $scope.columdef;

            }



        }],
        link: function (scope, element, attrs) {



        },
        template: ' <div id="grid1" ui-grid="gridOptions" ui-grid-selection ui-grid-grouping ui-grid-exporter ui-grid-resize-columns ui-grid-pinning ui-grid-move-columns ui-grid-pagination class="grid"></div>  '
    };
});


indsectorapp.directive('mygriddy', function () {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            //HeaderId: '=popid',
            currentgrid: '=currentgrid',
            source: '=source',
            gridname: '=gridname',
            columdef: '=columdef',
            popupmodel: '=popupmodel',
            popupdata: '=popupdata',
            modeldata: '=modeldata',
        },
        controller: ['$scope', '$rootScope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
        function ($scope, $rootScope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {


            $scope.currentgrid = {
                toggleFiltering: "",
                expandAll: "",
                toggleRow: "",
                changeGrouping: "",
                gridrefresh: "",
                CreatePop: ""
            };

            var gridname = $scope.gridname;
            gridview();
            function gridview() {

                $templateCache.put('ui-grid/selectionRowHeaderButtons',
       "<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
       );

                $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                    if (col.filters[0].term) {
                        return 'header-filtered';
                    } else {
                        return '';
                    }
                };

                $scope.gridOptions = {
                    enableFiltering: false,
                    enableColumnResizing: true,
                    showHeader: true,
                    enableGridMenu: true,
                    enableRowSelection: true,
                    enableSelectAll: true,
                    selectionRowHeaderWidth: 35,
                    exporterMenuPdf: false,
                    enableGroupHeaderSelection: true,
                    exporterCsvFilename: gridname + '.csv',
                    paginationPageSizes: [15, 25, 50, 100, 500],
                    paginationPageSize: 15,
                    onRegisterApi: function (gridApi) {
                        $scope.gridApi = gridApi;

                        $scope.gridApi.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                            if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                                // this is a group header
                                children = $scope.gridApi.treeBase.getRowChildren(rowChanged);
                                children.forEach(function (child) {
                                    if (rowChanged.isSelected) {
                                        $scope.gridApi.selection.selectRow(child.entity);
                                    } else {
                                        $scope.gridApi.selection.unSelectRow(child.entity);
                                    }
                                });
                            }
                        });
                    },



                };
            }



            $scope.$watch("source", function () {

                
                if ($scope.source && $scope.source.length > 0) {
                    $scope.gridOptions.data = $scope.source;
                }
            });


            var edit = {
                field: 'Action', pinnedLeft: true, name: '', width: '60', enableFiltering: false, enableSorting: false,
                headerCellTemplate: '<div></div>',
                cellTemplate: '<div class="ui-grid-cell-contents"> <span  class="glyphicon glyphicon-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetRequestById(row.entity)" ></span> | <span ng-if="grid.appScope.modeldata.IsHaveConsumer"  class="glyphicon glyphicon-list-alt themeprimary " ng-click="grid.appScope.GetConsumerPage(grid.appScope.modeldata.ConsumerId,grid.appScope.modeldata.ConsumerType,row.entity.RecId)"  ></span> </div>'
            };

            //if ($scope.columdef) {

            //   // $scope.gridOptions.columnDefs = $scope.columdef;


            //}



            $scope.$watch("columdef", function () {
                if ($scope.columdef && $scope.columdef.length > 0) {
                    // alert("col");
                    



                    //$scope.columdef.forEach(function (row) {
                    //    row.headerCellClass = 'header-filtered';
                    //});

                    $scope.columdef.push(edit);

                    $scope.gridOptions.columnDefs = $scope.columdef;
                    //   $scope.gridOptions.columnDefs.push(edit);
                }
            });


            $scope.GetRequestById = function (e) {
                
                //$scope.currentgrid.$show();
                $scope.popupmodel.$cancel();

                $scope.popupdata[0] = e;

                var popupid = gridname.replace(/[\s]/g, '');
                $("#showpopup_" + popupid).click();
                $scope.popupmodel.$show();

            };


            $scope.currentgrid.CreatePop = function () {
                
                $scope.popupmodel.$cancel();
                $scope.popupdata[0] = {};
                var popupid = gridname.replace(/[\s]/g, '');
                $("#showpopup_" + popupid).click();
                $scope.popupmodel.$show();
            };

            $scope.GetConsumerPage = function (consumerid, consumertype, input) {
                alert("Click");
                //function GetConsumerPage(consumerid, consumertype, input) {
                $rootScope.$emit("GetConsumerPageRoot", { consumerid: consumerid, consumertype: consumertype, input: input });
            }


            $scope.currentgrid.toggleFiltering = function () {

                $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
                $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
            };
            $scope.currentgrid.expandAll = function () {
                $scope.gridApi.treeBase.expandAllRows();
            };

            $scope.currentgrid.toggleRow = function (rowNum) {
                $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
            };

            $scope.currentgrid.changeGrouping = function () {
                $scope.gridApi.grouping.clearGrouping();

            };

            $scope.currentgrid.gridrefresh = function () {

                $scope.gridOptions.data = $scope.source;
                // $scope.gridOptions.columnDefs = $scope.columdef;

            }



        }],
        link: function (scope, element, attrs) {



        },
        template: ' <div id="grid1" ui-grid="gridOptions" ui-grid-selection ui-grid-grouping ui-grid-exporter ui-grid-resize-columns ui-grid-pinning ui-grid-move-columns ui-grid-pagination class="grid"></div> '
    };
});




indsectorapp.directive('commentautopopup', function () {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            //HeaderId: '=popid',
            LookupTypId: '=reftable',
            onSelect: '=onSelec'
        },
        controller: ['$scope', '$http', function ($scope, $http) {



            $scope.AsigntoDropList = [];

            $scope.Agdroupdownmenu = false;
            $scope.Agdroupdownmenucaret = true;

            $scope.ShowAddtxt = false;




            function load() {
                $http.get("/api/GetMasterbyId?id=" + $scope.LookupTypId)
            .success(function (data) {
                
                $scope.AsigntoDropList = data;



            });
            }

            load();

            $scope.isfinish = false;


            $scope.$watch("onSelect", function () {

                //$scope.AsigntoDropList.forEach(function (child) {
                //    if (rowChanged.isSelected) {
                //        $scope.gridApi.selection.selectRow(child.entity);
                //    } else {
                //        $scope.gridApi.selection.unSelectRow(child.entity);
                //    }
                //});
                
                isfinish = false;
                $scope.AsigntoDropList.filter(function (d) {

                    if (!isfinish) {
                        if (d.Id == $scope.onSelect) {
                            $scope.AssignedToName = d.Desc;
                            $scope.AssignedToId = d.Id;
                            isfinish = true
                        }
                        else {
                            $scope.AssignedToName = null;
                            $scope.AssignedToId = null;
                        }
                    }
                });
            });





            $scope.AddAgdroupdownmenu = function () {
                
                $scope.ShowAddtxt = true;

            }



            $scope.addlookupval = function () {
                
                if ($scope.txtlookvalue) {
                    $scope.LookUpValues = {};
                    $scope.LookUpValues.LookupTypeId = $scope.LookupTypId;
                    $scope.LookUpValues.Description = $scope.txtlookvalue;

                    var response = $http({
                        method: "post",
                        url: "/api/CreateLookUpValues",
                        params: {
                            LookUpValues: JSON.stringify($scope.LookUpValues)
                        }
                    });
                    response.then(function (emp) {
                        
                        swal("", emp.data, "success");
                        $scope.txtlookvalue = null;
                        load();
                    });
                }
            }


            $scope.selectAssignedTo = function (data) {
                
                //$scope.AssignedToName = data.Desc;
                //$scope.AssignedToId = data.Id;
                $scope.Agdroupdownmenu = false;
                $scope.Agdroupdownmenucaret = true;
                isfinish = false;
                $scope.onSelect = data.Id;
            }
            $scope.showAgdroupdownmenu = function () {
                
                $scope.Agdroupdownmenucaret = false;
                $scope.Agdroupdownmenu = true;
            }
            $scope.closeAgdroupdownmenu = function () {
                
                //$scope.AssignedToName = "";
                //$scope.AssignedToId = "";
                $scope.AssignedTofilter = "";
                $scope.Agdroupdownmenu = false;
                $scope.Agdroupdownmenucaret = true;
                $scope.ShowAddtxt = false;
            }

        }],
        link: function (scope, element, attrs) {

            //scope.onSelect(iAttrs.uiItems, function () {

            //    

            //    $scope.AsigntoDropList.filter(function (d) {

            //        if (!isfinish) {
            //            if (d.Id == $scope.onSelect) {
            //                $scope.AssignedToName = d.Desc;
            //                $scope.AssignedToId = d.Id;
            //                isfinish = true
            //            }
            //            else {
            //                $scope.AssignedToName = null;
            //                $scope.AssignedToId = null;
            //            }
            //        }
            //    });
            //});

        },
        templateUrl: '/app/Agent/CommonLookup.html'
    };
});



indsectorapp.directive('resizable1', function () {
    return {
        restrict: 'A',
        scope: {
            callback: '&onResize'
        },
        link: function postLink(scope, elem, attrs) {
            elem.resizable({
                handles: 'e',
            });
            elem.on('resize', function (evt, ui) {
                scope.$apply(function () {
                    if (scope.callback) {
                        scope.callback({ $evt: evt, $ui: ui });
                    }
                })
            });
        }
    };
});
indsectorapp.directive('resizableleft', function () {
    
    return {
        restrict: 'A',
        scope: {
            callback: '&onResize'
        },
        link: function postLink(scope, elem, attrs) {
            
            elem.resizable({
                handles: 'w',
                maxWidth: 750,
                minWidth: 200
            });
            elem.on('resize', function (evt, ui) {
                scope.$apply(function () {
                    if (scope.callback) {
                        scope.callback({ $evt: evt, $ui: ui });
                    }
                })
            });
        }
    };
});


indsectorapp.directive('resizer333', function ($document) {

    return function ($scope, $element, $attrs) {
        alert("zxcvxzc");

        $element.on('mousedown', function (event) {
            event.preventDefault();

            $document.on('mousemove', mousemove);
            $document.on('mouseup', mouseup);
        });

        function mousemove(event) {

            if ($attrs.resizer == 'vertical') {
                // Handle vertical resizer
                var x = event.pageX;

                if ($attrs.resizerMax && x > $attrs.resizerMax) {
                    x = parseInt($attrs.resizerMax);
                }

                $element.css({
                    left: x + 'px'
                });

                $($attrs.resizerLeft).css({
                    width: x + 'px'
                });
                $($attrs.resizerRight).css({
                    left: (x + parseInt($attrs.resizerWidth)) + 'px'
                });

            } else {
                // Handle horizontal resizer
                var y = window.innerHeight - event.pageY;

                $element.css({
                    bottom: y + 'px'
                });

                $($attrs.resizerTop).css({
                    bottom: (y + parseInt($attrs.resizerHeight)) + 'px'
                });
                $($attrs.resizerBottom).css({
                    height: y + 'px'
                });
            }
        }

        function mouseup() {
            $document.unbind('mousemove', mousemove);
            $document.unbind('mouseup', mouseup);
        }
    };
});


indsectorapp.directive('resizable22', function () {
    
    alert("ss");
    var toCall;
    function throttle(fun) {
        if (toCall === undefined) {
            toCall = fun;
            setTimeout(function () {
                toCall();
                toCall = undefined;
            }, 100);
        } else {
            toCall = fun;
        }
    }
    return {
        restrict: 'AE',
        scope: {
            rDirections: '=',
            rCenteredX: '=',
            rCenteredY: '=',
            rWidth: '=',
            rHeight: '=',
            rFlex: '=',
            rGrabber: '@',
            rDisabled: '@'
        },
        link: function (scope, element, attr) {
            var flexBasis = 'flexBasis' in document.documentElement.style ? 'flexBasis' :
                'webkitFlexBasis' in document.documentElement.style ? 'webkitFlexBasis' :
                'msFlexPreferredSize' in document.documentElement.style ? 'msFlexPreferredSize' : 'flexBasis';

            // register watchers on width and height attributes if they are set
            scope.$watch('rWidth', function (value) {
                element[0].style.width = scope.rWidth + 'px';
            });
            scope.$watch('rHeight', function (value) {
                element[0].style.height = scope.rHeight + 'px';
            });

            element.addClass('resizable');

            var style = window.getComputedStyle(element[0], null),
                w,
                h,
                dir = scope.rDirections,
                vx = scope.rCenteredX ? 2 : 1, // if centered double velocity
                vy = scope.rCenteredY ? 2 : 1, // if centered double velocity
                inner = scope.rGrabber ? scope.rGrabber : '<span></span>',
                start,
                dragDir,
                axis,
                info = {};

            var updateInfo = function (e) {
                info.width = false; info.height = false;
                if (axis === 'x')
                    info.width = parseInt(element[0].style[scope.rFlex ? flexBasis : 'width']);
                else
                    info.height = parseInt(element[0].style[scope.rFlex ? flexBasis : 'height']);
                info.id = element[0].id;
                info.evt = e;
            };

            var dragging = function (e) {
                var prop, offset = axis === 'x' ? start - e.clientX : start - e.clientY;
                switch (dragDir) {
                    case 'top':
                        prop = scope.rFlex ? flexBasis : 'height';
                        element[0].style[prop] = h + (offset * vy) + 'px';
                        break;
                    case 'bottom':
                        prop = scope.rFlex ? flexBasis : 'height';
                        element[0].style[prop] = h - (offset * vy) + 'px';
                        break;
                    case 'right':
                        prop = scope.rFlex ? flexBasis : 'width';
                        element[0].style[prop] = w - (offset * vx) + 'px';
                        break;
                    case 'left':
                        prop = scope.rFlex ? flexBasis : 'width';
                        element[0].style[prop] = w + (offset * vx) + 'px';
                        break;
                }
                updateInfo(e);
                throttle(function () { scope.$emit('angular-resizable.resizing', info); });
            };
            var dragEnd = function (e) {
                updateInfo();
                scope.$emit('angular-resizable.resizeEnd', info);
                scope.$apply();
                document.removeEventListener('mouseup', dragEnd, false);
                document.removeEventListener('mousemove', dragging, false);
                element.removeClass('no-transition');
            };
            var dragStart = function (e, direction) {
                dragDir = direction;
                axis = dragDir === 'left' || dragDir === 'right' ? 'x' : 'y';
                start = axis === 'x' ? e.clientX : e.clientY;
                w = parseInt(style.getPropertyValue('width'));
                h = parseInt(style.getPropertyValue('height'));

                //prevent transition while dragging
                element.addClass('no-transition');

                document.addEventListener('mouseup', dragEnd, false);
                document.addEventListener('mousemove', dragging, false);

                // Disable highlighting while dragging
                if (e.stopPropagation) e.stopPropagation();
                if (e.preventDefault) e.preventDefault();
                e.cancelBubble = true;
                e.returnValue = false;

                updateInfo(e);
                scope.$emit('angular-resizable.resizeStart', info);
                scope.$apply();
            };

            dir.forEach(function (direction) {
                var grabber = document.createElement('div');

                // add class for styling purposes
                grabber.setAttribute('class', 'rg-' + direction);
                grabber.innerHTML = inner;
                element[0].appendChild(grabber);
                grabber.ondragstart = function () { return false; };
                grabber.addEventListener('mousedown', function (e) {
                    var disabled = (scope.rDisabled === 'true');
                    if (!disabled && e.which === 1) {
                        // left mouse click
                        dragStart(e, direction);
                    }
                }, false);
            });
        }
    };
});




indsectorapp.directive('commenautocomplete', function () {
    var index = -1;

    return {
        restrict: 'E',
        scope: {
            //HeaderId: '=popid',
            LookupTypId: '=source',
            onSelect: '=onSelect'
        },
        controller: ['$scope', '$http', function ($scope, $http) {



            $scope.AsigntoDropList = $scope.LookupTypId;

            $scope.Agdroupdownmenu = false;
            $scope.Agdroupdownmenucaret = true;

            $scope.ShowAddtxt = false;






            $scope.isfinish = false;


            $scope.$watch("LookupTypId", function () {
                $scope.AsigntoDropList = $scope.LookupTypId;
            });

            $scope.$watch("onSelect", function () {

                //$scope.AsigntoDropList.forEach(function (child) {
                //    if (rowChanged.isSelected) {
                //        $scope.gridApi.selection.selectRow(child.entity);
                //    } else {
                //        $scope.gridApi.selection.unSelectRow(child.entity);
                //    }
                //});
                
                isfinish = false;
                $scope.AsigntoDropList.filter(function (d) {

                    if (!isfinish) {
                        if (d.Id == $scope.onSelect) {
                            $scope.AssignedToName = d.Desc;
                            $scope.AssignedToId = d.Id;
                            isfinish = true
                        }
                        else {
                            $scope.AssignedToName = null;
                            $scope.AssignedToId = null;
                        }
                    }
                });
            });


            $scope.selectedIndex = -1;


            $scope.setIndex = function (i) {
                $scope.selectedIndex = parseInt(i);
            };

            $scope.getIndex = function (i) {
                return $scope.selectedIndex;
            };




            $scope.AddAgdroupdownmenu = function () {
                
                $scope.ShowAddtxt = true;

            }



            $scope.addlookupval = function () {
                
                if ($scope.txtlookvalue) {
                    $scope.LookUpValues = {};
                    $scope.LookUpValues.LookupTypeId = $scope.LookupTypId;
                    $scope.LookUpValues.Description = $scope.txtlookvalue;

                    var response = $http({
                        method: "post",
                        url: "/api/CreateLookUpValues",
                        params: {
                            LookUpValues: JSON.stringify($scope.LookUpValues)
                        }
                    });
                    response.then(function (emp) {
                        
                        swal("", emp.data, "success");
                        $scope.txtlookvalue = null;
                        load();
                    });
                }
            }


            $scope.selectAssignedTo = function (data) {
                
                //$scope.AssignedToName = data.Desc;
                //$scope.AssignedToId = data.Id;
                $scope.Agdroupdownmenu = false;
                $scope.Agdroupdownmenucaret = true;
                isfinish = false;
                $scope.onSelect = data.Id;
            }

            $scope.closeAgdroupdownmenu = function () {
                
                //$scope.AssignedToName = "";
                //$scope.AssignedToId = "";
                $scope.AssignedTofilter = "";
                $scope.Agdroupdownmenu = false;
                $scope.Agdroupdownmenucaret = true;
                $scope.ShowAddtxt = false;
            }

        }],
        link: function (scope, element, attrs) {
            

            //document.addEventListener("keydown", function (e) {
            //    var keycode = e.keyCode || e.which;

            //    switch (keycode) {
            //        case key.esc:
            //            // disable suggestions on escape
            //            scope.select();
            //            scope.setIndex(-1);
            //            scope.$apply();
            //            e.preventDefault();
            //    }
            //}, true);
            scope.showAgdroupdownmenu = function () {
                
                scope.Agdroupdownmenucaret = false;
                scope.Agdroupdownmenu = true;

                element.find('#filteid').focus();
                ///  $(this).children().eq(0).children().eq(4).children().eq(0).children().eq(0).focus();
                //    element.find("text").focus();
            }

            var key = { left: 37, up: 38, right: 39, down: 40, enter: 13, esc: 27, tab: 9 };

            element.find('#filteid').on("keydown", function (e) {

                
                var keycode = e.keyCode || e.which;

                var l = element.find('.dropdown-insidemenu li').length;

                // this allows submitting forms by pressing Enter in the autocompleted field
                //     if (!scope.completing || l == 0) return;

                // implementation of the up and down movement in the list of suggestions
                switch (keycode) {
                    case key.up:

                        index = scope.getIndex() - 1;
                        if (index < -1) {
                            index = l - 1;
                        } else if (index >= l) {
                            index = -1;
                            scope.setIndex(index);
                            //   scope.preSelectOff();
                            break;
                        }
                        scope.setIndex(index);

                        if (index !== -1)
                            scope.AssignedTofilter = element.find('.dropdown-insidemenu li')[index].children.text();

                        scope.$apply();

                        break;
                    case key.down:
                        index = scope.getIndex() + 1;
                        if (index < -1) {
                            index = l - 1;
                        } else if (index >= l) {
                            index = -1;
                            scope.setIndex(index);
                            // scope.preSelectOff();
                            scope.$apply();
                            break;
                        }
                        scope.setIndex(index);

                        if (index !== -1)
                            scope.AssignedTofilter = angular.element(angular.element(this).find('li')[index]).text();

                        break;
                    case key.left:
                        break;
                    case key.right:
                        //case key.enter:
                        //case key.tab:

                        //    index = scope.getIndex();
                        //    // scope.preSelectOff();
                        //    if (index !== -1) {
                        //        scope.select(angular.element(angular.element(this).find('li')[index]).text());
                        //        if (keycode == key.enter) {
                        //            e.preventDefault();
                        //        }
                        //    } else {
                        //        if (keycode == key.enter) {
                        //            scope.select();
                        //        }
                        //    }
                        //    scope.setIndex(-1);
                        //    scope.$apply();

                        //    break;
                    case key.esc:
                        // disable suggestions on escape
                        scope.select();
                        scope.setIndex(-1);
                        scope.$apply();
                        e.preventDefault();
                        break;
                    default:
                        return;
                }

            });

            //document.addEventListener("blur", function (e) {
            //    // disable suggestions on blur
            //    // we do a timeout to prevent hiding it before a click event is registered
            //    alert("blur");
            //    setTimeout(function () {
            //        scope.closeAgdroupdownmenu();
            //        scope.$apply();
            //    }, 150);
            //}, true);


            //scope.onSelect(iAttrs.uiItems, function () {

            //    

            //    $scope.AsigntoDropList.filter(function (d) {

            //        if (!isfinish) {
            //            if (d.Id == $scope.onSelect) {
            //                $scope.AssignedToName = d.Desc;
            //                $scope.AssignedToId = d.Id;
            //                isfinish = true
            //            }
            //            else {
            //                $scope.AssignedToName = null;
            //                $scope.AssignedToId = null;
            //            }
            //        }
            //    });
            //});

        },
        templateUrl: '/app/Agent/CommonAutoComplete.html'
    };
});


//Maximize Widget
angular.module('IndividualSectorApp')
    .directive('widgetMaximize', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-expand"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var button = el.find("i").eq(0);
                    var compress = "fa-compress";
                    var expand = "fa-expand";
                    if (widget.hasClass("maximized")) {
                        if (button) {
                            button.addClass(expand).removeClass(compress);
                        }
                        widget.removeClass("maximized");
                        widget.find(".widget-body").css("height", "auto");
                    } else {
                        if (button) {
                            button.addClass(compress).removeClass(expand);
                        }
                        widget.addClass("maximized");
                        if (widget) {
                            var windowHeight = $(window).height();
                            var headerHeight = widget.find(".widget-header").height();
                            widget.find(".widget-body").height(windowHeight - headerHeight);
                        }
                    }
                });
            }
        };
    });


//Collapse Widget
angular.module('IndividualSectorApp')
    .directive('widgetCollapse', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-minus"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var body = widget.find(".widget-body");
                    var button = el.find("i");
                    var down = "fa-plus";
                    var up = "fa-minus";
                    var slidedowninterval = 300;
                    var slideupinterval = 200;
                    if (widget.hasClass("collapsed")) {
                        if (button) {
                            button.addClass(up).removeClass(down);
                        }
                        widget.removeClass("collapsed");
                        body.slideUp(0, function () {
                            body.slideDown(slidedowninterval);
                        });
                    } else {
                        if (button) {
                            button.addClass(down)
                                .removeClass(up);
                        }
                        body.slideUp(slideupinterval, function () {
                            widget.addClass("collapsed");
                        });
                    }
                });
            }
        };
    });

//Expand Widget
angular.module('IndividualSectorApp')
    .directive('widgetExpand', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-plus"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var body = widget.find(".widget-body");
                    var button = el.find("i");
                    var down = "fa-plus";
                    var up = "fa-minus";
                    var slidedowninterval = 300;
                    var slideupinterval = 200;
                    if (widget.hasClass("collapsed")) {
                        if (button) {
                            button.addClass(up).removeClass(down);
                        }
                        widget.removeClass("collapsed");
                        body.slideUp(0, function () {
                            body.slideDown(slidedowninterval);
                        });
                    } else {
                        if (button) {
                            button.addClass(down)
                                .removeClass(up);
                        }
                        body.slideUp(slideupinterval, function () {
                            widget.addClass("collapsed");
                        });
                    }
                });
            }
        };
    });

//Dispose Widget
angular.module('IndividualSectorApp')
    .directive('widgetDispose', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-times"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    var widget = el.parents(".widget").eq(0);
                    var disposeinterval = 300;
                    widget.hide(disposeinterval, function () {
                        widget.remove();
                    });
                });
            }
        };
    });

//Config Widget
angular.module('IndividualSectorApp')
    .directive('widgetConfig', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-cog"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    //Do what you intend for configing widgets
                });
            }
        };
    });

//Config Widget
angular.module('IndividualSectorApp')
    .directive('widgetRefresh', function () {
        return {
            restrict: 'A',
            template: '<i class="fa fa-undo"></i>',
            link: function (scope, el, attr) {
                el.on('click', function () {
                    //Refresh widget content
                });
            }
        };
    });



function CommonFilter($scope, $http) {

    $("#ShowAllList").removeClass("inactiveLink");
    $("#ShowAllList").addClass("bg-themeprimary");


    $("#ShowAllList").click(function () {

        $("#popuplist").click();

    });
    $scope.popupclick = function () {

        $("#customerlistpopup").show();

        $scope.loadTableId();
        $scope.loadTableID2();

    };
    $scope.popupclick1 = function () {
        $("#Viewpopup").show();
    };




    $scope.filtersearch = [];
    $scope.filterorder = [];
    $scope.user2 = {};
    $scope.txtbx = {};
    $scope.AddinQ = function () {


        $scope.filtersearch.push($scope.user2);
        $scope.user2 = "";
    };

    $scope.deleteItem = function (index) {

        $scope.filtersearch.splice(index, 1);

    };



    $scope.AddorderinQ = function () {

        $scope.filterorder.push($scope.morder);
        $scope.morder = {};
    };

    $scope.deleteorderItem = function (index) {

        $scope.filterorder.splice(index, 1);

    };




    $scope.ViewQ = function () {

        $("#Viewpopup").show();
    }

    $scope.SaveQ = function (txtname) {

        $("#Viewpopup").hide();

        var listOptions =
         {
             orderBy: $scope.filterorder,
             startRowIndex: null,
             maximumRows: null,
             filters: $scope.filtersearch
         };

        var myview =
     {

         name: txtname,
         option: listOptions,

     };
        localStorage.setItem("myviewLS", myview);
    };















    $scope.Table = [];
    getDataSource();
    function getDataSource() {
        var dataid = 9;
        $http.get("/InventryMaster/Datatable?id=" + dataid)
.success(function (data) {
    $scope.Table = data;
});
    }



    //$scope.Table = [{ TableId: 1, TableName: "Customer", field: "CustomerID", OrderBy: "=" },
    //    { TableId: 1, TableName: "Customer", field: "CustomerName", OrderBy: "=" },
    //    { TableId: 1, TableName: "Customer", field: "AddressID", OrderBy: "=" },
    //    { TableId: 1, TableName: "Customer", field: "CustomerAccountID", OrderBy: "=" },
    //    { TableId: 1, TableName: "Customer", field: "CustomerGroupID", OrderBy: "=" },
    //    { TableId: 1, TableName: "Customer", field: "MiscChargeGroupID", OrderBy: "=" },
    //    { TableId: 1, TableName: "Customer", field: "ItemCustomerGroupID", OrderBy: "=" },
    //       { TableId: 1, TableName: "Customer", field: "Is Need FollowUp", OrderBy: "=" },
    //          { TableId: 2, TableName: "AddressTable", field: "CustomerAccountID", OrderBy: "=" },
    //            { TableId: 2, TableName: "AddressTable", field: "CustomerID", OrderBy: "=" },

    //];






    $scope.AutoTableDetails = [];
    $scope.loadTableId = function () {



        $scope.AutoTableDetails = [];
        $scope.Table.forEach(function addDates(row, index) {
            var t = {
                Id: row.TableName,
                Desc: row.TableName
            };
            //var key = row[keyname];

            if ($scope.AutoTableDetails.indexOf(t) === -1) {
                $scope.AutoTableDetails.push(t);
            }

        });


    };




    $scope.AutoFieldDetails = [];
    $scope.AutoFieldDetailsforOrder = [];

    $scope.Fieldname = function (id) {

        $scope.AutoFieldDetails = [];
        $scope.FilteredOrder = $scope.Table.filter(function (d) {
            return (d.TableName == id)
        });
        $scope.FilteredOrder.forEach(function addDates(row, index) {
            $scope.user2.Alias = row.Alias
        });
        $scope.FilteredOrder.forEach(function addDates(row, index) {
            $scope.FieldDrop = {
                Id: row.field,
                Desc: row.field
            };


            //$scope.FieldDrop.Id = row.TableId;
            //$scope.FieldDrop.Desc = row.field;
            if ($scope.AutoTableDetails.indexOf($scope.FieldDrop) === -1) {
                $scope.AutoFieldDetails.push($scope.FieldDrop);
            }
        });
    };


    $scope.Fieldnamefororder = function (id) {

        $scope.AutoFieldDetailsforOrder = [];

        $scope.FilteredOrder = $scope.Table.filter(function (d) {
            return (d.TableName == id)
        });



        $scope.FilteredOrder.forEach(function addDates(row, index) {
            $scope.FieldDrop = {
                Id: row.field,
                Desc: row.field
            };
            //$scope.FieldDrop.Id = row.TableId;
            //$scope.FieldDrop.Desc = row.field;
            if ($scope.AutoFieldDetailsforOrder.indexOf($scope.FieldDrop) === -1) {
                $scope.AutoFieldDetailsforOrder.push($scope.FieldDrop);
            }
        });
    };

    $scope.AutoTableDetailsforOrder = [];


    $scope.loadTableID2 = function () {
        $scope.AutoTableDetailsforOrder = [];
        $http.get("/InventryMaster/table")
.success(function (data) {

    $scope.TableIDDrop2 = data;
    $scope.TableIDDrop2.forEach(function addDates(row, index) {
        $scope.TableDrop = {
            Id: row,
            Desc: row
        };

        if ($scope.AutoTableDetailsforOrder.indexOf($scope.TableDrop) === -1) {
            $scope.AutoTableDetailsforOrder.push($scope.TableDrop);
        }
    });
});
    };

    $scope.AutoTablefieldDetailsforOrder = [];
    $scope.ChangeFieldnamefororder = function (id) {
        $scope.AutoTablefieldDetailsforOrder = [];
        $http.get("/InventryMaster/tablefield?id=" + id)
.success(function (data) {
    $scope.TablefieldDrop2 = data;
    $scope.TablefieldDrop2.forEach(function addDates(row, index) {
        $scope.TablefieldDrop = {
            Id: row,
            Desc: row
        };

        if ($scope.AutoTablefieldDetailsforOrder.indexOf($scope.TablefieldDrop) === -1) {
            $scope.AutoTablefieldDetailsforOrder.push($scope.TablefieldDrop);
        }
    });
});

    };




}



indsectorapp.run(function ($rootScope, $state) {
    $rootScope.$on('$stateChangeStart', function (event, next, nextParams, fromState) {
        $("[name='sidemenu_324']").parent().parent().addClass("active open");

        //if ('data' in next && 'authorizedRoles' in next.data) {
        //    var authorizedRoles = next.data.authorizedRoles;
        //    if (!AuthService.isAuthorized(authorizedRoles)) {
        //        event.preventDefault();
        //        $state.go($state.current, {}, {reload: true});
        //        $rootScope.$broadcast(AUTH_EVENTS.notAuthorized);
        //    }
        //}

        //if (!AuthService.isAuthenticated()) {
        //    if (next.name !== 'login') {
        //        event.preventDefault();
        //        $state.go('login');
        //    }
        //}
    });
});


//indsectorapp.run(function ($rootScope) {
//    $rootScope.langfilter = function (object, field, prefixEn, suffixAr) {

//        var lang = $rootScope.currentLang;

//        if (lang == "Ar") {
//            if (prefixEn == "2") {
//                field = field.slice(0, -2);
//            }
//            if (suffixAr == "1") {
//                if (object[field + 'AR']) {
//                    return object[field + 'AR'];
//                }
//                else {
//                    return object[field];
//                }
//            }
//            else {
//                if (object[field + 'Ar']) {
//                    return object[field + 'Ar'];
//                }
//                else {
//                    return object[field];
//                }
//            }
//        }
//        else {
//            return object[field];
//        }
//    };
//});


//indsectorapp.controller('ChangeLang', ['$scope', '$rootScope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache', '$translate', 'cssInjector',
//function ($scope, $rootScope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache, $translate, cssInjector) {

//    $scope.currentLanguage = $translate.preferredLanguage();
//    $rootScope.currentLang = $translate.preferredLanguage();

//    if ($scope.currentLanguage == "Ar") {
//        cssInjector.add("/assets/css/beyond-rtl.css");
//        cssInjector.add("/assets/css/bootstrap-rtl.css");
//     //   cssInjector.add("/assets/css/CustomerPortal/rtl-beyond.css");
//    }
//    else {
//        cssInjector.removeAll();

//    }

//    $scope.changeLanguage = function () {
//        //$scope.currentLanguage = key;
//        $translate.use($scope.currentLanguage);
//        $rootScope.currentLang = $scope.currentLanguage;
//        window.location.reload();

//        if ($scope.currentLanguage == "Ar") {
//            cssInjector.add("/assets/css/beyond-rtl.css");
//            cssInjector.add("/assets/css/bootstrap-rtl.css");
//            //cssInjector.add("/assets/css/CustomerPortal/rtl-beyond.css");
//        }
//        else {
//            cssInjector.removeAll();
//        }
//        //
//    };

//    $scope.changeLang = function (lang) {
//        $scope.currentLanguage = lang;
//        $translate.use($scope.currentLanguage);
//        window.location.reload();

//        $rootScope.currentLang = $scope.currentLanguage;

//        if ($scope.currentLanguage == "Ar") {
//            cssInjector.add("/assets/css/beyond-rtl.css");
//            cssInjector.add("/assets/css/bootstrap-rtl.css");
//         //   cssInjector.add("/assets/css/CustomerPortal/rtl-beyond.css");
//        }
//        else {
//            cssInjector.removeAll();
//        }
//    };

  

//}]);


