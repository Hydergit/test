﻿indsectorapp.service('ApiCall', ['$http', function ($http) {
    var result;

    // This is used for calling get methods from web api
    this.get = function (url, obj) {

        var response = $http({
            method: "get",
            url: url,
            params:obj
            //data: data,
        });

        //result = $http.get(url, obj).success(function (data, status) {
        //    result = (data);
        //}).error(function () {
        //    alert("Something went wrong");
        //});
        return response;
    };

    // This is used for calling post methods from web api with passing some data to the web api controller
    this.post = function (url, obj) {
      //  $http.defaults.headers.common['Authorization'] = "Basic sk_test_GNhhspRZe8jQxZkn9bGCTUnHo5QTvxJJimnvk4k6:123";
        var response = $http({
            method: "post",
            url: url,
         //   headers: { 'WWW-Authenticate': 'Basic sk_test_GNhhspRZe8jQxZkn9bGCTUnHo5QTvxJJimnvk4k6:123' },
            params: obj
            //data: data,
        });
        //result = $http.post(url, obj).success(function (data, status) {
        //    result = (data);
        //}).error(function () {
        //    alert("Something went wrong");
        //});
        return response;
    };

}]);

