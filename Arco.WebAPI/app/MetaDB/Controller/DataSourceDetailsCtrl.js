﻿
app.controller('DataSourceDetailsCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
 function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {
 
     $scope.TableNew = null;
     $scope.Dtable = [];
     $scope.CreateTable = function () {
       

         $scope.Dtable = {
             "DataSourceID": null,
             "TableName":null,
             "Alias":null
         }
         $("#UpdateTablePopup").show();
         $scope.TableNew = true;
     };


     


     $scope.ManageTable = function () {

         $http.post("/MetaDb/DataSource/DeleteDataSourceById", { id: checkIds })
             .success(function (data) {

                 alert(data);
                 $scope.gridrefresh();


             });
         $("#UpdateTablePopup").show();
         $scope.TableNew = true;
     };


     
 

     $scope.TableNameDrop = [];
     TableNameDrop();

     function TableNameDrop() {
         debugger;


         $http.post("/MetaDb/DataSource/TableNameDrop", { DataSourceId: $("#DataSourceId").val() })
.success(function (data) {
    $scope.TableNameDrop = data;

});
     }




     var columnDefs = [
           {
               field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
               headerCellTemplate: '<div></div>',
               cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetDataTable(row.entity.RecId)" ></span></div></div>'
           },

            { field: 'DataSourceID', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'TableName', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'Alias', name: 'Alias', headerCellClass: $scope.highlightFilteredHeader },
                   
     ];
        
         function gridview() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'EmployeeOnBoard.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi = gridApi;

                     $scope.gridApi.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefs
             };
         }
         gridview();
         griddata();


         function griddata() {
             debugger;


             $http.post("/MetaDb/DataSource/GetDataSourceTable", { id: $("#DataSourceId").val() })
       .success(function (data) {
           $scope.gridOptions.data = data;





       });


         }


         $scope.DeleteTable = function () {


             checkIds = $(".ckb").filter(".ui-grid-row-selected").map(function () {
                 return this.id;
             });

             //checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
             //    return gridRow.RecId;
             //});
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/DataSource/DeleteDataSourceTableById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };




         $scope.GetDataTable = function (id) {
             
             $scope.TableNew = false;
             $("#UpdateTablePopup").show();
             $http.post("/MetaDb/DataSource/GetDataSourceTableById", { Id: id })
        .success(function (datam) {
            $scope.Dtable = datam;

        });
         };



         $scope.SaveDTable = function () {
             debugger;
             

             if ($scope.TableNew == true) {

                

                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/CreateTable",
                     params: {
                         type: JSON.stringify($scope.Dtable),
                         DataSourceId: $("#DataSourceId").val()
                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateTablePopup").hide();
                 });
             }
             else {
                
                
                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/UpdateTable",
                     params: {


                         type: JSON.stringify($scope.Dtable),
                         DataSourceId: $("#DataSourceId").val()

                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateTablePopup").hide();
                 });
                 // return $http.post('/saveUser', data);
             }

         };















         

         $scope.JoinNew = null;

         $scope.CreateJoins = function () {

             ParentTableJoinFieldDrop();
             ChildTableFieldDrop();
             $scope.DJoin = {
                 "ParentTableAlias": null,
                 "ParentTableJoinField": null,
                 "ChildTableAlias": null,
                 "ChildTableField": null
             }
             $("#UpdateJoinPopup").show();
             $scope.JoinNew = true;
         };




         var columnDefsjoin = [
             {
                 field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
                 headerCellTemplate: '<div></div>',
                 cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetDataJoin(row.entity.RecId)" ></span></div></div>'
             },

              { field: 'ParentTableAlias', headerCellClass: $scope.highlightFilteredHeader },
                      { field: 'ParentTableJoinField', headerCellClass: $scope.highlightFilteredHeader },
                      // pre-populated search field
                      { field: 'ChildTableAlias', name: 'ChildTableAlias', headerCellClass: $scope.highlightFilteredHeader },

                       { field: 'ChildTableField', name: 'ChildTableField', headerCellClass: $scope.highlightFilteredHeader },

         ];

         function gridviewjoin() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb2 ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions2 = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'DataSourceJoins.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi2 = gridApi;

                     $scope.gridApi2.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi2.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi2.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi2.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefsjoin
             };
         }

         gridviewjoin();


         griddatajoin();


         function griddatajoin() {
             debugger;


             $http.post("/MetaDb/DataSource/GetDataSourceJoin", { id: $("#DataSourceId").val() })
       .success(function (data) {
           $scope.gridOptions2.data = data;





       });


         }


         $scope.Deletejoins = function () {

             debugger;
            
             checkIds = $(".ckb2").filter(".ui-grid-row-selected").map(function () {
                 return this.id;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/DataSource/DeleteDataSourceTablejoinById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };




         $scope.GetDataJoin = function (id) {


             ParentTableJoinFieldDrop();
             ChildTableFieldDrop();
             $scope.TableNew = false;
             $("#UpdateJoinPopup").show();
             $http.post("/MetaDb/DataSource/GetDataSourceTablejoinById", { Id: id })
        .success(function (datam) {
            $scope.DJoin = datam;

        });
         };



         $scope.SaveDJoin = function () {
             debugger;


             if ($scope.JoinNew == true) {



                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/CreateJoin",
                     params: {
                         type: JSON.stringify($scope.DJoin),
                         DataSourceId: $("#DataSourceId").val()
                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateJoinPopup").hide();
                 });
             }
             else {


                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/UpdateJoin",
                     params: {


                         type: JSON.stringify($scope.DJoin),
                         DataSourceId: $("#DataSourceId").val()

                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateJoinPopup").hide();
                 });
                 // return $http.post('/saveUser', data);
             }

         };




         $scope.ParentTableAliasDrop = [];
         ParentTableAliasDrop();

         function ParentTableAliasDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/ParentTableAliasDrop", { DataSourceId: $("#DataSourceId").val() })
    .success(function (data) {
        $scope.ParentTableAliasDrop = data;

    });
         }



         $scope.ParentTableJoinFieldDrop = [];
      

         function ParentTableJoinFieldDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val() })
    .success(function (data) {
        $scope.ParentTableJoinFieldDrop = data;

    });
         }



         $scope.ChildTableFieldDrop = [];
        

         function ChildTableFieldDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/ChildTableFieldDrop", { DataSourceId: $("#DataSourceId").val() })
    .success(function (data) {
        $scope.ChildTableFieldDrop = data;

    });
         }


         $scope.ParentAliasChange =function (id) {
             debugger;


             $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val(), Alias: id })
    .success(function (data) {
        $scope.ParentTableJoinFieldDrop = data;

    });
         }

         $scope.ChildAliasChange = function (id) {
             debugger;


             $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val(), Alias: id })
    .success(function (data) {
        $scope.ChildTableFieldDrop = data;

    });
         }




        










         $scope.WhereNew = null;

         $scope.CreateWhere = function () {
             ParentTableAliasDrop();
           
             $scope.DWhere = {
                 "Alias": null,
                 "Field": null,
                 "Value": null,
                 "Status": null
             }
             $("#UpdateWherePopup").show();
             $scope.WhereNew = true;
         };

         var columnDefsWhere = [
             {
                 field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
                 headerCellTemplate: '<div></div>',
                 cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetDataWhere(row.entity.RecId)" ></span></div></div>'
             },

              { field: 'Alias', headerCellClass: $scope.highlightFilteredHeader },
                      { field: 'Field', headerCellClass: $scope.highlightFilteredHeader },
                      // pre-populated search field
                      { field: 'Operator', name: 'Operator', headerCellClass: $scope.highlightFilteredHeader },

                       { field: 'Value', name: 'Value', headerCellClass: $scope.highlightFilteredHeader },

                        { field: 'Status', name: 'Status', headerCellClass: $scope.highlightFilteredHeader },

         ];

         function gridvieWhere() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb3 ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions3 = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'DataSourceJoins.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi3 = gridApi;

                     $scope.gridApi3.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi3.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi3.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi3.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefsWhere
             };
         }

         gridvieWhere();
         griddatawhere();


        


         function griddatawhere() {
             debugger;


             $http.post("/MetaDb/DataSource/GetDataSourceWhere", { id: $("#DataSourceId").val() })
       .success(function (data) {
           $scope.gridOptions3.data = data;





       });


         }


         $scope.DeleteWhere = function () {


             checkIds = $(".ckb3").filter(".ui-grid-row-selected").map(function () {
                 return this.id;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/DataSource/DeleteDataSourceTableWhereById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };




         $scope.GetDataWhere = function (id) {
             ParentTableAliasDrop();

         
             $scope.WhereNew = false;
             $("#UpdateWherePopup").show();
             $http.post("/MetaDb/DataSource/GetDataSourceTableWhereById", { Id: id })
        .success(function (datam) {
            $scope.DWhere = datam;

        });
         };



         $scope.SaveDWhere = function () {
             debugger;


             if ($scope.WhereNew == true) {



                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/CreateWhere",
                     params: {
                         type: JSON.stringify($scope.DWhere),
                         DataSourceId: $("#DataSourceId").val()
                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateWherePopup").hide();
                 });
             }
             else {


                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/UpdateWhere",
                     params: {


                         type: JSON.stringify($scope.DWhere),
                         DataSourceId: $("#DataSourceId").val()

                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateWherePopup").hide();
                 });
                 // return $http.post('/saveUser', data);
             }

         };


         
         $scope.OperatorDrop = [];

         OperatorDrop();
         function OperatorDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/OperatorDrop")
    .success(function (data) {
        $scope.OperatorDrop = data;

    });
         }



         $scope.StatusDrop = [];


         function StatusDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/StatusDrop")
    .success(function (data) {
        $scope.StatusDrop = data;

    });
         }


         $scope.WhereAliasChange = function (id) {
             debugger;


             $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val(), Alias: id })
    .success(function (data) {
        $scope.WhereFieldDrop = data;

    });
         }
























         $scope.OrderNew = null;

         $scope.CreateOrder = function () {
             ParentTableAliasDrop();
            
             $scope.DOrder = {
                 "Alias": null,
                 "Field": null,
                 "Direction": null,
                 "SortOrder": null,
                 "Status": null
             }
             $("#UpdateOrderPopup").show();
             $scope.OrderNew = true;
         };

         var columnDefsOrder = [
             {
                 field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
                 headerCellTemplate: '<div></div>',
                 cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetDataOrder(row.entity.RecId)" ></span></div></div>'
             },

              { field: 'Alias', headerCellClass: $scope.highlightFilteredHeader },
                      { field: 'Field', headerCellClass: $scope.highlightFilteredHeader },
                      // pre-populated search field
                      { field: 'Direction', name: 'Direction', headerCellClass: $scope.highlightFilteredHeader },

                       { field: 'SortOrder', name: 'SortOrder', headerCellClass: $scope.highlightFilteredHeader },

                        { field: 'Status', name: 'Status', headerCellClass: $scope.highlightFilteredHeader },

         ];

         function gridviewOrder() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb3 ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions4 = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'DataSourceJoins.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi4 = gridApi;

                     $scope.gridApi4.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi4.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi4.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi4.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefsOrder
             };
         }
         gridviewOrder();
         griddataOrder();





         function griddataOrder() {
             debugger;


             $http.post("/MetaDb/DataSource/GetDataSourceOrder", { id: $("#DataSourceId").val() })
       .success(function (data) {
           $scope.gridOptions4.data = data;





       });


         }


         $scope.DeleteOrder = function () {

             checkIds = $(".ckb4").filter(".ui-grid-row-selected").map(function () {
                 return this.id;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/DataSource/DeleteDataSourceTableOrderById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };




         $scope.GetDataOrder = function (id) {
            
             ParentTableAliasDrop();
           

             $scope.OrderNew = false;
             $("#UpdateOrderPopup").show();
             $http.post("/MetaDb/DataSource/GetDataSourceTableOrderById", { Id: id })
        .success(function (datam) {
            $scope.DOrder = datam;

        });
         };



         $scope.SaveDOrder = function () {
             debugger;


             if ($scope.OrderNew == true) {



                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/CreateOrder",
                     params: {
                         type: JSON.stringify($scope.DOrder),
                         DataSourceId: $("#DataSourceId").val()
                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateOrderPopup").hide();
                 });
             }
             else {


                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/UpdateOrder",
                     params: {


                         type: JSON.stringify($scope.DOrder),
                         DataSourceId: $("#DataSourceId").val()

                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateOrderPopup").hide();
                 });
                 // return $http.post('/saveUser', data);
             }

         };


         
         $scope.DirectionDrop = [];
         DirectionDrop();
         StatusDrop();
         function DirectionDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/DirectionDrop")
    .success(function (data) {
        $scope.DirectionDrop = data;

    });
         }






























         $scope.FieldNew = null;

         $scope.CreateField = function () {
             ParentTableAliasDrop();

             $scope.DField = {
                 "Alias": null,
                 "Field": null,
                 "DataType": null,
                 "CDT": null,
                 "IsMandatory": null,
                 "LabelId": null,
                 "HelpTextId": null,
                 "AggregateFunction": null,
             }
             $("#UpdateFieldPopup").show();
             $scope.FieldNew = true;
         };
         var columnDefsField = [
             {
                 field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
                 headerCellTemplate: '<div></div>',
                 cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetDataField(row.entity.RecId)" ></span></div></div>'
             },

              { field: 'Alias', headerCellClass: $scope.highlightFilteredHeader },
                      { field: 'Field', headerCellClass: $scope.highlightFilteredHeader },
                      { field: 'DataType', headerCellClass: $scope.highlightFilteredHeader },
                       { field: 'CDT', headerCellClass: $scope.highlightFilteredHeader },
                        { field: 'IsMandatory', name: 'IsMandatory', headerCellClass: $scope.highlightFilteredHeader },
  { field: 'LabelId', headerCellClass: $scope.highlightFilteredHeader },
  { field: 'HelpTextId', headerCellClass: $scope.highlightFilteredHeader },
  { field: 'AggregateFunction', headerCellClass: $scope.highlightFilteredHeader },
 

         ];



         function gridviewField() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb5 ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions5 = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'DataSourceJoins.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi5 = gridApi;

                     $scope.gridApi5.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi5.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi5.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi5.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefsField
             };
         }
         gridviewField();
         griddataField();





         function griddataField() {
             debugger;


             $http.post("/MetaDb/DataSource/GetDataSourceField", { id: $("#DataSourceId").val() })
       .success(function (data) {
           $scope.gridOptions5.data = data;





       });


         }


         $scope.DeleteField = function () {


             checkIds = $(".ckb5").filter(".ui-grid-row-selected").map(function () {
                 return this.id;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/DataSource/DeleteDataSourceTableFieldById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };




         $scope.GetDataField = function (id) {

             ParentTableAliasDrop();


             $scope.FieldNew = false;
             $("#UpdateFieldPopup").show();
             $http.post("/MetaDb/DataSource/GetDataSourceTableFieldById", { Id: id })
        .success(function (datam) {
            $scope.DField = datam;

        });
         };



         $scope.SaveDField = function () {
             debugger;


             if ($scope.FieldNew == true) {



                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/CreateField",
                     params: {
                         type: JSON.stringify($scope.DField),
                         DataSourceId: $("#DataSourceId").val()
                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateFieldPopup").hide();
                 });
             }
             else {


                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/UpdateField",
                     params: {


                         type: JSON.stringify($scope.DField),
                         DataSourceId: $("#DataSourceId").val()

                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert(emp.data);
                     $scope.gridrefresh();
                     $("#UpdateFieldPopup").hide();
                 });
                 // return $http.post('/saveUser', data);
             }

         };

         

         $scope.DataTypeDrop = [];
         DataTypeDrop();

         function DataTypeDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/DataTypeDrop")
    .success(function (data) {
        $scope.DataTypeDrop = data;

    });
         }


         $scope.CDTDrop = [];
         CDTDrop();

         function CDTDrop() {
             debugger;


             $http.post("/MetaDb/DataSource/CDTDrop")
    .success(function (data) {
        $scope.CDTDrop = data;

    });
         }

         



     







       

       
      

             
         $scope.toggleFiltering = function () {
        
             $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
             $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
         };
         $scope.expandAll = function () {
             $scope.gridApi.treeBase.expandAllRows();
         };

         $scope.toggleRow = function (rowNum) {
             $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
         };

         $scope.changeGrouping = function () {
             $scope.gridApi.grouping.clearGrouping();

         };
         $scope.gridrefresh = function () {
             $scope.gridOptions.columnDefs = columnDefs;
             griddata();
             griddatajoin();
             griddatawhere();

             griddataOrder();
             griddataField();
         }
    
     }]);

