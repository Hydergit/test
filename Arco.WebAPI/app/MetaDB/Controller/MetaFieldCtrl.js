﻿
app.controller('MetaFieldCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
 function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {
 

     var columnDefs = [
          //{
          //    field: 'Check', pinnedLeft: true, name: 'Check', width: '35',
          //    cellTemplate: '<input id="{{row.entity.RecId}}" type="checkbox" class="ckb" />', headerCellTemplate: ' <input type="checkbox" id="SelectAll" />'
          //},
             //{
             //    field: 'Action', pinnedLeft: true, name: '', width: '50', enableFiltering: false, enableSorting: false,
             //    headerCellTemplate: '<div></div>',
             //    cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="glyphicon  glyphicon-pencil  cmtbtn themeprimary" OnClick="Edit(this)" style=" cursor:pointer;"></span></div></div>'

             //},
              {
                  field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
                  headerCellTemplate: '<div></div>',
                  cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetRequestById(row.entity.RecId)" ></span></div></div>'
              },

                    { field: 'Field1', name: 'FieldName', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'DataType', name: 'DataType', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'CDTId', name: 'CDTName', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'IsUnique', name: 'IsUnique', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'IsVisible', name: 'IsVisible', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'IsReadOnly', name: 'IsReadOnly', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'IsMandatory', name: 'IsMandatory', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'LabelId', name: 'LabelId', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'HelpTextId', name: 'HelpTextId', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'DefaultValue', name: 'DefaultValue', headerCellClass: $scope.highlightFilteredHeader },
                     { field: 'EnumType', name: 'EnumTypeName', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'Lenght', name: 'Lenght', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'Fraction', name: 'Fraction', headerCellClass: $scope.highlightFilteredHeader },
                   

     ];
        
         function gridview() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'EmployeeOnBoard.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi = gridApi;

                     $scope.gridApi.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefs
             };
         }
         gridview();
         griddata();
       
       

       


         function griddata() {
             debugger;
           

             $http.post("/MetaDb/MetaTable/GetMetaFieldList", {id:$("#TableId").val()})
       .success(function (data) {
           $scope.gridOptions.data = data;

         

        
         

       });
           

         }

 
         $scope.toggleFiltering = function () {
        
             $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
             $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
         };
         $scope.expandAll = function () {
             $scope.gridApi.treeBase.expandAllRows();
         };

         $scope.toggleRow = function (rowNum) {
             $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
         };

         $scope.changeGrouping = function () {
             $scope.gridApi.grouping.clearGrouping();

         };
         $scope.gridrefresh = function () {
             $scope.gridOptions.columnDefs = columnDefs;
             griddata();
         }




         
         $scope.Field;
         $scope.New = false;


         $scope.CDTIddrop = [];
         CDTIddrop();

         function CDTIddrop() {
             debugger;


                 $http.post("/MetaDb/MetaTable/CDTIddrop")
                .success(function (data) {
                  $scope.CDTIddrop = data;

                     });
         }


         $scope.EnumTypeDrop = [];
         EnumTypeDrop();

         function EnumTypeDrop() {
             debugger;


             $http.post("/MetaDb/MetaTable/EnumTypeDrop")
   .success(function (data1) {
       debugger;
       $scope.EnumTypeDrop = data1;

   });


         }

         $scope.gridnew = function () {
             $scope.New = true;


             $scope.Field = {
                 "TableId": null,
                 "Field1": null,
                 "DataType": null,
                 "CDTId": null,
                 "IsUnique": null,
                 "IsVisible": null,
                 "IsReadOnly": null,
                 "IsMandatory": null,
                 "LabelId": null,
                 "HelpTextId": null,
                 "DefaultValue": null,
                 "EnumType": null,
                 "Lenght": null,
                 "Fraction": null,
                 "CreatedDatetime": null,
                 "CreatedBy": null,
                 "ModifiedDatetime": null,
                 "ModifiedBy": null,
                
             }
             
         }
         $scope.MetaTableInsert = function () {
            
             $scope.Save();
         }

         $scope.GetRequestById = function (id) {

             $scope.New = false;
             $("#popup").show();
             $http.post("/MetaDb/MetaTable/MetaFieldEdit", { Id: id })
        .success(function (datam) {
            $scope.Field = datam;

        });
         };
         $scope.GetRequestBydel = function (id) {

            
             $http.post("/MetaDb/MetaTable/MetaFielddelete", { Id: id })
        .success(function (datam) {
            $scope.Field = datam;

        });
         };


         $scope.DeleteClick = function () {


             checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                 return gridRow.RecId;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/MetaTable/DeleteFieldById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };


         $scope.Edit = function (id) {
             $http.get("/MetaDb/MetaTable/MetaFieldEdit/?obj=" + JSON.stringify($scope.Field))
                       .success(function (data) {
                        
                           $("#popup").hide();
                       });
           

          
             $scope.New = false;

             $("#popup").show();
         }


         $scope.save = function () {
             debugger;

             if ($scope.Field != null) {


                 if ($scope.New == true) {

                     $http.get("/MetaDb/MetaTable/MetaFieldCreate/?obj=" + JSON.stringify($scope.Field) + "&TableId=" + $("#TableId").val())
                        .success(function (data) {
                            $scope.gridrefresh();
                            
                                     $("#popup").hide();
                                                });

                 }
                 else {

                     $http.get("/MetaDb/MetaTable/MetaFieldUpdate/?obj=" + JSON.stringify($scope.Field))
                         .success(function (data) {
                             
                             $scope.gridrefresh();
                            
                             $("#popup").hide();
                         });
                   

                 }

                 $("#popup").hide();

             }
             else {
                 $scope.errmsg = "Please Fill The Form";
             }

         }
    
     }]);

