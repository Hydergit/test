﻿
app.controller('RepositoryEditCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
    function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {







        //$scope.New = false;






        $scope.gridnew = function () {
            $scope.Repository = {
                Name: null, Description: null,
            };
            $scope.New = true;

        }
        $scope.MetaTableInsert = function () {

            $scope.Save();
        }


        $scope.GetTableById = function (id) {


            $("#rephd").val(id);
            $http.post("/MetaDb/Repository/RepositoryTablepop", { Id: id })
                .success(function (datam) {
                    $scope.ReposTables = datam;
                    $("#tablepopup").show();

                });
        };



        $scope.GetRequestById = function (id) {

            $scope.New = false;
            $("#popup").show();
            $http.post("/MetaDb/Repository/RepositoryEdit", { Id: id })
                .success(function (datam) {
                    $scope.Repository = datam;

                });
        };
        $scope.GetRequestBydel = function (id) {


            $http.post("/MetaDb/Repository/Repositorydelete", { Id: id })
                .success(function (datam) {
                    $scope.Repository = datam;

                });
        };


        $scope.DeleteClick = function () {


            checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                return gridRow.RecId;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/Repository/Repositorydelete", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };


        $scope.Edit = function (id) {
            $http.get("/MetaDb/Repository/RepositoryEdit/?obj=" + JSON.stringify($scope.Repository))
                .success(function (data) {

                    $("#popup").hide();
                });



            $scope.New = false;

            $("#popup").show();
        }


        $scope.save = function () {
            debugger;

            if ($scope.Repository != null) {


                if ($scope.New == true) {

                    $http.get("/MetaDb/Repository/RepositoryCreate/?obj=" + JSON.stringify($scope.Repository))
                        .success(function (data) {
                            $scope.gridrefresh();

                            $("#popup").hide();
                        });

                }
                else {

                    $http.get("/MetaDb/Repository/RepositoryUpdate/?obj=" + JSON.stringify($scope.Repository))
                        .success(function (data) {

                            $scope.gridrefresh();

                            $("#popup").hide();
                        });


                }

                $("#popup").hide();

            }
            else {
                $scope.errmsg = "Please Fill The Form";
            }

        }





        $scope.Delete = function (id) {

            if (confirm("Are You Sure to Delete")) {

                var getdata = CustomerDetailSvc.CustomerDetailsDelete(id);
                getdata.then(function (d) {
                    toastr.success('Customer Details', 'Deleted ');
                    GetAllCustomerDetail();
                },
                    function (error) {
                        toastr.error('Error While Deleting', 'Error');
                    });
            }
        }


        $scope.AssignTable = function () {
            debugger;
            checkIds = $(".ckbRoll").filter(":checked").map(function () {
                return this.id;
            });
            var repoid = $("#rephd").val();

            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Assign?")) {
                    $http.post("/MetaDb/Repository/Tableassign", { id: checkIds.toArray(), repoid: repoid })
                        .success(function (data) {

                            $scope.GetTableById(repoid);



                        });
                }
            }
            else {
                alert("Select Any")
            }

        }


        $scope.DeleteTable = function () {
            checkIds = $(".ckbRolls").filter(":checked").map(function () {
                return this.id;
            });
            var repoid = $("#rephd").val();


            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Remove?")) {
                    $http.post("/MetaDb/Repository/DeleteTable", { id: checkIds.toArray(), repoid: repoid })
                        .success(function (data) {

                            $scope.GetTableById(repoid);


                        });
                }
            }
            else {
                alert("Select Any")
            }

        }



    }]);

