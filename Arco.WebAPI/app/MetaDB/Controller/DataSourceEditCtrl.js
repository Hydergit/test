﻿



app.controller('DataSourceEditCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
    function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {

        //   $scope.New = null;

        $scope.CreateClick = function () {


            $scope.src = {
                "Name": null,
                "RepoId": null
            }



            $("#UpdateType").show();
            $scope.New = true;

        };



        $scope.DeleteClick = function () {


            checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                return gridRow.RecId;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/DataSource/DeleteDataSourceById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };




        $scope.GetRequestById = function (id) {

            $scope.New = false;
            $("#UpdateType").show();
            $http.post("/MetaDb/DataSource/GetDataSourceById", { Id: id })
                .success(function (datam) {
                    $scope.src = datam;

                });
        };



        $scope.SaveSource = function () {
            debugger;


            if ($scope.New == true) {



                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/CreateSource",
                    params: {
                        type: JSON.stringify($scope.src)
                    }
                });
                response.then(function (emp) {
                    debugger;
                    if (emp.data.status) {
                        alert(emp.data.msg);
                        $scope.gridrefresh();
                        $("#UpdateType").hide();
                    }
                    else {
                        alert(emp.data.msg);
                        $scope.gridrefresh();
                        $("#UpdateType").hide();
                    }
                });
            }

            // return $http.post('/saveUser', data);

            else {


                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/UpdateSource",
                    params: {


                        type: JSON.stringify($scope.src)

                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert("Sucess");
                    $scope.gridrefresh();
                    $("#UpdateType").hide();
                });
                // return $http.post('/saveUser', data);
            }

        };







        $scope.RepoIdDrop = [];
        RepoIdDrop();
        function RepoIdDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/RepoIdDrop")
                .success(function (data) {
                    $scope.RepoIdDrop = data;

                });


        }




    }]);

