﻿
app.controller('RepositoryCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
 function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {
 

     var columnDefs = [
          //{
          //    field: 'Check', pinnedLeft: true, name: 'Check', width: '35',
          //    cellTemplate: '<input id="{{row.entity.RecId}}" type="checkbox" class="ckb" />', headerCellTemplate: ' <input type="checkbox" id="SelectAll" />'
          //},
             //{
             //    field: 'Action', pinnedLeft: true, name: '', width: '50', enableFiltering: false, enableSorting: false,
             //    headerCellTemplate: '<div></div>',
             //    cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="glyphicon  glyphicon-pencil  cmtbtn themeprimary" OnClick="Edit(this)" style=" cursor:pointer;"></span></div></div>'

             //},
              {
                  field: 'Action', pinnedLeft: true, name: '', width: '70', enableFiltering: false, enableSorting: false,
                  headerCellTemplate: '<div></div>',
                  cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetRequestById(row.entity.RecId)" ></span>|<span id="{{row.entity.RecId}}" class="glyphicon glyphicon-plus" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetTableById(row.entity.RepoId)"></span></div></div>'
              },
               { field: 'RepoId', name: 'Id', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'Name', name: 'Name', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'Description', name: 'Description', headerCellClass: $scope.highlightFilteredHeader },
                   

     ];
        
         function gridview() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'Repository.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi = gridApi;

                     $scope.gridApi.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefs
             };
         }
         gridview();
         griddata();
       
       

       


         function griddata() {
             debugger;
           

             $http.post("/MetaDb/Repository/GetRepositoryList")
       .success(function (data) {
           $scope.gridOptions.data = data;

         

        
         

       });
           

         }

 
         $scope.toggleFiltering = function () {
        
             $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
             $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
         };
         $scope.expandAll = function () {
             $scope.gridApi.treeBase.expandAllRows();
         };

         $scope.toggleRow = function (rowNum) {
             $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
         };

         $scope.changeGrouping = function () {
             $scope.gridApi.grouping.clearGrouping();

         };
         $scope.gridrefresh = function () {
             $scope.gridOptions.columnDefs = columnDefs;
             griddata();
         }




         
         $scope.Repository;
         $scope.New = false;






         $scope.gridnew = function () {
             $scope.Repository = {
                 Name: null, Description: null,
             };
             $scope.New = true;
             
         }
         $scope.MetaTableInsert = function () {
            
             $scope.Save();
         }


         $scope.GetTableById = function (id) {

             
             $("#rephd").val(id);
             $http.post("/MetaDb/Repository/RepositoryTablepop", { Id: id })
        .success(function (datam) {
            $scope.ReposTables = datam;
            $("#tablepopup").show();

        });
         };



         $scope.GetRequestById = function (id) {

             $scope.New = false;
             $("#popup").show();
             $http.post("/MetaDb/Repository/RepositoryEdit", { Id: id })
        .success(function (datam) {
            $scope.Repository = datam;

        });
         };
         $scope.GetRequestBydel = function (id) {

            
             $http.post("/MetaDb/Repository/Repositorydelete", { Id: id })
        .success(function (datam) {
            $scope.Repository = datam;

        });
         };


         $scope.DeleteClick = function () {


             checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                 return gridRow.RecId;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/Repository/Repositorydelete", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };


         $scope.Edit = function (id) {
             $http.get("/MetaDb/Repository/RepositoryEdit/?obj=" + JSON.stringify($scope.Repository))
                       .success(function (data) {
                        
                           $("#popup").hide();
                       });
           

          
             $scope.New = false;

             $("#popup").show();
         }


         $scope.save = function () {
             debugger;

             if ($scope.Repository != null) {


                 if ($scope.New == true) {

                     $http.get("/MetaDb/Repository/RepositoryCreate/?obj=" + JSON.stringify($scope.Repository))
                        .success(function (data) {
                            $scope.gridrefresh();
                            
                                     $("#popup").hide();
                                                });

                 }
                 else {

                     $http.get("/MetaDb/Repository/RepositoryUpdate/?obj=" + JSON.stringify($scope.Repository))
                         .success(function (data) {
                             
                             $scope.gridrefresh();
                            
                             $("#popup").hide();
                         });
                   

                 }

                 $("#popup").hide();

             }
             else {
                 $scope.errmsg = "Please Fill The Form";
             }

         }





         $scope.Delete = function (id) {

             if (confirm("Are You Sure to Delete")) {

                 var getdata = CustomerDetailSvc.CustomerDetailsDelete(id);
                 getdata.then(function (d) {
                     toastr.success('Customer Details', 'Deleted ');
                     GetAllCustomerDetail();
                 },
                 function (error) {
                     toastr.error('Error While Deleting', 'Error');
                 });
             }
         }


         $scope.AssignTable = function () {
             debugger;
             checkIds = $(".ckbRoll").filter(":checked").map(function () {
                 return this.id;
             });
             var repoid = $("#rephd").val();


             if (confirm("Are You Sure to Assign?")) {
                 $http.post("/MetaDb/Repository/Tableassign", { id: checkIds.toArray(), repoid: repoid })
           .success(function (data) {

               $scope.GetTableById(repoid);
             


           });
             }

         }


         $scope.DeleteTable = function () {
             checkIds = $(".ckbRolls").filter(":checked").map(function () {
                 return this.id;
             });
             var repoid = $("#rephd").val();
             if (confirm("Are You Sure to Remove?")) {
                 $http.post("/MetaDb/Repository/DeleteTable", { id: checkIds.toArray(), repoid: repoid })
           .success(function (data) {

               $scope.GetTableById(repoid);


           });
             }

         }


    
     }]);

