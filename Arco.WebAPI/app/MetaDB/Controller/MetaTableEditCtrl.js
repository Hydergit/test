﻿
app.controller('MetaTableEditCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
    function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {






        $scope.Table;


        $scope.gridnew = function () {
            $scope.New = true;

        }
        $scope.MetaTableInsert = function () {

            $scope.Save();
        }

        $scope.GetRequestById = function (id) {

            $scope.New = false;
            $("#popup").show();
            $http.post("/MetaDb/MetaTable/MetaTableEdit", { Id: id })
                .success(function (datam) {
                    $scope.Table = datam;

                });
        };
        $scope.GetRequestBydel = function (id) {


            $http.post("/MetaDb/MetaTable/MetaTabledelete", { Id: id })
                .success(function (datam) {
                    $scope.Table = datam;

                });
        };


        $scope.DeleteClick = function () {


            checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                return gridRow.RecId;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/MetaTable/DeletetableById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };


        $scope.Edit = function (id) {
            $http.get("/MetaDb/MetaTable/MetaTableEdit/?obj=" + JSON.stringify($scope.Table))
                .success(function (data) {

                    $("#popup").hide();
                });



            $scope.New = false;

            $("#popup").show();
        }


        $scope.save = function () {
            debugger;

            if ($scope.Table != null) {


                if ($scope.New == true) {

                    $http.get("/MetaDb/MetaTable/MetaTableCreate/?obj=" + JSON.stringify($scope.Table))
                        .success(function (data) {
                            $scope.gridrefresh();

                            $("#popup").hide();
                        });

                }
                else {

                    $http.get("/MetaDb/MetaTable/MetaTableUpdate/?obj=" + JSON.stringify($scope.Table))
                        .success(function (data) {

                            $scope.gridrefresh();

                            $("#popup").hide();
                        });


                }

                $("#popup").hide();

            }
            else {
                $scope.errmsg = "Please Fill The Form";
            }

        }





        $scope.Delete = function (id) {

            if (confirm("Are You Sure to Delete")) {

                var getdata = CustomerDetailSvc.CustomerDetailsDelete(id);
                getdata.then(function (d) {
                    toastr.success('Customer Details', 'Deleted ');
                    GetAllCustomerDetail();
                },
                    function (error) {
                        toastr.error('Error While Deleting', 'Error');
                    });
            }
        }






    }]);

