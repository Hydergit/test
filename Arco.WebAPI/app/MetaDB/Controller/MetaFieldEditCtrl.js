﻿
app.controller('MetaFieldEditCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
    function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {








        $scope.CDTIddrop = [];
        CDTIddrop();

        function CDTIddrop() {
            debugger;


            $http.post("/MetaDb/MetaTable/CDTIddrop")
                .success(function (data) {
                    $scope.CDTIddrop = data;

                });
        }


        $scope.EnumTypeDrop = [];
        EnumTypeDrop();

        function EnumTypeDrop() {
            debugger;


            $http.post("/MetaDb/MetaTable/EnumTypeDrop")
                .success(function (data1) {
                    debugger;
                    $scope.EnumTypeDrop = data1;

                });


        }



        $scope.DataTypeDrop = [];
        DataTypeDrop();
        function DataTypeDrop() {
            debugger;


            $http.post("/MetaDb/CustomDataType/DataTypeDrop")
                .success(function (data) {
                    $scope.DataTypeDrop = data;

                });


        }


        $scope.gridnew = function () {
            $scope.New = true;


            $scope.Field = {
                "TableId": null,
                "Field1": null,
                "DataType": null,
                "CDTId": null,
                "IsUnique": null,
                "IsVisible": null,
                "IsReadOnly": null,
                "IsMandatory": null,
                "LabelId": null,
                "HelpTextId": null,
                "DefaultValue": null,
                "EnumType": null,
                "Lenght": null,
                "Fraction": null,
                "CreatedDatetime": null,
                "CreatedBy": null,
                "ModifiedDatetime": null,
                "ModifiedBy": null,

            }

        }
        $scope.MetaTableInsert = function () {

            $scope.Save();
        }

        $scope.GetRequestById = function (id) {

            $scope.New = false;
            $("#popup").show();
            $http.post("/MetaDb/MetaTable/MetaFieldEdit", { Id: id })
                .success(function (datam) {
                    $scope.Field = datam;

                });
        };
        $scope.GetRequestBydel = function (id) {


            $http.post("/MetaDb/MetaTable/MetaFielddelete", { Id: id })
                .success(function (datam) {
                    $scope.Field = datam;

                });
        };


        $scope.DeleteClick = function () {


            checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                return gridRow.RecId;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/MetaTable/DeleteFieldById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };


        $scope.Edit = function (id) {
            $http.get("/MetaDb/MetaTable/MetaFieldEdit/?obj=" + JSON.stringify($scope.Field))
                .success(function (data) {

                    $("#popup").hide();
                });



            $scope.New = false;

            $("#popup").show();
        }


        $scope.save = function () {
            debugger;

            if ($scope.Field != null) {


                if ($scope.New == true) {

                    $http.get("/MetaDb/MetaTable/MetaFieldCreate/?obj=" + JSON.stringify($scope.Field) + "&TableId=" + $("#TableId").val())
                        .success(function (data) {
                            $scope.gridrefresh();

                            $("#popup").hide();
                        });

                }
                else {

                    $http.get("/MetaDb/MetaTable/MetaFieldUpdate/?obj=" + JSON.stringify($scope.Field))
                        .success(function (data) {

                            $scope.gridrefresh();

                            $("#popup").hide();
                        });


                }

                $("#popup").hide();

            }
            else {
                $scope.errmsg = "Please Fill The Form";
            }

        }














        $scope.GetRequestFById = function (id) {

            $scope.FieldNew = false;
            $("#popupfield").show();
            $http.post("/MetaDb/MetaTable/MetaFieldGroupEdit", { Id: id })
                .success(function (datam) {
                    $scope.FieldGroup = datam;

                });
        };





        $scope.DeleteClick1 = function () {
            debugger;
            checkIds = $(".ckb1").filter(":checked").map(function (id) {



                return id;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/MetaTable/DeleteFieldGroupById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };


        $scope.Edit = function (id) {
            $http.get("/MetaDb/MetaTable/MetaFieldGroupEdit/?obj=" + JSON.stringify($scope.FieldGroup))
                .success(function (data) {

                    $("#popupfield").hide();
                });



            $scope.FieldNew = false;

            $("#popupfield").show();
        }


        $scope.savef = function () {
            debugger;

            if ($scope.FieldGroup != null) {


                if ($scope.FieldNew == true) {

                    $http.get("/MetaDb/MetaTable/MetaFieldGroupCreate/?obj=" + JSON.stringify($scope.FieldGroup) + "&TableId=" + $("#TableId").val())
                        .success(function (data) {


                            $("#popupfield").hide();
                            window.location.reload();

                        });

                }
                else {

                    $http.get("/MetaDb/MetaTable/MetaFieldGroupUpdate/?obj=" + JSON.stringify($scope.FieldGroup))
                        .success(function (data) {



                            $("#popupfield").hide();
                            window.location.reload();
                        });


                }

                $("#popupfield").hide();

            }
            else {
                $scope.errmsg = "Please Fill The Form";
            }

        }





        $scope.GetFieldById = function (id) {
            debugger;
            alert(id);
            $("#rephd").val(id);
            $http.post("/MetaDb/MetaTable/FieldGroupwindow", { Id: id })
                .success(function (datam) {
                    $scope.FieldGroupName = datam;
                    $("#FieldGpopup").show();

                });
        };





        $scope.AssignField = function () {
            debugger;
            checkIds = $(".ckbRoll").filter(":checked").map(function () {
                return this.id;
            });
            var Id = $("#rephd").val();
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Assign?")) {
                    $http.post("/MetaDb/MetaTable/FieldAssign", { id: checkIds.toArray(), GId: Id })
                        .success(function (data) {
                            $scope.GetFieldById(Id);
                        });
                }
                else {
                    alert("Please Select Fields");
                }
            }

        }


        $scope.DeleteField = function () {
            checkIds = $(".ckbRolls").filter(":checked").map(function () {
                return this.id;
            });
            var FId = $("#rephd").val();
            if (confirm("Are You Sure to Remove?")) {
                $http.post("/MetaDb/MetaTable/DeleteField", { id: checkIds.toArray(), FId: FId })
                    .success(function (data) {

                        $scope.GetFieldById(FId);


                    });
            }

        }












        $scope.GetRequestFById = function (id) {

            $scope.TableIndexNew = false;

            $http.post("/MetaDb/MetaTable/MetaTableIndexEdit", { Id: id })
                .success(function (datam) {
                    $scope.TableIndexL = datam;

                });
        };





        $scope.DeleteTableIndexClick = function () {
            debugger;
            checkIds = $(".ckb1").filter(":checked").map(function (id) {



                return id;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/MetaTable/DeleteFieldGroupById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };





        $scope.saveTableIndex = function () {
            debugger;

            if ($scope.TableIndexNew != null) {


                if ($scope.TableIndexNew == true) {

                    $http.get("/MetaDb/MetaTable/MetaTableIndexCreate/?obj=" + JSON.stringify($scope.TableIndexL) + "&TableId=" + $("#TableId").val())
                        .success(function (data) {


                            alert(data);

                        });

                }
                else {

                    $http.get("/MetaDb/MetaTable/MetaTableIndexUpdate/?obj=" + JSON.stringify($scope.TableIndexL))
                        .success(function (data) {




                            alert(data);
                        });


                }



            }
            else {
                $scope.errmsg = "Please Fill The Form";
            }

        }


        $scope.GetTableIndexFieldwindow = function (id) {
            debugger;
            alert(id);
            $("#rephd").val(id);
            $http.post("/MetaDb/MetaTable/TableIndexFieldwindow", { Id: id })
                .success(function (datam) {
                    $scope.FieldGroupName = datam;
                    $("#FieldGpopup").show();

                });
        };





        $scope.AssignFieldTableIndex = function () {
            debugger;
            checkIds = $(".ckbRoll").filter(":checked").map(function () {
                return this.id;
            });
            var Id = $("#rephd").val();
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Assign?")) {
                    $http.post("/MetaDb/MetaTable/FieldAssignTableIndex", { id: checkIds.toArray(), GId: Id })
                        .success(function (data) {
                            $scope.GetTableIndexFieldwindow(Id);
                        });
                }
                else {
                    alert("Please Select Fields");
                }
            }

        }


        $scope.DeleteFieldTableIndex = function () {
            checkIds = $(".ckbRolls").filter(":checked").map(function () {
                return this.id;
            });
            var FId = $("#rephd").val();
            if (confirm("Are You Sure to Remove?")) {
                $http.post("/MetaDb/MetaTable/DeleteFieldTableIndex", { id: checkIds.toArray(), FId: FId })
                    .success(function (data) {

                        $scope.GetTableIndexFieldwindow(FId);


                    });
            }

        }


    }]);