﻿
app.controller('MetaFieldGroupCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
 function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {
 

     var columnDefs = [
          //{
          //    field: 'Check', pinnedLeft: true, name: 'Check', width: '35',
          //    cellTemplate: '<input id="{{row.entity.RecId}}" type="checkbox" class="ckb" />', headerCellTemplate: ' <input type="checkbox" id="SelectAll" />'
          //},
             //{
             //    field: 'Action', pinnedLeft: true, name: '', width: '50', enableFiltering: false, enableSorting: false,
             //    headerCellTemplate: '<div></div>',
             //    cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="glyphicon  glyphicon-pencil  cmtbtn themeprimary" OnClick="Edit(this)" style=" cursor:pointer;"></span></div></div>'

             //},
              {
                  field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
                  headerCellTemplate: '<div></div>',
                  cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetRequestById(row.entity.RecId)" ></span></div></div>'
              },

     //{ field: 'DatsSourceType', name: 'DatsSourceType', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'Name', name: 'Name', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'Columns', name: 'Columns', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'DisplayStyle', name: 'DisplayStyle', headerCellClass: $scope.highlightFilteredHeader },
                   
     ];
        
         function gridview() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'EmployeeOnBoard.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi = gridApi;

                     $scope.gridApi.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefs
             };
         }
         gridview();
         griddata();
       
       

       


         function griddata() {
             debugger;
           

             $http.post("/MetaDb/MetaTable/GetMetaFieldGroupList")
       .success(function (data) {
           $scope.gridOptions.data = data;

         

        
         

       });
           

         }

 
         $scope.toggleFiltering = function () {
        
             $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
             $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
         };
         $scope.expandAll = function () {
             $scope.gridApi.treeBase.expandAllRows();
         };

         $scope.toggleRow = function (rowNum) {
             $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
         };

         $scope.changeGrouping = function () {
             $scope.gridApi.grouping.clearGrouping();

         };
         $scope.gridrefresh = function () {
             $scope.gridOptions.columnDefs = columnDefs;
             griddata();
         }




         
         $scope.FieldGroup;
         $scope.New = false;

         $scope.DataSourceTypeDrop = [];
         DataSourceTypeDrop();

         function DataSourceTypeDrop() {
             debugger;


             $http.post("/MetaDb/MetaTable/DataSourceTypeDrop")
                .success(function (data) {
                    $scope.DataSourceTypeDrop = data;

                     });
         }


         

        

         $scope.gridnew = function () {
             $scope.New = true;


             //$scope.FieldGroup = {
                 
                
             //}
             
         }
        
         $scope.GetRequestById = function (id) {

             $scope.New = false;
             $("#popup").show();
             $http.post("/MetaDb/MetaTable/MetaFieldGroupEdit", { Id: id })
        .success(function (datam) {
            $scope.FieldGroup = datam;

        });
         };
         $scope.GetRequestBydel = function (id) {

            
             $http.post("/MetaDb/MetaTable/MetaFielddelete", { Id: id })
        .success(function (datam) {
            $scope.FieldGroup = datam;

        });
         };


         $scope.DeleteClick = function () {


             checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                 return gridRow.RecId;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/MetaTable/DeleteFieldGroupById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };


         $scope.Edit = function (id) {
             $http.get("/MetaDb/MetaTable/MetaFieldGroupEdit/?obj=" + JSON.stringify($scope.FieldGroup))
                       .success(function (data) {
                        
                           $("#popup").hide();
                       });
           

          
             $scope.New = false;

             $("#popup").show();
         }


         $scope.save = function () {
             debugger;

             if ($scope.FieldGroup != null) {


                 if ($scope.New == true) {

                     $http.get("/MetaDb/MetaTable/MetaFieldGroupCreate/?obj=" + JSON.stringify($scope.FieldGroup))
                        .success(function (data) {
                            $scope.gridrefresh();
                            
                                     $("#popup").hide();
                                                });

                 }
                 else {

                     $http.get("/MetaDb/MetaTable/MetaFieldGroupUpdate/?obj=" + JSON.stringify($scope.FieldGroup))
                         .success(function (data) {
                             
                             $scope.gridrefresh();
                            
                             $("#popup").hide();
                         });
                   

                 }

                 $("#popup").hide();

             }
             else {
                 $scope.errmsg = "Please Fill The Form";
             }

         }
    
     }]);

