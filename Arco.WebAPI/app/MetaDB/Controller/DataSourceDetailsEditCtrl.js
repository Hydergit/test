﻿



app.controller('DataSourceDetailsEditCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
    function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {

        //  $scope.TableNew = null;
        //$scope.Dtable = [];
        $scope.CreateTable = function () {


            $scope.Dtable = {
                "DataSourceID": null,
                "TableName": null,
                "Alias": null
            }
            $("#UpdateTablePopup").show();
            $scope.TableNew = true;
        };








        $scope.TableNameDrop = [];

        TableNameDrop();
        function TableNameDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/TableNameDrop", { DataSourceId: $("#DataSourceId").val() })
                .success(function (data) {
                    $scope.TableNameDrop = data;

                });
        }


        $scope.TableNameDropfn = function () {

            $http.post("/MetaDb/DataSource/TableNameDrop", { DataSourceId: $("#DataSourceId").val() })
                .success(function (data) {
                    $scope.TableNameDrop = data;

                });
        };







        $scope.DeleteTable = function () {


            checkIds = $(".ckb").filter(".ui-grid-row-selected").map(function () {
                return this.id;
            });

            //checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
            //    return gridRow.RecId;
            //});
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/DataSource/DeleteDataSourceTableById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };




        $scope.GetDataTable = function (id) {

            $scope.TableNew = false;
            $("#UpdateTablePopup").show();
            $http.post("/MetaDb/DataSource/GetDataSourceTableById", { Id: id })
                .success(function (datam) {
                    $scope.Dtable = datam;

                });
        };



        $scope.SaveDTable = function () {
            debugger;


            if ($scope.TableNew == true) {



                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/CreateTable",
                    params: {
                        type: JSON.stringify($scope.Dtable),
                        DataSourceId: $("#DataSourceId").val()
                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);

                });
            }
            else {


                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/UpdateTable",
                    params: {


                        type: JSON.stringify($scope.Dtable),
                        DataSourceId: $("#DataSourceId").val()

                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);

                });
                // return $http.post('/saveUser', data);
            }

        };

















        //  $scope.JoinNew = null;

        $scope.CreateJoins = function () {

            ParentTableJoinFieldDrop();
            ChildTableFieldDrop();
            $scope.DJoin = {
                "ParentTableAlias": null,
                "ParentTableJoinField": null,
                "ChildTableAlias": null,
                "ChildTableField": null
            }
            $("#UpdateJoinPopup").show();
            $scope.JoinNew = true;
        };




        $scope.Deletejoins = function () {

            debugger;

            checkIds = $(".ckb2").filter(".ui-grid-row-selected").map(function () {
                return this.id;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/DataSource/DeleteDataSourceTablejoinById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };




        $scope.GetDataJoin = function (id) {


            ParentTableJoinFieldDrop();
            ChildTableFieldDrop();
            $scope.TableNew = false;
            $("#UpdateJoinPopup").show();
            $http.post("/MetaDb/DataSource/GetDataSourceTablejoinById", { Id: id })
                .success(function (datam) {
                    $scope.DJoin = datam;

                });
        };



        $scope.SaveDJoin = function () {
            debugger;


            if ($scope.JoinNew == true) {



                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/CreateJoin",
                    params: {
                        type: JSON.stringify($scope.DJoin),
                        DataSourceId: $("#DataSourceId").val()
                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateJoinPopup").hide();
                });
            }
            else {


                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/UpdateJoin",
                    params: {


                        type: JSON.stringify($scope.DJoin),
                        DataSourceId: $("#DataSourceId").val()

                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateJoinPopup").hide();
                });
                // return $http.post('/saveUser', data);
            }

        };




        $scope.ParentTableAliasDrop = [];
        ParentTableAliasDrop();

        function ParentTableAliasDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/ParentTableAliasDrop", { DataSourceId: $("#DataSourceId").val() })
                .success(function (data) {
                    $scope.ParentTableAliasDrop = data;

                });
        }



        $scope.ParentTableAliasDropFn = function () {
            debugger;


            $http.post("/MetaDb/DataSource/ParentTableAliasDrop", { DataSourceId: $("#DataSourceId").val() })
                .success(function (data) {
                    $scope.ParentTableAliasDrop = data;

                });
        };



        $scope.ParentTableJoinFieldDrop = [];


        function ParentTableJoinFieldDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val() })
                .success(function (data) {
                    $scope.ParentTableJoinFieldDrop = data;

                });
        }



        $scope.ChildTableFieldDrop = [];


        function ChildTableFieldDrop(id) {
            debugger;


            $http.post("/MetaDb/DataSource/ChildTableFieldDrop", { DataSourceId: $("#DataSourceId").val(), Alias: id })
                .success(function (data) {
                    $scope.ChildTableFieldDrop = data;

                });
        }


        $scope.ParentAliasChange = function (id) {
            debugger;


            $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val(), Alias: id })
                .success(function (data) {
                    $scope.ParentTableJoinFieldDrop = data;

                });
        }


        $scope.GetFieldByTable = function (id) {
            debugger;


            $http.post("/MetaDb/DataSource/GetFieldByTable", { DataSourceId: $("#DataSourceId").val(), Alias: id })
                .success(function (data) {
                    $scope.ParentTableJoinFieldDrop = data;

                });
        }


        $scope.GetFieldByTable = function (id) {
            debugger;


            $http.post("/MetaDb/DataSource/GetFieldByTable", { DataSourceId: $("#DataSourceId").val(), Alias: id })
                .success(function (data) {
                    $scope.GetFieldByTableDrop = data;

                });
        }


        $scope.GetParentFieldByDataType = function (id, child) {
            debugger;
            alert(id + " /" + child);

            $http.post("/MetaDb/DataSource/GetParentFieldByDataType", { DataSourceId: $("#DataSourceId").val(), Alias: id, ChildField: child })
                .success(function (data) {
                    $scope.GetParentFieldByDataTypeDrop = data;

                });
        }




        $scope.ChildAliasChange = function (id) {
            debugger;


            $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val(), Alias: id })
                .success(function (data) {
                    $scope.ChildTableFieldDrop = data;

                });
        }

        $scope.$watch("DJoin.ChildTableAlias", function () {
            if ($scope.DJoin && $scope.DJoin.ChildTableAlias) {
                $scope.ChildAliasChange($scope.DJoin.ChildTableAlias);
            }
        });

        $scope.$watch("DJoin.ParentTableAlias", function () {
            if ($scope.DJoin && $scope.DJoin.ParentTableAlias) {
                $scope.GetParentFieldByDataType($scope.DJoin.ParentTableAlias, $scope.DJoin.ChildTableAlias);
            }
        });













        //  $scope.WhereNew = null;

        $scope.CreateWhere = function () {
            ParentTableAliasDrop();

            $scope.DWhere = {
                "Alias": null,
                "Field": null,
                "Value": null,
                "Status": null
            }
            $("#UpdateWherePopup").show();
            $scope.WhereNew = true;
        };



        $scope.DeleteWhere = function () {


            checkIds = $(".ckb3").filter(".ui-grid-row-selected").map(function () {
                return this.id;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/DataSource/DeleteDataSourceTableWhereById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };




        $scope.GetDataWhere = function (id) {
            ParentTableAliasDrop();


            $scope.WhereNew = false;
            $("#UpdateWherePopup").show();
            $http.post("/MetaDb/DataSource/GetDataSourceTableWhereById", { Id: id })
                .success(function (datam) {
                    $scope.DWhere = datam;

                });
        };



        $scope.SaveDWhere = function () {
            debugger;


            if ($scope.WhereNew == true) {



                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/CreateWhere",
                    params: {
                        type: JSON.stringify($scope.DWhere),
                        DataSourceId: $("#DataSourceId").val()
                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateWherePopup").hide();
                });
            }
            else {


                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/UpdateWhere",
                    params: {


                        type: JSON.stringify($scope.DWhere),
                        DataSourceId: $("#DataSourceId").val()

                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateWherePopup").hide();
                });
                // return $http.post('/saveUser', data);
            }

        };



        $scope.OperatorDrop = [];

        OperatorDrop();
        function OperatorDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/OperatorDrop")
                .success(function (data) {
                    $scope.OperatorDrop = data;

                });
        }



        $scope.StatusDrop = [];


        function StatusDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/StatusDrop")
                .success(function (data) {
                    $scope.StatusDrop = data;

                });
        }


        $scope.WhereAliasChange = function (id) {
            debugger;


            $http.post("/MetaDb/DataSource/ParentTableJoinFieldDrop", { DataSourceId: $("#DataSourceId").val(), Alias: id })
                .success(function (data) {
                    $scope.WhereFieldDrop = data;

                });
        }
























        // $scope.OrderNew = null;

        $scope.CreateOrder = function () {
            ParentTableAliasDrop();

            $scope.DOrder = {
                "Alias": null,
                "Field": null,
                "Direction": null,
                "SortOrder": null,
                "Status": null
            }
            $("#UpdateOrderPopup").show();
            $scope.OrderNew = true;
        };



        $scope.DeleteOrder = function () {

            checkIds = $(".ckb4").filter(".ui-grid-row-selected").map(function () {
                return this.id;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/DataSource/DeleteDataSourceTableOrderById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };




        $scope.GetDataOrder = function (id) {

            ParentTableAliasDrop();


            $scope.OrderNew = false;
            $("#UpdateOrderPopup").show();
            $http.post("/MetaDb/DataSource/GetDataSourceTableOrderById", { Id: id })
                .success(function (datam) {
                    $scope.DOrder = datam;

                });
        };



        $scope.SaveDOrder = function () {
            debugger;


            if ($scope.OrderNew == true) {



                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/CreateOrder",
                    params: {
                        type: JSON.stringify($scope.DOrder),
                        DataSourceId: $("#DataSourceId").val()
                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateOrderPopup").hide();
                });
            }
            else {


                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/UpdateOrder",
                    params: {


                        type: JSON.stringify($scope.DOrder),
                        DataSourceId: $("#DataSourceId").val()

                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateOrderPopup").hide();
                });
                // return $http.post('/saveUser', data);
            }

        };



        $scope.DirectionDrop = [];
        DirectionDrop();
        StatusDrop();
        function DirectionDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/DirectionDrop")
                .success(function (data) {
                    $scope.DirectionDrop = data;

                });
        }






























        // $scope.FieldNew = null;

        $scope.CreateField = function () {
            ParentTableAliasDrop();

            $scope.DField = {
                "Alias": null,
                "Field": null,
                "DataType": null,
                "CDT": null,
                "IsMandatory": null,
                "LabelId": null,
                "HelpTextId": null,
                "AggregateFunction": null,
            }
            $("#UpdateFieldPopup").show();
            $scope.FieldNew = true;
        };



        $scope.DeleteField = function () {


            checkIds = $(".ckb5").filter(".ui-grid-row-selected").map(function () {
                return this.id;
            });
            if (checkIds.length > 0) {
                if (confirm("Are You Sure to Delete?")) {
                    $http.post("/MetaDb/DataSource/DeleteDataSourceTableFieldById", { id: checkIds })
                        .success(function (data) {

                            alert(data);
                            $scope.gridrefresh();


                        });
                }
            }
            else {
                alert("Please Select the Record");
            }
        };




        $scope.GetDataField = function (id) {

            ParentTableAliasDrop();


            $scope.FieldNew = false;
            $("#UpdateFieldPopup").show();
            $http.post("/MetaDb/DataSource/GetDataSourceTableFieldById", { Id: id })
                .success(function (datam) {
                    $scope.DField = datam;

                });
        };



        $scope.SaveDField = function () {
            debugger;


            if ($scope.FieldNew == true) {



                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/CreateField",
                    params: {
                        type: JSON.stringify($scope.DField),
                        DataSourceId: $("#DataSourceId").val()
                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateFieldPopup").hide();
                });
            }
            else {


                var response = $http({
                    method: "post",
                    url: "/MetaDb/DataSource/UpdateField",
                    params: {


                        type: JSON.stringify($scope.DField),
                        DataSourceId: $("#DataSourceId").val()

                    }
                });
                response.then(function (emp) {
                    debugger;
                    alert(emp.data);
                    $scope.gridrefresh();
                    $("#UpdateFieldPopup").hide();
                });
                // return $http.post('/saveUser', data);
            }

        };



        $scope.DataTypeDrop = [];
        DataTypeDrop();

        function DataTypeDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/DataTypeDrop")
                .success(function (data) {
                    $scope.DataTypeDrop = data;

                });
        }


        $scope.CDTDrop = [];
        CDTDrop();

        function CDTDrop() {
            debugger;


            $http.post("/MetaDb/DataSource/CDTDrop")
                .success(function (data) {
                    $scope.CDTDrop = data;

                });
        }


















        $scope.gridrefresh = function () {
            $scope.gridOptions.columnDefs = columnDefs;
            griddata();
            griddatajoin();
            griddatawhere();

            griddataOrder();
            griddataField();
        }

    }]);




