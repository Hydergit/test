﻿
app.controller('DataSourceCtrl', ['$scope', '$http', '$timeout', '$interval', 'uiGridConstants', 'uiGridGroupingConstants', '$templateCache',
 function ($scope, $http, $timeout, $interval, uiGridConstants, uiGridGroupingConstants, $templateCache) {
 
     $scope.New = null;

     $scope.CreateClick = function () {


         $scope.src = {
             "Name": null,
             "RepoId":null
         }


         
         $("#UpdateType").show();
         $scope.New = true;

     };

     var columnDefs = [
           {
               field: 'Action', pinnedLeft: true, name: '', width: '45', enableFiltering: false, enableSorting: false,
               headerCellTemplate: '<div></div>',
               cellTemplate: '<div class="ui-grid-cell-contents"> <div ng-show="row.entity.RecId != null"><span id="{{row.entity.RecId}}" class="fa fa-pencil themeprimary" style="font-size: 16px;padding-left: 9px;" ng-click="grid.appScope.GetRequestById(row.entity.RecId)" ></span>|<a class="glyphicon glyphicon-list-alt" href="/MetaDb/DataSource/DataSourceDetails?id={{row.entity.DataSourceID}}"</a></div></div>'
           },

            { field: 'DataSourceID', headerCellClass: $scope.highlightFilteredHeader },
                    { field: 'Name', headerCellClass: $scope.highlightFilteredHeader },
                    // pre-populated search field
                    { field: 'RepoId',name:'Repository', headerCellClass: $scope.highlightFilteredHeader },
                   
     ];
        
         function gridview() {

             $templateCache.put('ui-grid/selectionRowHeaderButtons',
"<div id='{{row.entity.RecId}}' class=\"ui-grid-selection-row-header-buttons ckb ui-grid-icon-ok\"  ng-class=\"{'ui-grid-row-selected': row.isSelected }\" ng-click=\"selectButtonClick(row, $event)\">&nbsp;</div>"
);

             $scope.highlightFilteredHeader = function (row, rowRenderIndex, col, colRenderIndex) {
                 if (col.filters[0].term) {
                     return 'header-filtered';
                 } else {
                     return '';
                 }
             };

             $scope.gridOptions = {
                 enableFiltering: false,
                 enableColumnResizing: true,
                 enableGridMenu: true,
                 enableRowSelection: true,
                 enableSelectAll: true,
                 selectionRowHeaderWidth: 35,
                 exporterMenuPdf: false,
                 enableGroupHeaderSelection: true,
                 exporterCsvFilename: 'EmployeeOnBoard.csv',
                 paginationPageSizes: [15, 25, 50, 100, 500],
                 paginationPageSize: 15,
                 onRegisterApi: function (gridApi) {
                     $scope.gridApi = gridApi;

                     $scope.gridApi.selection.on.rowSelectionChanged($scope, function (rowChanged) {
                         if (typeof (rowChanged.treeLevel) !== 'undefined' && rowChanged.treeLevel > -1) {
                             // this is a group header
                             children = $scope.gridApi.treeBase.getRowChildren(rowChanged);
                             children.forEach(function (child) {
                                 if (rowChanged.isSelected) {
                                     $scope.gridApi.selection.selectRow(child.entity);
                                 } else {
                                     $scope.gridApi.selection.unSelectRow(child.entity);
                                 }
                             });
                         }
                     });
                 },

                 columnDefs: columnDefs
             };
         }
         gridview();
         griddata();


         function griddata() {
             debugger;


             $http.post("/MetaDb/DataSource/GetDataSource")
       .success(function (data) {
           $scope.gridOptions.data = data;





       });


         }


         $scope.DeleteClick = function () {


             checkIds = $scope.gridApi.selection.getPageSelectRows().map(function (gridRow) {
                 return gridRow.RecId;
             });
             if (checkIds.length > 0) {
                 if (confirm("Are You Sure to Delete?")) {
                     $http.post("/MetaDb/DataSource/DeleteDataSourceById", { id: checkIds })
               .success(function (data) {

                   alert(data);
                   $scope.gridrefresh();


               });
                 }
             }
             else {
                 alert("Please Select the Record");
             }
         };




         $scope.GetRequestById = function (id) {
             
             $scope.New = false;
             $("#UpdateType").show();
             $http.post("/MetaDb/DataSource/GetDataSourceById", { Id: id })
        .success(function (datam) {
            $scope.src = datam;

        });
         };



         $scope.SaveSource = function () {
             debugger;
             

             if ($scope.New == true) {

                

                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/CreateSource",
                     params: {
                         type: JSON.stringify($scope.src)
                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert("Sucess");
                     $scope.gridrefresh();
                     $("#UpdateType").hide();
                 });
             }

                 // return $http.post('/saveUser', data);

             else {
                
                
                 var response = $http({
                     method: "post",
                     url: "/MetaDb/DataSource/UpdateSource",
                     params: {


                         type: JSON.stringify($scope.src)

                     }
                 });
                 response.then(function (emp) {
                     debugger;
                     alert("Sucess");
                     $scope.gridrefresh();
                     $("#UpdateType").hide();
                 });
                 // return $http.post('/saveUser', data);
             }

         };


       
       

        
       
         $scope.RepoIdDrop = [];
         RepoIdDrop();
         function RepoIdDrop() {
             debugger;
           

             $http.post("/MetaDb/DataSource/RepoIdDrop")
       .success(function (data) {
           $scope.RepoIdDrop = data;

       });
           

             }

             
         $scope.toggleFiltering = function () {
        
             $scope.gridOptions.enableFiltering = !$scope.gridOptions.enableFiltering;
             $scope.gridApi.core.notifyDataChange( uiGridConstants.dataChange.COLUMN );
         };
         $scope.expandAll = function () {
             $scope.gridApi.treeBase.expandAllRows();
         };

         $scope.toggleRow = function (rowNum) {
             $scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
         };

         $scope.changeGrouping = function () {
             $scope.gridApi.grouping.clearGrouping();

         };
         $scope.gridrefresh = function () {
             $scope.gridOptions.columnDefs = columnDefs;
             griddata();
         }
    
     }]);

