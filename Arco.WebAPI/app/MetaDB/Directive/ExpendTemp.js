﻿
var directiveModule = angular.module('ExpendTemp', []);

directiveModule.directive('dynamicExpendGrid', ['$filter', '$document', '$compile', '$parse',

    function ($filter, $document, $compile, $parse) {

        return {
            restrict: 'AE',
            scope: {
                settingsinfo: '=',


            },
            template: function (element, attrs) {

               
           
                element.html(template);
            },
            controller: ['$scope', '$http', function ($scope, $http) {
                var inside = this;
                inside.settingsinfo.getselectedcheck = function () {

                    debugger;
                    return inside.HeaderCollection.filter(function (row, index) {

                        return (row.IsActive == true)
                    });
                }

              
                function init() {

                    inside.HeaderCollection = angular.copy(inside.settingsinfo.HeaderCollection);


                    inside.pagenos = [1, 5, 10, 15, 30];

                    inside.LinesCollection = [];
                    inside.HeaderPerPage = 10;

                    inside.headercurrentPage = 1;

                    inside.HeaderPageTotalSize = Math.ceil(inside.HeaderCollection.length / inside.HeaderPerPage);


                }


                $scope.$watch('inside.settingsinfo.HeaderCollection', function (newValue) {

                    init();
                });

                inside.changeHeaderpage = function (value) {
                    debugger;

                    inside.headercurrentPage = value;
                    inside.CheckALL = false;
                };

                inside.changeHeaderpagesize = function (value) {
                    debugger;
                    inside.HeaderPerPage = parseInt(value.toString());

                    inside.headercurrentPage = 1;
                    inside.HeaderPageTotalSize = Math.ceil(inside.HeaderCollection.length / inside.HeaderPerPage);

                };
                inside.checkAll = function () {
                    var begin, end;
                    begin = (inside.headercurrentPage - 1) * inside.HeaderPerPage;
                    end = begin + inside.HeaderPerPage;
                    angular.forEach(inside.HeaderCollection, function (row, index) {

                        inside.HeaderCollection[index].IsActive = (begin <= index && index < end) ? inside.CheckALL : inside.HeaderCollection[index].IsActive;
                    })

                };
                inside.HeaderPaginate = function (value) {
                    var begin, end, index;
                    begin = (inside.headercurrentPage - 1) * inside.HeaderPerPage;
                    end = begin + inside.HeaderPerPage;
                    index = inside.HeaderCollection.indexOf(value);
                    return (begin <= index && index < end);
                };

                inside.ShowLineCollection = function (index, foreignkey) {
                    debugger;
                    HideAllOpenExpends("ShowLineCollection(");

                    $("#" + inside.settingsinfo.GridId + "HeaderOpen_" + index).hide();
                    $("#" + inside.settingsinfo.GridId + "LinesCollection_" + index).show();
                    $("#" + inside.settingsinfo.GridId + "HeaderClose_" + index).show();
                    inside.LinesCollection = [];
                    GetLinesGrid(foreignkey);
                };

                inside.CloseLineCollection = function (index) {
                    debugger;
                    HideAllOpenExpends("CloseLineList(");

                    $("#" + inside.settingsinfo.GridId + "#HeaderOpen_" + index).show();

                    inside.LinesCollection = [];

                };

                function GetLinesGrid(foreignkey) {

                    $http.get(inside.settingsinfo.LineUrl + foreignkey)
                        .success(function (data) {
                            inside.LinesCollection = data;

                            inside.LinePerPage = 5;

                            inside.LinecurrentPage = 1;

                            inside.LinePageTotalSize = Math.ceil(inside.LinesCollection.length / inside.LinePerPage);


                        });
                }

                inside.changeLinespage = function (value) {
                    debugger;

                    inside.LinecurrentPage = value;

                };

                inside.changeLinespagesize = function (value) {
                    debugger;
                    inside.LinePerPage = parseInt(value.toString());

                    inside.LinecurrentPage = 1;
                    inside.LinePageTotalSize = Math.ceil(inside.LinesCollection.length / inside.LinePerPage);

                };

                inside.LinesPaginate = function (value) {
                    var begin, end, index;
                    begin = (inside.LinecurrentPage - 1) * inside.LinePerPage;
                    end = begin + inside.LinePerPage;
                    index = inside.LinesCollection.indexOf(value);
                    return (begin <= index && index < end);
                };
                function HideAllOpenExpends(test) {
                    angular.forEach(inside.HeaderCollection, function (row, index) {

                        $("#" + inside.settingsinfo.GridId + "HeaderOpen_" + index).show();
                        $("#" + inside.settingsinfo.GridId + "LinesCollection_" + index).hide();
                        $("#" + inside.settingsinfo.GridId + "HeaderClose_" + index).hide();
                    })
                }





                $scope.selected = [];

                $scope.toggleSelection = function (item) {
                    debugger;
                    var idx = $scope.selected.indexOf(item);
                    if (idx > -1) {
                        $scope.selected.splice(idx, 1);
                    }

                    else {

                        $scope.selected.push(item);
                    }
                }



                inside.IsValidField = function (key, type) {
                    var HaveLength;
                    if (type == "header") {
                        HaveLength = inside.settingsinfo.HeaderFields.filter(function (row, index) {
                            return (row.Field == key)
                        }).length ? true : false;

                    }
                    else if (type == "line") {
                        HaveLength = inside.settingsinfo.LineFields.filter(function (row, index) {
                            return (row.Field == key)
                        }).length ? true : false;

                    }
                    return HaveLength ? true : false;

                };

                inside.getDataByFilter = function (data, filter) {
                    var value = data;
                    if (data && filter) {
                        debugger;
                        try {


                            var tempValue = $filter(filter)(data);

                            if (tempValue) {
                                value = tempValue;
                            }
                        } catch (e) {
                            debugger;
                            return value;
                        }
                    }
                    return value;

                };



            }]

            ,
            controllerAs: 'inside',
            bindToController: true,
            link: function ($scope, $element, $attrs) {

            }
        };
    }]);