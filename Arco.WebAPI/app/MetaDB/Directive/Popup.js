﻿(function ($) {
    $.fn.MyPopup = function (options) {
        debugger;
        var popupid = "";
        var popuptitle = "";
        var popupcontent = "";
        var actionid = "";
        var contentdesp = "";
        var popupwidth = "";
        var show = false;
        if (options != null) {
            debugger;
            if (options.popupid != null) { popupid = options.popupid; }
            if (options.popuptitle != null) { popuptitle = options.popuptitle; }
            if (options.popupcontent != null) { popupcontent = options.popupcontent; }
            if (options.actionid != null) { actionid = options.actionid; }
            if (options.contentdesp != null) { contentdesp = options.contentdesp; }
            if (options.show != null) { show = options.show; }
            if (options.popupwidth != null) { popupwidth = options.popupwidth; }

        }

        var template =
            '<div id="' + popupid + '" class="modal">' +
            '<div class="modal-dialog">' +
            '<div class="modal-content">' +
            '<div class="modal-header">' +
            '<button type="button" id="btnclose" class="close closepopup" data-dismiss="modal" aria-hidden="true">×</button>' +
            '<h4 class="modal-title">' + popuptitle + '</h4><p class="popupdesp">' + contentdesp + '</p>' +
            '</div>' +
            '<div class="modal-body"> '
        
        '<div id="popupcontent">'
            + popupcontent +
            ' </div>'
        '</div>' +
            '<div class="modal-footer">' +
            '<button type="button" id="close" class="closepopup btn btn-warning">Cancel</button>' +
            ' <button type="button" id="' + actionid + '" class="btn btn-success">OK</button>' +
            '</div>' +
            ' </div>' +
            '</div>' +
            '</div>';


        //$(this).html(template);
        debugger;
        $(this).append(template);
        if (actionid == "") {
            debugger;
            $("#" + popupid + " .modal-dialog .btn-success").hide();
        }
        if (show == true) {
            debugger;
            $(this).children(".modal").show();
        }
        $(".closepopup").click(function () {
            debugger;
            $(this).closest(".modal").hide();
        });

        if (popupwidth > 0) {
            debugger;
            $("#" + popupid + " .modal-dialog").width(popupwidth);
        }

    };


})(jQuery);


function popupcontent(popupid, url, datas, param) {
    debugger;
    var data = new FormData();

    if (datas != null) {
        debugger;
        for (i = 0; i < datas.length; i++) {
            data.append(datas[i].title, datas[i].value);
        }
    }

    $.ajax({
        type: "POST",
        url: url,
        data: { "partialvalue": param },
        traditional: true,
        enctype: 'multipart/form-data',
        contentType: false,
        processData: false,
        success: function (data) {
            debugger;
            //  alert(data);
            $("#" + popupid + " #popupcontent").html(data);
        },
    });
}




var template = `<div class="displayflex col-lg-12 col-ls-8 col-md-12 col-sm-12">
    <div class="profile-widjet location-view">
       
        <div class="profile-repeat-list" style="background:#fff" ng-if="inside.HeaderCollection.length>0">
            <div class="profile-widjet-view location-widjet-view">
                <table  class="table table-striped table-bordered">
                    <thead>
                        <tr>
                            <th>
                                <input type="checkbox" name="" ng-model="inside.CheckALL" ng-click="inside.checkAll()" />
                            </th>
 <th>
                                
                            </th>
  <th ng-repeat="item in inside.settingsinfo.HeaderFields">{{item.Name==null?item.Field:item.Name}}</th>
                          
                           

                        </tr>
                    </thead>
                    
                   
                    <tbody ng-repeat="list in inside.HeaderCollection|filter : inside.HeaderPaginate">
                        <tr>
                            <td>
                                <input type="checkbox" name="" ng-model="list.IsActive" ng-click="toggleSelection(list)" />

                            <td>
                                <button id="{{inside.settingsinfo.GridId}}HeaderOpen_{{$index}}" class=" btn-primary showsalary" ng-click="inside.ShowLineCollection($index,list[inside.settingsinfo.foreignkey])"><i class="fa fa-plus"></i></button>
                                <button id="{{inside.settingsinfo.GridId}}HeaderClose_{{$index}}" class=" btn-primary closesalary" ng-click="inside.CloseLineCollection(list[inside.settingsinfo.foreignkey])" style="display:none;"><i class="fa fa-minus"></i></button>
                            </td>
                            
               
    <td ng-repeat="HeaderData in  inside.settingsinfo.HeaderFields" >
    
    <span ng-if="!HeaderData.filter" >{{list[HeaderData.Field]}}</span>
    

      <span ng-if="HeaderData.filter" >{{inside.getDataByFilter(list[HeaderData.Field],HeaderData.filter)}}</span>
    </td>
   
 
                                             
                        </tr>

                        </tr>               
                   

                    </tbody>
                    <tbody ng-if="inside.HeaderCollection.length < 1">
                        <tr>
                            <td colspan="8">No available list</td>
                        </tr>
                    </tbody>
                </table>
                <ul class="pagination" ng-if="inside.HeaderCollection && inside.HeaderCollection.length > 0">
                    <li class="paging symbol"><a href="" ng-click="inside.changeHeaderpage(1)" ng-class="{inactiveLink: inside.headercurrentPage < 2}" class="glyphicon glyphicon-step-backward themeprimary"></a></li>
                    <li class="paging symbol"><a href="" ng-click="inside.changeHeaderpage(inside.headercurrentPage - 1)" ng-class="{inactiveLink: inside.headercurrentPage < 2}" class="glyphicon glyphicon-chevron-left themeprimary"></a></li>
                    <li class="paging pagetext">
                        <span>
                            <em> Page</em><input type="number" ng-model="inside.headercurrentPage" min="1" max="{{inside.HeaderPageTotalSize}}" class="txtPage inactiveLink">
                            <em>of</em> <span class="endpage themeprimary">{{inside.HeaderPageTotalSize}}</span>
                        </span>
                    </li>
                    <li class="paging symbol"><a href="" ng-click="inside.changeHeaderpage(inside.headercurrentPage + 1)" ng-class="{inactiveLink: inside.headercurrentPage >= inside.HeaderPageTotalSize}" class="glyphicon glyphicon-chevron-right themeprimary"></a></li>
                    <li class="paging symbol"><a href="" ng-click="inside.changeHeaderpage(inside.HeaderPageTotalSize)" ng-class="{inactiveLink: inside.headercurrentPage >=inside.HeaderPageTotalSize}" class="glyphicon glyphicon-step-forward themeprimary"></a></li>
                    <li class="paging pagetext">
                        <span>
                            <select class="txtPage" ng-model="inside.HeaderPerPage" ng-change="inside.changeHeaderpagesize(inside.HeaderPerPage)" ng-options="item for item in inside.pagenos" style="padding:2px 0px">

                            </select>
                        </span>
                    </li>
                    <li class="paging-totalrecord themeprimary pull-right"><p class="themeprimary"><i>Total Records:{{inside.HeaderCollection.length}}</i></p></li>
                </ul>
            </div>
        </div>
    </div>
</div>
`;
