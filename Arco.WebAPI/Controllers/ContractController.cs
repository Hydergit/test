﻿using Arco.Business.IoC;
using Arco.Model.Interface.Service.Contract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Practices.Unity;
using Arco.Core.Ioc;

namespace Arco.WebAPI.Controllers
{

    public class ContractController : BaseApiController
    {

        IContractService contractservice;
        public ContractController(IContractService _IContractService)
        {
            var container = UnityHelper.Start();
            contractservice = container.Resolve<IContractService>();
          
        }

        [HttpGet]
        [Route("api/GetCustomer")]
        public dynamic GetCustomer()
        {
        
            return Json(contractservice.GetCustomers());
        }

         [HttpGet]
        [Route("api/GetCustomersById")]
      //   [Route("api/GetCustomersById/{CustomerId}")]
        public dynamic GetCustomersById(string CustomerId)
        {
            return Json(contractservice.GetCustomersById(CustomerId));
        }

         [HttpGet]
        [Route("api/CreateCustomer")]
        public dynamic CreateCustomer(string CustomerId,string Name,string IDNumber)
        {
            return Json(contractservice.CreateCustomer( CustomerId, Name, IDNumber));
        }


         [HttpGet]
         [Route("api/GetCustomersPaged")]
        public dynamic GetCustomersPaged(string Criteria)
        {
            return Json(contractservice.GetCustomersPaged(Criteria));
        }

         [HttpGet]
         [Route("api/GetContractsPaged")]
        public dynamic GetContractsPaged(string Criteria)
        {
            return Json(contractservice.GetContractsPaged(Criteria));
        }

      

    }
}
