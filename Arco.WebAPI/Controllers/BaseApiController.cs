﻿using Arco.WebAPI.ActionFilter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Arco.WebAPI.Controllers
{
    [NotImplExceptionFilter]
    [AuthorizationRequired]
    public class BaseApiController : ApiController
    {
    }
}
