﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Arco.Model.Interface.Service.Contract
{
    public interface IContractService
    {
        dynamic GetCustomers();
        dynamic GetCustomersById(string CustomerId);
        dynamic CreateCustomer(string CustomerId,string Name,string IDNumber);
        dynamic GetCustomersPaged(string Criteria);

        dynamic GetContractsPaged(string Criteria);
    }
}
