using System;
using Arco.Core.AppBase;
namespace Arco.Model.Entity
{
    // AMWContractHeader
    
    public partial class AMWContractHeader : ModelBase
    {

 public string ContractNumber { get; set; } // ContractNumber (Primary key)

 public string CustomerID { get; set; } // CustomerID

 public DateTime? ContractDate { get; set; } // ContractDate

 public string ContractSignedBy { get; set; } // ContractSignedBy

 public DateTime? ValidFrom { get; set; } // ValidFrom

 public DateTime? ValidTo { get; set; } // ValidTo

 public int? ServiceType { get; set; } // ServiceType

 public decimal? ContractValue { get; set; } // ContractValue

 public int? ContractStatus { get; set; } // ContractStatus

 public int? ProcessStatus { get; set; } // ProcessStatus








 public int? TotalManPower { get; set; } // TotalManPower

 public decimal? AdvancePayment { get; set; } // AdvancePayment

 public string CountryRegion { get; set; } // CountryRegion

 public string City { get; set; } // City

 public bool? DirectContract { get; set; } // DirectContract

 public string VirtualAccountId { get; set; } // VirtualAccountId

 public string InternalContractNumber { get; set; } // InternalContractNumber

 public string Remarks { get; set; } // Remarks

 public string ExternalID { get; set; } // ExternalID

 public string ExternalDisplayCode { get; set; } // ExternalDisplayCode

 public string ExternalDisplayName { get; set; } // ExternalDisplayName

 public string ExternalFamousName { get; set; } // ExternalFamousName

 public string PayGroupID { get; set; } // PayGroupID

 public string FeeSetup { get; set; } // FeeSetup

 public int? ContractType { get; set; } // ContractType

 public int? ContractSalesTotal { get; set; } // ContractSalesTotal

 public string InvoiceRuleID { get; set; } // InvoiceRuleID

 public string Approvedby { get; set; } // Approvedby

 public DateTime? ApprovedDate { get; set; } // ApprovedDate

 public string FinanceCustomerID { get; set; } // FinanceCustomerID

 public string RejectReason { get; set; } // RejectReason

 public string SignedbyEmployee { get; set; } // SignedbyEmployee

 public DateTime? SignedDate { get; set; } // SignedDate

 public long? ContractTemplate { get; set; } // ContractTemplate

 public string ManagerComments { get; set; } // ManagerComments

 public string SignedPersonNationality { get; set; } // SignedPersonNationality

 public string SignedPersonIDNumber { get; set; } // SignedPersonIDNumber

 public string SignedPersonPosition { get; set; } // SignedPersonPosition

 public long? Quotation { get; set; } // Quotation

 public int? ContractYears { get; set; } // ContractYears

 public long? ChargesSetupRecId { get; set; } // ChargesSetupRecId

 public string RevisionNumber { get; set; } // RevisionNumber

 public int? Gender { get; set; } // Gender

 public DateTime? ChequeAccuralDate { get; set; } // ChequeAccuralDate

 public string Article { get; set; } // Article

 public long? TemplateID { get; set; } // TemplateID

 public decimal? PromissoryAmount { get; set; } // PromissoryAmount

 public string ContractContent { get; set; } // ContractContent

 public int? ContractCreationType { get; set; } // ContractCreationType

 public string OrignalContractNumber { get; set; } // OrignalContractNumber

 public int? UsedDays { get; set; } // UsedDays

 public int? TotalDays { get; set; } // TotalDays

 public int? RemainingDays { get; set; } // RemainingDays

 public string PreviousContractNumber { get; set; } // PreviousContractNumber

 public bool? isExtended { get; set; } // isExtended

 public string ExtendedContractNumber { get; set; } // ExtendedContractNumber

 public string CRM_GUID { get; set; } // CRM_GUID

 public int? Channel { get; set; } // Channel

 public decimal? PenalityPerDay { get; set; } // PenalityPerDay

 public decimal? TotalPenalityAmount { get; set; } // TotalPenalityAmount



        
        public AMWContractHeader()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
