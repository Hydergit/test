using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace Arco.Model.Entity
{
    // AMWCustomer
    public partial class AMWCustomerConfiguration : EntityTypeConfiguration<AMWCustomer>
    {
        public AMWCustomerConfiguration()
            : this("dbo")
        {
        }
 
        public AMWCustomerConfiguration(string schema)
        {
            ToTable(schema + ".AMWCustomer");
            HasKey(x => new { x.CustomerID, x.DataAreaId });

            Property(x => x.CustomerID).HasColumnName("CustomerID").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.FirstName).HasColumnName("FirstName").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.MiddleName).HasColumnName("MiddleName").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.LastName).HasColumnName("LastName").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.FamilyName).HasColumnName("FamilyName").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.Gender).HasColumnName("Gender").IsOptional().HasColumnType("int");
            Property(x => x.Nationality).HasColumnName("Nationality").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DOBHijri).HasColumnName("DOBHijri").IsOptional().HasColumnType("date");
            Property(x => x.DOBGeorgian).HasColumnName("DOBGeorgian").IsOptional().HasColumnType("date");
            Property(x => x.IDNumber).HasColumnName("IDNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.MobileNumber).HasColumnName("MobileNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.PhoneNumber).HasColumnName("PhoneNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.Street).HasColumnName("Street").IsOptional().HasColumnType("nvarchar").HasMaxLength(200);
            Property(x => x.POBOXNumber).HasColumnName("POBOXNumber").IsOptional().HasColumnType("decimal");
            Property(x => x.CountryRegion).HasColumnName("CountryRegion").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.City).HasColumnName("City").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.PostalCode).HasColumnName("PostalCode").IsOptional().HasColumnType("decimal");
            Property(x => x.Email).HasColumnName("Email").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.WorkingSector).HasColumnName("WorkingSector").IsOptional().HasColumnType("int");
            Property(x => x.Occupation).HasColumnName("Occupation").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.MonthlySalary).HasColumnName("MonthlySalary").IsOptional().HasColumnType("decimal").HasPrecision(10,3);
            Property(x => x.OtherIncome).HasColumnName("OtherIncome").IsOptional().HasColumnType("decimal").HasPrecision(10,3);
            Property(x => x.KnowVia).HasColumnName("KnowVia").IsOptional().HasColumnType("int");
            Property(x => x.Notes).HasColumnName("Notes").IsOptional().HasColumnType("nvarchar").HasMaxLength(150);
            Property(x => x.CompanyName).HasColumnName("CompanyName").IsOptional().HasColumnType("nvarchar").HasMaxLength(150);
            Property(x => x.LegalName).HasColumnName("LegalName").IsOptional().HasColumnType("nvarchar").HasMaxLength(150);
            Property(x => x.RegistrationNo).HasColumnName("RegistrationNo").IsOptional().HasColumnType("nvarchar").HasMaxLength(30);
            Property(x => x.COCNo).HasColumnName("COCNo").IsOptional().HasColumnType("nvarchar").HasMaxLength(30);
            Property(x => x.CompanyEmail).HasColumnName("CompanyEmail").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.CompanyWebsite).HasColumnName("CompanyWebsite").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.NatureofBusiness).HasColumnName("NatureofBusiness").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.Sector).HasColumnName("Sector").IsOptional().HasColumnType("int");
            Property(x => x.TotalEmployees).HasColumnName("TotalEmployees").IsOptional().HasColumnType("int");
            Property(x => x.TotalSaudiEmployees).HasColumnName("TotalSaudiEmployees").IsOptional().HasColumnType("int");
            Property(x => x.CompanyNoLabourOffice).HasColumnName("CompanyNoLabourOffice").IsOptional().HasColumnType("nvarchar").HasMaxLength(30);
            Property(x => x.CompanyActivity).HasColumnName("CompanyActivity").IsOptional().HasColumnType("nvarchar").HasMaxLength(150);
            Property(x => x.ContactPerson).HasColumnName("ContactPerson").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.Extension).HasColumnName("Extension").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.CustomerType).HasColumnName("CustomerType").IsOptional().HasColumnType("int");
            Property(x => x.ContactPersonEmail).HasColumnName("ContactPersonEmail").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.CustomerStatus).HasColumnName("CustomerStatus").IsOptional().HasColumnType("int");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.AccountManager).HasColumnName("AccountManager").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.Supervisor).HasColumnName("Supervisor").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.SalesPerson).HasColumnName("SalesPerson").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.NoofFamilyMembers).HasColumnName("NoofFamilyMembers").IsOptional().HasColumnType("int");
            Property(x => x.NoofWifes).HasColumnName("NoofWifes").IsOptional().HasColumnType("int");
            Property(x => x.OldAgePerson).HasColumnName("OldAgePerson").IsOptional().HasColumnType("bit");
            Property(x => x.NoofOldAgePerson).HasColumnName("NoofOldAgePerson").IsOptional().HasColumnType("int");
            Property(x => x.OldAgePersonNotes).HasColumnName("OldAgePersonNotes").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.SpecialDisabled).HasColumnName("SpecialDisabled").IsOptional().HasColumnType("bit");
            Property(x => x.NoofSpecialDisabled).HasColumnName("NoofSpecialDisabled").IsOptional().HasColumnType("int");
            Property(x => x.SpecialDisabledNotes).HasColumnName("SpecialDisabledNotes").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.SalaryRange).HasColumnName("SalaryRange").IsOptional().HasColumnType("int");
            Property(x => x.OccupationDesc).HasColumnName("OccupationDesc").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.CreatedSource).HasColumnName("CreatedSource").IsOptional().HasColumnType("int");
            Property(x => x.ExternalID).HasColumnName("ExternalID").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.EnquiryRefId).HasColumnName("EnquiryRefId").IsOptional().HasColumnType("bigint");
            Property(x => x.FaxNumber).HasColumnName("FaxNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.CustomerNameEN).HasColumnName("CustomerNameEN").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.WorkPlace).HasColumnName("WorkPlace").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.WorkAddress).HasColumnName("WorkAddress").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.WorkEmail).HasColumnName("WorkEmail").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.WorkTel).HasColumnName("WorkTel").IsOptional().HasColumnType("nvarchar").HasMaxLength(25);
            Property(x => x.WorkFax).HasColumnName("WorkFax").IsOptional().HasColumnType("nvarchar").HasMaxLength(25);
            Property(x => x.WorkPoBox).HasColumnName("WorkPoBox").IsOptional().HasColumnType("decimal");
            Property(x => x.MailAddress).HasColumnName("MailAddress").IsOptional().HasColumnType("decimal");
            Property(x => x.WorkMailAddress).HasColumnName("WorkMailAddress").IsOptional().HasColumnType("decimal");
            Property(x => x.WorkMailBox).HasColumnName("WorkMailBox").IsOptional().HasColumnType("decimal");
            Property(x => x.MailBox).HasColumnName("MailBox").IsOptional().HasColumnType("decimal");
            Property(x => x.WorkMobile).HasColumnName("WorkMobile").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.CRM_GUID).HasColumnName("CRM_GUID").IsOptional().HasColumnType("nvarchar").HasMaxLength(200);
            Property(x => x.Channel).HasColumnName("Channel").IsOptional().HasColumnType("int");
            Property(x => x.FemaleContactPersonPhoneNumber).HasColumnName("FemaleContactPersonPhoneNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.VerificationStatus).HasColumnName("VerificationStatus").IsOptional().HasColumnType("int");
            Property(x => x.VerifiedBy).HasColumnName("VerifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.VerifiedDate).HasColumnName("VerifiedDate").IsOptional().HasColumnType("datetime");
            Property(x => x.VirtualAccountId).HasColumnName("VirtualAccountId").IsOptional().HasColumnType("nvarchar").HasMaxLength(25);
            Property(x => x.AffiliateCode).HasColumnName("AffiliateCode").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.IBANNumber).HasColumnName("IBANNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.PromoCode).HasColumnName("PromoCode").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.WorkDateOfJoining).HasColumnName("WorkDateOfJoining").IsOptional().HasColumnType("date");
            Property(x => x.IqamaExpiryDate).HasColumnName("IqamaExpiryDate").IsOptional().HasColumnType("datetime");

            // Foreign keys
            //HasOptional(a => a.EmployeeMaster_AccountManager).WithMany(b => b.AMWCustomers_AccountManager).HasForeignKey(c => new { c.AccountManager, c.DataAreaId }); // FK_AMWCustomer_AccountManager
            //HasRequired(a => a.EmployeeMaster_DataAreaId).WithMany(b => b.AMWCustomers_DataAreaId).HasForeignKey(c => new { c.Supervisor, c.DataAreaId }); // FK_AMWCustomer_Supervisor
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
