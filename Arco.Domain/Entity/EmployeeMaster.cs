using System;
using Arco.Core.AppBase;
namespace Arco.Model.Entity
{
    // EmployeeMaster
    
    public partial class EmployeeMaster : ModelBase
    {

 public string EmployeeId { get; set; } // EmployeeId (Primary key)

 public string Name { get; set; } // Name







 public string FirstNameEnglish { get; set; } // FirstNameEnglish

 public string MiddleNameEnglish { get; set; } // MiddleNameEnglish

 public string LastNameEnglish { get; set; } // LastNameEnglish

 public string FirstNameArabic { get; set; } // FirstNameArabic

 public string MiddleNameArabic { get; set; } // MiddleNameArabic

 public string LastNameArabic { get; set; } // LastNameArabic

 public string FatherName { get; set; } // FatherName

 public string MotherName { get; set; } // MotherName

 public int? Gender { get; set; } // Gender

 public DateTime? DOB { get; set; } // DOB

 public byte[] Photo { get; set; } // Photo

 public string Nationality { get; set; } // Nationality

 public int? MaritalStatus { get; set; } // MaritalStatus

 public string NativeLanguage { get; set; } // NativeLanguage

 public int? BloodGroup { get; set; } // BloodGroup

 public string VisaProfession { get; set; } // VisaProfession

 public DateTime? DOJ { get; set; } // DOJ

 public int? Recordtype { get; set; } // Recordtype

 public string DependentEmployee { get; set; } // DependentEmployee

 public int? EmployeeRelation { get; set; } // EmployeeRelation


 public int? ResidentType { get; set; } // ResidentType

 public int? Religion { get; set; } // Religion

 public string CountryEntranceNo { get; set; } // CountryEntranceNo

 public DateTime? CountryEntranceDate { get; set; } // CountryEntranceDate

 public string CountryEntranceGate { get; set; } // CountryEntranceGate

 public DateTime? DateofJoingHJ { get; set; } // DateofJoingHJ

 public int? AnnualLeave { get; set; } // AnnualLeave

 public string LoanGroup { get; set; } // LoanGroup

 public decimal? MaxCashLimit { get; set; } // MaxCashLimit

 public int? PayrollMode { get; set; } // PayrollMode

 public decimal? BasicPay { get; set; } // BasicPay

 public int? FamilyVisaStatus { get; set; } // FamilyVisaStatus

 public int? EmployeeType { get; set; } // EmployeeType

 public string WorkTimeTemplate { get; set; } // WorkTimeTemplate

 public int? EmployeeStatus { get; set; } // EmployeeStatus

 public string AddressKSA { get; set; } // AddressKSA

 public string AddressHome { get; set; } // AddressHome

 public int? WorkStatus { get; set; } // WorkStatus

 public int? ProblemStatus { get; set; } // ProblemStatus

 public string PositionID { get; set; } // PositionID

 public string OrganizationUnitID { get; set; } // OrganizationUnitID

 public DateTime? StartSalaryDate { get; set; } // StartSalaryDate

 public decimal? HeightinCM { get; set; } // HeightinCM

 public decimal? HeightinFeet { get; set; } // HeightinFeet

 public decimal? WeightinKg { get; set; } // WeightinKg

 public decimal? WeightinPound { get; set; } // WeightinPound

 public int? Education { get; set; } // Education

 public int? TotalExperience { get; set; } // TotalExperience

 public int? GCCExperience { get; set; } // GCCExperience

 public int? SaudiExperience { get; set; } // SaudiExperience

 public string Remarks { get; set; } // Remarks

 public string SSICalculationID { get; set; } // SSICalculationID

 public int? PaymentType { get; set; } // PaymentType

 public string EmployeeStatusID { get; set; } // EmployeeStatusID

 public string MobileNumber { get; set; } // MobileNumber

 public string Dimensions { get; set; } // Dimensions

 public string project { get; set; } // project

 public string iqamanumber { get; set; } // iqamanumber

 public string passportnumber { get; set; } // passportnumber

 public DateTime? ResignDate { get; set; } // ResignDate

 public string ResignReason { get; set; } // ResignReason

 public string ExitReasonTypeID { get; set; } // ExitReasonTypeID

 public string ExitTypeID { get; set; } // ExitTypeID

 public string ExitApprovalBy { get; set; } // ExitApprovalBy

 public DateTime? ExitApprovalDate { get; set; } // ExitApprovalDate

 public string ExitApprovalRemarks { get; set; } // ExitApprovalRemarks

 public long? EoSprofileID { get; set; } // EoSprofileID

 public string Email { get; set; } // Email

 public string RegistrationNo { get; set; } // RegistrationNo

 public DateTime? ResignRequestDate { get; set; } // ResignRequestDate

 public string CurrentContractNumber { get; set; } // CurrentContractNumber

 public string CurrentCustomerID { get; set; } // CurrentCustomerID

 public string IqamaProfessionId { get; set; } // IqamaProfessionId

 public string CandidateID { get; set; } // CandidateID

 public bool? CanSpeakArabic { get; set; } // CanSpeakArabic

 public bool? CanSpeakEnglish { get; set; } // CanSpeakEnglish

 public string OtherLanguage { get; set; } // OtherLanguage

 public bool? CleaningHouses { get; set; } // CleaningHouses

 public bool? ChildCare { get; set; } // ChildCare

 public bool? OldageCare { get; set; } // OldageCare

 public bool? Cooking { get; set; } // Cooking

 public bool? isActive { get; set; } // isActive

 public int? PhysicalStatus { get; set; } // PhysicalStatus

 public bool? isIdCardCreated { get; set; } // isIdCardCreated

 public byte[] BarCodeImage { get; set; } // BarCodeImage

 public int? IdCardProcess { get; set; } // IdCardProcess

 public int? OtherExperience { get; set; } // OtherExperience

 public string StatuslikeMoqueem { get; set; } // StatuslikeMoqueem

 public int? MoqueemStatus { get; set; } // MoqueemStatus

 public string CRM_GUID { get; set; } // CRM_GUID

 public int? Channel { get; set; } // Channel

 public bool? IsArcoDrivingLicenseSponsor { get; set; } // IsArcoDrivingLicenseSponsor

 public string JobSpecification { get; set; } // JobSpecification



        
        public EmployeeMaster()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }




}
