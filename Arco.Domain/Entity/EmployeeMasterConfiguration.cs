using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace Arco.Model.Entity
{
    // EmployeeMaster
    public partial class EmployeeMasterConfiguration : EntityTypeConfiguration<EmployeeMaster>
    {
        public EmployeeMasterConfiguration()
            : this("dbo")
        {
        }
 
        public EmployeeMasterConfiguration(string schema)
        {
            ToTable(schema + ".EmployeeMaster");
            HasKey(x => new { x.EmployeeId, x.DataAreaId });

            Property(x => x.EmployeeId).HasColumnName("EmployeeId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.Name).HasColumnName("Name").IsOptional().HasColumnType("nvarchar").HasMaxLength(300);
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.FirstNameEnglish).HasColumnName("FirstNameEnglish").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.MiddleNameEnglish).HasColumnName("MiddleNameEnglish").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.LastNameEnglish).HasColumnName("LastNameEnglish").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.FirstNameArabic).HasColumnName("FirstNameArabic").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.MiddleNameArabic).HasColumnName("MiddleNameArabic").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.LastNameArabic).HasColumnName("LastNameArabic").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.FatherName).HasColumnName("FatherName").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.MotherName).HasColumnName("MotherName").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.Gender).HasColumnName("Gender").IsOptional().HasColumnType("int");
            Property(x => x.DOB).HasColumnName("DOB").IsOptional().HasColumnType("date");
            Property(x => x.Photo).HasColumnName("Photo").IsOptional().HasColumnType("image").HasMaxLength(2147483647);
            Property(x => x.Nationality).HasColumnName("Nationality").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.MaritalStatus).HasColumnName("MaritalStatus").IsOptional().HasColumnType("int");
            Property(x => x.NativeLanguage).HasColumnName("NativeLanguage").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.BloodGroup).HasColumnName("BloodGroup").IsOptional().HasColumnType("int");
            Property(x => x.VisaProfession).HasColumnName("VisaProfession").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DOJ).HasColumnName("DOJ").IsOptional().HasColumnType("date");
            Property(x => x.Recordtype).HasColumnName("Recordtype").IsOptional().HasColumnType("int");
            Property(x => x.DependentEmployee).HasColumnName("DependentEmployee").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.EmployeeRelation).HasColumnName("EmployeeRelation").IsOptional().HasColumnType("int");
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.ResidentType).HasColumnName("ResidentType").IsOptional().HasColumnType("int");
            Property(x => x.Religion).HasColumnName("Religion").IsOptional().HasColumnType("int");
            Property(x => x.CountryEntranceNo).HasColumnName("CountryEntranceNo").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.CountryEntranceDate).HasColumnName("CountryEntranceDate").IsOptional().HasColumnType("date");
            Property(x => x.CountryEntranceGate).HasColumnName("CountryEntranceGate").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DateofJoingHJ).HasColumnName("DateofJoingHJ").IsOptional().HasColumnType("date");
            Property(x => x.AnnualLeave).HasColumnName("AnnualLeave").IsOptional().HasColumnType("int");
            Property(x => x.LoanGroup).HasColumnName("LoanGroup").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.MaxCashLimit).HasColumnName("MaxCashLimit").IsOptional().HasColumnType("decimal").HasPrecision(18,3);
            Property(x => x.PayrollMode).HasColumnName("PayrollMode").IsOptional().HasColumnType("int");
            Property(x => x.BasicPay).HasColumnName("BasicPay").IsOptional().HasColumnType("decimal").HasPrecision(18,3);
            Property(x => x.FamilyVisaStatus).HasColumnName("FamilyVisaStatus").IsOptional().HasColumnType("int");
            Property(x => x.EmployeeType).HasColumnName("EmployeeType").IsOptional().HasColumnType("int");
            Property(x => x.WorkTimeTemplate).HasColumnName("WorkTimeTemplate").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.EmployeeStatus).HasColumnName("EmployeeStatus").IsOptional().HasColumnType("int");
            Property(x => x.AddressKSA).HasColumnName("AddressKSA").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.AddressHome).HasColumnName("AddressHome").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.WorkStatus).HasColumnName("WorkStatus").IsOptional().HasColumnType("int");
            Property(x => x.ProblemStatus).HasColumnName("ProblemStatus").IsOptional().HasColumnType("int");
            Property(x => x.PositionID).HasColumnName("PositionID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.OrganizationUnitID).HasColumnName("OrganizationUnitID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.StartSalaryDate).HasColumnName("StartSalaryDate").IsOptional().HasColumnType("date");
            Property(x => x.HeightinCM).HasColumnName("HeightinCM").IsOptional().HasColumnType("decimal").HasPrecision(5,2);
            Property(x => x.HeightinFeet).HasColumnName("HeightinFeet").IsOptional().HasColumnType("decimal").HasPrecision(7,5);
            Property(x => x.WeightinKg).HasColumnName("WeightinKg").IsOptional().HasColumnType("decimal").HasPrecision(5,2);
            Property(x => x.WeightinPound).HasColumnName("WeightinPound").IsOptional().HasColumnType("decimal").HasPrecision(8,5);
            Property(x => x.Education).HasColumnName("Education").IsOptional().HasColumnType("int");
            Property(x => x.TotalExperience).HasColumnName("TotalExperience").IsOptional().HasColumnType("int");
            Property(x => x.GCCExperience).HasColumnName("GCCExperience").IsOptional().HasColumnType("int");
            Property(x => x.SaudiExperience).HasColumnName("SaudiExperience").IsOptional().HasColumnType("int");
            Property(x => x.Remarks).HasColumnName("Remarks").IsOptional().HasColumnType("nvarchar");
            Property(x => x.SSICalculationID).HasColumnName("SSICalculationID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.PaymentType).HasColumnName("PaymentType").IsOptional().HasColumnType("int");
            Property(x => x.EmployeeStatusID).HasColumnName("EmployeeStatusID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.MobileNumber).HasColumnName("MobileNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.Dimensions).HasColumnName("Dimensions").IsOptional().HasColumnType("nvarchar");
            Property(x => x.project).HasColumnName("project").IsOptional().HasColumnType("nvarchar").HasMaxLength(300);
            Property(x => x.iqamanumber).HasColumnName("iqamanumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.passportnumber).HasColumnName("passportnumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.ResignDate).HasColumnName("ResignDate").IsOptional().HasColumnType("datetime");
            Property(x => x.ResignReason).HasColumnName("ResignReason").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.ExitReasonTypeID).HasColumnName("ExitReasonTypeID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ExitTypeID).HasColumnName("ExitTypeID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ExitApprovalBy).HasColumnName("ExitApprovalBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ExitApprovalDate).HasColumnName("ExitApprovalDate").IsOptional().HasColumnType("date");
            Property(x => x.ExitApprovalRemarks).HasColumnName("ExitApprovalRemarks").IsOptional().HasColumnType("nvarchar").HasMaxLength(250);
            Property(x => x.EoSprofileID).HasColumnName("EoSprofileID").IsOptional().HasColumnType("bigint");
            Property(x => x.Email).HasColumnName("Email").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.RegistrationNo).HasColumnName("RegistrationNo").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.ResignRequestDate).HasColumnName("ResignRequestDate").IsOptional().HasColumnType("datetime");
            Property(x => x.CurrentContractNumber).HasColumnName("CurrentContractNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.CurrentCustomerID).HasColumnName("CurrentCustomerID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.IqamaProfessionId).HasColumnName("IqamaProfessionId").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.CandidateID).HasColumnName("CandidateID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.CanSpeakArabic).HasColumnName("CanSpeakArabic").IsOptional().HasColumnType("bit");
            Property(x => x.CanSpeakEnglish).HasColumnName("CanSpeakEnglish").IsOptional().HasColumnType("bit");
            Property(x => x.OtherLanguage).HasColumnName("OtherLanguage").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.CleaningHouses).HasColumnName("CleaningHouses").IsOptional().HasColumnType("bit");
            Property(x => x.ChildCare).HasColumnName("ChildCare").IsOptional().HasColumnType("bit");
            Property(x => x.OldageCare).HasColumnName("OldageCare").IsOptional().HasColumnType("bit");
            Property(x => x.Cooking).HasColumnName("Cooking").IsOptional().HasColumnType("bit");
            Property(x => x.isActive).HasColumnName("isActive").IsOptional().HasColumnType("bit");
            Property(x => x.PhysicalStatus).HasColumnName("PhysicalStatus").IsOptional().HasColumnType("int");
            Property(x => x.isIdCardCreated).HasColumnName("isIdCardCreated").IsOptional().HasColumnType("bit");
            Property(x => x.BarCodeImage).HasColumnName("BarCodeImage").IsOptional().HasColumnType("image").HasMaxLength(2147483647);
            Property(x => x.IdCardProcess).HasColumnName("IdCardProcess").IsOptional().HasColumnType("int");
            Property(x => x.OtherExperience).HasColumnName("OtherExperience").IsOptional().HasColumnType("int");
            Property(x => x.StatuslikeMoqueem).HasColumnName("StatuslikeMoqueem").IsOptional().HasColumnType("nvarchar").HasMaxLength(25);
            Property(x => x.MoqueemStatus).HasColumnName("MoqueemStatus").IsOptional().HasColumnType("int");
            Property(x => x.CRM_GUID).HasColumnName("CRM_GUID").IsOptional().HasColumnType("nvarchar").HasMaxLength(200);
            Property(x => x.Channel).HasColumnName("Channel").IsOptional().HasColumnType("int");
            Property(x => x.IsArcoDrivingLicenseSponsor).HasColumnName("IsArcoDrivingLicenseSponsor").IsOptional().HasColumnType("bit");
            Property(x => x.JobSpecification).HasColumnName("JobSpecification").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);

            // Foreign keys
            //HasOptional(a => a.AMWContractHeader).WithMany(b => b.EmployeeMasters).HasForeignKey(c => new { c.CurrentContractNumber, c.DataAreaId }); // FK_EmployeeMaster_AMWContractHeader
            //HasOptional(a => a.AMWCustomer).WithMany(b => b.EmployeeMasters).HasForeignKey(c => new { c.CurrentCustomerID, c.DataAreaId }); // FK_EmployeeMaster_AMWCustomer
            //HasRequired(a => a.EmployeeMaster1).WithMany(b => b.EmployeeMasters_DataAreaId).HasForeignKey(c => new { c.DependentEmployee, c.DataAreaId }); // FK_EmployeeMaster_Employee
            //HasRequired(a => a.EmployeeMaster2).WithMany(b => b.EmployeeMasters1).HasForeignKey(c => new { c.ExitApprovalBy, c.DataAreaId }); // FK_EmployeeMaster_ExitApprovalBy
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
