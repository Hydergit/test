using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
namespace Arco.Model.Entity
{
    // AMWContractHeader
    public partial class AMWContractHeaderConfiguration : EntityTypeConfiguration<AMWContractHeader>
    {
        public AMWContractHeaderConfiguration()
            : this("dbo")
        {
        }
 
        public AMWContractHeaderConfiguration(string schema)
        {
            ToTable(schema + ".AMWContractHeader");
            HasKey(x => new { x.ContractNumber, x.DataAreaId });

            Property(x => x.ContractNumber).HasColumnName("ContractNumber").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.CustomerID).HasColumnName("CustomerID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ContractDate).HasColumnName("ContractDate").IsOptional().HasColumnType("date");
            Property(x => x.ContractSignedBy).HasColumnName("ContractSignedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.ValidFrom).HasColumnName("ValidFrom").IsOptional().HasColumnType("date");
            Property(x => x.ValidTo).HasColumnName("ValidTo").IsOptional().HasColumnType("date");
            Property(x => x.ServiceType).HasColumnName("ServiceType").IsOptional().HasColumnType("int");
            Property(x => x.ContractValue).HasColumnName("ContractValue").IsOptional().HasColumnType("money").HasPrecision(19,4);
            Property(x => x.ContractStatus).HasColumnName("ContractStatus").IsOptional().HasColumnType("int");
            Property(x => x.ProcessStatus).HasColumnName("ProcessStatus").IsOptional().HasColumnType("int");
            Property(x => x.CreatedDatetime).HasColumnName("CreatedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.CreatedBy).HasColumnName("CreatedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ModifiedDatetime).HasColumnName("ModifiedDatetime").IsOptional().HasColumnType("datetime");
            Property(x => x.ModifiedBy).HasColumnName("ModifiedBy").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DataAreaId).HasColumnName("DataAreaId").IsRequired().HasColumnType("nvarchar").HasMaxLength(10).HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);
            Property(x => x.RecId).HasColumnName("RecId").IsRequired().HasColumnType("bigint").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
            Property(x => x.RecVersion).HasColumnName("RecVersion").IsOptional().HasColumnType("timestamp").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.TotalManPower).HasColumnName("TotalManPower").IsOptional().HasColumnType("int");
            Property(x => x.AdvancePayment).HasColumnName("AdvancePayment").IsOptional().HasColumnType("money").HasPrecision(19,4);
            Property(x => x.CountryRegion).HasColumnName("CountryRegion").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.City).HasColumnName("City").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.DirectContract).HasColumnName("DirectContract").IsOptional().HasColumnType("bit");
            Property(x => x.VirtualAccountId).HasColumnName("VirtualAccountId").IsOptional().HasColumnType("nvarchar").HasMaxLength(25);
            Property(x => x.InternalContractNumber).HasColumnName("InternalContractNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.Remarks).HasColumnName("Remarks").IsOptional().HasColumnType("nvarchar").HasMaxLength(150);
            Property(x => x.ExternalID).HasColumnName("ExternalID").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.ExternalDisplayCode).HasColumnName("ExternalDisplayCode").IsOptional().HasColumnType("nvarchar").HasMaxLength(50);
            Property(x => x.ExternalDisplayName).HasColumnName("ExternalDisplayName").IsOptional().HasColumnType("nvarchar").HasMaxLength(300);
            Property(x => x.ExternalFamousName).HasColumnName("ExternalFamousName").IsOptional().HasColumnType("nvarchar").HasMaxLength(300);
            Property(x => x.PayGroupID).HasColumnName("PayGroupID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.FeeSetup).HasColumnName("FeeSetup").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ContractType).HasColumnName("ContractType").IsOptional().HasColumnType("int");
            Property(x => x.ContractSalesTotal).HasColumnName("ContractSalesTotal").IsOptional().HasColumnType("int");
            Property(x => x.InvoiceRuleID).HasColumnName("InvoiceRuleID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.Approvedby).HasColumnName("Approvedby").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.ApprovedDate).HasColumnName("ApprovedDate").IsOptional().HasColumnType("date");
            Property(x => x.FinanceCustomerID).HasColumnName("FinanceCustomerID").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.RejectReason).HasColumnName("RejectReason").IsOptional().HasColumnType("nvarchar");
            Property(x => x.SignedbyEmployee).HasColumnName("SignedbyEmployee").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.SignedDate).HasColumnName("SignedDate").IsOptional().HasColumnType("date");
            Property(x => x.ContractTemplate).HasColumnName("ContractTemplate").IsOptional().HasColumnType("bigint");
            Property(x => x.ManagerComments).HasColumnName("ManagerComments").IsOptional().HasColumnType("nvarchar");
            Property(x => x.SignedPersonNationality).HasColumnName("SignedPersonNationality").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.SignedPersonIDNumber).HasColumnName("SignedPersonIDNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(20);
            Property(x => x.SignedPersonPosition).HasColumnName("SignedPersonPosition").IsOptional().HasColumnType("nvarchar").HasMaxLength(100);
            Property(x => x.Quotation).HasColumnName("Quotation").IsOptional().HasColumnType("bigint");
            Property(x => x.ContractYears).HasColumnName("ContractYears").IsOptional().HasColumnType("int");
            Property(x => x.ChargesSetupRecId).HasColumnName("ChargesSetupRecId").IsOptional().HasColumnType("bigint");
            Property(x => x.RevisionNumber).HasColumnName("RevisionNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.Gender).HasColumnName("Gender").IsOptional().HasColumnType("int");
            Property(x => x.ChequeAccuralDate).HasColumnName("ChequeAccuralDate").IsOptional().HasColumnType("datetime");
            Property(x => x.Article).HasColumnName("Article").IsOptional().HasColumnType("nvarchar");
            Property(x => x.TemplateID).HasColumnName("TemplateID").IsOptional().HasColumnType("bigint");
            Property(x => x.PromissoryAmount).HasColumnName("PromissoryAmount").IsOptional().HasColumnType("money").HasPrecision(19,4);
            Property(x => x.ContractContent).HasColumnName("ContractContent").IsOptional().HasColumnType("nvarchar");
            Property(x => x.ContractCreationType).HasColumnName("ContractCreationType").IsOptional().HasColumnType("int");
            Property(x => x.OrignalContractNumber).HasColumnName("OrignalContractNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.UsedDays).HasColumnName("UsedDays").IsOptional().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.TotalDays).HasColumnName("TotalDays").IsOptional().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.RemainingDays).HasColumnName("RemainingDays").IsOptional().HasColumnType("int").HasDatabaseGeneratedOption(DatabaseGeneratedOption.Computed);
            Property(x => x.PreviousContractNumber).HasColumnName("PreviousContractNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.isExtended).HasColumnName("isExtended").IsOptional().HasColumnType("bit");
            Property(x => x.ExtendedContractNumber).HasColumnName("ExtendedContractNumber").IsOptional().HasColumnType("nvarchar").HasMaxLength(10);
            Property(x => x.CRM_GUID).HasColumnName("CRM_GUID").IsOptional().HasColumnType("nvarchar").HasMaxLength(200);
            Property(x => x.Channel).HasColumnName("Channel").IsOptional().HasColumnType("int");
            Property(x => x.PenalityPerDay).HasColumnName("PenalityPerDay").IsOptional().HasColumnType("decimal").HasPrecision(18,3);
            Property(x => x.TotalPenalityAmount).HasColumnName("TotalPenalityAmount").IsOptional().HasColumnType("decimal").HasPrecision(18,3);

            // Foreign keys
            //HasOptional(a => a.AMWCustomer).WithMany(b => b.AMWContractHeaders).HasForeignKey(c => new { c.CustomerID, c.DataAreaId }); // FK_AMWContractHeader_AMWCustomer
            //HasOptional(a => a.EmployeeMaster_Approvedby).WithMany(b => b.AMWContractHeaders_Approvedby).HasForeignKey(c => new { c.Approvedby, c.DataAreaId }); // FK_AMWContractHeader_Approvedby
            //HasRequired(a => a.EmployeeMaster_DataAreaId).WithMany(b => b.AMWContractHeaders_DataAreaId).HasForeignKey(c => new { c.SignedbyEmployee, c.DataAreaId }); // FK_AMWContractHeader_SignedbyEmployee
            InitializePartial();
        }
        partial void InitializePartial();
    }

}
