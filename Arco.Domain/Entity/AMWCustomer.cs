using System;
using Arco.Core.AppBase;
namespace Arco.Model.Entity
{
    // AMWCustomer
    
    public partial class AMWCustomer : ModelBase
    {

 public string CustomerID { get; set; } // CustomerID (Primary key)

 public string FirstName { get; set; } // FirstName

 public string MiddleName { get; set; } // MiddleName

 public string LastName { get; set; } // LastName

 public string FamilyName { get; set; } // FamilyName

 public int? Gender { get; set; } // Gender

 public string Nationality { get; set; } // Nationality

 public DateTime? DOBHijri { get; set; } // DOBHijri

 public DateTime? DOBGeorgian { get; set; } // DOBGeorgian

 public string IDNumber { get; set; } // IDNumber

 public string MobileNumber { get; set; } // MobileNumber

 public string PhoneNumber { get; set; } // PhoneNumber

 public string Street { get; set; } // Street

 public decimal? POBOXNumber { get; set; } // POBOXNumber

 public string CountryRegion { get; set; } // CountryRegion

 public string City { get; set; } // City

 public decimal? PostalCode { get; set; } // PostalCode

 public string Email { get; set; } // Email

 public int? WorkingSector { get; set; } // WorkingSector

 public string Occupation { get; set; } // Occupation

 public decimal? MonthlySalary { get; set; } // MonthlySalary

 public decimal? OtherIncome { get; set; } // OtherIncome

 public int? KnowVia { get; set; } // KnowVia

 public string Notes { get; set; } // Notes

 public string CompanyName { get; set; } // CompanyName

 public string LegalName { get; set; } // LegalName

 public string RegistrationNo { get; set; } // RegistrationNo

 public string COCNo { get; set; } // COCNo

 public string CompanyEmail { get; set; } // CompanyEmail

 public string CompanyWebsite { get; set; } // CompanyWebsite

 public string NatureofBusiness { get; set; } // NatureofBusiness

 public int? Sector { get; set; } // Sector

 public int? TotalEmployees { get; set; } // TotalEmployees

 public int? TotalSaudiEmployees { get; set; } // TotalSaudiEmployees

 public string CompanyNoLabourOffice { get; set; } // CompanyNoLabourOffice

 public string CompanyActivity { get; set; } // CompanyActivity

 public string ContactPerson { get; set; } // ContactPerson

 public string Extension { get; set; } // Extension

 public int? CustomerType { get; set; } // CustomerType

 public string ContactPersonEmail { get; set; } // ContactPersonEmail

 public int? CustomerStatus { get; set; } // CustomerStatus








 public string AccountManager { get; set; } // AccountManager

 public string Supervisor { get; set; } // Supervisor

 public string SalesPerson { get; set; } // SalesPerson

 public int? NoofFamilyMembers { get; set; } // NoofFamilyMembers

 public int? NoofWifes { get; set; } // NoofWifes

 public bool? OldAgePerson { get; set; } // OldAgePerson

 public int? NoofOldAgePerson { get; set; } // NoofOldAgePerson

 public string OldAgePersonNotes { get; set; } // OldAgePersonNotes

 public bool? SpecialDisabled { get; set; } // SpecialDisabled

 public int? NoofSpecialDisabled { get; set; } // NoofSpecialDisabled

 public string SpecialDisabledNotes { get; set; } // SpecialDisabledNotes

 public int? SalaryRange { get; set; } // SalaryRange

 public string OccupationDesc { get; set; } // OccupationDesc

 public int? CreatedSource { get; set; } // CreatedSource

 public string ExternalID { get; set; } // ExternalID

 public long? EnquiryRefId { get; set; } // EnquiryRefId

 public string FaxNumber { get; set; } // FaxNumber

 public string CustomerNameEN { get; set; } // CustomerNameEN

 public string WorkPlace { get; set; } // WorkPlace

 public string WorkAddress { get; set; } // WorkAddress

 public string WorkEmail { get; set; } // WorkEmail

 public string WorkTel { get; set; } // WorkTel

 public string WorkFax { get; set; } // WorkFax

 public decimal? WorkPoBox { get; set; } // WorkPoBox

 public decimal? MailAddress { get; set; } // MailAddress

 public decimal? WorkMailAddress { get; set; } // WorkMailAddress

 public decimal? WorkMailBox { get; set; } // WorkMailBox

 public decimal? MailBox { get; set; } // MailBox

 public string WorkMobile { get; set; } // WorkMobile

 public string CRM_GUID { get; set; } // CRM_GUID

 public int? Channel { get; set; } // Channel

 public string FemaleContactPersonPhoneNumber { get; set; } // FemaleContactPersonPhoneNumber

 public int? VerificationStatus { get; set; } // VerificationStatus

 public string VerifiedBy { get; set; } // VerifiedBy

 public DateTime? VerifiedDate { get; set; } // VerifiedDate

 public string VirtualAccountId { get; set; } // VirtualAccountId

 public string AffiliateCode { get; set; } // AffiliateCode

 public string IBANNumber { get; set; } // IBANNumber

 public string PromoCode { get; set; } // PromoCode

 public DateTime? WorkDateOfJoining { get; set; } // WorkDateOfJoining

 public DateTime? IqamaExpiryDate { get; set; } // IqamaExpiryDate



        
        public AMWCustomer()
        {
            InitializePartial();
        }

        partial void InitializePartial();
    }

}
